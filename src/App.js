import React from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import history from './utils/history';
import Main from './containers/App';
import './App.css';

import configureStore from './configureStore';

// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);

function App() {
  return (
    <Provider store={store}>
       <ConnectedRouter history={history}>
        <Main />
      </ConnectedRouter> 
    </Provider>    
  );
}

export default App;
