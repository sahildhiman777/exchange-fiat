import en from '../i18n/en'
import vn from '../i18n/vn'

const languages = {
    en,
    vn
};

let defaultLanguage = window.navigator.language === 'vi' ? 'vn' : 'en';

if (localStorage.getItem("i18nCode")) {
    defaultLanguage = localStorage.getItem("i18nCode");
} else {
    localStorage.setItem("i18nCode", defaultLanguage)
}

window.i18nCode = defaultLanguage;
window.i18nData = languages[defaultLanguage];
window.i18n = (key) => window.i18nData[key];
window.changeLanguage = (lang) => {
    localStorage.setItem("i18nCode", lang);
    window.i18nCode = lang;
    window.i18nData = languages[lang];
}