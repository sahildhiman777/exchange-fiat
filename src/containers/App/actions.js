import { USER, USER_SUCCESS, USER_ERROR, REDIRECT } from './constants';

export function redirect(data) {
    return { type: REDIRECT, data };
}
export function loadUser() {
    return { type: USER };
}
export function userSuccess(data) {
    return { type: USER_SUCCESS, data };
}
export function userFailed(error) {
    return { type: USER_ERROR, error };
}