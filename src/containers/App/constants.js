export const USER = 'exchangefiatUserPanel/App/USER';
export const USER_SUCCESS = 'exchangefiatUserPanel/App/USER_SUCCESS';
export const USER_ERROR = 'exchangefiatUserPanel/App/USER_ERROR';
export const REDIRECT = 'exchangefiatUserPanel/App/REDIRECT';