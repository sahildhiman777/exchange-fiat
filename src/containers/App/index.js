import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import {
    makeSelectUser,
    makeSelectLoading,
    makeSelectError,
    makeSelectSuccess,
    makeSelectRedirect,
} from './selectors';
import { makeSelectUser as makeLoggedInUser, makeSelect2faSuccess } from "../Login/selectors"
import { loadUser } from './actions';
import reducer from './reducer';
import saga from './saga';
import { BrowserRouter as Router, Redirect, Route, Switch, withRouter, Link } from 'react-router-dom';
import LandingPage from '../LandingPage';
import Login from '../../containers/Login';
import ForgotPassword from '../../containers/ForgotPassword';
import ResetPassword from '../../containers/ResetPassword';
import Register from '../../containers/Register';
import Dashboard from '../../containers/Dashboard';
import BuyCoin from '../../containers/Dashboard/BuyCoin';
import SellCoin from '../../containers/Dashboard/SellCoin';
import Profile from '../../containers/Dashboard/Account/Profile';
import Verify from '../../containers/Register/verify';
import Wallets from '../../containers/Dashboard/Account/Wallets';
import GoogleTwofa from '../Dashboard/Account/Settings/Google2fa';
import Kyc from '../../containers/Dashboard/Account/Kyc';
import Header from '../../components/common/Header';
import Footer from '../../components/common/Footer';
import Sidebar from '../../components/common/sidebar';
import NotFoundPage from '../../components/NotFoundPage';
import Bank from '../../containers/Dashboard/Account/Banks';
import Transactions from '../../containers/Dashboard/Account/Transactions'
import '../../services/localizationService';

let accessToken = localStorage.getItem("exchangefiatUserPanel");

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.logout = this.logout.bind(this);
    }

    componentWillMount() {
        accessToken = localStorage.getItem("exchangefiatUserPanel");
        if (accessToken) {
            this.props.dispatch(loadUser())
        } else {
            const rt = this.props.history.location.pathname;
            if (rt !== '/login' && rt !== '/register' && rt !== '/forgotpassword' && rt !== '/resetpassword' ) {
                this.props.history.push('/');
            }
            if (this.props.user == false && rt === '/dashboard') {
                this.props.history.push(`/login`)
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            accessToken = localStorage.getItem("exchangefiatUserPanel");
        }
    }

    componentDidUpdate() {
        const rt = this.props.history.location.pathname;
        let user = this.props.user;

        if (this.props.redirect && rt !== '/login') {
            this.props.history.push(`/${this.props.redirect}`)
        }
        else if (user && rt === '/login') {
            this.props.history.push(`/dashboard`)
        }
        else if (user == false && rt === '/buycoin') {
            this.props.history.push(`/`)
        }
        else if (user == true && rt === '/buycoin') {
            this.props.history.push(`/dashboard`)
        }
        else if (user == false && rt === '/sellcoin') {
            this.props.history.push(`/`)
        }
        else if (user == true && rt === '/sellcoin') {
            this.props.history.push(`/dashboard`)
        }
        else if (user == true && rt === '/sellcoin/transactions') {
            this.props.history.push(`/transactions`)
        }
        else if (user == true && rt === '/buycoin/transactions') {
            this.props.history.push(`/transactions`)
        }
    }

    logout() {
        let lang = localStorage.getItem("i18nCode");
        localStorage.clear();
        localStorage.setItem("i18nCode", lang);
        window.location.reload();
    }

    changeLanguage = (e) => {
        window.changeLanguage(e.target.dataset.language);
        this.forceUpdate();
    }

    render() {
        const { user, redirect } = this.props;
        
        return (

            <React.Fragment>
                {(accessToken && !redirect) ?
                    <Switch>
                        <Route exact path="/">
                            <LandingPage logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                        </Route>
                        <Route exact path='/dashboard' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <Dashboard user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='/profile' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <Profile />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                       
                        <Route path='/bank' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <Bank user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>

                        <Route exact path='/transactions' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <Transactions user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>

                        <Route exact path='/wallets' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <Wallets user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='/buycoin/:coin_code' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <BuyCoin user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='/sellcoin/:coin_code' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <SellCoin user={user} />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='/verify/:id/:user_code' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        <Verify  user={user}/>
                                    </div>
                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='/kyc' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        <Kyc user={user}/>
                                    </div>
                                    <Footer />
                                </div>
                            </div>
                        </Route>
                        <Route path='*' >
                            <div id="wrapper">
                                <Sidebar />
                                <div id="content-wrapper" className="d-flex flex-column">
                                    {/* <!-- Main Content --> */}
                                    <div id="content">
                                        <Header logout={this.logout} user={user} changeLanguage={this.changeLanguage} />
                                        {/* <!-- Page Heading --> */}
                                        <NotFoundPage />
                                    </div>
                                    {/* <!-- End of Main Content --> */}

                                    <Footer />
                                </div>
                            </div>
                        </Route>
                    </Switch>
                    :
                    <Switch>
                        <Route exact path="/" >
                            <LandingPage user={user} changeLanguage={this.changeLanguage} />
                        </Route>
                        <Route path="/login">
                            <Login changeLanguage={this.changeLanguage} />
                        </Route>
                        <Route path="/forgotpassword">
                            <ForgotPassword changeLanguage={this.changeLanguage} />
                        </Route>
                        <Route exact path="/register">
                            <Register changeLanguage={this.changeLanguage} />
                        </Route>
                        <Route path="/resetpassword" component={ResetPassword} />
                        <Route path="/verify/:id/:user_code" component={Verify} />
                    </Switch>
                }
            </React.Fragment>

        )
    }
}

index.propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    user: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    // loggedUser: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
    loadUser: PropTypes.func,
    location: PropTypes.object,
    history: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
    // loggedUser: makeLoggedInUser(),
    success2fa: makeSelect2faSuccess(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    success: makeSelectSuccess(),
    redirect: makeSelectRedirect()
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'main', reducer });
const withSaga = injectSaga({ key: 'main', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter,
)(index);


