import produce from 'immer';
import { USER, USER_ERROR, USER_SUCCESS, REDIRECT } from './constants';

// The initial state of the App
export const initialState = {
    user: false,
    success: false,
    error: false,
    loading: false,
    redirect: false,
    twofaSuccess: false,
};
/* eslint-disable default-case, no-param-reassign */
const appReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case USER:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.user = false;
                break;

            case USER_SUCCESS:
                draft.user = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case USER_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
            case REDIRECT:
                draft.error = false;
                draft.success = false;
                draft.loading = false;
                draft.redirect = action.data;
                break;
        }
    });

export default appReducer;
