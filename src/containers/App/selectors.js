/**
 * Homepage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMain = state => state.main || initialState;

const makeSelectUser = () =>
    createSelector(selectMain, mainState => mainState.user);
const makeSelectError = () =>
    createSelector(selectMain, mainState => mainState.error);
const makeSelectLoading = () =>
    createSelector(selectMain, mainState => mainState.loading);
const makeSelectSuccess = () =>
    createSelector(selectMain, mainState => mainState.success);
const makeSelectRedirect = () =>
    createSelector(selectMain, mainState => mainState.redirect);

export { selectMain, makeSelectRedirect, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser };



