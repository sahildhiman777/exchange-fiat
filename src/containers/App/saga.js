import { call, put, takeLatest } from 'redux-saga/effects';
import { USER } from './constants';
import { userFailed, userSuccess, redirect } from './actions';

import { requestSecure } from '../../utils/request';
import Define from '../../utils/config';

export function* getUser() {
    const requestURL = `${Define.API_URL}current`;
    const options = {
        method: 'GET',
    }
    try {
        const res = yield call(requestSecure, requestURL, options)
        if (res.status) {
            yield put(userSuccess(res.data.user));
        } else {
            if (res.redirect) {
                yield put(redirect(res.redirect));
            } else {
                yield put(userFailed(res));
            }
        }
    } catch (err) {
        yield put(userFailed(err));
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* appData() {

    yield takeLatest(USER, getUser);
}
