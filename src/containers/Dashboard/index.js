import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  selectMywallets,
  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,
  makeSelectCurrencies,
  makeSelectBuyCoin,
  makeSelectBuyCoinError,
  makeSelectBuyCoinLoading,
  makeSelectBuyCoinSuccess,
  makeSelectBanks,
  makeSelectSellBank,
  makeSelectSellBankError,
  makeSelectSellBankLoading,
  makeSelectSellBankSuccess,
  makeSelectSellCoin,
  makeSelectSellCoinError,
  makeSelectSellCoinLoading,
  makeSelectSellCoinSuccess,
} from './selectors';

import { wallets, getBanks, getSellBanks,email } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import config from '../../utils/config';
import { withRouter } from "react-router-dom";
import ReactLoading from "react-loading";

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coinName: '',
      coinprice: '',
      step2: false,
      coin_amount: '',
      bank_name: '',
      account_number: '',
      bank_code: '',
      country: '',
      holder_name: '',
      bank_address: '',
      step3: false,
      currencyType: '',
      total: '',
      price: '',
      coin: [],
      items: [],
      coinaddress: '',
      code: '',
      agree: false,
      currency: '',
      bank_id: '',
      created_at: '',
      updated_at: '',
      activeCoins: {},
      activebtc: false,
      activeeth: false,
      activeusdt: false,
      icon: '',
      activename: '',
      sell_section: false,
      buy_section: false,
      buy_form_section: false,
      sell_form_section: false,
      blockloader: true

    }
    this.handleChange = this.handleChange.bind(this);
    this.clickState = this.clickState.bind(this);
  }

  componentDidMount() {
    let buydata = JSON.parse(localStorage.getItem('homeBuyData'));
    let selldata = JSON.parse(localStorage.getItem('homeSellData'));

    if (buydata) {
      this.props.history.push(`/buycoin/${buydata.symbol}`)
    }
    if (selldata) {
      this.props.history.push(`/sellcoin/${selldata.symbol}`)
    }
  }

  clickState(e, data) {
    if (e.target.name == 'buy') {
      this.props.history.push(`/buycoin/${data.symbol}`)
    }
    if (e.target.name == 'sell') {
      this.props.history.push(`/sellcoin/${data.symbol}`)
    }

  }

  handleChange(e) {
    e.preventDefault();
    const name = e.target.name;
    if (name == 'coin_name') {
      const value = e.target.value;
      this.setState({ coinName: value });
    }
    if (name == 'coin_address') {
      const value = e.target.value;
      this.setState({ coinaddress: value });
    }
    if (name == 'coin_amount') {
      const value = e.target.value;
      let total = value * this.state.coinprice;
      this.setState({ coin_amount: value, total: total });
    }
  }

  handleCheck(e) {
    this.setState({ agree: e.target.checked });
  }

  validateForm = () => {
    if (this.state.agree == true && this.state.coinaddress.length > 0 && this.state.coin_amount.length > 0) {
      return true
    }
    if (this.state.coin_amount.length > 0) {
      return true
    }
  }

  componentWillMount() {
    this.props.dispatch(wallets());
    let currency = localStorage.getItem('currency');
    if (currency) {
      this.setState({ currencyType: currency })
    }
  }
componentDidUpdate(){
  let currency = localStorage.getItem('currency');
  if (currency) {
    console.log(currency,'currency');
    
    this.state.currencyType = currency
  }
}
  componentWillReceiveProps(nextProps) {
    if (nextProps.wallets && nextProps.wallets.length > 0) {
      this.setState({ blockloader: false });
    }
  }

  renderBuyCoinButtons = () => {
    const buyCoins = this.props.wallets && this.props.wallets.filter(coin => coin.buy_status);
    const self = this;
    if (buyCoins) {
      return buyCoins.map(coin => {
        return (
          <button key={`buy-button-${coin.symbol}`} className={"bank_button" + (self.state.activeCoins[coin.symbol] ? ' activebtc' : '')} name={coin.name} onClick={(e) => self.clickBankButton(e)}>
            <img src={require(`../../images/${coin.icon}.png`)} /> {coin.symbol}
          </button>
        );
      })
    } else {
      return "";
    }
  }

  buyPrice = (price) => {
    return Number(price) + (Number(price) * (Number(config.buyFee) / 100))
  }

  sellPrice = (price) => {
    return Number(price) - (Number(price) * (Number(config.sellFee) / 100))
  }

  render() {
    let asset = this.props.wallets;
    let currency = this.state.currencyType;
    return (
      <div className="dashboard_section">

        {(this.state.blockloader) &&
          <div className="blockloader">
            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
            </div>
          </div>
        }
        <div className="dash_bank_section">
          <div className="container">
            <div className="row">
              {(() => {
                const options = [];
                if (asset) {
                  for (let i = 0; i < asset.length; i++) {
                    options.push(
                      <div key={`buy-now-header-${i}`} className="col-md-4 mb-4 ">
                        <div className="buy-now-header">
                          <div className="coin-header coin-header-btc">
                            <span className="coin-header-span">
                              <h2 className="coin-header-h2">
                                <img src={require('../../images/' + asset[i].icon + '.png')} />
                                {
                                  asset[i].symbol
                                }
                              </h2>
                            </span>
                          </div>
                          {
                            asset[i].buy_status ?
                              <div className="card border-left-buy">
                                <div className="card-body">
                                  <div className="row">
                                    <div className="col-lg-7 col-md-12">
                                      <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{window.i18n('DashboardBestPrice')}</div>
                                      <div className="h5 mb-0 font-weight-bold text-gray-800 price-text">{new Intl.NumberFormat('en-US').format(this.buyPrice(asset[i].price))} {currency}/{asset[i].symbol}</div>
                                    </div>
                                    <div className="col-lg-5 col-md-12">
                                      <button type="submit" className="buy_button buy-sell-button" name="buy" onClick={(e) => this.clickState(e, asset[i])}>{window.i18n('BuyNow')}  </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              :
                              ''
                          }
                          {/* Sell section */}
                          {
                            asset[i].sell_status ?
                              <div className="card border-left-sell">
                                <div className="card-body">
                                  <div className="row">
                                    <div className="col-lg-7 col-md-12">
                                      <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{window.i18n('DashboardBestSellPrice')}</div>
                                      <div className="h5 mb-0 font-weight-bold text-gray-800 price-text">{new Intl.NumberFormat('en-US').format(this.sellPrice(asset[i].price))} {currency}/{asset[i].symbol}</div>
                                    </div>
                                    <div className="col-lg-5 col-md-12">
                                      <button type="submit" className="sell_button buy-sell-button" name="sell" onClick={(e) => this.clickState(e, asset[i])}>{window.i18n('SellNow')}  </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              :
                              ''
                          }
                        </div>
                      </div>
                    );
                  }
                  return options;
                }
              })()}
            </div>
          </div>
        </div>
      </div>

    )
  }
}


const mapStateToProps = createStructuredSelector({
  wallets: makeSelectMyWallets(),
  loading: makeSelectMyWalletsLoading(),
  error: makeSelectMyWalletsError(),
  success: makeSelectMyWalletsSuccess(),
  currencies: makeSelectCurrencies(),

  buycoin: makeSelectBuyCoin(),
  buycoinloading: makeSelectBuyCoinLoading(),
  buycoinerror: makeSelectBuyCoinError(),
  buycoinsuccess: makeSelectBuyCoinSuccess(),
  banks: makeSelectBanks(),

  sellbank: makeSelectSellBank(),
  sellbankError: makeSelectSellBankError(),
  sellbankLoading: makeSelectSellBankLoading(),
  sellbankSuccess: makeSelectSellBankSuccess(),

  sellcoin: makeSelectSellCoin(),
  sellcoinerror: makeSelectSellCoinError(),
  sellcoinloading: makeSelectSellCoinLoading(),
  sellcoinsuccess: makeSelectSellCoinSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'mywallets', reducer });
const withSaga = injectSaga({ key: 'mywallets', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);

