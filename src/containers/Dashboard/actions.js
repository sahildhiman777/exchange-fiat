import {
  WALLETS,
  WALLETS_ERROR,
  WALLETS_SUCCESS,
  GET_CURRENCY,
  GET_CURRENCY_ERROR,
  GET_CURRENCY_SUCCESS,

  BUY_COIN,
  BUY_COIN_ERROR,
  BUY_COIN_SUCCESS,

  GET_BANKS,
  GET_BANKS_ERROR,
  GET_BANKS_SUCCESS,

  GET_SELL_BANKS,
  GET_SELL_BANKS_ERROR,
  GET_SELL_BANKS_SUCCESS,

  SELL_COIN,
  SELL_COIN_ERROR,
  SELL_COIN_SUCCESS,

  EMAIL,
  EMAIL_ERROR,
  EMAIL_SUCCESS,
  EMAIL_RESET,

  VERIFYEMAIL,
  VERIFYEMAIL_ERROR,
  VERIFYEMAIL_SUCCESS,
  VERIFIY_RESET,
  GET_WALLETS,
  GET_WALLETS_ERROR,
  GET_WALLETS_SUCCESS,
} from './constants';

export function wallets() {
  return { type: WALLETS };
}
export function walletsSuccess(data) {
  return { type: WALLETS_SUCCESS, data };
}
export function walletsFailed(error) {
  return { type: WALLETS_ERROR, error };
}

export function getCurrencies() {
  return {
    type: GET_CURRENCY,
  };
}
export function getCurrenciesSuccess(data) {
  return {
    type: GET_CURRENCY_SUCCESS,
    data,
  };
}
export function getCurrenciesFailed(error) {
  return {
    type: GET_CURRENCY_ERROR,
    error,
  };
}

export function buy_coin(data) {
  return {
    type: BUY_COIN, data
  };
}
export function buy_coin_Success(data) {
  return {
    type: BUY_COIN_SUCCESS,
    data,
  };
}
export function buy_coin_Failed(error) {
  return {
    type: BUY_COIN_ERROR,
    error,
  };
}

export function getBanks() {
  return {
    type: GET_BANKS,
  };
}
export function getBanksSuccess(data) {
  return {
    type: GET_BANKS_SUCCESS,
    data,
  };
}
export function getBanksFailed(error) {
  return {
    type: GET_BANKS_ERROR,
    error,
  };
}

export function getSellBanks() {
  return {
    type: GET_SELL_BANKS,
  };
}
export function getSellBanksSuccess(data) {
  return {
    type: GET_SELL_BANKS_SUCCESS,
    data,
  };
}
export function getSellBanksFailed(error) {
  return {
    type: GET_SELL_BANKS_ERROR,
    error,
  };
}

export function sell_coin(data) {
  return {
    type: SELL_COIN, data
  };
}
export function sell_coin_Success(data) {
  return {
    type: SELL_COIN_SUCCESS,
    data,
  };
}
export function sell_coin_Failed(error) {
  return {
    type: SELL_COIN_ERROR,
    error,
  };
}

export function email(data) {
  return { type: EMAIL, data };
}
export function emailSuccess(data) {

  return { type: EMAIL_SUCCESS, data };
}
export function emailFailed(error) {
  return { type: EMAIL_ERROR, error };
}

export function verifyemail(data) {
  return { type: VERIFYEMAIL, data };
}
export function verifyemailSuccess(data) {
  return { type: VERIFYEMAIL_SUCCESS, data };
}
export function verifyemailFailed(error) {
  return { type: VERIFYEMAIL_ERROR, error };
}
export function emailReset() {
  return { type: EMAIL_RESET }
}
export function verifiyReset() {
  return { type: VERIFIY_RESET }
}
export function getwallets() {
  return {
    type: GET_WALLETS,
  };
}
export function getwalletsSuccess(data) {
  return {
    type: GET_WALLETS_SUCCESS,
    data,
  };
}
export function getwalletsFailed(error) {
  return {
    type: GET_WALLETS_ERROR,
    error,
  };
}