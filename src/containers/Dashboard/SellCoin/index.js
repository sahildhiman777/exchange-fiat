import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,
  makeSelectCurrencies,
  makeSelectSellBank,
  makeSelectSellBankError,
  makeSelectSellBankLoading,
  makeSelectSellBankSuccess,
  makeSelectSellCoin,
  makeSelectSellCoinError,
  makeSelectSellCoinLoading,
  makeSelectSellCoinSuccess,

  makeSelectMyEmail,
  makeSelectMyEmailError,
  makeSelectMyEmailLoading,
  makeSelectMyEmailSuccess,

  makeSelectMyVerifyEmail,
  makeSelectMyVerifyEmailError,
  makeSelectMyVerifyEmailLoading,
  makeSelectMyVerifyEmailSuccess,
} from '../selectors';

import { wallets, getCurrencies, buy_coin, getBanks, getSellBanks, sell_coin, email, verifyemail, emailReset, verifiyReset, sell_coin_Failed } from '../actions';
import { loadUser } from "../../App/actions";
import reducer from '../reducer';
import saga from '../saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../utils/injectReducer';
import injectSaga from '../../../utils/injectSaga';
import { FormGroup, FormControl, FormLabel, Form } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../toast/toast';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Config from '../../../utils/config';
import ReactLoading from "react-loading";

var QRCode = require('qrcode.react');

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coinName: '',
      coinprice: '',
      step2: false,
      coin_amount: '',
      bank_name: '',
      account_number: '',
      bank_code: '',
      country: '',
      holder_name: '',
      bank_address: '',
      step3: false,
      currencyType: '',
      total: '',
      price: '',
      coin: [],
      items: [],
      coinaddress: '',
      code: '',
      agree: false,
      currency: '',
      bank_id: '',
      created_at: '',
      updated_at: '',
      activeCoins: {},
      activebtc: false,
      activeeth: false,
      activeusdt: false,
      icon: '',
      activename: '',
      sell_section: false,
      sell_form_section: false,
      blockloader: false,

      type_2fa: '',
      google_2fa: false,
      email_2fa: false,
      verifiy2fa: false,
      otpcode: '',
      otperror: false,
      qrcodeshow: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSellSubmit = this.handleSellSubmit.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getSellBanks());
    this.props.dispatch(wallets());
    this.props.dispatch(loadUser());
  }

  componentWillReceiveProps(props) {
    let coin_code = props.match.params.coin_code;
    let coinData = props.wallets;
    if (coinData) {
      let coin = coinData.find(coin => coin.symbol == coin_code);
      if (coin) {
        this.state.step2 = true;
        this.state.sell_section = true;
        this.state.coinName = coin.symbol;
        this.state.coinprice = this.sellPrice(coin.price) || 0;
        this.state.icon = coin.icon;
        this.state.activename = coin.name;
        this.state.coin_amount = coin.amount ? coin.amount : this.state.coin_amount;
        this.state.total = (this.sellPrice(coin.price) * Number(coin.amount ? coin.amount : this.state.coin_amount)) || 0;
        this.state.currencyType = localStorage.getItem('currency') || "VND";

        for (let key in this.state.activeCoins) {
          this.state.activeCoins[key] = false;
        }
        this.state.activeCoins[coin.symbol] = true;
        this.setState(this.state);
      } else {
        const accessToken = localStorage.getItem("exchangefiatUserPanel");
        if (accessToken) {
          const rt = this.props.history.location.pathname;
          if (rt === '/sellcoin/transactions')
            this.props.history.push('/transactions')
        } else {
          this.props.history.push('/')

        }
      }
    }
    if (props.user) {
      this.state.id = props.user._id
      this.state.email = props.user.email
      this.state.firstName = props.user.firstname
      this.setState({ blockloader: false })
      if (this.props.user.google2fa == true) {
        this.setState({ type_2fa: "google_2fa" });
      } else if (this.props.user.email_2fa == true) {
        this.setState({ type_2fa: "email_2fa" });
      } else {
        this.setState({ type_2fa: "" });
      }
      if (props.verifiyemailsuccess == true) {
        if (props.verifiyemail == 'verifiy successfully') {
          this.setState({ verifyemail: true, blockloader: false, google_2fa: false, email_2fa: false, otpcode: '' });
          let data = {
            total: this.state.total,
            user_id: props.user._id,
            platform: 'exchangefiat',
            coin_amount: this.state.coin_amount,
            coin_name: this.state.coinName,
            email: props.user.email,
            phone: '',
            user_bank_name: this.state.bank_name,
            user_bank_account_number: this.state.account_number,
            user_bank_acount_holder_name: this.state.holder_name,
            user_currency_code: this.state.currencyType,
            status: 'pending',
            notify_url: 'http://www.testurl.com/nofiy',
            return_url: '/transactions',
            transaction_id: new Date().getTime(),
          }
          this.props.dispatch(sell_coin(data));
          this.props.dispatch(verifiyReset());
        }
      }
      if (props.sellcoin) {
        this.state.qrcodeshow = true
        this.setState({ blockloader: false })
      }

      if (props.emailsuccess == true) {
        this.setState({ email_2fa: true, blockloader: false, otpcode: '' });
        this.props.dispatch(emailReset());
      }
    }

    this.setState({ blockloader: props.loading })
  }

  componentDidMount() {
    let coin_code = this.props.match.params.coin_code;
    let coinData = this.props.wallets;
    let currency = localStorage.getItem('currency');
    if (currency) {
      this.setState({ currencyType: currency })
    }

    if (coinData) {
      let coin = coinData.find(coin => coin.symbol == coin_code);
      if (coin) {
        this.state.step2 = true;
        this.state.sell_section = true;
        this.state.coinName = coin.symbol;
        this.state.coinprice = this.sellPrice(coin.price) || 0;
        this.state.icon = coin.icon;
        this.state.activename = coin.name;
        this.state.coin_amount = this.state.coin_amount || coin.amount;

        for (let key in this.state.activeCoins) {
          this.state.activeCoins[key] = false;
        }
        this.state.activeCoins[coin.symbol] = true;
        this.setState(this.state);
      } else {
        if (this.props.user) {
          this.props.history.push('/dashboard')
        } else {
          this.props.history.push('/')
        }
      }
    }
  }

  componentDidUpdate() {
    if (this.props.sellcoinerror && this.props.sellcoinerror != "") {
      ToastTopEndErrorFire(this.props.sellcoinerror)
      // Swal.fire({
      //   title: this.props.sellcoinerror,
      //   type: "error",
      //   showConfirmButton: false,
      //   timer: 1000
      // }).then(() => {
        this.props.dispatch(sell_coin_Failed(""));
      // });
    }
  }

  clickSell(data) {
    let code = Math.floor(100000 + Math.random() * 900000);
    this.setState({
      step3: true,
      step2: false,
      coin_amount: 0,
      sell_form_section: true,
      bank_name: data.user_bank_name,
      account_number: data.user_bank_account_number,
      country: data.bank_country_name,
      holder_name: data.user_bank_acount_holder_name,
      bank_id: data._id,
      bank_address: data.bank_country_name,
      code: code,
    })

  }

  handleChange(e) {
    e.preventDefault();
    const name = e.target.name;
    if (name == 'coin_name') {
      const value = e.target.value;
      this.setState({ coinName: value });
    }
    if (name == 'coin_address') {
      const value = e.target.value;
      this.setState({ coinaddress: value });
    }
    if (name == 'coin_amount') {
      const value = e.target.value;
      let total = value * this.state.coinprice;
      this.setState({ coin_amount: value, total: total });
    }
  }

  handleCheck(e) {
    this.setState({ agree: e.target.checked });
  }

  validateForm = () => {
    if (this.state.coin_amount && this.state.coin_amount.length > 0) {
      return true
    }
  }
  handleSellSubmit = (e) => {
    e.preventDefault();
    if(this.props.user.emailstatus){
      if (this.state.type_2fa == 'google_2fa' || this.state.type_2fa == 'email_2fa') {
        this.setState({ blockloader: true });
        const data = {
          id: this.state.id,
          email: this.state.email,
          name: this.state.firstName,
        }
        if (this.state.type_2fa == 'google_2fa') {
          this.setState({ google_2fa: true, blockloader: false });
        } else if (this.state.type_2fa == 'email_2fa') {
          this.props.dispatch(email(data));
        }
      } else {
        ToastTopEndErrorFire(window.i18n('enable2fa'));
      }
    }else{
      ToastTopEndErrorFire('Please check your email and verify your email');
  }
  }
  onOtpCodeChange = (e) => {
    this.setState({ otpcode: e.target.value, otperror: false },
      () => {
        if (this.state.otpcode.length >= 6) {
          this.submitOtp(e);
        }
      })
  }
  submitOtp = () => {
    this.setState({ blockloader: true })
    let verifiydata = {
      id: this.state.id,
      code: this.state.otpcode,
      code_type: this.state.type_2fa
    }
    if (this.state.otpcode.length < 6) {
      this.setState({ otperror: true, blockloader: false });
      return
    }
    this.props.dispatch(verifyemail(verifiydata));
  }
  handleSellCancel(e) {
    ToastTopEndSuccessFire("Your sell transaction has been generated. You will receive coins after fiat admin approval ")
    // Swal.fire({
    //   title: "Your sell transaction has been generated. You will receive coins after fiat admin approval ",
    //   type: "success",
    //   showConfirmButton: false,
    //   timer: 2000
    // }).then(result => {
      this.props.history.push('/transactions')
      window.location.reload()
    // });

  }
  backclick() {
    this.props.history.push('/dashboard')
    this.setState({ step3: false, step2: false, buy_section: false, sell_section: false, total: '', coin_amount: '', coinaddress: '' })
  }
  backclickbuy() {
    this.setState({ step3: false, step2: true, total: '', coin_amount: '', coinaddress: '', })
  }

  clickBankButton(e) {
    let name = e.target.name
    let list = [];
    let coinlist = this.props.wallets;
    let selectedCoin = coinlist.find(coin => coin.name == name);

    if (selectedCoin) {
      this.props.history.push(`/sellcoin/${selectedCoin.symbol}`)
      for (let coin_code in this.state.activeCoins) {
        this.state.activeCoins[coin_code] = false;
      }

      this.state.activeCoins[selectedCoin.symbol] = true;
      this.state.coinName = selectedCoin.symbol
      this.state.coinprice = this.sellPrice(selectedCoin.price)
      this.state.icon = selectedCoin.icon
    }

    this.setState(this.state);
  }

  renderBuyCoinButtons = () => {
    // const buyCoins = this.props.wallets && this.props.wallets.filter(coin => coin.buy_status);
    const sellCoins = this.props.wallets && this.props.wallets.filter(coin => coin.sell_status);
    const self = this;
    if (sellCoins) {
      return sellCoins.map(coin => {
        return (
          <button key={`buy-button-${coin.symbol}`} className={"bank_button" + (self.state.activeCoins[coin.symbol] ? ' activebtc' : '')} name={coin.name} onClick={(e) => self.clickBankButton(e)}>
            <img src={require(`../../../images/${coin.icon}.png`)} /> {coin.symbol}
          </button>
        );
      })
    } else {
      return "";
    }
  }

  sellPrice = (price) => {
    return Number(price) - (Number(price) * (Number(Config.sellFee) / 100))
  }

  render() {
    let currency = localStorage.getItem('currency') || "VND";
    let sellbank = [];
    let bank = this.props.sellbank;
    let local_currency = localStorage.getItem('currency');
    for(let i=0; i<bank.length; i++){
      if(bank[i].currency == local_currency){
        sellbank.push(bank[i]);
      }
    }
    
    let coin_address = '';
    if (this.props.sellcoin) {
      coin_address = this.props.sellcoin.coin_address
    }

    return (

      <div className="dashboard_section">
        {(this.state.blockloader) &&
          <div className="blockloader">
            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
            </div>
          </div>
        }
        {/* 2fa modal..... */}
        {(this.state.google_2fa == true || this.state.email_2fa == true) && <div className="modal_2fa">
          <div className="card w-55">
            <div className="card-body">
              <i style={{ float: 'right', cursor: 'pointer' }} className="fa fa-times" aria-hidden="true" onClick={() => this.setState({ google_2fa: false, email_2fa: false })}></i>
              <div className="col-lg-8" style={{ margin: 'auto' }}>
                <div className="p-5" style={{ padding: '7rem !important' }}>
                  <div className="text-center">
                    {this.state.email_2fa == true && <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInPlaceholder')}</h1>}
                    {this.state.google_2fa == true && <h1 className="h4 text-gray-900 mb-4">Enter Google OTP</h1>}
                  </div>
                  <form className="user">
                    <div className="form-group">
                      {this.state.email_2fa == true && <input type="number" onChange={this.onOtpCodeChange} name="otpcode" className="form-control form-control-user" id="exampleInputPassword" placeholder={window.i18n('LogInPlaceholder')} value={this.state.otpcode} required />}
                      {this.state.google_2fa == true && <input type="number" onChange={this.onOtpCodeChange} name="otpcode" className="form-control form-control-user" id="exampleInputPassword" placeholder='Enter Google OTP' value={this.state.otpcode} required />}
                    </div>
                    {this.state.otperror && <p style={{ fontSize: '12px', color: 'red' }}>Please enter at least 6 digit</p>}
                    <button type="button" onClick={this.submitOtp} className="btn btn-primary btn-user btn-block">
                    Submit OTP
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>}
        {/* //end 2fa modal..... */}
        {this.state.qrcodeshow &&
          <div className="qrcodeshow">


            <div className="modal-dialog sell-modal">
              <div className="modal-content">
                <div className="modal-body">
                  <p>Please deposit your coins to address below. Admin will approved your request once coins are received.</p>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="center-text">
                        <QRCode value={coin_address} />
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="center-text">
                        <p className="coin_address">Coin Address : {coin_address}</p>
                        <CopyToClipboard text={coin_address} onCopy={() => {
                          ToastTopEndSuccessFire("Fund transfer code has been copied successfully")
                          // Swal.fire({
                          //   title: "Copied",
                          //   type: "success",
                          //   showConfirmButton: false,
                          //   timer: 800
                          // });
                        }}>
                          <i className="fa fa-copy pointer"> Copy coin address</i>
                        </CopyToClipboard>
                      </div>
                      <button type="button" className="graybtn buy_button buy-sell-button right-popup-button right-popup-button" data-dismiss="modal" onClick={(e) => { this.handleSellCancel(e) }}>close</button>
                    </div>
                  </div>
                </div></div>

            </div>
          </div>
        }


        {this.state.step3 ?
          <div className="buy_form_section">
            {this.state.sell_form_section &&
              <div className="container">
                <div className="row">
                  <div className="col-md-8">
                    <div className="left_side">
                      <div className="back_button"><a href="#" className="cancel_button" onClick={(e) => this.backclickbuy(e)}><i className="fa fa-angle-left"></i> {window.i18n('Back')}</a></div>
                      <h3>{window.i18n('SellNowSelling')} {this.state.coinName}</h3>
                      <form onSubmit={(e) => { this.handleSellSubmit(e) }}>
                        <div className="row">
                          <div className="col-md-12">
                            <FormGroup controlId="coin_amount" className="field_width sell_form_field">
                              <FormLabel>{window.i18n('BuyAmount')}</FormLabel>
                              <FormControl
                                name="coin_amount"
                                type="number"
                                step="any"
                                value={this.state.coin_amount}
                                onChange={(e) => this.handleChange(e)}
                              />
                            </FormGroup>

                            <p className="equivalentt">{window.i18n('Equivalent')} ≈ <span className="estimate-value estimate-value-buy">{new Intl.NumberFormat('en-US').format(this.state.total)}</span> <span className="estimate-value-buy">{this.state.currencyType}</span></p>

                            <div className="submit_button">
                              <button className="sell_button buy-sell-button width-buy" type="submit" data-toggle="modal" disabled={!this.validateForm()}>{window.i18n('Sell')} {this.state.coinName} </button>
                            </div>
                            <p className="important_msg">{window.i18n('ImportantBuyer')} </p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="right_side">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="buyer-seller">
                            <p className="small-text">{window.i18n('Buyer')}</p>
                            <p className="normal-bold-text owner-ads-buy">{window.i18n('ExchangeFiatDashboard')}</p>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="coin-info">
                            <p className="small-text">{window.i18n('asset')}</p>
                            <p className="normal-bold-text">
                              <img src={require('../../../images/' + this.state.icon + '.png')} />
                              {this.state.coinName}
                            </p>
                          </div>
                        </div>
                      </div>
                      <hr></hr>
                      <div className="item-info" >
                        <span>{window.i18n('Price')}</span>
                        <span className="normal-bold-text-buy" >{new Intl.NumberFormat('en-US').format(this.state.coinprice)} {this.state.currencyType}<span className="normal-bold-text">/{this.state.coinName}</span></span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('Limit')}</span>
                        <span className="normal-bold-text right-float" >0.01 ~ <span className="count-maximum" >0.05623</span> <span className="left-count">0.05623</span> {this.state.coinName}</span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('BankName')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.bank_name}</span>
                        </span>
                      </div>

                      <div className="last_div">
                        <span>{window.i18n('AccountNumber')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.account_number}</span>
                        </span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('HolderName')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.holder_name}</span>
                        </span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('BankAddress')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.bank_address}</span>
                        </span>
                      </div>
                      <div className="last_div ">
                        <span>{window.i18n('Country')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.country}</span>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
          :

          <div>
            {
              this.state.step2 ?
                <div className="bank-section">
                  {this.state.sell_section &&
                    <div className="container">
                      <h3>{window.i18n('SellNow')}</h3>
                      <div className="bank_main_div">
                        <div className="back_button"><a href="#" className="cancel_button" onClick={(e) => this.backclick(e)}><i className="fa fa-angle-left"></i> {window.i18n('Back')}</a></div>
                        <div className="bank_coin_button">
                          {this.renderBuyCoinButtons()}
                        </div>
                        {(() => {
                          const options = [];
                          if (sellbank) {
                            for (let i = 0; i < sellbank.length; i++) {
                              options.push(
                                <div key={`buy-coin-${i}`} className="buy-btc">
                                  <div className="card">
                                    <div className="row">
                                      <div className="col-md-2">
                                        <div>
                                          <img className="img-thumbnail" src={`${Config.bank_image_url}/images/default.png`} />
                                        </div>
                                      </div>
                                      <div className="col-md-6">
                                        <p><span className="price_card">{new Intl.NumberFormat('en-US').format(this.state.coinprice)}</span> {currency}/{this.state.coinName}</p>
                                        <p>{window.i18n('AccountNumber')} {sellbank[i].user_bank_account_number}</p>
                                        <p>{window.i18n('BankName')}: {sellbank[i].user_bank_name}</p>
                                        <p>{window.i18n('HolderName')}:  {sellbank[i].user_bank_acount_holder_name}</p>
                                      </div>
                                      <div className="col-md-4">
                                        <div className="dash-buy-button">
                                          <button type="submit" className=" perpalbtn buy_button buy-sell-button" onClick={() => this.clickSell(sellbank[i])}>Select</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              );
                            }
                            return options;
                          }
                        })()}
                      </div>
                      {sellbank == '' &&
                        <div className='banklink' >
                          <a style={{ cursor: 'pointer' }} href="/bank"><i className="fas fa-university" aria-hidden="true"></i>{window.i18n('PleaseAddyourbank')}</a>
                        </div>}
                    </div>
                  }
                </div>
                :
                ''
            }
          </div>
        }
      </div>

    )
  }
}


const mapStateToProps = createStructuredSelector({
  wallets: makeSelectMyWallets(),
  loading: makeSelectMyWalletsLoading(),
  error: makeSelectMyWalletsError(),
  success: makeSelectMyWalletsSuccess(),
  currencies: makeSelectCurrencies(),

  sellbank: makeSelectSellBank(),
  sellbankError: makeSelectSellBankError(),
  sellbankLoading: makeSelectSellBankLoading(),
  sellbankSuccess: makeSelectSellBankSuccess(),

  sellcoin: makeSelectSellCoin(),
  sellcoinerror: makeSelectSellCoinError(),
  sellcoinloading: makeSelectSellCoinLoading(),
  sellcoinsuccess: makeSelectSellCoinSuccess(),

  email: makeSelectMyEmail(),
  emailloading: makeSelectMyEmailLoading(),
  emailerror: makeSelectMyEmailError(),
  emailsuccess: makeSelectMyEmailSuccess(),

  verifiyemail: makeSelectMyVerifyEmail(),
  verifiyemailerror: makeSelectMyVerifyEmailError(),
  verifiyemailloading: makeSelectMyVerifyEmailLoading(),
  verifiyemailsuccess: makeSelectMyVerifyEmailSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'mywallets', reducer });
const withSaga = injectSaga({ key: 'mywallets', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);

