import { call, put, takeLatest } from 'redux-saga/effects';
import { WALLETS, GET_CURRENCY, BUY_COIN, GET_BANKS, GET_SELL_BANKS, SELL_COIN, EMAIL, VERIFYEMAIL,GET_WALLETS } from './constants';
import {
  getBanksFailed, getBanksSuccess, wallets, walletsSuccess, walletsFailed, getCurrenciesSuccess,
  getCurrenciesFailed, buy_coin_Failed, buy_coin_Success, getSellBanksSuccess, getSellBanksFailed, sell_coin_Failed, sell_coin_Success, emailSuccess, emailFailed, verifyemailFailed, verifyemailSuccess,  getwalletsFailed, getwalletsSuccess
} from './actions';
import request from '../../utils/request';
import { requestSecure } from '../../utils/request';
import Define from '../../utils/config';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';


export function* myWallets(action) {
  let currency = localStorage.getItem('currency') || "VND";
  const requestURL = `${Define.API_URL}currency/get_wallets?currency=${currency.toLowerCase()}`;
  // const requestURL = `${Define.API_URL}userwallets/wallets_list`;
  const options = {
    method: 'GET',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(walletsSuccess(res.message));


  } catch (err) {
    yield put(walletsFailed(err));
  }
}

export function* getCurr() {
  const requestURL = `${Define.API_URL}currency`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getCurrenciesSuccess(res.data));
    } else {

      yield put(getCurrenciesFailed(res.data));


    }

  } catch (err) {
    yield put(getCurrenciesFailed(err));
  }
}

export function* buycoin(action) {

  const requestURL = `${Define.API_URL}transaction/buycoin`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status && res.status == 200) {
      yield put(buy_coin_Success(res.data));
    } else {
      yield put(buy_coin_Failed(res.message));
    }

  } catch (err) {
    yield put(buy_coin_Failed(err));
  }
}

export function* getBanks() {
  let currency = localStorage.getItem('currency') || "VND";
  const requestURL = `${Define.API_URL}banks/active?code=${currency}`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getBanksSuccess(res.data));
    } else {
      yield put(getBanksFailed(res.data));
    }
  } catch (err) {
    yield put(getBanksFailed(err));
  }
}

export function* getSellbank() {
  const requestURL = `${Define.API_URL}userbank/bank_list`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getSellBanksSuccess(res.data));
    } else {
      yield put(getSellBanksFailed(res.data));
    }
  } catch (err) {
    yield put(getSellBanksFailed(err));
  }
}

export function* sellcoin(action) {
  
  const requestURL = `${Define.API_URL}transaction/withdraw`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    
    if (res.status == true || res.status == 200) {
      yield put(sell_coin_Success(res.data));
    } else {
      yield put(sell_coin_Failed(res.message));
    }
  } catch (err) {
    yield put(sell_coin_Failed(err));
  }
}

export function* myEmail(action) {
  const requestURL = `${Define.API_URL}send2fa_email`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(emailSuccess(res.message));
    ToastTopEndSuccessFire(res.message)
  } catch (err) {
    yield put(emailFailed(err));
    ToastTopEndErrorFire("Could not send verification email");
  }
}

export function* myVerifyEmail(action) {
  const requestURL = `${Define.API_URL}verifiy_2facode`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    if (res.status == "success"|| res.status == 200) {
      yield put(verifyemailSuccess(res.message));
    } else if (res.status == false || res.status == "failed") {
      ToastTopEndErrorFire("Invalid code");
      yield put(verifyemailFailed(res.data || res.message));
    }
  } catch (err) {
    yield put(verifyemailFailed(err));
  }
}

export function* getWallets() {
  const requestURL = `${Define.API_URL}userwallets/wallets_list`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    
    yield put(getwalletsSuccess(res.data));


  } catch (err) {
    yield put(getwalletsFailed(err));
  }
}

/**
* Root saga manages watcher lifecycle
*/
export default function* appData() {
  yield takeLatest(WALLETS, myWallets);
  yield takeLatest(GET_CURRENCY, getCurr);
  yield takeLatest(BUY_COIN, buycoin);
  yield takeLatest(GET_BANKS, getBanks);
  yield takeLatest(GET_SELL_BANKS, getSellbank);
  yield takeLatest(SELL_COIN, sellcoin);

  yield takeLatest(EMAIL, myEmail);
  yield takeLatest(VERIFYEMAIL, myVerifyEmail);
  yield takeLatest(GET_WALLETS, getWallets);
}