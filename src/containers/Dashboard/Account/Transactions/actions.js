
import {  GET_TRANSACTIONS, GET_TRANSACTIONS_SUCCESS, GET_TRANSACTIONS_ERROR,
  ALTER_TRANSACTION,
  ALTER_TRANSACTION_ERROR,
  ALTER_TRANSACTION_SUCCESS,
  ALTER_RESET,
  GET_AV_BANKS,
  GET_AV_BANKS_SUCCESS,
  GET_AV_BANKS_ERROR,
  GET_CURRENCY,
  GET_CURRENCY_ERROR,
  GET_CURRENCY_SUCCESS,
  GET_SETTING,
  GET_SETTING_ERROR,
  GET_SETTING_SUCCESS,
   } from './constants';


export function getTransactions(data) { 
  return {
    type: GET_TRANSACTIONS,
    data
  };
}
export function transactionsSuccess(transactions) {
  return {
    type: GET_TRANSACTIONS_SUCCESS,
    transactions,
  };
}
export function transactionsError(error) {
  return {
    type: GET_TRANSACTIONS_ERROR,
    error,
  };
}
export function alterReset() {
  return {
    type: ALTER_RESET,
  };
}

export function alterTransaction(transaction) {
  return {
    type: ALTER_TRANSACTION,
    transaction,
    
  };
}
export function alterTransactionSuccess(data) {
  return {
    type: ALTER_TRANSACTION_SUCCESS,
    data,
    
  };
}
export function alterTransactionFailed(error) {
  return {
    type: ALTER_TRANSACTION_ERROR,
    error,
  };
}

export function getAvBanks(data) {
  return {
    type: GET_AV_BANKS,
    data
  };
}
export function getAvBanksSuccess(banks) {
  return {
    type: GET_AV_BANKS_SUCCESS,
    banks,
  };
}
export function getAvBanksError(error) {
  return {
    type: GET_AV_BANKS_ERROR,
    error,
  };
}

export function getCurrencies() {
  return {
    type: GET_CURRENCY,
  };
}
export function getCurrenciesSuccess(data) {
  return {
    type: GET_CURRENCY_SUCCESS,
    data,
  };
}
export function getCurrenciesFailed(error) {
  return {
    type: GET_CURRENCY_ERROR,
    error,
  };
}

export function getSetting(data) {
  return {
    type: GET_SETTING,
    data
  };
}
export function getSettingSuccess(setting) {
  return {
    type: GET_SETTING_SUCCESS,
    setting,
  };
}
export function getSettingFailed(error) {
  return {
    type: GET_SETTING_ERROR,
    error,
  };
}


