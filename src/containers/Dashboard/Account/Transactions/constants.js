export const GET_TRANSACTIONS = 'exchangefiatUserPanel/Transactions/GET_TRANSACTIONS';
export const GET_TRANSACTIONS_SUCCESS = 'exchangefiatUserPanel/Transactions/GET_TRANSACTIONS_SUCCESS';
export const GET_TRANSACTIONS_ERROR = 'exchangefiatUserPanel/Transactions/GET_TRANSACTIONS_ERROR';

export const ALTER_BANK = 'exchangefiatUserPanel/Transactions/ALTER_BANK';
export const ALTER_BANK_SUCCESS = 'exchangefiatUserPanel/Transactions/ALTER_BANK_SUCCESS';
export const ALTER_BANK_ERROR = 'exchangefiatUserPanel/Transactions/ALTER_BANK_ERROR';

export const ALTER_TRANSACTION = 'exchangefiatUserPanel/Transactions/ALTER_TRANSACTION';
export const ALTER_TRANSACTION_SUCCESS = 'exchangefiatUserPanel/Transactions/ALTER_TRANSACTION_SUCCESS';
export const ALTER_TRANSACTION_ERROR = 'exchangefiatUserPanel/Transactions/ALTER_TRANSACTION_ERROR';
export const ALTER_RESET = 'exchangefiatUserPanel/Transactions/ALTER_RESET';

export const GET_CURRENCY = 'exchangefiatUserPanel/Transactions/GET_CURRENCY';
export const GET_CURRENCY_SUCCESS = 'exchangefiatUserPanel/Transactions/GET_CURRENCY_SUCCESS';
export const GET_CURRENCY_ERROR = 'exchangefiatUserPanel/Transactions/GET_CURRENCY_ERROR';

export const GET_AV_BANKS = 'exchangefiatUserPanel/Transactions/GET_AV_BANKS';
export const GET_AV_BANKS_SUCCESS = 'exchangefiatUserPanel/Transactions/GET_AV_BANKS_SUCCESS';
export const GET_AV_BANKS_ERROR = 'exchangefiatUserPanel/Transactions/GET_AV_BANKS_ERROR';

export const GET_SETTING = 'exchangefiatUserPanel/Transactions/GET_SETTING';
export const GET_SETTING_SUCCESS = 'exchangefiatUserPanel/Transactions/GET_SETTING_SUCCESS';
export const GET_SETTING_ERROR = 'exchangefiatUserPanel/Transactions/GET_SETTING_ERROR';
