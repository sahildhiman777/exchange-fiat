import React from 'react'
import { connect } from 'react-redux';
import { compose } from 'redux';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
// import config from '../../utils/config';

// const platformData = config.platforms;

class Details extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const transaction = this.props.data;
        return (
            <React.Fragment>
                {transaction &&
                    <Modal
                        show={this.props.show}
                        onHide={this.props.onHide}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter-details"
                        centered
                    >
                        <Modal.Header closeButton>
                            {/* <Modal.Title id="contained-modal-title-vcenter-details"> */}
                            <div className='heading_id'>
                                {window.i18n('TransactionDetails')} (#{transaction._id})
                            </div>
                            {/* </Modal.Title> */}
                        </Modal.Header>
                        <Modal.Body className="detailtable">
                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="kt-wizard-v4" id="kt_apps_transaction_add_transaction-details" data-ktwizard-state="first">
                                    <div className="kt-portlet">
                                        <div className="kt-portlet__body kt-portlet__body--fit detail_border">
                                            {!transaction.deposit &&
                                                <div>
                                                    <p style={{ padding: '10px', color: '#716ACA' }}>Note: {!transaction.transaction_hash ? 'Amount is not deposited to given address.' : (transaction.actual_deposit_amount ? transaction.actual_deposit_amount + ' ' + transaction.coin_name + ' has been received successfully.' : '0 ' + transaction.coin_name + ' has been initiated by blockchain successfully. You will receive it soon.')} <a className="btn btn-link" href={transaction.payus_payment_tracking} target="_blank" >{window.i18n('TransactionClickToTrack')}</a></p>
                                                </div>
                                            }
                                            <table className="table">
                                                <tbody>
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionPlatform')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.platform}</span>

                                                            {/* <span className="kt-badge  kt-badge--success kt-badge--inline kt-badge--pill" >{ platformData[(transaction.platform).toLowerCase()]['label'] }</span> 
                                                        </td>
                                                    </tr> */}
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionUserID')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.user_id}</span>
                                                        </td>
                                                    </tr> */}
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionEmail')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.email}</span>
                                                        </td>
                                                    </tr> */}
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionPhone')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.phone}</span>
                                                        </td>
                                                    </tr> */}
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionCoinName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.coin_name}</span>
                                                        </td>
                                                    </tr> */}
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionCoinAmount')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.coin_amount ? transaction.coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : 0} {transaction.coin_name}</span>
                                                        </td>
                                                    </tr>
                                                    {!transaction.deposit &&
                                                        <tr className="nonlyphonerow">
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span>{window.i18n('TransactionActualDepositAmount')}</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span>{transaction.actual_deposit_amount ? transaction.actual_deposit_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : 0} {transaction.coin_name}</span>
                                                            </td>
                                                        </tr>
                                                    }
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionCoinAddress')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.coin_address}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionCurrencyName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.currency_code : transaction.user_currency_code}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionTotalAmount')} </span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.total ? transaction.total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} {transaction.deposit ? transaction.currency_code : transaction.user_currency_code}</span>
                                                        </td>
                                                    </tr>
                                                    {/* {transaction.deposit &&
                                                        <tr className="nonlyphonerow">
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span>{window.i18n('TransactionReturnURL')}</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span>{transaction.return_url}</span>
                                                            </td>
                                                        </tr>
                                                    } */}
                                                   
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionInvoiceNumber')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.transaction_id}</span>
                                                        </td>
                                                    </tr>
                                                    {transaction.deposit &&
                                                        <tr className="nonlyphonerow">
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span className='bank_row_txt'>Bank Details</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span></span>
                                                            </td>
                                                        </tr>
                                                    }

                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionAccountHolderName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.holder_name : transaction.user_bank_acount_holder_name}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionBankName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.bank_name : transaction.user_bank_name}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AccountNumber')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.account_number : transaction.user_bank_account_number}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('BankCode')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.bank_code : ''}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllBankAddress')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.bank_address : ''}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllCountry')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.deposit ? transaction.bank.country : ''}</span>
                                                        </td>
                                                    </tr>

                                                    <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionStatus')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span className="label label-warning">{transaction.status}</span>
                                                        </td>
                                                    </tr>
                                                    {transaction.payus_transaction_id &&
                                                        <tr className="nonlyphonerow">
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span>{window.i18n('TransactionPayus')}</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span>{transaction.payus_transaction_id}</span>
                                                            </td>
                                                        </tr>}
                                                   
                                                    {transaction.deposit &&
                                                        <tr className="nonlyphonerow">
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span>{window.i18n('TransactionFundTransferCode')}</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span>{transaction.code}</span>
                                                            </td>
                                                        </tr>
                                                    }
                                                    {transaction.transaction_hash &&
                                                        <tr className="nonlyphonerow" >
                                                            <td className="kt-datatable__cell phoneheading">
                                                                <span>{window.i18n('TransactionHash')}</span>
                                                            </td>
                                                            <td className="kt-datatable__cell phonecontnent">
                                                                <span>{transaction.transaction_hash}</span>
                                                            </td>
                                                        </tr>}
                                                    {/* <tr className="nonlyphonerow">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('TransactionAdminName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent">
                                                            <span>{transaction.edited_by ? transaction.edited_by.name : 'System'}</span>
                                                        </td>
                                                    </tr> */}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Modal.Body>
                        <Modal.Footer>
                            <Button onClick={this.props.onHide} variant="secondary">{window.i18n('WalletsClose')}</Button>
                        </Modal.Footer>
                    </Modal>
                }
            </React.Fragment>
        )
    }
};

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}


const withConnect = connect(
    mapDispatchToProps,
);

export default compose(
    withConnect
)(Details);
