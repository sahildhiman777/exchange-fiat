import React from 'react'
import DataTable from 'react-data-table-component';
import moment from 'moment';
import Update from './Update';
import styled from 'styled-components';
import Details from './Details';
import { Row, Col } from 'react-bootstrap'
import config from '../../../../utils/config';
const TextField = styled.input`
  height: 32px;
  width: 200px;
  border-radius: 3px;
  border: 1px solid #e5e5e5;
  padding: 16px;
  &:hover {
    cursor: pointer;
  }
`;

class Withdraws extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            create: false,
            edit: false,
            info: false
        }

        this.approve = this.approve.bind(this);
        this.reject = this.reject.bind(this);
        this.search = this.search.bind(this);
        this.info = this.info.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handlePlatformChange = this.handlePlatformChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    handleChange(e) {
        this.props.onStatusChange((e.target.value).toLowerCase());
    };

    handleSelect(e) {
        this.props.onCurrencyChange(e.target.value);
    };

    handlePlatformChange(e) {
        this.props.onPlatformChange((e.target.value).toLowerCase());
    };

    approve(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            editType: 'Approve',
            edit: true
        })
    }

    reject(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            editType: 'Reject',
            edit: true
        })
    }

    info(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            info: true
        })
    }

    search(event) {
        this.props.onSearchTextChange(event.target.value);
    }
    onChangePage(pageNumber) {
        this.props.onChangePage(pageNumber);
    }

    render() {
        const { error, loading, success, data, setting, currencies } = this.props;
        let tableData = [];
        if (this.props.state.trxType == 0) {
            tableData = this.props.data;
        }

        let columns = [
            {
                name: 'Transaction ID',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row._id}</div> : (row.status == 'pending' ? <div className="trading_table">{row._id}</div> : <div className="trading_table_green">{row._id}</div>)}</>
                cell: row => <div className="table_cell">{row._id}</div>
            },
            {
                name: 'User',
                sortable: false,
                selector: 'email',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.email}</div> : (row.status == 'pending' ? <div className="trading_table">{row.email}</div> : <div className="trading_table_green">{row.email}</div>)}</>
                cell: row => <div className="table_cell">{row.email}</div>
            },
            {
                name: 'Account Details',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.user_bank_account_number}<br /></div> : (row.status == 'pending' ? <div className="trading_table">{row.user_bank_acount_holder_name}<br /></div> : <div className="trading_table_green">{row.user_bank_name}<br /></div>)}</>
                cell: row => <div className="table_cell">{row.user_bank_account_number}<br />
                    {row.user_bank_acount_holder_name}<br />
                    {row.user_bank_name}<br />
                </div>
            },
            {
                name: 'Amount',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.total ? row.total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} &nbsp;
                // {row.user_currency_code ? row.user_currency_code : ''}<br />
                //     {row.coin_amount ? row.coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br /></div> : (row.status == 'pending' ? <div className="trading_table">{row.total ? row.total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} &nbsp;
                // {row.user_currency_code ? row.user_currency_code : ''}<br />
                //     {row.coin_amount ? row.coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br /></div> : <div className="trading_table_green">{row.total ? row.total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} &nbsp;
                // {row.user_currency_code ? row.user_currency_code : ''}<br />
                //     {row.coin_amount ? row.coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br /></div>)}</>
                cell: row => <div className="table_cell">{row.total ? row.total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} &nbsp;
                {row.user_currency_code ? row.user_currency_code : ''}<br />
                    {row.coin_amount ? row.coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                {row.coin_name ? row.coin_name : ''}<br /></div>
            },
            {
                name: 'Status',
                sortable: false,
                selector: '_id',
                cell: row => <div className="table_cell">{(row.status == 'rejected') ? <div className="trading_table_red">Rejected</div> : (row.status == 'pending' ? <div className="trading_table">Pending</div> : <div className="trading_table_green">Approved</div>)}</div>
                // cell: row => <div className="table_cell">{row.payus_status}</div>
            },
            {
                name: 'Action',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div> : (row.status == 'pending' ? <div className="trading_table"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div> : <div className="trading_table_green"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div>)}</>
                cell: row => <div className="table_cell"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div>
            },
        ]

        return (
            <React.Fragment>

                <div className="kt-portlet kt-portlet">
                    {
                        this.state.edit &&
                        <Update
                            entityType="withdraws"
                            show={this.state.edit}
                            type={this.state.editType}
                            data={this.state.transactionData}
                            avBanks={this.props.avBanks}
                            onHide={() => this.setState({ edit: false })}
                            onTransactionAlter={this.props.onTransactionAlter}
                        />
                    }

                    {
                        this.state.info &&
                        <Details
                            entityType="deposits"
                            show={this.state.info}
                            data={this.state.transactionData}
                            onHide={() => this.setState({ info: false })}
                        />
                    }

                    <div className="kt-portlet__head">
                        <Row style={{ width: '100%', margin : '0px' }}>
                            <Col sm={12} md={12} lg={4} >
                                <select style={{ marginBottom: '10px' }} className="form-control" onChange={this.handleChange} value={this.props.state.status}>
                                    <option value="pending">{window.i18n('TransactionsPending')}</option>
                                    <option value="approved">{window.i18n('TransactionApproved')}</option>
                                    <option value="rejected">{window.i18n('TransactionRejected')}</option>
                                </select>
                            </Col>

                            <Col sm={12} md={12} lg={4}>
                                <TextField style={{ marginBottom: '10px' }} type="search" role="search" placeholder={window.i18n('Search')} onChange={this.search} value={this.props.state.searchText} />
                            </Col>
                        </Row>
                    </div>

                    {/* <div className="kt-portlet__body kt-portlet__body--fit outer">     */}


                    {/* NEW table Start */}
                    {/* <div className="new_table table-responsive">
                        {tableData != '' ?
                    <table className="table">
                            <thead>
                                <tr>
                                <th scope="col">Transaction ID</th>
                                <th scope="col">User</th>
                                <th scope="col">Account Details</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Withdraw Status</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {(() => {
                const options = [];
                
                  for (let i = 0; i < tableData.length; i++) {
                      
                    options.push(
                        <tr key={`depositCoin${i}`}>
                            <td>{tableData[i]._id}</td>
                            <td>{tableData[i].email}</td>
                            <td>
                                {tableData[i].user_bank_account_number}<br/>
                                {tableData[i].user_bank_acount_holder_name}<br/>
                                {tableData[i].user_bank_name}
                            </td>
                            <td>
                                {tableData[i].total ? tableData[i].total.toLocaleString(true, {maximumFractionDigits: 2}) : 0} &nbsp;
                                {tableData[i].user_currency_code ? tableData[i].user_currency_code : ''}<br />
                                                        {tableData[i].coin_amount ? tableData[i].coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                                {tableData[i].coin_name ? tableData[i].coin_name : ''}
                            </td>
                            <td>{tableData[i].status}</td>
                            <td>
                                <i onClick={this.info.bind(this, tableData[i]._id)} className="fa fa-eye"></i>
                            </td>
                        </tr>
                      
                    );
                  }
                
                return options;

                                </tbody>
                            </table>
                            :
                            <div className="data_not">
                                <h3>There are no records to display</h3>
                            </div>
                        }
                    </div> */}
                    {/* New Table End */}


                    <div className="withdraws_table">
                        <DataTable
                            highlightOnHover={true}
                            pagination={true}
                            // pointerOnHover={true}
                            // onRowClicked={this.onRowClicked}
                            defaultSortAsc={false}
                            columns={columns}
                            paginationPerPage={25}
                            noDataComponent = {window.i18n('AllThereAre')}
                            // paginationRowsPerPageOptions={[25]}
                            // onChangePage={ this.onChangePage }
                            // paginationDefaultPage={this.props.pageNumber}
                            // paginationTotalRows={this.props.totalRecord}
                            paginationServer={true}
                            data={tableData}
                        />
                    </div>
                </div>
                {/* </div> */}
            </React.Fragment>

        )
    }
};
export default Withdraws;
