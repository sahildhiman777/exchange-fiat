
import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_TRANSACTIONS, GET_CURRENCY,  ALTER_TRANSACTION, GET_AV_BANKS, GET_SETTING } from './constants';
import { transactionsSuccess, transactionsError,
  getAvBanksSuccess, 
  getCurrenciesSuccess,
  getCurrenciesFailed, 
  getAvBanksError,    
  alterTransactionFailed,
  alterTransactionSuccess,
  getSettingSuccess,
  getSettingFailed
} from './actions';

import request from '../../../../utils/request';
import { requestSecure} from '../../../../utils/request';
import config from '../../../../utils/config';
export function* alterTransaction(action) {
  const type = action.transaction.type;
  let requestURL = '';
  let options = {};
  if(type=== 'create'){
     requestURL = `${config.API_URL}transaction`;
     options = {
      method: 'POST',
      body: JSON.stringify(action.transaction)
    }
  } else if(type=== 'put'){
     requestURL = `${config.API_URL}transaction/${action.transaction.id}`;
     options = {
      method: 'PUT',
      body: JSON.stringify(action.transaction)
    }
  } else if(type=== 'delete'){
     requestURL = `${config.API_URL}transaction/${action.transaction.id}`;
     options = {
      method: 'DELETE',
      //body: JSON.stringify(action.data)
    }
  } else if(type=== 'get'){
     requestURL = `${config.API_URL}transaction/${action.transaction.id}`;
     options = {
      method: 'GET',
      //body: JSON.stringify(action.data)
    }
  }
  
  try {
    const res = yield call(requestSecure, requestURL, options);
    if(res.status){
      yield put(alterTransactionSuccess(res.data));
      
    } else {
        yield put(alterTransactionFailed(res.data));      
    }
    
  } catch (err) {
    yield put(alterTransactionFailed(err));
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export function* getCurr() {
  const requestURL = `${config.API_URL}currency`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if(res.status){
      yield put(getCurrenciesSuccess(res.data));
    } else {
     
        yield put(getCurrenciesFailed(res.data));
      
      
    }
    
  } catch (err) {
    yield put(getCurrenciesFailed(err));
  }
}
export function* avBanks(action) {
  const requestURL = `${config.API_URL}banks/active?code=${action.data.code}&type=payout`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(request, requestURL, options);
    if(res.status){
      
      yield put(getAvBanksSuccess(res.data));
    } 
   else {
    yield put(getAvBanksError(res.data));
   }
    
    
  } catch (err) {
    yield put(getAvBanksError('cv'));
  }
}
export function* getData(action) {
  const requestURL = `${config.API_URL}transaction/list`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data)
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if(res.status){
      yield put(transactionsSuccess(res.data));
    } else {
     
        yield put(transactionsError(res.data));
      
      
    }
    
  } catch (err) {
    yield put(transactionsError(err));
  }
}

export function* getSetting(action) {
  const requestURL = `${config.API_URL}settings/status`;
  const options = {
    method: "POST",
    body: JSON.stringify(action.data)
  };
  try {
    const res = yield call(requestSecure, requestURL, options);
    if(res.status){
      yield put(getSettingSuccess(res.data));
    } else {
        yield put(getSettingFailed(res.data));
    }
    
  } catch (err) {
    yield put(getSettingFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* userData() {
 
  yield takeLatest(GET_TRANSACTIONS, getData);
  yield takeLatest(ALTER_TRANSACTION, alterTransaction);
  yield takeLatest(GET_AV_BANKS, avBanks);
  yield takeLatest(GET_CURRENCY, getCurr);
  yield takeLatest(GET_SETTING, getSetting);
}
