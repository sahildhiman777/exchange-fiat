import React from 'react'
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import {
	makeSelectData,
	makeSelectError,
	makeSelectLoading,
	makeSelectSuccess,
	makeSelectSetting,
	makeSelectSettingLoading,
	makeSelectSettingSuccess,
	makeSelectSettingError,
	makeSelectCurrencies
} from './selectors';
import { getTransactions, getSetting, getCurrencies } from './actions';
import reducer from './reducer';
import saga from './saga';
import Withdraws from './Withdraws';
import Deposits from './Deposits';
// import { subscribeToTimer } from 'utils/SocketClient';
import { Tab, Row, Col, Nav } from 'react-bootstrap';
// import * as usertz from 'user-timezone';
// import * as playAlert from 'alert-sound-notify';
import { subscribeToTimer } from '../../../../utils/SocketClient';
import ReactLoading from "react-loading";

class Transactions extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			timestamp: false,
			timestampC: false,
			platform: '',
			currency: '',
			searchText: '',
			trxType: 1,
			status: 'pending',
			pageNumber: 1,
			blockloader: false
		};
		subscribeToTimer((err, timestamp) => {
			this.setState({ timestamp })
		});
		this.onTabChange = this.onTabChange.bind(this);
	}

	componentWillMount() {
		this.props.dispatch(getCurrencies())
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: this.state.platform,
			currency_code: this.state.currency,
			searchText: this.state.searchText,
			pageNumber: this.state.pageNumber
		}))
		// this.props.dispatch(getSetting({timezone: usertz.getTimeZone()}))
		this.props.dispatch(getSetting())
	}

	componentDidUpdate() {
		if (this.state.timestamp !== this.state.timestampC) {
			this.setState({ timestampC: this.state.timestamp, blockloader: false })
			this.props.dispatch(getTransactions({
				type: this.state.trxType,
				status: this.state.status,
				platform: this.state.platform,
				currency_code: this.state.currency,
				searchText: this.state.searchText,
				pageNumber: this.state.pageNumber
			}))
		}
	}

	handleStatus = (status) => {
		this.setState({ status: status });
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: status,
			platform: this.state.platform,
			currency_code: this.state.currency,
			searchText: this.state.searchText,
			pageNumber: 1
		}))
	}

	handlePlatform = (platform) => {
		this.setState({ platform: platform });
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: platform,
			currency_code: this.state.currency,
			searchText: this.state.searchText,
			pageNumber: 1
		}))
	}

	handleTransactionAlter = () => {
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: this.state.platform,
			currency_code: this.state.currency,
			searchText: this.state.searchText,
			pageNumber: this.state.pageNumber
		}))
	}

	handleSearchText = (searchText) => {
		this.setState({ searchText: searchText });
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: this.state.platform,
			currency_code: this.state.currency,
			searchText: searchText,
			pageNumber: 1
		}))
	}

	handleCurrency = (currency) => {
		this.setState({ currency: currency });
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: this.state.platform,
			currency_code: currency,
			searchText: this.state.searchText,
			pageNumber: 1
		}))
	}

	handlePageChange = (pageNumber) => {

		this.setState({ pageNumber: pageNumber });
		this.props.dispatch(getTransactions({
			type: this.state.trxType,
			status: this.state.status,
			platform: this.state.platform,
			currency_code: this.state.currency,
			searchText: this.state.searchText,
			pageNumber: pageNumber
		}))
	}

	onTabChange(type) {
		this.props.dispatch(getTransactions({
			type: type,
			status: 'pending',
			platform: '',
			currency_code: '',
			searchText: '',
			pageNumber: 1
		}))

		this.setState({
			platform: '',
			currency: '',
			searchText: '',
			trxType: type,
			status: 'pending',
			pageNumber: 1
		});
	}

	render() {
		const { error, loading, success, data, settingSuccess, setting, currencies } = this.props;
		return (
			<div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch transactions-section">
				{(this.state.blockloader) &&
					<>
						<div className="blockloader">
							<div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
								<ReactLoading color={'#3445CF'} type="spinningBubbles" />
							</div>
						</div>
					</>}
				<div className="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
					<div className="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

							<div className="kt-portlet kt-portlet">
								<div className="kt-portlet__body kt-portlet__body--fit outer" style={{ paddingTop: '20px' }}>
									<Tab.Container id="left-tabs-example" defaultActiveKey="deposit">
										<Row>
											<Col sm={12}>
												<Nav variant="pills">
													{/* <Nav.Item> */}
													<Col sm={6}>
														<Nav.Link className="tab_class" eventKey="deposit" onClick={this.onTabChange.bind(this, 1)}>{window.i18n('TransactionsBuyTransactions')}</Nav.Link>
													</Col>
													{/* </Nav.Item> */}
													{/* <Nav.Item> */}
													<Col sm={6}>
														<Nav.Link className="tab_class" eventKey="withdraw" onClick={this.onTabChange.bind(this, 0)}>{window.i18n('TransactionsSellTransactions')}</Nav.Link>
													</Col>
													{/* </Nav.Item> */}
												</Nav>
												
											</Col>
											<Col sm={12}>
												<Tab.Content>
													<Tab.Pane eventKey="deposit">
														<Deposits loading={loading} pageNumber={this.state.pageNumber} totalRecord={data.total ? data.total : 0} state={this.state} data={data.data ? data.data : []} currencies={currencies} onStatusChange={this.handleStatus} onPlatformChange={this.handlePlatform} onSearchTextChange={this.handleSearchText} onCurrencyChange={this.handleCurrency} onChangePage={this.handlePageChange} onTransactionAlter={this.handleTransactionAlter} />
													</Tab.Pane>
													<Tab.Pane eventKey="withdraw">
														<Withdraws loading={loading} pageNumber={this.state.pageNumber} totalRecord={data.total ? data.total : 0} state={this.state} data={data.data ? data.data : []} setting={setting} currencies={currencies} onStatusChange={this.handleStatus} onPlatformChange={this.handlePlatform} onSearchTextChange={this.handleSearchText} onCurrencyChange={this.handleCurrency} onChangePage={this.handlePageChange} onTransactionAlter={this.handleTransactionAlter} />
													</Tab.Pane>
												</Tab.Content>
											</Col>
										</Row>
									</Tab.Container>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>



		);
	}
}
const mapStateToProps = createStructuredSelector({
	data: makeSelectData(),
	error: makeSelectError(),
	loading: makeSelectLoading(),
	success: makeSelectSuccess(),
	setting: makeSelectSetting(),
	settingLoading: makeSelectSettingLoading(),
	settingSuccess: makeSelectSettingSuccess(),
	settingError: makeSelectSettingError(),
	currencies: makeSelectCurrencies()
});

export function mapDispatchToProps(dispatch) {
	return {
		dispatch,
	};
}

const withReducer = injectReducer({ key: 'transactions', reducer });
const withSaga = injectSaga({ key: 'transactions', saga });

const withConnect = connect(
	mapStateToProps,
	mapDispatchToProps,
);

export default compose(
	withReducer,
	withSaga,
	withConnect,
	withRouter,
)(Transactions);
