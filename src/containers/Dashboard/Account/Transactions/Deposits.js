import React from 'react'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component';
import moment from 'moment';
import Update from './Update';
import styled from 'styled-components';
import Details from './Details';
import { Row, Col } from 'react-bootstrap'
// import config from '../../../../utils/config';
// import LoadingIndicator from "../../components/LoadingIndicator";

// const platformData = config.platforms;
import ReactLoading from "react-loading";

const TextField = styled.input`
  height: 32px;
  width: 200px;
  border-radius: 3px;
  border: 1px solid #e5e5e5;
  padding: 16px;
  &:hover {
    cursor: pointer;
  }
`;
class Deposits extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            info: false,
            blockloader: true,
        }


        this.approve = this.approve.bind(this);
        this.reject = this.reject.bind(this);
        this.search = this.search.bind(this);
        this.info = this.info.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handlePlatformChange = this.handlePlatformChange.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
    }

    handleChange(e) {
        console.log(this.props.onStatusChange,'this.props.onStatusChange');
        
        this.props.onStatusChange((e.target.value).toLowerCase());
    };

    handleSelect(e) {
        this.props.onCurrencyChange(e.target.value);
    };

    handlePlatformChange(e) {
        this.props.onPlatformChange((e.target.value).toLowerCase());
    };

    approve(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            editType: 'Approve',
            edit: true
        })
    }

    reject(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            editType: 'Reject',
            edit: true
        })
    }

    info(id) {
        const Data = this.props.data.filter(d => d._id === id)
        this.setState({
            transactionData: Data[0],
            info: true
        })
    }

    search(event) {
        this.props.onSearchTextChange((event.target.value).toLowerCase());
    }
    onChangePage(pageNumber) {
        this.props.onChangePage(pageNumber);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.data.length > 0 || nextProps.data.length == 0) {
            this.setState({ blockloader: false });
        }
    }
    componentDidMount() {
        // setTimeout(function () {
        //     this.setState={blockloader: false};
        // }, 3000);
    }
    loader() {
        // this.setState({ blockloader: false });
    }
    render() {
        const { error, loading, success, data, currencies } = this.props;
        let tableData = [];
        if(this.props.state.trxType == 1){
            tableData = this.props.data;
        }
        // console.log(tableData,'tableData');
        
        let columns = [
            {
                name: 'Transaction ID',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row._id}</div> : (row.status == 'pending' ? <div className="trading_table">{row._id}</div> : <div className="trading_table_green">{row._id}</div>)}</>
                cell: row => <div className="table_cell">{row._id}</div>
            },
            {
                name: 'User',
                sortable: false,
                selector: 'email',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.email}</div> : (row.status == 'pending' ? <div className="trading_table">{row.email}</div> : <div className="trading_table_green">{row.email}</div>)}</>
                cell: row => <div className="table_cell">{row.email }</div>
            },
            {
                name: 'Account Details',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.bank && row.bank.account_number}<br/>
                // {row.bank && row.bank.holder_name}<br/>
                // {row.bank && row.bank.bank_name}<br/>
                // {row.bank && row.bank.address}</div> : (row.status == 'pending' ? <div className="trading_table">{row.bank && row.bank.account_number}<br/>
                // {row.bank && row.bank.holder_name}<br/>
                // {row.bank && row.bank.bank_name}<br/>
                // {row.bank && row.bank.address}</div> : <div className="trading_table_green">{row.bank && row.bank.account_number}<br/>
                // {row.bank && row.bank.holder_name}<br/>
                // {row.bank && row.bank.bank_name}<br/>
                // {row.bank && row.bank.address}</div>)}</>
                cell: row => <div className="table_cell">{row.bank && row.bank.account_number}<br/>
                {row.bank && row.bank.holder_name}<br/>
                {row.bank && row.bank.bank_name}<br/>
                {row.bank && row.bank.address}</div>
            },
            {
                name: 'Amount',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{row.total ? row.total.toLocaleString(true, {maximumFractionDigits: 2}) : 0} &nbsp;
                // {row.currency_code ? row.currency_code : ''}<br />
                // {row.coin_amount ? row.coin_amount.toLocaleString(true, {maximumFractionDigits: 8}) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br />
                // Code: {row.code ? row.code : '--'}<br /></div> : (row.status == 'pending' ? <div className="trading_table">{row.total ? row.total.toLocaleString(true, {maximumFractionDigits: 2}) : 0} &nbsp;
                // {row.currency_code ? row.currency_code : ''}<br />
                // {row.coin_amount ? row.coin_amount.toLocaleString(true, {maximumFractionDigits: 8}) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br />
                // Code: {row.code ? row.code : '--'}<br /></div> : <div className="trading_table_green">{row.total ? row.total.toLocaleString(true, {maximumFractionDigits: 2}) : 0} &nbsp;
                // {row.currency_code ? row.currency_code : ''}<br />
                // {row.coin_amount ? row.coin_amount.toLocaleString(true, {maximumFractionDigits: 8}) : ''} &nbsp;
                // {row.coin_name ? row.coin_name : ''}<br />
                // Code: {row.code ? row.code : '--'}<br /></div>)}</>
                cell: row => <div className="table_cell">{row.total ? row.total.toLocaleString(true, {maximumFractionDigits: 2}) : 0} &nbsp;
                {row.currency_code ? row.currency_code : ''}<br />
                {row.coin_amount ? row.coin_amount.toLocaleString(true, {maximumFractionDigits: 8}) : ''} &nbsp;
                {row.coin_name ? row.coin_name : ''}<br />
                Code: {row.code ? row.code : '--'}<br /></div>
            },
            {
                name: 'Created at',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red">{moment(row.createdAt).format("D/MM/YYYY")}<br/>
                // {moment(row.createdAt).format("h:mm:ss a")}</div> : (row.status == 'pending' ? <div className="trading_table">{moment(row.createdAt).format("D/MM/YYYY")}<br/>
                // {moment(row.createdAt).format("h:mm:ss a")}</div> : <div className="trading_table_green">{moment(row.createdAt).format("D/MM/YYYY")}<br/>
                // {moment(row.createdAt).format("h:mm:ss a")}</div>)}</>
                cell: row => <div className="table_cell">{moment(row.createdAt).format("D/MM/YYYY")}<br/>
                {moment(row.createdAt).format("h:mm:ss a")}</div>
            },
            {
                name: 'Status',
                sortable: false,
                selector: '_id',
                cell: row => <div className="table_cell">{(row.status == 'rejected') ? <div className="trading_table_red">Rejected</div> : (row.status == 'pending' ? <div className="trading_table">Pending</div> : <div className="trading_table_green">Approved</div>)}</div>
                // cell: row => <div className="table_cell">{row.status}</div>
            },
            {
                name: 'Action',
                sortable: false,
                selector: '_id',
                // cell: row => <>{(row.status == 'rejected') ? <div className="trading_table_red"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div> : (row.status == 'pending' ? <div className="trading_table"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div> : <div className="trading_table_green"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div>)}</>
                cell: row => <div className="table_cell"><i onClick={this.info.bind(this, row._id)} className="fa fa-eye"></i></div>
            },
    ]
        return (
            <React.Fragment>
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <div className="kt-portlet kt-portlet">
                    {
                        this.state.edit &&
                        <Update
                            entityType="deposits"
                            show={this.state.edit}
                            type={this.state.editType}
                            data={this.state.transactionData}
                            onHide={() => this.setState({ edit: false })}
                            onTransactionAlter={this.props.onTransactionAlter}
                        />
                    }
                    {
                        this.state.info &&
                        <Details
                            entityType="deposits"
                            show={this.state.info}
                            data={this.state.transactionData}
                            onHide={() => this.setState({ info: false })}
                        />
                    }
                    <div className="kt-portlet__head">
                        <Row style={{ width: '100%' , margin : '0px'}}>
                            <Col sm={12} md={12} lg={4} >
                                <select style={{ marginBottom: '10px' }} className="form-control" onChange={this.handleChange} value={this.props.state.status}>
                                    <option value="pending">{window.i18n('TransactionsPending')}</option>
                                    <option value="approved">{window.i18n('TransactionsApproved')}</option>
                                    <option value="rejected">{window.i18n('TransactionsRejected')}</option>
                                </select>
                            </Col>
                            
                            <Col sm={12} md={12} lg={4} style={{ justifyContent: 'flex-end' }}>
                                <TextField style={{ marginBottom: '10px' }} type="search" role="search" placeholder={window.i18n('Search')} onChange={this.search} value={this.props.state.searchText} />
                            </Col>
                        </Row>
                    </div>
                    <div className="kt-portlet__body kt-portlet__body--fit outer">
                        <div className="deposit_table"> 
                            {/* {!loading && */}
                            <DataTable
                                highlightOnHover={true}
                                pagination={true}
                                // pointerOnHover={true}
                                // onRowClicked={this.onRowClicked}
                                columns={columns}
                                noDataComponent = {window.i18n('AllThereAre')}
                                // paginationPerPage={25}
                                // paginationRowsPerPageOptions={[25]}
                                // onChangePage={ this.onChangePage }
                                // paginationDefaultPage={this.props.pageNumber}
                                // paginationTotalRows={this.props.totalRecord}
                                // paginationServer={true}
                                data={ tableData }
                            />
                            {/* } */}
                        </div>
                    </div>

                    {/* NEW table Start */}
                    {/* <div className="new_table table-responsive">
                        {tableData != '' ?
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th scope="col">Transaction ID</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Account Details</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Created at</th>
                                        <th scope="col">Withdraw Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {(() => {
                                        const options = [];

                                        for (let i = 0; i < tableData.length; i++) {

                                            options.push(
                                                <tr key={`depositCoin${i}`}>
                                                    <td>{tableData[i]._id}</td>
                                                    <td>{tableData[i].email}</td>
                                                    <td>
                                                        {tableData[i].bank.account_number}<br />
                                                        {tableData[i].bank.holder_name}<br />
                                                        {tableData[i].bank.bank_name}<br />
                                                        {tableData[i].bank.address}
                                                    </td>
                                                    <td>
                                                        {tableData[i].total ? tableData[i].total.toLocaleString(true, { maximumFractionDigits: 2 }) : 0} &nbsp;
                                {tableData[i].currency_code ? tableData[i].currency_code : ''}<br />
                                                        {tableData[i].coin_amount ? tableData[i].coin_amount.toLocaleString(true, { maximumFractionDigits: 8 }) : ''} &nbsp;
                                {tableData[i].coin_name ? tableData[i].coin_name : ''}<br />
                                Code: {tableData[i].code ? tableData[i].code : '--'}<br />
                                                    </td>
                                                    <td>
                                                        {moment(tableData[i].created_at).format("D/MM/YYYY")}<br />
                                                        {moment(tableData[i].created_at).format("h:mm:ss a")}
                                                    </td>
                                                    <td>{tableData[i].payus_status}</td>
                                                    <td>
                                                        <i onClick={this.info.bind(this, tableData[i]._id)} className="fa fa-eye"></i>
                                                    </td>
                                                </tr>

                                            );
                                        }

                                        return options;

                                    })()}

                                </tbody>
                            </table>
                            :
                            <div className="data_not">
                                <h3>There are no records to display</h3>
                            </div>
                        }
                    </div> */}
                    {/* New Table End */}
                </div>
            </React.Fragment>

        )
    }
};


export default Deposits;
