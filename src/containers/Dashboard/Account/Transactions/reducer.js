import produce from 'immer';
import {
  GET_TRANSACTIONS,
  GET_TRANSACTIONS_ERROR,
  GET_TRANSACTIONS_SUCCESS,
  ALTER_TRANSACTION,
  ALTER_TRANSACTION_ERROR,
  ALTER_TRANSACTION_SUCCESS,
  ALTER_RESET,
  GET_AV_BANKS_SUCCESS,
  GET_AV_BANKS,
  GET_CURRENCY_SUCCESS,
  GET_SETTING,
  GET_SETTING_ERROR,
  GET_SETTING_SUCCESS,
} from './constants';

// The initial state of the App
export const initialState = {
  data: false,
  success: false,
  error: false,
  loading: false,
  currencies: [],
  avBanks: [],
  alterTransaction: false,
  alterLoading: false,
  alterSuccess: false,
  alterError: false,
  setting: false,
  settingLoading: false,
  settingSuccess: false,
  settingError: false,
};
/* eslint-disable default-case, no-param-reassign */
const transactionsReducer = (state = initialState, action) =>
  produce(state, draft => {

    switch (action.type) {

      case GET_TRANSACTIONS:
        draft.loading = true;
        draft.error = false;
        draft.success = false;
        break;

      case GET_TRANSACTIONS_SUCCESS:
        draft.data = action.transactions;
        draft.error = false;
        draft.success = true;
        draft.loading = false;
        break;

      case GET_TRANSACTIONS_ERROR:
        draft.error = action.error;
        draft.success = false;
        draft.loading = false;
        break;
      case ALTER_TRANSACTION:
        draft.alterLoading = true;
        draft.alterError = false;
        draft.alterSuccess = false;
        draft.alterTransaction = false;
        break;

      case ALTER_TRANSACTION_SUCCESS:
        draft.alterTransaction = action.data ? action.data : [];
        draft.alterError = false;
        draft.alterSuccess = true;
        draft.alterLoading = false;
        break;

      case ALTER_TRANSACTION_ERROR:
        draft.alterError = action.error.toString();
        draft.alterSuccess = false;
        draft.alterLoading = false;
        break;
      case ALTER_RESET:
        draft.alterError = false;
        draft.alterSuccess = false;
        draft.alterLoading = false;
        break;
      case GET_AV_BANKS_SUCCESS:
        draft.avBanks = action.banks;
        break;
      case GET_CURRENCY_SUCCESS:
        draft.currencies = action.data;
        break;
      case GET_SETTING_SUCCESS:
        draft.setting = action.setting
        draft.settingError = false;
        draft.settingSuccess = true;
        draft.settingLoading = false;
    }
  });

export default transactionsReducer;
