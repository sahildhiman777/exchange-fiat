import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Route, Switch, withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../../toast/toast';
import injectSaga from '../../../../utils/injectSaga';
import {
    makeSelectAlterTransaction,
    makeSelectAlterTransactionError,
    makeSelectAlterTransactionLoading,
    makeSelectAlterTransactionSuccess,
    makeSelectAvBanks,
} from './selectors';
import { alterTransaction, alterReset, getAvBanks } from './actions';
import reducer from './reducer';
import saga from './saga';
// import LoadingIndicator from '../../components/LoadingIndicator';
import Modal from 'react-bootstrap/Modal';


class Update extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            bank: '',
            swal_show: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
            deleted: false,

        };
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
        this.delete = this.delete.bind(this);
    }

    handleChange = (e) => {
        this.props.dispatch(alterReset())
        this.setState({
            swal_show: false
        });
        this.setState({ [e.target.name]: e.target.value, swal: false });
    };

    componentDidMount() {
        this.props.dispatch(getAvBanks({code: this.props.data.deposit ? this.props.data.currency_code: this.props.data.user_currency_code }))
        this.setState({ status: this.props.type === 'Approve' ? 'approved' : 'rejected' , swal: false });
        if (this.props.type === 'delete') {
            this.delete();
        }
    }


    submit = (e) => {
        e.preventDefault();
        const { status, bank } = this.state;
        if (status.length == 0) {
            Swal.fire  (`{window.i18n('TransactionsPleaseSelect')}`)
        } else if (status == 'approved' && bank.length == 0 && !this.props.data.deposit) {
            Swal.fire(`{window.i18n('TransactionsPleaseSelectBank')}`)
        } else {
            this.setState(
                {
                    swal_show: true,
                    swal_loading: 'updating Transaction...',
                    swal_msg_s: 'Transaction has been '+this.props.type+ 'ed successfully!',
                    swal_msg_e: 'error while updating Transaction ',
                }
                , () => {
                    Swal.fire({
                        title: this.state.swal_loading
                    });
                    Swal.showLoading();
                });
            const data = {
                status: this.state.status ? this.state.status : this.props.data.status,
                bank,
                type: 'put',
                id: this.props.data._id
            }

            this.props.dispatch(alterTransaction(data, this.props.entityType));
        }
    }



    delete = e => {
        const data = {
            type: 'delete',
            id: this.props.data._id
        }
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this Transaction!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.setState(
                    {
                        swal_show: true,
                        swal_loading: 'Deleting Transaction ...',
                        swal_msg_s: 'Transaction Deleted successfully!!',
                        swal_msg_e: 'error while deleting Transaction ',
                    }
                    , () => {
                        Swal.fire({
                            title: this.state.swal_loading
                        });
                        Swal.showLoading();
                    });

                this.props.dispatch(alterTransaction(data, this.props.entityType));

            } else {
                this.props.onHide(true);
            }
        })
    }

    componentDidUpdate() {
        if (this.props.success) {
            this.props.dispatch({
                type: 'exchangefiatUserPanel/Transactions/GET_TRANSACTIONS',
              })
              ToastTopEndSuccessFire(this.state.swal_msg_s);
            // Swal.fire({
            //     title: "Success",
            //     type: "success",
            //     text: this.state.swal_msg_s,
            //     timer: 15000
            // }).then((result) => {
                this.props.onHide(true);
                this.props.onTransactionAlter();
            // });
        }
        if (this.props.error) {
            ToastTopEndErrorFire(this.props.error);
            // Swal.fire({
            //     title: "Error",
            //     type: "error",
            //     text: this.props.error,
            //     timer: 15000
            // }).then(d => {
                this.props.dispatch(alterReset())
            // });
        }
        this.props.dispatch(alterReset())
        

    }


    render() {

        const transaction = this.props.data;
        return (

            <React.Fragment>
                {transaction &&
                    <Modal
                        show={this.props.show}
                        onHide={this.props.onHide}
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                            {this.props.type}{window.i18n('Transaction1')}  
                     </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>

                            <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                                <div className="kt-wizard-v4" id="kt_apps_transaction_add_transaction" data-ktwizard-state="first">

                                    <div className="kt-portlet">
                                        <div className="kt-portlet__body kt-portlet__body--fit">
                                            <div className="kt-grid">
                                                <div className="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">

                                                    <form className="kt-form" id="kt_apps_transaction_add_transaction_form" onSubmit={this.submit}>

                                                        <div className="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                            <div className="kt-section kt-section--first">
                                                            <h4> {window.i18n('TransactionWantTo')}  {this.props.type} {window.i18n('TransactionWantTo1')}</h4>
                                                                <div className="kt-wizard-v4__form">
                                                                    <div className="row">
                                                                        <div className="col-xl-12">
                                                                            <div className="kt-section__body">

                                                                                {/* <div className="form-group row">
                                                                                    <label className="col-xl-3 col-lg-3 col-form-label"> Transaction ID</label>
                                                                                    <div className="col-lg-9 col-xl-9">
                                                                                        <input className="form-control" type="text" name="name" value={transaction._id} disabled/>
                                                                                    </div>
                                                                                </div> */}
                                                                                
                                                                                {/* <div className="form-group row">
                                                                                    <label className="col-xl-3 col-lg-3 col-form-label"> Approve / Reject  </label>
                                                                                    <div className="col-lg-9 col-xl-9">
                                                                                        <select onChange={this.handleChange} className="form-control" name="status" defaultValue={this.state.status ? this.state.status : transaction.status} required >
                                                                                            <option >Select</option>
                                                                                            <option value="approved">Approve</option>
                                                                                            <option value="rejected">Reject</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div> */}
                                                                                {!this.props.data.deposit &&
                                                                                    <div className="form-group row">
                                                                                        <label className="col-xl-3 col-lg-3 col-form-label"> {window.i18n('UpdateBank')}   </label>
                                                                                        <div className="col-lg-9 col-xl-9">
                                                                                            <select onChange={this.handleChange} className="form-control" name="bank" defaultValue={this.state.bank ? this.state.bank : transaction.bank} required >
                                                                                                <option >{window.i18n('Select')}</option>
                                                                                                {this.props.avBanks.map(bk =>
                                                                                                    <option key={`ds${bk._id}`} value={bk._id}>{`${bk.holder_name} - ${bk.account_number}-${bk.bank_name}-${bk.bank_code}`}</option>
                                                                                                )}
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                }

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="kt-form__actions">

                                                            <button
                                                                disabled={transaction.status !== 'pending' ?
                                                                    true : this.state.status !== '' ? false : true}
                                                                onSubmit={this.submit} className={ this.props.type === 'Approve' ? "btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" : "btn btn-danger btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" } >
                                                                {this.props.type}
                                                        </button>



                                                            <span onClick={this.props.onHide} className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u span-btn" >
                                                            {window.i18n('AllCancel')} 
                                                        </span>

                                                        </div>
                                                        <div className="kt-form__actions">


                                                            {/* {this.props.loading && <LoadingIndicator />} */}

                                                        </div>


                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Modal.Body>

                    </Modal>
                }
            </React.Fragment>

        )
    }
};
const mapStateToProps = createStructuredSelector({
    transaction: makeSelectAlterTransaction(),
    error: makeSelectAlterTransactionError(),
    success: makeSelectAlterTransactionSuccess(),
    loading: makeSelectAlterTransactionLoading(),
    avBanks: makeSelectAvBanks(),
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'transactions', reducer });
const withSaga = injectSaga({ key: 'transactions', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter,
)(Update);
