
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTransactions = state => state.transactions || initialState;

const makeSelectData = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.data,
  );
const makeSelectError = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.error,
  );
const makeSelectLoading = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.loading,
  );
const makeSelectSuccess = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.success,
  );

  const makeSelectAvBanks = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.avBanks,
  );

  const makeSelectCurrencies = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.currencies,
  );


  const makeSelectAlterTransaction = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.alterTransaction,
  );
const makeSelectAlterTransactionError = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.alterError,
  );
const makeSelectAlterTransactionLoading = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.alterLoading,
  );
const makeSelectAlterTransactionSuccess = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.alterSuccess,
  ); 

  const makeSelectSetting = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.setting,
  );
  const makeSelectSettingLoading = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.settingLoading,
  );
  const makeSelectSettingSuccess = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.settingSuccess,
  );
  const makeSelectSettingError = () =>
  createSelector(
    selectTransactions,
    transactionsState => transactionsState.settingError,
  );

export { selectTransactions,makeSelectAlterTransaction,
  makeSelectAlterTransactionError,
  makeSelectAlterTransactionLoading,
  makeSelectAlterTransactionSuccess,
  makeSelectAvBanks,
  makeSelectCurrencies,
  makeSelectData,
  makeSelectError,
  makeSelectLoading,
  makeSelectSuccess,
  makeSelectSetting,
  makeSelectSettingLoading,
  makeSelectSettingSuccess,
  makeSelectSettingError
};
