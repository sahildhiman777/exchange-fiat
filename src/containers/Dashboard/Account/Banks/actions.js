
import { 
  GET_BANKS,
  GET_BANKS_ERROR,
  GET_BANKS_SUCCESS,
  ALTER_BANK,
  ALTER_BANK_ERROR,
  ALTER_BANK_SUCCESS,
  ALTER_RESET,
  UPLOAD_RESET,
  UPLOAD_FILE,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR,
  GET_COUNTRY,
  GET_COUNTRY_ERROR,
  GET_COUNTRY_SUCCESS,

} from './constants';


export function alterReset() {
  return {
    type: ALTER_RESET,
  };
}
export function getBanks() {
  return {
    type: GET_BANKS,
  };
}
export function getBanksSuccess(data) {
  return {
    type: GET_BANKS_SUCCESS,
    data,
  };
}
export function getBanksFailed(error) {
  return {
    type: GET_BANKS_ERROR,
    error,
  };
}

export function getCountry() {
  return {
    type: GET_COUNTRY,
  };
}
export function getCountrySuccess(data) {
  return {
    type:GET_COUNTRY_SUCCESS,
    data,
  };
}
export function getCountryFailed(error) {
  return {
    type: GET_COUNTRY_ERROR,
    error,
  };
}


export function alterBanks(data) {
  return {
    type: ALTER_BANK,
    data,
  };
}
export function alterBanksSuccess(data) {
  return {
    type: ALTER_BANK_SUCCESS,
    data,
  };
}
export function alterBanksFailed(error) {
  return {
    type: ALTER_BANK_ERROR,
    error,
  };
}
export function uploadReset() {
  return {
    type: UPLOAD_RESET,
  };
}
export function uploadFile(data) {
  return {
    type: UPLOAD_FILE,
    data,
  };
}

export function uploadFileSuccess(data) {
  return {
    type: UPLOAD_FILE_SUCCESS,
    data,
  };
}

export function uploadFileFailed(error) {
  return {
    type: UPLOAD_FILE_ERROR,
    error,
  };
}