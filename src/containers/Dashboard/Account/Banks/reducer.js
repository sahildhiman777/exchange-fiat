import produce from 'immer';
import {
  GET_BANKS,
  GET_BANKS_ERROR,
  GET_BANKS_SUCCESS,
  ALTER_BANK,
  ALTER_BANK_ERROR,
  ALTER_BANK_SUCCESS,
  ALTER_RESET,
  UPLOAD_RESET,
  UPLOAD_FILE,
  UPLOAD_FILE_SUCCESS,
  UPLOAD_FILE_ERROR,
  GET_COUNTRY,
  GET_COUNTRY_ERROR,
  GET_COUNTRY_SUCCESS
} from './constants';

// The initial state of the App
export const initialState = {
  banks: [],
  currency: [],
  alterBank: false,
  success: false,
  error: false,
  loading: false,
  alterLoading: false,
  alterSuccess: false,
  alterError: false,
  uploadError: false,
  uploadSuccess: false,
  uploadLoading: false,
  Country: [],
  Countrysuccess: false,
  Countryerror: false,
  Countryloading: false,
};
/* eslint-disable default-case, no-param-reassign */
const banksReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {

      case GET_BANKS:
        draft.loading = true;
        // draft.error = false;
        // draft.success = false;
        // draft.banks= [];
        break;

      case GET_BANKS_SUCCESS:
        draft.banks = action.data ? action.data : [];
        draft.currency = action.data.currency ? action.data.currency : [];
        draft.error = false;
        draft.success = true;
        draft.loading = false;
        break;

      case GET_BANKS_ERROR:
        draft.error = action.error.toString();
        draft.success = false;
        draft.loading = false;
        draft.banks = [];
        break;


      case GET_COUNTRY:
        draft.Countryloading = true;
        break;

      case GET_COUNTRY_SUCCESS:
        draft.Country = action.data ? action.data : [];
        draft.Countryerror = false;
        draft.Countrysuccess = true;
        draft.Countryloading = false;
        break;

      case GET_COUNTRY_ERROR:
        draft.Countryerror = action.error.toString();
        draft.Countrysuccess = false;
        draft.Countryloading = false;
        draft.banks = [];
        break;



      case ALTER_BANK:
        draft.alterLoading = true;
        draft.alterError = false;
        draft.alterSuccess = false;
        break;

      case ALTER_BANK_SUCCESS:
        const banks = state.alterBank;
        if (action.data.ctype === 'create') {
          const nb = action.data.d;
          draft.alterBank = banks.concat([nb]);
        } else if (action.data.ctype === 'delete') {
          const del = banks.filter(d => d._id !== action.data.d._id);
          draft.alterBank = del;
        } else if (action.data.ctype === 'put') {
          const c = banks.filter(d => d._id !== action.data.d._id);
          draft.alterBank = c.concat([action.data.d]);
        } else {
          draft.alterBank = action.data.d ? action.data.d : [];
        }

        draft.alterError = false;
        draft.alterSuccess = true;
        draft.alterLoading = false;
        break;


      case ALTER_BANK_ERROR:
        draft.alterError = action.error.toString();
        draft.alterSuccess = false;
        draft.alterLoading = false;
        break;
      case ALTER_RESET:
        draft.alterError = false;
        draft.alterSuccess = false;
        draft.alterLoading = false;
        break;

      case UPLOAD_FILE:
        draft.uploadError = false;
        draft.uploadSuccess = false;
        draft.uploadLoading = true;
        break;
      case UPLOAD_FILE_SUCCESS:
        draft.uploadError = false;
        draft.uploadSuccess = action.data;
        draft.uploadLoading = false;
        break;
      case UPLOAD_FILE_ERROR:
        draft.uploadError = action.error.toString();
        draft.uploadSuccess = false;
        draft.uploadLoading = false;
        break;
      case UPLOAD_RESET:
        draft.uploadError = false;
        draft.uploadSuccess = false;
        draft.uploadLoading = false;
        break;
    }
  });

export default banksReducer;
