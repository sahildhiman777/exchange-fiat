import React from 'react'
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import {
  makeSelectBanks,
  makeSelectError,
  makeSelectLoading,
  makeSelectSuccess,
  makeSelectCurrency,
  makeSelectCountry,
  makeSelectCountryError,
  makeSelectCountryLoading,
  makeSelectCountrySuccess,
  makeSelectAlterBank,
  makeSelectAlterBankError,
  makeSelectAlterBankLoading,
  makeSelectAlterBankSuccess,
} from './selectors';
import { getBanks, alterReset, getCountry, alterBanks } from './actions';
import reducer from './reducer';
import saga from './saga';
import Create from './Create';
import Update from './Update';
import moment from 'moment';
import ReactLoading from "react-loading";
import Modal from 'react-bootstrap/Modal';
import DataTable from 'react-data-table-component';

class All extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      create: false,
      edit: false,
      visible: false,
      swal_show: false,
      deleted: false,
      banksData: [],
      filteredData: false,
      statusFilter: 'payin',
      search: '',
      bankId: '',
      blockloader: false,
      showDelete: false
    };
  }
  componentWillMount() {
    let data = {
      type: 'get'
    };
    this.props.dispatch(getCountry());
    this.props.dispatch(alterBanks(data));
    this.props.dispatch(getBanks());
  }

  componentDidUpdate() {
    if (this.props.getbanksuccess) {
      this.props.dispatch(alterReset());
      this.setState({ blockloader: false })
    }
  }

  showDelete = () => {
    this.setState({
      showDelete: true
    });
  }

  hideDelete = () => {
    this.setState({ 
      blockloader: false,
      showDelete: false
    });
  }

  handleChange = (e) => {
    this.setState({ statusFilter: (e.target.value).toLowerCase() });
    this.search({ target: { value: this.state.search } }, (e.target.value).toLowerCase());
  };

  edit(data) {
    this.setState({ edit: true, banksData: data })
  }

  delete(data) {
    this.setState({ bankId: data._id, showDelete: true })
  }

  deleteData() {
    if (this.props.user) {
      let deletedata = {
        user_id: this.props.user._id,
        bankId: this.state.bankId,
        type: 'delete'
      };
      this.setState({ blockloader: true, showDelete: false })
      this.props.dispatch(alterBanks(deletedata));
    }

  }


  render() {
    const { banks } = this.props;
    let bankList= this.props.getbank;
    // let bank = this.props.getbank;
    let gettotal = banks;
    gettotal = gettotal ? gettotal.length : 0;
    let local_currency = localStorage.getItem('currency') ;
    // for (let i=0; i< bank.length; i++){
    //   if(bank[i].currency == local_currency){
    //     bankList.push(bank[i]);
    //   }
    // }

    let columns = [
      {
        name: `${window.i18n('AccountNumber')}`,
        sortable: false,
        selector: 'user_bank_account_number',
        cell: row => <div className="table_cell">{row.user_bank_account_number}</div>
      },
      {
        name: `${window.i18n('BankName')}`,
        sortable: false,
        selector: 'user_bank_name',
        cell: row => <div className="table_cell">{row.user_bank_name}</div>
      },
      {
        name: `${window.i18n('HolderName')}`,
        sortable: false,
        selector: 'user_bank_acount_holder_name',
        cell: row => <div className="table_cell">{row.user_bank_acount_holder_name}</div>
      },
      {
        name: `${window.i18n('Country')}`,
        sortable: false,
        selector: 'bank_country_name',
        cell: row => <div className="table_cell">{row.bank_country_name}</div>
      },
      {
        name: `${window.i18n('CreatedAt')}`,
        sortable: false,
        selector: 'createdAt',
        cell: row => <div className="table_cell">{moment(row.createdAt).format("D/MM/YYYY")}</div>
      },
      {
        name: `${window.i18n('Action')}`,
        sortable: false,
        cell: row => (
          <div className="table_cell">
            <button className="bank_table_button" onClick={() => this.edit(row)}><i className="fa fa-edit"></i></button>
            <button className="bank_table_button" onClick={() => this.delete(row)}><i className="fa fa-trash-alt"></i></button>
          </div>
        )
      },
    ]

    return (
      <React.Fragment>
        <div className="bank-section">
          {(this.state.blockloader) &&
            <>
              <div className="blockloader">
                <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                  <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                </div>
              </div>
            </>}

          <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            {
              this.state.create &&
              <Create
                show={this.state.create}
                countryAvailable={this.props.currency}
                banklist={this.props.banks}
                countryList={this.props.country}
                user={this.props.user}
                onHide={() => this.setState({
                  create: false,
                  visible: false
                })}
              />
            }
            {
              this.state.edit &&
              <Update
                show={this.state.edit}
                type={this.state.editType}
                data={this.state.banksData}
                countryList={this.props.country}
                banklist={this.props.banks}
                onHide={() => this.setState({
                  edit: false,
                  visible: false
                })}
              />
            }
            <div className="kt-portlet kt-portlet--mobile">
              <div className="kt-subheader__toolbar">
                <button onClick={() => this.setState({ create: true })} className="btn btn-label-brand btn-bold bank-button">
                  {window.i18n('AddBank')}
                </button>
              </div>

              <div className="kt-portlet__body kt-portlet__body--fit outer">
                <div className="onlytablet">

                  {/* NEW table Start */}
                  <div className="banklisttable table-responsive">
                  <DataTable
            highlightOnHover={true}
            columns={columns}
            noDataComponent={window.i18n('AllThereAre')}
            data={bankList}
          />
                    {/* {bankList != '' ?
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">{window.i18n('AccountNumber')}</th>
                            <th scope="col">{window.i18n('BankName')}</th>
                            <th scope="col">{window.i18n('HolderName')}</th>
                            <th scope="col">{window.i18n('Country')}</th>
                            <th scope="col">{window.i18n('CreatedAt')}</th>
                            <th scope="col">{window.i18n('Action')}</th>
                          </tr>
                        </thead>
                        <tbody>
                          {(() => {
                            const options = [];
                            for (let i = 0; i < bankList.length; i++) {
                              options.push(
                                <tr key={`bank-list-${i}`}>
                                  <td>{bankList[i].user_bank_account_number}</td>
                                  <td>{bankList[i].user_bank_name}</td>
                                  <td>
                                    {bankList[i].user_bank_acount_holder_name}
                                  </td>
                                  <td>
                                    {bankList[i].bank_country_name}
                                  </td>
                                  <td>
                                    {moment(bankList[i].createdAt).format("D/MM/YYYY")}
                                  </td>
                                  <td>
                                    <button className="bank_table_button" onClick={() => this.edit(bankList[i])}><i className="fa fa-edit"></i></button>
                                    <button className="bank_table_button" onClick={() => this.delete(bankList[i])}><i className="fa fa-trash-alt"></i></button>
                                  </td>
                                </tr>
                              );
                            }
                            return options;
                          })()}
                        </tbody>
                      </table>
                      :
                      <div className="data_not">
                        <h3>{window.i18n('AllThereAre')}</h3>
                      </div>
                    } */}
                  </div>
                  {/* New Table End */}
                </div>
              </div>
            </div>
          </div>
        </div>


        <Modal
          show={this.state.showDelete}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          id="deletemodal"
        >
          <Modal.Body>
            <h4 className="modal-title">Are you sure you want to delete the bank?</h4>
            <button type="button" className="padding bank-button btn btn-success" onClick={() => this.deleteData()} >Yes</button>
            <button type="button" className="padding btn btn-secondary" onClick={() => this.hideDelete()}>No</button>
          </Modal.Body>
        </Modal>
      </React.Fragment>

    )
  }
};
const mapStateToProps = createStructuredSelector({
  getbank: makeSelectAlterBank(),
  getbankerror: makeSelectAlterBankError(),
  getbanksuccess: makeSelectAlterBankSuccess(),
  getbankloading: makeSelectAlterBankLoading(),
  banks: makeSelectBanks(),
  error: makeSelectError(),
  loading: makeSelectLoading(),
  success: makeSelectSuccess(),
  currency: makeSelectCurrency(),
  country: makeSelectCountry(),
  countryerror: makeSelectCountryError(),
  countryloading: makeSelectCountryLoading(),
  countrysuccess: makeSelectCountrySuccess(),
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withReducer = injectReducer({ key: 'banks', reducer });
const withSaga = injectSaga({ key: 'banks', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter,
)(All);
