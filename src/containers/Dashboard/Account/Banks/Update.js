import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Route, Switch, withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import {
    makeSelectAlterBank,
    makeSelectAlterBankError,
    makeSelectAlterBankLoading,
    makeSelectAlterBankSuccess,
    makeSelectUploadFileError,
    makeSelectUploadFileSuccess,
    makeSelectUploadFileLoading
} from './selectors';
import { alterBanks, alterReset, uploadFile, uploadReset } from './actions';
import reducer from './reducer';
import saga from './saga';
// import LoadingIndicator from '../../components/LoadingIndicator';
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../../toast/toast';
import ReactLoading from "react-loading";

class Update extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            account_number: '',
            bank_name: '',
            holder_name: '',
            bank_address: '',
            bank_code: '',
            country: '',
            type: '',
            currency_id: '',
            status: undefined,
            error: false,
            swal_show: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
            deleted: false,
            currencyAvailable: [],
            filename: '',
            bankId: '',
            user_id: '',
            bank: '',
            bankData: [],

        };
        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
        this.handleChangeCountry = this.handleChangeCountry.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
    }


    handleChange = (e) => {
        // this.props.dispatch(alterReset())
        if (e.target.type === 'checkbox') {
            this.setState({ status: this.state.status !== undefined ? !this.state.status : !this.props.bank.status, swal_show: false });
        } else {
            this.setState({ swal: false });
            this.setState({ [e.target.name]: e.target.value, swal_show: false });
        }

    };


    componentWillMount() {
        this.props.dispatch(alterReset());
        if (this.props.data) {
            this.setState({
                bankId: this.props.data._id,
                user_id: this.props.data.user_id,
                account_number: this.props.data.user_bank_account_number,
                bank_name: this.props.data.user_bank_name,
                holder_name: this.props.data.user_bank_acount_holder_name,
                country: this.props.data.bank_country_name,
                bank: this.props.data.user_bank_name,
            });
            if (this.props.data.bank_country_name) {
                let country = this.props.data.bank_country_name
                const currencyAvailable = this.props.bank.filter(function (data) {
                    if (data.bank_country_name == country) {

                        return data;
                    }
                })
                // console.log(currencyAvailable,'data');
                this.setState({ currencyAvailable: currencyAvailable[0].user_bank_name });
            }
        }
    }
    handleChangeCountry = (e) => {
        this.setState({ currencyAvailable: '' })
        let bank = [];
        bank = this.props.banklist.filter(data => data.country == e.target.value);
        this.setState({ country: e.target.value, bankData: bank, swal: false });
    };

    handleChangeType = (e) => {
        this.setState({ bank_name: e.target.value, bank: e.target.value, swal: false });
    };
    submit = (e) => {
        e.preventDefault();
        const data = {
            user_id: this.props.data.user_id,
            country_name: this.state.country,
            bank_name: this.state.bank_name,
            account_number: this.state.account_number,
            account_holder_name: this.state.holder_name,
            type: 'put',
            bankId: this.state.bankId,
        };
        this.setState({ swal: true, blockloader: true });
        this.props.dispatch(alterBanks(data));
    }

    componentDidUpdate() {
        if (this.props.success) {
            ToastTopEndSuccessFire('Bank details has been updated')
            // Swal.fire({
            //     title: "Success",
            //     type: "success",
            //     text: "Bank details has been updated",
            //     showConfirmButton: false,
            //     timer: 2000
            // }).then((result) => {
                this.props.dispatch(alterReset());
                this.setState({ blockloader: false })
                this.props.onHide(true);
            // });
        }

        if (this.props.error) {
            ToastTopEndErrorFire(this.props.error);
            // Swal.fire({
            //     title: "Error",
            //     type: "error",
            //     text: this.props.error,
            //     timer: 2000
            // }).then(d => {
                this.setState({ blockloader: false })
                this.props.dispatch(alterReset())
            // });
        }
    }

    render() {
        const { error, success, loading } = this.props;
        let country = this.props.countryList;
        let bank = this.props.banklist;

        return (
            <React.Fragment>
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">
                        {window.i18n('UpdateBank')}
                     </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="kt-wizard-v4" id="kt_apps_user_add_user" data-ktwizard-state="first">
                                <div className="kt-portlet">
                                    <div className="kt-portlet__body kt-portlet__body--fit">
                                        <div className="kt-grid">
                                            <div className="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                                                <form className="kt-form" id="kt_apps_user_add_user_form" onSubmit={this.submit}>
                                                    <div className="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                        <div className="kt-section kt-section--first">
                                                            <div className="kt-wizard-v4__form">
                                                                <div className="row">
                                                                    <div className="col-xl-12">
                                                                        <div className="kt-section__body">

                                                                            <div className="form-group row">
                                                                                <label className="col-xl-3 col-lg-3 col-form-label">{window.i18n('AllAccountNumber')}  </label>
                                                                                <div className="col-lg-9 col-xl-9">
                                                                                    <input onChange={this.handleChange} className="form-control" type="text" name="account_number"
                                                                                        value={this.state.account_number} required />
                                                                                </div>
                                                                            </div>
                                                                            <div className="form-group row">
                                                                                <label className="col-xl-3 col-lg-3 col-form-label">{window.i18n('AllHolderName')}  </label>
                                                                                <div className="col-lg-9 col-xl-9">
                                                                                    <input onChange={this.handleChange} className="form-control" type="text" name="holder_name"
                                                                                        value={this.state.holder_name} required />
                                                                                </div>
                                                                            </div>
                                                                            <div className="form-group row">
                                                                                <label className="col-xl-3 col-lg-3 col-form-label"> {window.i18n('AllCountry')}  </label>
                                                                                <div className="col-lg-9 col-xl-9">
                                                                                    <select onChange={(e) => this.handleChangeCountry(e)} className="form-control" name="country" value={this.state.country} required >
                                                                                        <option >{window.i18n('AllSelectCountry')}</option>
                                                                                        {(() => {
                                                                                            const options = [];
                                                                                            if (country) {
                                                                                                for (let i = 0; i < country.length; i++) {
                                                                                                    options.push(
                                                                                                        <option key={country[i][0]._id} value={country[i][0]._id}>{country[i][0]._id}</option>
                                                                                                    );
                                                                                                }
                                                                                                return options;
                                                                                            }
                                                                                        })()}
                                                                                    </select>
                                                                                </div>
                                                                            </div>

                                                                            <div className="form-group row">
                                                                                <label className="col-xl-3 col-lg-3 col-form-label"> {window.i18n('UpdateBank')}</label>
                                                                                <div className="col-lg-9 col-xl-9">
                                                                                    <select className="form-control" onChange={(e) => this.handleChangeType(e)} name="type" value={this.state.bank} required>
                                                                                        <option value={this.state.country} >{this.state.currencyAvailable ? this.state.currencyAvailable : 'Select bank'} </option>
                                                                                        {(() => {
                                                                                            const options = [];
                                                                                            if (this.state.bankData) {
                                                                                                let banklist = this.state.bankData;
                                                                                                for (let i = 0; i < banklist.length; i++) {
                                                                                                    options.push(
                                                                                                        <option value={banklist[i].name}>{banklist[i].name}</option>
                                                                                                    );
                                                                                                }
                                                                                                return options;
                                                                                            }
                                                                                        })()}
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="kt-form__actions">

                                                        <button onSubmit={this.submit} className=" bank-button btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" >
                                                            {window.i18n('Update')}
                                                        </button>
                                                        <span onClick={this.props.onHide} className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u span-btn" >
                                                            {window.i18n('UpdateCancel')}
                                                        </span>

                                                    </div>
                                                    <div className="kt-form__actions">
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>

            </React.Fragment>
        )
    }
};
const mapStateToProps = createStructuredSelector({
    bank: makeSelectAlterBank(),
    error: makeSelectAlterBankError(),
    success: makeSelectAlterBankSuccess(),
    loading: makeSelectAlterBankLoading(),
    uploadError: makeSelectUploadFileError(),
    uploadSuccess: makeSelectUploadFileSuccess(),
    uploadLoading: makeSelectUploadFileLoading()
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'banks', reducer });
const withSaga = injectSaga({ key: 'banks', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter,
)(Update);
