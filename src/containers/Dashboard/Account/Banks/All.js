import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import {
    makeSelectBanks,
    makeSelectError,
    makeSelectLoading,
    makeSelectSuccess,
    makeSelectCurrency
} from './selectors';
import { getBanks, alterReset } from './actions';
import reducer from './reducer';
import saga from './saga';
// import LoadingIndicator from '../../components/LoadingIndicator';
import DataTable from 'react-data-table-component';
import Create from './Create';
import Update from './Update';
import Tooltip from './Tooltip';
import moment from 'moment';
class All extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            create: false,
            edit: false,
            visible: false,
            swal_show: false,
            deleted: false,
            banksData:false,
            filteredData: false,
            statusFilter: 'payin',
            search: '',
        };
        this.onRowClicked = this.onRowClicked.bind(this);
        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
        this.search = this.search.bind(this);
        this.DirectUpdate = this.DirectUpdate.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {        
        this.props.dispatch(getBanks());
    }

    handleChange = (e) => {
        this.setState({ statusFilter: (e.target.value).toLowerCase() });
        this.search({target: { value: this.state.search}}, (e.target.value).toLowerCase());
    };
    onRowClicked(e) {
       this.setState({visible: this.state.visible ===e._id ? false :e._id })
    }

    edit(id) {
        const banksData= this.props.banks.filter(d=>d._id === id)
       
        this.setState({
            banksData: banksData[0],
            editType:'edit',
            edit:true
        })

    }

    delete(id) {
        const banksData= this.props.banks.filter(d=>d._id === id)
        this.setState({
            banksData: banksData[0],
            editType:'delete',
            edit:true
        })

    }

    DirectUpdate(id) {
        const banksData= this.props.banks.filter(d=>d._id === id)
        this.setState({
            banksData: banksData[0],
            editType:'DirectUpdate',
            edit:true
        })

    }

    search(event, statusFilter) {
        const str =event.target.value.toLowerCase();
        const filteredData = this.props.banks.filter(item => (
            item.type === statusFilter && 
            (item.account_number.toLowerCase().includes(str) ||
            item.bank_name.toLowerCase().includes(str) ||
            item.holder_name.toLowerCase().includes(str) ||
            item.bank_address.toLowerCase().includes(str) ||
            item.bank_code.toLowerCase().includes(str) ||
            item.country.toLowerCase().includes(str) )
         ));

         this.setState({filteredData, search:event.target.value})

    }

    
    

    render() {
        const { error, loading, success, banks } = this.props;
        
        const columns = [

            {
                name: 'Account number',
                ignoreRowClick: true,
                selector: 'account_number',
                sortable: true,
            },
            {
                name: 'Bank name',
                selector: 'bank_name',
                ignoreRowClick: true,
                sortable: true,
            },
            {
                name: 'Holder name',
                selector: 'holder_name',
                ignoreRowClick: true,
                sortable: true,
            },
            {
                selector: 'bank_address',
                ignoreRowClick: true,
                name: 'Bank address',
                sortable: true,
            },
            {
                name: 'Bank code',
                selector: 'bank_code',
                ignoreRowClick: true,
                sortable: true,
            },
            {
                name: 'Country',
                selector: 'country',
                ignoreRowClick: true,
                sortable: true,
            },
            {
                name: 'Status',
                ignoreRowClick: true,
                allowOverflow: true,
                button: true,
                cell: row => <span className="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                    <label>
                        <input onChange={this.DirectUpdate.bind(this, row._id)} type="checkbox" defaultChecked={row.status} name="" />
                        <span></span>
                    </label>
                </span>
            },
            {
                name: 'Created at',
                ignoreRowClick: true,
                cell: row => <span>{moment(row.created_at).format("D/MM/YYYY")}<br />
                    {moment(row.created_at).format("h:mm:ss a")}
                </span>
            },
            {
                name: 'Updated at',
                ignoreRowClick: true,
                cell: row => <span>{moment(row.updated_at).format("D/MM/YYYY")}<br />
                    {moment(row.updated_at).format("h:mm:ss a")}
                </span>
            },
            {
                name: 'Action',
                ignoreRowClick: true,
                allowOverflow: true,
                cell: row => <span>
                    <i onClick={this.edit.bind(this, row._id)} className="kt-nav__link-icon flaticon2-edit" style={{marginRight: '10px'}}></i>
                    <i onClick={this.delete.bind(this, row._id)} className="kt-nav__link-icon flaticon2-trash"></i>                    
                </span>
            },
        ];
        let gettotal = banks;
        gettotal = gettotal ? gettotal.length :0;
        return (
            <React.Fragment>

                <div className="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div className="kt-container ">
                        <div className="kt-subheader__main">
                            <h3 className="kt-subheader__title">
                                {window.i18n('AllBanks')}
                            </h3>
                            <div className="kt-subheader__group" id="kt_subheader_search">
                                <span className="kt-subheader__desc" id="kt_subheader_total">
                                    {gettotal} {window.i18n('AllTotal')}
                                </span>

                                <form className="kt-margin-l-20" id="kt_subheader_search_form">
                                    <div className="kt-input-icon kt-input-icon--right kt-subheader__search">
                                        <input onChange={e => this.search(e, this.state.statusFilter)} value={this.state.search} type="text" className="form-control" placeholder="Search..." id="generalSearch" />
                                        <span className="kt-input-icon__icon kt-input-icon__icon--right">
                                            <span>
                                                <i className="flaticon2-search-1"></i>
                                            </span>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="kt-subheader__toolbar">

                            <button onClick={() => this.setState({ create: true })} className="btn btn-label-brand btn-bold">
                            {window.i18n('AllAddBanks')}   
                        </button>


                        </div>
                    </div>
                </div>
                {/* {loading && <LoadingIndicator />} */}
                <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">


                    {
                        this.state.create && 
                        <Create
                            show={this.state.create}
                            countryAvailable={this.props.currency}
                            onHide={() => this.setState({ create: false, visible:false })}
                        />
                    }
                    {
                        this.state.edit && 
                        <Update
                            show={this.state.edit}
                            type={this.state.editType}
                            data={this.state.banksData}
                            countryAvailable={this.props.currency}
                            onHide={() => this.setState({ edit: false, visible:false })}
                        />
                    }



                    <div className="kt-portlet kt-portlet--mobile">
                        <div className="kt-portlet__head" style={{justifyContent: 'flex-end', display: 'flex', marginTop: '10px'}}>
                            <select style={{width: '200px', marginBottom: '10px'}} className="form-control" onChange={this.handleChange} defaultValue={this.state.statusFilter ? this.state.statusFilter : 'payin'}>
                                <option value="payin">{window.i18n('AllPay-inBanks')}</option>
                                <option value="payout">{window.i18n('AllPay-outBanks')}</option>
                            </select>
                        </div>
                        <div className="kt-portlet__body kt-portlet__body--fit outer">
                        <div className="onlyphones">
                                {(this.state.filteredData ? this.state.filteredData : banks.filter(d => d.type === this.state.statusFilter)).map(row =>
                                    <div className="kt-portlet kt-portlet" key={`wd${row._id}`}>
                                        <div className="kt-portlet__body kt-portlet__body--fit">
                                            <table className="table">
                                                <tbody>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllAccountNumber')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            {row.account_number}
                                                        </td>
                                                    </tr>

                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllBankName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span >{row.bank_name}</span>


                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllHolderName')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span >{row.holder_name}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllBankAddress')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span >{row.bank_address}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllBankCode')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span >{row.bank_code}</span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllAccountNumber')}Country</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span >{row.country}</span>
                                                        </td>
                                                    </tr>

                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllStatus')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                        <span className="kt-switch kt-switch--outline kt-switch--icon kt-switch--success">
                                                            <label>
                                                                <input onChange={this.DirectUpdate.bind(this, row._id)} type="checkbox" defaultChecked={row.status} name="" />
                                                                <span></span>
                                                            </label>
                                                        </span>
                                                        </td>
                                                    </tr>

                                                
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllCreatedAt')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span>{moment(row.created_at).format("D/MM/YYYY")}<br />
                                                                {moment(row.created_at).format("h:mm:ss a")}
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllUpdatedAt')}</span>
                                                        </td>
                                                        <td className="kt-datatable__cell phonecontnent" >
                                                            <span>{moment(row.updated_at).format("D/MM/YYYY")}<br />
                                                                {moment(row.created_at).format("h:mm:ss a")}
                                                            </span>
                                                        </td>
                                                    </tr>

                                                    <tr className="nonlyphonerow row">
                                                        <td className="kt-datatable__cell phoneheading">
                                                            <span>{window.i18n('AllAction')}</span>
                                                        </td>
                                                        <td style={{width: '200px'}}>
                                                            <i onClick={this.edit.bind(this, row._id)} className="fa fa-edit" style={{marginRight: '10px'}}></i>
                                                            <i onClick={this.delete.bind(this, row._id)} className="fa fa-trash-alt"></i>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                )
                                }
                            </div>

                            <div className="onlytablet">
                                <DataTable
                                    highlightOnHover={true}
                                    pagination ={true}
                                    pointerOnHover={true}
                                    onRowClicked={this.onRowClicked}
                                    columns={columns}
                                    data={this.state.filteredData ? this.state.filteredData : banks.filter(d => d.type === this.state.statusFilter)} 
                                />
                            </div>
                            
                        </div>
                    </div>
                </div>
            </React.Fragment>

        )
    }
};
const mapStateToProps = createStructuredSelector({
    banks: makeSelectBanks(),
    error: makeSelectError(),
    loading: makeSelectLoading(),
    success: makeSelectSuccess(),
    currency: makeSelectCurrency(),
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'banks', reducer });
const withSaga = injectSaga({ key: 'banks', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter,
)(All);
