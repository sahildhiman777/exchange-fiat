/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectBanks = state => state.banks || initialState;


const makeSelectBanks = () =>
  createSelector(
    selectBanks,
    banksState => banksState.banks,
  );
  const makeSelectCurrency = () =>
  createSelector(
    selectBanks,
    banksState => banksState.currency,
  );
const makeSelectError = () =>
  createSelector(
    selectBanks,
    banksState => banksState.error,
  );
const makeSelectLoading = () =>
  createSelector(
    selectBanks,
    banksState => banksState.loading,
  );
const makeSelectSuccess = () =>
  createSelector(
    selectBanks,
    banksState => banksState.success,
  );

  const makeSelectAlterBank = () =>
  createSelector(
    selectBanks,
    banksState => banksState.alterBank,
  );
const makeSelectAlterBankError = () =>
  createSelector(
    selectBanks,
    banksState => banksState.alterError,
  );
const makeSelectAlterBankLoading = () =>
  createSelector(
    selectBanks,
    banksState => banksState.alterLoading,
  );
const makeSelectAlterBankSuccess = () =>
  createSelector(
    selectBanks,
    banksState => banksState.alterSuccess,
  ); 
  
const makeSelectUploadFileError = () =>
  createSelector(
    selectBanks,
    banksState => banksState.uploadError,
  );
const makeSelectUploadFileLoading = () =>
  createSelector(
    selectBanks,
    banksState => banksState.uploadLoading,
  );
const makeSelectUploadFileSuccess = () =>
  createSelector(
    selectBanks,
    banksState => banksState.uploadSuccess,
  );

  const makeSelectCountry = () =>
  createSelector(
    selectBanks,
    banksState => banksState.Country,
  );
  const makeSelectCountryError = () =>
  createSelector(
    selectBanks,
    banksState => banksState.Countryerror,
  );
const makeSelectCountryLoading = () =>
  createSelector(
    selectBanks,
    banksState => banksState.Countryloading,
  );
const makeSelectCountrySuccess = () =>
  createSelector(
    selectBanks,
    banksState => banksState.Countrysuccess,
  );

export { 
  selectBanks,
    makeSelectError,
    makeSelectLoading,
    makeSelectSuccess,
    makeSelectBanks,
    makeSelectAlterBank,
    makeSelectAlterBankError,
    makeSelectAlterBankLoading,
    makeSelectAlterBankSuccess,
    makeSelectCurrency,
    makeSelectUploadFileError,
    makeSelectUploadFileLoading,
    makeSelectUploadFileSuccess,
    makeSelectCountry,
    makeSelectCountryError,
    makeSelectCountryLoading,
    makeSelectCountrySuccess
   };
