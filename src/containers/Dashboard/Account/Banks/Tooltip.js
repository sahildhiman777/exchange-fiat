import React from 'react';
export default function Tooltip(props){
    return (        
    <div className="dropdown-menu dropdown-menu-right show text-right" style={{  position: 'absolute', willChange: 'transform; top: 0px', left: '-50px', }}>
            <ul className="kt-nav">
                <li className="kt-nav__item">																    
                    <span onClick={props.edit} className="kt-nav__link" >
                        <i className="kt-nav__link-icon flaticon2-contract"></i>
                        <span className="kt-nav__link-text">{window.i18n('TooltipEdit')}</span>
                    </span>
                </li>
                <li className="kt-nav__item">
                    <span onClick={props.delete} className="kt-nav__link" >
                        <i className="kt-nav__link-icon flaticon2-trash"></i>
                        <span className="kt-nav__link-text">{window.i18n('TooltipDelete')}</span>
                    </span>
                </li>
            </ul>
        </div>)
}; 