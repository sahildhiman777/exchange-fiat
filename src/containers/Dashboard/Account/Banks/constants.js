
export const GET_BANKS = 'exchangefiatUserPanel/Banks/GET_ALL_BANKS';
export const GET_BANKS_SUCCESS = 'exchangefiatUserPanel/Banks/GET_BANKS_SUCCESS';
export const GET_BANKS_ERROR = 'exchangefiatUserPanel/Banks/GET_BANKS_ERROR';

export const ALTER_BANK = 'exchangefiatUserPanel/Banks/ALTER_BANK';
export const ALTER_BANK_SUCCESS = 'exchangefiatUserPanel/Banks/ALTER_BANK_SUCCESS';
export const ALTER_BANK_ERROR = 'exchangefiatUserPanel/Banks/ALTER_BANK_ERROR';

export const ALTER_RESET = 'exchangefiatUserPanel/Banks/ALTER_RESET';

export const UPLOAD_RESET = 'exchangefiatUserPanel/Banks/UPLOAD_RESET';
export const UPLOAD_FILE = 'exchangefiatUserPanel/Banks/UPLOAD_FILE';
export const UPLOAD_FILE_SUCCESS = 'exchangefiatUserPanel/Banks/UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_ERROR = 'exchangefiatUserPanel/Banks/UPLOAD_FILE_ERROR';


export const GET_COUNTRY = 'exchangefiatUserPanel/Banks/GET_COUNTRY';
export const GET_COUNTRY_ERROR = 'exchangefiatUserPanel/Banks/GET_COUNTRY_ERROR';
export const GET_COUNTRY_SUCCESS = 'exchangefiatUserPanel/Banks/GET_COUNTRY_SUCCESS';