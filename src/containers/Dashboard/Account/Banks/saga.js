/**
 * Gets the repositories of the user from Github
 */

import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_BANKS, ALTER_BANK, UPLOAD_FILE, GET_COUNTRY } from './constants';
import { getBanksFailed, getBanksSuccess, alterBanksFailed, alterBanksSuccess, uploadFileSuccess, uploadFileFailed, getCountryFailed, getCountrySuccess } from './actions';

import { requestSecure, requestFile } from '../../../../utils/request';
import Define from '../../../../utils/config';

export function* getBanks() {
  const requestURL = `${Define.API_URL}banks/list`;
  const options = {
    method: 'GET',
  }

  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getBanksSuccess(res.data));
    } else {
      yield put(getBanksFailed(res.data));
    }
  } catch (err) {
    yield put(getBanksFailed(err));
  }
}

export function* getCountry() {
  const requestURL = `${Define.API_URL}country/list`;
  const options = {
    method: 'GET',
  }

  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getCountrySuccess(res.data));
    } else {
      yield put(getCountryFailed(res.data));
    }
  } catch (err) {
    yield put(getCountryFailed(err));
  }
}


export function* alterBank(action) {
  const type = action.data.type;
  let requestURL = '';
  let options = {};
  if (type === 'create') {
    requestURL = `${Define.API_URL}userbank/add_Bank`;
    options = {
      method: 'POST',
      body: JSON.stringify(action.data)
    }
  } else if (type === 'put') {
    requestURL = `${Define.API_URL}userbank/editbank`;
    options = {
      method: 'PUT',
      body: JSON.stringify(action.data)
    }
  } else if (type === 'delete') {
    requestURL = `${Define.API_URL}userbank/deletebank/${action.data.user_id}`;
    options = {
      method: 'POST',
      body: JSON.stringify(action.data)
    }
  } else if (type === 'get') {
    requestURL = `${Define.API_URL}userbank/bank_list`;
    options = {
      method: 'GET',
    }
  }

  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      if (type === 'delete') {
        const del = { ctype: type, d: { _id: action.data.bankId } };
        yield put(alterBanksSuccess(del));
      } else if (type === 'put') {
        const update = { ctype: type, d: res.data };
        yield put(alterBanksSuccess(update));
      } else {
        yield put(alterBanksSuccess({ ctype: type, d: res.data }));
      }
    } else {
      yield put(alterBanksFailed(res.message));
    }

  } catch (err) {
    yield put(alterBanksFailed(err));
  }
}

export function* uploadLogo(action) {
  const requestURL = `${Define.APP_URL}/upload-file`;
  var formData = new FormData();
  formData.append('logo', action.data);
  const options = {
    method: 'POST',
    body: formData
  }
  try {
    const res = yield call(requestFile, requestURL, options);
    if (res.status) {
      yield put(uploadFileSuccess(res));
    } else {
      yield put(uploadFileFailed(res.message));
    }
  } catch (err) {
    yield put(uploadFileFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* appData() {

  yield takeLatest(GET_BANKS, getBanks);
  yield takeLatest(GET_COUNTRY, getCountry);
  yield takeLatest(ALTER_BANK, alterBank);
  yield takeLatest(UPLOAD_FILE, uploadLogo);
}
