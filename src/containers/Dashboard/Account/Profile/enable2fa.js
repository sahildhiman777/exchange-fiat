import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Route, Switch, withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import googlePlayimg from '../../../../../src/google-play.png';
import appstorelogo from '../../../../../src/Available-on-app-store.png';
import Swal from 'sweetalert2';
import { createQR } from './actions';
import jwt from 'jwt-decode';
import Disable from './disable';
import {

    makeSelectAlterMyProfile,
    makeSelectAlterMyProfileError,
    makeSelectAlterMyProfileLoading,
    makeSelectAlterMyProfileSuccess,

    makeSelectG2fa,
    makeSelectG2faError,
    makeSelectG2faSuccess,
    makeSelectDisableG2faSuccess,

    makeSelectDisableG2faEmail,
    makeSelectDisableG2faEmailError,
    makeSelectDisableG2faEmailSuccess,
    makeSelectDisableG2faEmailLoading,

    makeSelectUser,
    makeSelectLoading,
    makeSelectError,
    makeSelectSuccess,
    makeSelectRedirect,

    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,

    makeSelectMyEmail,
    makeSelectMyEmailError,
    makeSelectMyEmailLoading,
    makeSelectMyEmailSuccess,

    makeSelectMyEmailVerify,
    makeSelectMyEmailVerifyError,
    makeSelectMyEmailVerifyLoading,
    makeSelectMyEmailVerifySuccess,

    makeSelectEmail2FaEnable,
    makeSelectEmail2FaEnableSuccess,
    makeSelectEmail2FaEnableError,
    makeSelectEmail2FaEnableLoading,

    makeSelectEnableAlterG2fa,
    makeSelectEnableG2fa,
    makeSelectEnableG2faError,
    makeSelectEnableG2faSuccess,
    makeSelectEnableG2faLoading
} from './selectors';
import { enable2fa, enable2faReset,alterProfile, loadUser, wallets, disable2faemail, enable2FaEmail, enable2FaEmailReset, emailverify } from './actions';
import reducer from './reducer';
import saga from './saga';
import Modal from 'react-bootstrap/Modal';

class Enable2fa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            error: false,
            swal_show: false,
            buttonClicked: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
            google2faCheckedValue: false,
            text: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.passingProps = this.passingProps.bind(this);
        this.cancel = this.cancel.bind(this);
        this.submit = this.submit.bind(this);
    }
    cancel(e) {
        this.setState({
            google2faCheckedValue: 'false'
        }, () => this.props.getInput(this.state.google2faCheckedValue))
        this.props.onHide(false);
    }
    passingProps(e) {
        var newInput = e.target.value;

        this.setState({
            text: newInput
        }, () => this.props.getInput(this.state.text));
    }
    handleChange = (e) => {
        this.setState({ swal_show: false });
        this.setState({ [e.target.name]: e.target.value, swal: false });
    };

    submit = (e) => {
        e.preventDefault();
        this.setState({
            buttonClicked: true
        })

        const data = {
            code: this.state.code,
            google2fa: true,
            Secfakey: this.props.Skey,
            type: 'enable',
            id: this.props.user._id
        }
        this.props.dispatch(enable2fa(data));
    }

    componentWillMount() {
        const accessToken = localStorage.getItem("exchangefiatUserPanel");
        let decoded = jwt(accessToken);
        const data = {
            id: decoded.id
        }
        if (data.id) {
            this.props.dispatch(createQR(data));
        }
        if (this.props.user.emailstatus) {
            this.setState({ emailstatus: true })
        }

        this.props.dispatch(enable2faReset());
    }
    componentDidMount(){
        if (this.props.user) {
            if (this.props.user.google2fa === true) {
                this.setState({ checked: true, disableVisible: true })
            } else {
                this.setState({ checked: false, disableVisible: false })
            }
        }
    }

    componentDidUpdate() {
        if (this.props.enable === true && this.props.success) {
            this.props.onHide(true);
        }
        // if (this.props.user) {
        //     if (this.props.user.google2fa === true) {
        //         this.setState({ checked: true, disableVisible: true })
        //     } else {
        //         this.setState({ checked: false, disableVisible: false })
        //     }
        // }
    }
    handleSubmit = (e) => {
        if (this.state.code.length >= 6)
            this.submit(e);
    }
    render() {
        const { QR2fa, user } = this.props;
        return (
            <div>
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    size="md"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    className="enable_popup"
                >
                    <Modal.Header closeButton>
                        <div className="modal-title">{window.i18n('ProfileFactor')}</div>
                    </Modal.Header>
                    <Modal.Body>
                        <form onSubmit={this.submit}>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">{window.i18n('Code')}</label>
                                <input onChange={this.handleChange} className="form-control" type="text" name="code" value={this.state.code} placeholder={`${window.i18n('secretCode')}`} onKeyUp={this.handleSubmit} required />
                            </div>
                            <button onSubmit={this.submit} className="bank-button btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" >{window.i18n('ProfileEnable')}</button>
                            <span onClick={this.cancel} style={{ marginLeft: '10px' }} className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u span-btn" >{window.i18n('ProfileCancel')}</span>
                        </form>
                        <div className="enable_disable">
                        <div>
                                                    {this.state.disableVisible ?

                                                        <Disable
                                                            user={user}
                                                        />
                                                        :
                                                        <>
                                                            <div className="kt-subheader kt-grid__item" id="kt_subheader">
                                                                <div className="kt-container ">
                                                                    <div className="kt-subheader__main">
                                                                        <div className=''>
                                                                            <div className=' column-2'>
                                                                                <div className='top-div-in-column-2'>
                                                                                    <h2 className='auth-h2'>{window.i18n('ProfileEnablefactor')}</h2>
                                                                                </div>
                                                                                <div className='middle-div-in-column-2'>
                                                                                    <div style={{ marginBottom: '15px' }}>
                                                                                        <div>
                                                                                            <img src={QR2fa.image_data} />
                                                                                        </div>
                                                                                        <div className='playstorelogo'>
                                                                                            <a href='https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_IN' target="_blank"><img style={{ paddingRight: '8px' }} src={googlePlayimg} /></a>
                                                                                            <img src={appstorelogo} />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style={{ marginLeft: '15px' }}>
                                                                                        <p>{window.i18n('ProfileFirst')} <br />{window.i18n('ProfileDownloadGoogle1')}</p>
                                                                                        <p>{window.i18n('ProfileScanBarcode')}<br />{window.i18n('ProfileScanBarcode1')}<br />{window.i18n('ProfileScanBarcode2')}</p>
                                                                                        <h5>{window.i18n('ProfileSecurityKey')}: {QR2fa.data}</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div className='bottom-div-in-column-2'>
                                                                                    <div>
                                                                                        <h1 style={{ fontSize: '20px' }}>{window.i18n('ProfileGoogleGuide')}</h1>
                                                                                    </div>
                                                                                    <div >
                                                                                        {window.i18n('ProfileGoogleAndroidOrApple')}<br />
                                                                                        {window.i18n('ProfileGoTo')} <span className='span-color-red'>{window.i18n('ProfileMenu')}</span> -> <span className='span-color-red'>{window.i18n('ProfileSetupAccount')}</span><br />
                                                                                        3.{window.i18n('ProfileChoose')}  <span className='span-color-red'>{window.i18n('ProfileScan')}</span> {window.i18n('Profileoption')}<br />
                                                                                        <span style={{ fontStyle: 'italic' }}> 4. <span style={{ color: ' #2A2728' }}> {window.i18n('settingUnableToScan')} </span>{window.i18n('ProfileChoose')} <span className='span-color-red'>{window.i18n('ProfileEnter')}</span>{window.i18n('ProfileType')}  <span>{window.i18n('ProfileSecurityKey')}</span><br /></span>
                                                                                        {window.i18n('settingSixDigit')}<br />
                                                                                        {window.i18n('settingEveryTime')}<br />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </>
                                                   } 
                                                </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    enable: makeSelectEnableG2fa(),
    error: makeSelectEnableG2faError(),
    success: makeSelectEnableG2faSuccess(),
    loading: makeSelectEnableG2faLoading(),

    user: makeSelectUser(),
    QR2fa: makeSelectG2fa(),
    error: makeSelectG2faError(),
    success: makeSelectG2faSuccess(),
    usererror: makeSelectError(),
    userloading: makeSelectLoading(),
    usersuccess: makeSelectSuccess(),
    enablesuccess: makeSelectEnableG2faSuccess(),
    disablesuccess: makeSelectDisableG2faSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'myprofile', reducer });
const withSaga = injectSaga({ key: 'myprofile', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter,
)(Enable2fa);
