import {
  ALTER_PASSWORD,
  ALTER_PASSWORD_ERROR,
  ALTER_PASSWORD_SUCCESS,
  ALTER_RESET,
  ALTER_PROFILE,
  ALTER_PROFILE_SUCCESS,
  ALTER_PROFILE_ERROR,
  ALTER_PROFILE_RESET,

  CREATE_QR,
  CREATE_QR_SUCCESS,
  CREATE_QR_ERROR,
  ENABLE2FA,
  ENABLE2FA_SUCCESS,
  ENABLE2FA_ERROR,
  ENABLE2FA_RESET,
  DISABLE2FA,
  DISABLE2FA_SUCCESS,
  DISABLE2FA_ERROR,
  DISABLE2FA_RESET,
  USER, USER_SUCCESS, USER_ERROR, REDIRECT,
  DISABLE2FAEMAIL_RESET,

  WALLETS,
  WALLETS_ERROR,
  WALLETS_SUCCESS,

  EMAIL,
  EMAIL_ERROR,
  EMAIL_SUCCESS,
  EMAILVERIFY,
  EMAILVERIFY_ERROR,
  EMAILVERIFY_SUCCESS,

  DISABLE2FAEMAIL,
  DISABLE2FAEMAIL_SUCCESS,
  DISABLE2FAEMAIL_ERROR,

  ENABLE2FAEMAIL,
  ENABLE2FAEMAIL_SUCCESS,
  ENABLE2FAEMAIL_ERROR,
  ENABLE2FAEMAIL_RESET,
  SENDPHONECODE,
  SENDPHONECODE_ERROR,
  SENDPHONECODE_SUCCESS,

  VERIFYOTP,
  VERIFYOTP_ERROR,
  VERIFYOTP_SUCCESS,

  IMAGEUPLOAD,
  IMAGEUPLOAD_ERROR,
  IMAGEUPLOAD_SUCCESS,

} from './constants';

export function alterPassword(userAccount) {
  return { type: ALTER_PASSWORD, userAccount };
}
export function alterPasswordSuccess(data) {
  return { type: ALTER_PASSWORD_SUCCESS, data };
}
export function alterPasswordFailed(error) {
  return { type: ALTER_PASSWORD_ERROR, error };
}


export function alterProfile(userProfile) {
  return { type: ALTER_PROFILE, userProfile };
}
export function alterProfileSuccess(data) {
  return { type: ALTER_PROFILE_SUCCESS, data };
}
export function alterProfileFailed(error) {
  return { type: ALTER_PROFILE_ERROR, error };
}


export function createQR(data) {
  return { type: CREATE_QR, data };
}
export function createQRSuccess(data) {
  return { type: CREATE_QR_SUCCESS, data };
}
export function createQRFailed(error) {
  return { type: CREATE_QR_ERROR, error };
}
// enable
export function enable2fa(data) {
  return { type: ENABLE2FA, data };
}
export function enable2faSuccess(data) {
  return { type: ENABLE2FA_SUCCESS, data };
}
export function enable2faFailed(error) {
  return { type: ENABLE2FA_ERROR, error };
}
export function enable2faReset() {
  return { type: ENABLE2FA_RESET };
}
// Disable
export function disable2fa(data) {
  return { type: DISABLE2FA, data };
}
export function disable2faSuccess(data) {
  return { type: DISABLE2FA_SUCCESS, data };
}
export function disable2faFailed(error) {
  return { type: DISABLE2FA_ERROR, error };
}
export function disable2faReset() {
  return { type: DISABLE2FA_RESET }
}
export function disable2faEmailReset() {
  return { type: DISABLE2FAEMAIL_RESET }
}

// Enable 2fa email
export function enable2FaEmail(data) {
  return { type: ENABLE2FAEMAIL, data };
}
export function enable2FaEmailSuccess(data) {
  return { type: ENABLE2FAEMAIL_SUCCESS, data };
}
export function enable2FaEmailFailed(error) {
  return { type: ENABLE2FAEMAIL_ERROR, error };
}
export function enable2FaEmailReset(error) {
  return { type: ENABLE2FAEMAIL_RESET, error };
}

// Disable
export function disable2faemail(data) {
  return { type: DISABLE2FAEMAIL, data };
}
export function disable2faemailSuccess(data) {
  return { type: DISABLE2FAEMAIL_SUCCESS, data };
}
export function disable2faemailFailed(error) {
  return { type: DISABLE2FAEMAIL_ERROR, error };
}


export function redirect(data) {
  return { type: REDIRECT, data };
}
export function loadUser() {
  return { type: USER };
}
export function userSuccess(data) {
  return { type: USER_SUCCESS, data };
}
export function userFailed(error) {
  return { type: USER_ERROR, error };
}

export function wallets(data) {
  return { type: WALLETS, data };
}
export function walletsSuccess(data) {
  return { type: WALLETS_SUCCESS, data };
}
export function walletsFailed(error) {
  return { type: WALLETS_ERROR, error };
}

export function email(data) {
  return { type: EMAIL, data };
}
export function emailSuccess(data) {
  return { type: EMAIL_SUCCESS, data };
}
export function emailFailed(error) {
  return { type: EMAIL_ERROR, error };
}

export function emailverify(data) {
  return { type: EMAILVERIFY, data };
}
export function emailverifySuccess(data) {
  return { type: EMAILVERIFY_SUCCESS, data };
}
export function emailverifyFailed(error) {
  return { type: EMAILVERIFY_ERROR, error };
}

export function sendphonecode(data) {
  return { type: SENDPHONECODE, data };
}
export function sendphonecodeSuccess(data) {
  return { type: SENDPHONECODE_SUCCESS, data };
}
export function sendphonecodeFailed(error) {
  return { type: SENDPHONECODE_ERROR, error };
}

export function verifyotp(data) {
  return { type: VERIFYOTP, data };
}
export function verifyotpSuccess(data) {
  return { type: VERIFYOTP_SUCCESS, data };
}
export function verifyotpFailed(error) {
  return { type: VERIFYOTP_ERROR, error };
}

export function imageupload(data) {
  return { type: IMAGEUPLOAD, data };
}
export function imageuploadSuccess(data) {
  return { type: IMAGEUPLOAD_SUCCESS, data };
}
export function imageuploadFailed(error) {
  return { type: IMAGEUPLOAD_ERROR, error };
}

// export function verifyemail(data) {
//   return { type: VERIFYEMAIL, data };
// }
// export function verifyemailSuccess(data) {
//   return { type: VERIFYEMAIL_SUCCESS, data };
// }
// export function verifyemailFailed(error) {
//   return { type: VERIFYEMAIL_ERROR, error };
// }