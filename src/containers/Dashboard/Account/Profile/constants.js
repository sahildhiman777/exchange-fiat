export const ALTER_PASSWORD = 'exchangefiatUserPanel/Password/ALTER_PASSWORD';
export const ALTER_PASSWORD_SUCCESS = 'exchangefiatUserPanel/Password/ALTER_PASSWORD_SUCCESS';
export const ALTER_PASSWORD_ERROR = 'exchangefiatUserPanel/Password/ALTER_PASSWORD_ERROR';
export const ALTER_RESET = 'exchangefiatUserPanel/Password/ALTER_RESET';

export const ALTER_PROFILE = 'exchangefiatUserPanel/profile/ALTER_PROFILE';
export const ALTER_PROFILE_SUCCESS = 'exchangefiatUserPanel/profile/ALTER_PROFILE_SUCCESS';
export const ALTER_PROFILE_ERROR = 'exchangefiatUserPanel/profile/ALTER_PROFILE_ERROR';
export const ALTER_PROFILE_RESET = 'exchangefiatUserPanel/profile/ALTER_PROFILE_RESET';



export const CREATE_QR = 'giaodichcoin/g2fa/CREATE_QR';
export const CREATE_QR_SUCCESS = 'giaodichcoin/g2fa/CREATE_QR_SUCCESS';
export const CREATE_QR_ERROR = 'giaodichcoin/g2fa/CREATE_QR_ERROR';

export const ENABLE2FA = 'giaodichcoin/g2fa/ENABLE2FA'; 
export const ENABLE2FA_SUCCESS = 'giaodichcoin/g2fa/ENABLE2FA_SUCCESS';
export const ENABLE2FA_ERROR = 'giaodichcoin/g2fa/ENABLE2FA_ERROR';
export const ENABLE2FA_RESET = 'giaodichcoin/g2fa/ENABLE2FA_RESET'; 

export const DISABLE2FA = 'giaodichcoin/g2fa/DISABLE2FA'; 
export const DISABLE2FA_SUCCESS = 'giaodichcoin/g2fa/DISABLE2FA_SUCCESS';
export const DISABLE2FA_ERROR = 'giaodichcoin/g2fa/DISABLE2FA_ERROR';
export const DISABLE2FA_RESET = 'giaodichcoin/g2fa/DISABLE2FA_RESET';
export const DISABLE2FAEMAIL_RESET = 'giaodichcoin/g2fa/DISABLE2FAEMAIL_RESET';

export const ENABLE2FAEMAIL = 'giaodichcoin/g2fa/ENABLE2FAEMAIL'; 
export const ENABLE2FAEMAIL_SUCCESS = 'giaodichcoin/g2fa/ENABLE2FAEMAIL_SUCCESS';
export const ENABLE2FAEMAIL_ERROR = 'giaodichcoin/g2fa/ENABLE2FAEMAIL_ERROR';
export const ENABLE2FAEMAIL_RESET = 'giaodichcoin/g2fa/ENABLE2FAEMAIL_RESET';

export const DISABLE2FAEMAIL = 'giaodichcoin/g2fa/DISABLE2FAEMAIL'; 
export const DISABLE2FAEMAIL_SUCCESS = 'giaodichcoin/g2fa/DISABLE2FAEMAIL_SUCCESS';
export const DISABLE2FAEMAIL_ERROR = 'giaodichcoin/g2fa/DISABLE2FAEMAIL_ERROR';

export const USER = 'exchangefiatUserPanel/App/USER';
export const USER_SUCCESS = 'exchangefiatUserPanel/App/USER_SUCCESS';
export const USER_ERROR = 'exchangefiatUserPanel/App/USER_ERROR';
export const REDIRECT = 'exchangefiatUserPanel/App/REDIRECT';

export const WALLETS = 'exchangefiatUserPanel/wallets/WALLETS';
export const WALLETS_SUCCESS = 'exchangefiatUserPanel/wallets/WALLETS_SUCCESS';
export const WALLETS_ERROR = 'exchangefiatUserPanel/wallets/WALLETS_ERROR';

export const EMAIL = 'exchangefiatUserPanel/email/EMAIL';
export const EMAIL_SUCCESS = 'exchangefiatUserPanel/email/EMAIL_SUCCESS';
export const EMAIL_ERROR = 'exchangefiatUserPanel/email/EMAIL_ERROR';

export const EMAILVERIFY = 'exchangefiatUserPanel/email/EMAILVERIFY';
export const EMAILVERIFY_SUCCESS = 'exchangefiatUserPanel/email/EMAILVERIFY_SUCCESS';
export const EMAILVERIFY_ERROR = 'exchangefiatUserPanel/email/EMAILVERIFY_ERROR';

export const SENDPHONECODE = 'exchangefiatUserPanel/phone/SENDPHONECODE';
export const SENDPHONECODE_SUCCESS = 'exchangefiatUserPanel/phone/SENDPHONECODE_SUCCESS';
export const SENDPHONECODE_ERROR = 'exchangefiatUserPanel/phone/SENDPHONECODE_ERROR';

export const VERIFYOTP = 'exchangefiatUserPanel/phone/VERIFYOTP';
export const VERIFYOTP_SUCCESS = 'exchangefiatUserPanel/phone/VERIFYOTP_SUCCESS';
export const VERIFYOTP_ERROR = 'exchangefiatUserPanel/phone/VERIFYOTP_ERROR';

export const IMAGEUPLOAD = 'exchangefiatUserPanel/image/IMAGEUPLOAD';
export const IMAGEUPLOAD_SUCCESS = 'exchangefiatUserPanel/image/IMAGEUPLOAD_SUCCESS';
export const IMAGEUPLOAD_ERROR = 'exchangefiatUserPanel/image/IMAGEUPLOAD_ERROR';