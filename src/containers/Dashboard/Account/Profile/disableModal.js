import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { Route, Switch, withRouter, Link } from 'react-router-dom';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import Swal from 'sweetalert2'

import {
    makeSelectEnableAlterG2fa,
    makeSelectDisableG2fa,
    makeSelectDisableG2faError,
    makeSelectDisableG2faSuccess,
    makeSelectDisableG2faLoading
} from './selectors';
import { disable2fa, disable2faReset } from './actions';
import reducer from './reducer';
import saga from './saga';
import Modal from 'react-bootstrap/Modal';
class DisableModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            error: false,
            buttonClicked: false,
            swal_show: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
        };
    }
    handleChange = (e) => {
        this.setState({ swal_show: false });
        this.setState({ [e.target.name]: e.target.value, swal: false });
    };

    submit = (e) => {
        e.preventDefault();
        this.setState({ buttonClicked: true })
        const data = {
            code: this.state.code,
            google2fa: false,
            type: 'disable',
            id: this.props.user._id
        }
        this.props.dispatch(disable2fa(data));
    }

    componentWillMount() {
        this.props.dispatch(disable2faReset());
    }

    componentDidUpdate() {
        if (this.props.disable === false && this.props.success) {
            this.props.onHide(true);
        }
    }

    handleSubmit = (e) => {
        if (this.state.code.length >= 6) this.submit(e)
    };
    render() {
        return (
            <div>
                <React.Fragment>
                    <Modal
                        show={this.props.show}
                        onHide={this.props.onHide}
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <div className="modal-title">{window.i18n('Disable2')}</div>
                        </Modal.Header>
                        <Modal.Body>
                            <form onSubmit={this.submit}>
                                <div className="form-group">
                                    <label htmlFor="otpInput">Enter code</label>
                                    <input id="otpInput" onChange={this.handleChange} className="form-control" type="text" name="code" value={this.state.code} placeholder="Secret Code" onKeyUp={this.handleSubmit} required />
                                </div>

                                <button onSubmit={this.submit} className="bank-button btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" >Disable</button>
                                <span onClick={this.props.onHide} className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u span-btn" >Cancel</span>
                            </form>
                        </Modal.Body>
                    </Modal>
                </React.Fragment>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    disable: makeSelectDisableG2fa(),
    error: makeSelectDisableG2faError(),
    success: makeSelectDisableG2faSuccess(),
    loading: makeSelectDisableG2faLoading()
});

export function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const withReducer = injectReducer({ key: 'myprofile', reducer });
const withSaga = injectSaga({ key: 'myprofile', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(DisableModal);
