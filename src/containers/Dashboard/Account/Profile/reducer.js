import produce from 'immer';
import { USER, USER_SUCCESS, USER_ERROR, REDIRECT } from './../../../App/constants';
import {
    ALTER_PASSWORD,
    ALTER_PASSWORD_ERROR, ALTER_PASSWORD_SUCCESS, ALTER_RESET,
    ALTER_PROFILE,
    ALTER_PROFILE_SUCCESS,
    ALTER_PROFILE_ERROR,
    ALTER_PROFILE_RESET,


    CREATE_QR,
    CREATE_QR_SUCCESS,
    CREATE_QR_ERROR,
    ENABLE2FA,
    ENABLE2FA_SUCCESS,
    ENABLE2FA_ERROR,
    ENABLE2FA_RESET,
    DISABLE2FA,
    DISABLE2FA_RESET,
    DISABLE2FAEMAIL_RESET,
    DISABLE2FA_SUCCESS,
    DISABLE2FA_ERROR,
    DISABLE2FAEMAIL, DISABLE2FAEMAIL_SUCCESS, DISABLE2FAEMAIL_ERROR,
    WALLETS, WALLETS_ERROR, WALLETS_SUCCESS,
    EMAIL, EMAIL_ERROR, EMAIL_SUCCESS,
    EMAILVERIFY, EMAILVERIFY_ERROR, EMAILVERIFY_SUCCESS,
    ENABLE2FAEMAIL, ENABLE2FAEMAIL_SUCCESS, ENABLE2FAEMAIL_ERROR, ENABLE2FAEMAIL_RESET,
    SENDPHONECODE_SUCCESS, SENDPHONECODE_ERROR, SENDPHONECODE,
    VERIFYOTP,VERIFYOTP_SUCCESS,VERIFYOTP_ERROR,
    IMAGEUPLOAD, IMAGEUPLOAD_SUCCESS, IMAGEUPLOAD_ERROR,

} from './constants';

// The initial state of the App
export const initialState = {
    myaccount: [],
    alterMyAccount: false,
    success: false,
    error: false,
    loading: false,
    alterLoading: false,
    alterSuccess: false,
    alterError: false,

    myProfile: [],
    alterMyProfile: false,
    profile_success: false,
    profile_error: false,
    profile_loading: false,
    profile_alterLoading: false,
    profile_alterSuccess: false,
    profile_alterError: false,


    g2fa: false,
    success: false,
    error: false,
    enabletwo: false,
    enableSuccess: false,
    enableError: false,
    enableLoading: false,
    enableReset: false,

    disabletwo: false,
    disableError: false,
    disableSuccess: false,
    disableLoading: false,

    disableemailtwo: false,
    disableemailError: false,
    disableemailSuccess: false,
    disableemailLoading: false,

    user: false,
    usersuccess: false,
    usererror: false,
    userloading: false,
    userredirect: false,
    usertwofaSuccess: false,

    wallets: false,
    success: false,
    error: false,
    loading: false,

    email: false,
    emailsuccess: false,
    emailerror: false,
    emailloading: false,

    emailverify: false,
    emailverifysuccess: false,
    emailverifyerror: false,
    emailverifyloading: false,

    enableEmail2fa: false,
    enableEmail2faError: false,
    enableEmail2faSuccess: false,
    enableEmail2faLoading: false,

    sendphonecode:false,
    sendphonecodeError:false,
    sendphonecodeSuccess:false,
    sendphonecodeLoading:false,

    verifyotp:false,
    verifyotpError:false,
    verifyotpSuccess:false,
    verifyotpLoading:false,

    imageupload:false,
    imageuploadError:false,
    imageuploadSuccess:false,
    imageuploadLoading:false,

};
/* eslint-disable default-case, no-param-reassign */
const myAccountReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case ENABLE2FAEMAIL:
                draft.enableEmail2fa = false;
                draft.enableEmail2faError = false;
                draft.enableEmail2faSuccess = false;
                draft.enableEmail2faLoading = true;
            break;

            case ENABLE2FAEMAIL_SUCCESS:
                draft.enableEmail2fa = action.data.email_2fa;
                draft.user = action.data;
                draft.enableEmail2faError = false;
                draft.enableEmail2faSuccess = true;
                draft.enableEmail2faLoading = false;
            break;

            case ENABLE2FAEMAIL_ERROR:
                draft.enableEmail2fa = false;
                draft.enableEmail2faError = true;
                draft.enableEmail2faSuccess = false;
                draft.enableEmail2faLoading = false;
            break;

            case ENABLE2FAEMAIL_RESET:
                draft.enableEmail2faError = false;
                draft.enableEmail2faSuccess = false;
                draft.enableEmail2faLoading = false;
            break;

            case ALTER_PASSWORD:
                draft.alterLoading = true;
                draft.alterError = false;
                draft.alterSuccess = false;
                draft.alterMyAccount = false;
                break;

            case ALTER_PASSWORD_SUCCESS:
                const myaccount = state.myaccount;
                if (action.data.ctype === 'put') {
                    const c = state.myaccount.filter(d => d._id !== action.data._id);
                    draft.myaccount = c.concat([action.data]);
                }
                draft.alterMyAccount = action.data ? action.data : [];
                draft.alterError = false;
                draft.alterSuccess = true;
                draft.alterLoading = false;
                break;

            case ALTER_PASSWORD_ERROR:
                draft.alterError = action.error.toString();
                draft.alterSuccess = false;
                draft.alterLoading = false;
                break;
            case ALTER_RESET:
                draft.alterError = false;
                draft.alterSuccess = false;
                draft.alterLoading = false;
                break;


            case ALTER_PROFILE:
                draft.profile_alterLoading = true;
                draft.profile_alterError = false;
                draft.profile_alterSuccess = false;
                draft.alterMyProfile = false;
                break;

            case ALTER_PROFILE_SUCCESS:
                const myProfile = state.myProfile;
                if (action.data.ctype === 'put') {
                    const c = state.myProfile.filter(d => d._id !== action.data._id);
                    draft.myProfile = c.concat([action.data]);
                }
                draft.alterMyProfile = action.data ? action.data : [];
                draft.profile_alterError = false;
                draft.profile_alterSuccess = true;
                draft.profile_alterLoading = false;
                break;

            case ALTER_PROFILE_ERROR:
                draft.profile_alterError = action.error.toString();
                draft.profile_alterSuccess = false;
                draft.profile_alterLoading = false;
                break;
            case ALTER_PROFILE_RESET:
                draft.profile_alterError = false;
                draft.profile_alterSuccess = false;
                draft.profile_alterLoading = false;
                break;


            case CREATE_QR:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.g2fa = false;
                break;

            case CREATE_QR_SUCCESS:
                draft.g2fa = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case CREATE_QR_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;

            case ENABLE2FA:
                draft.enableError = false;
                draft.enableSuccess = false;
                draft.enabletwo = false;
                draft.enableLoading = true;
                break;

            case ENABLE2FA_SUCCESS:
                draft.enabletwo = action.data.google2fa;
                draft.user = action.data;
                draft.enableError = false;
                draft.enableSuccess = true;
                draft.enableLoading = false;
                break;

            case ENABLE2FA_ERROR:
                draft.enableError = action.error.toString();
                draft.enableSuccess = false;
                draft.enabletwo = false;
                draft.enableLoading = true;
                break;

            case ENABLE2FA_RESET:
                draft.enableError = false;
                draft.enableSuccess = false;
                draft.enabletwo = false;
                draft.enableLoading = false;
                break;

            case DISABLE2FA:
                draft.disableError = false;
                draft.disableSuccess = false;
                draft.disabletwo = false;
                draft.disableLoading = true;
                break;

            case DISABLE2FA_RESET:
                draft.disableError = false;
                draft.disableSuccess = false;
                draft.disabletwo = false;
                draft.disableLoading = false;
                break;

            case DISABLE2FAEMAIL_RESET:
                draft.email2fadisableerror = false;
                draft.email2fadisableloading = false;
                draft.email2fadisablesuccess = false;
                break;

            case DISABLE2FA_SUCCESS:
                draft.disabletwo = action.data.google2fa;
                draft.user = action.data;
                draft.disableError = false;
                draft.disableSuccess = true;
                draft.disableLoading = false;
                break;

            case DISABLE2FA_ERROR:
                draft.disableError = action.error.toString();
                draft.disabletwo = false;
                draft.disableSuccess = false;
                draft.disableLoading = false;
                break;

            case DISABLE2FAEMAIL:
                draft.disableemailtwo = false;
                draft.disableemailError = false;
                draft.disableemailSuccess = false;
                draft.disableemailLoading = true;
                break;

            case DISABLE2FAEMAIL_SUCCESS:
                draft.disableemailtwo = action.data.email_2fa;
                draft.user = action.data;
                draft.disableemailError = false;
                draft.disableemailSuccess = true;
                draft.disableemailLoading = false;
                break;

            case DISABLE2FAEMAIL_ERROR:
                draft.disableemailError = action.error.toString();
                draft.disableemailtwo = false;
                draft.disableemailSuccess = false;
                draft.disableemailLoading = false;
                break;

            case USER:
                draft.userloading = true;
                draft.usererror = false;
                draft.usersuccess = false;
                draft.user = false;
                break;

            case USER_SUCCESS:
                draft.user = action.data;
                draft.usererror = false;
                draft.usersuccess = true;
                draft.userloading = false;
                break;

            case USER_ERROR:
                draft.usererror = action.error.toString();
                draft.usersuccess = false;
                draft.userloading = false;
            case REDIRECT:
                draft.usererror = false;
                draft.usersuccess = false;
                draft.userloading = false;
                draft.userredirect = action.data;
                break;

            case WALLETS:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.wallets = false;
                break;

            case WALLETS_SUCCESS:
                draft.wallets = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case WALLETS_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;

            case EMAIL:
                draft.emailloading = true;
                draft.emailerror = false;
                draft.emailsuccess = false;
                draft.email = false;
                break;

            case EMAIL_SUCCESS:
                draft.email = action.data;
                draft.emailerror = false;
                draft.emailsuccess = true;
                draft.emailloading = false;
                break;

            case EMAIL_ERROR:
                draft.emailerror = action.error.toString();
                draft.emailsuccess = false;
                draft.emailloading = false;
                break;

                case EMAILVERIFY:
                    draft.emailverifyloading = true;
                    draft.emailverifyerror = false;
                    draft.emailverifysuccess = false;
                    draft.emailverify = false;
                    break;
    
                case EMAILVERIFY_SUCCESS:
                    draft.emailverify = action.data;
                    draft.emailverifyerror = false;
                    draft.emailverifysuccess = true;
                    draft.emailverifyverifyloading = false;
                    break;
    
                case EMAILVERIFY_ERROR:
                    draft.emailverifyerror = action.error.toString();
                    draft.emailverifysuccess = false;
                    draft.emailverifyloading = false;
                    break;

            case SENDPHONECODE:
                draft.sendphonecodeLoading = true;
                draft.sendphonecodeError = false;
                draft.sendphonecodeSuccess = false;
                draft.sendphonecode = false;
                break;

            case SENDPHONECODE_SUCCESS:
                draft.sendphonecode = action.data;
                draft.sendphonecodeError = false;
                draft.sendphonecodeSuccess = true;
                draft.sendphonecodeLoading = false;
                break;

            case SENDPHONECODE_ERROR:
                draft.sendphonecodeError = action.error.toString();
                draft.sendphonecodeSuccess = false;
                draft.sendphonecodeLoading = false;
                break;

            case VERIFYOTP:
                draft.verifyotpLoading = true;
                draft.verifyotpError = false;
                draft.verifyotpSuccess = false;
                draft.verifyotp = false;
                break;

            case VERIFYOTP_SUCCESS:
                draft.verifyotp = action.data;
                draft.verifyotpError = false;
                draft.verifyotpSuccess = true;
                draft.verifyotpLoading = false;
                break;

            case VERIFYOTP_ERROR:
                draft.verifyotpError = action.error.toString();
                draft.verifyotpSuccess = false;
                draft.verifyotpLoading = false;
                break;

            case IMAGEUPLOAD:
                draft.imageuploadLoading = true;
                draft.imageuploadError = false;
                draft.imageuploadSuccess = false;
                draft.imageupload = false;
                break;

            case IMAGEUPLOAD_SUCCESS:
                draft.imageupload = action.data;
                draft.imageuploadError = false;
                draft.imageuploadSuccess = true;
                draft.imageuploadLoading = false;
                break;

            case IMAGEUPLOAD_ERROR:
                draft.imageuploadError = action.error.toString();
                draft.imageuploadSuccess = false;
                draft.imageuploadLoading = false;
                break;
        }
    });

export default myAccountReducer;
