import React, { Component } from 'react';
import DisableModal from './disableModal';
// import './style.css';

class DisableTwofa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disable2fa: false,
            visible: false,
        };
    }


    render() {


        return (
            <div>

                {
                    this.state.disable2fa &&
                    <DisableModal
                        user={this.props.user}
                        show={this.state.disable2fa}
                        onHide={() => this.setState({ disable2fa: false, visible: false })}

                    />

                }

                <div className="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div className="kt-container ">
                        <div className="kt-subheader__main">

                            <div className=''>


                                <div className='column-2'>
                                    <div className='top-div-in-column-2'>
                                        <h2 className='auth-h2'>{window.i18n('ProfileDisable')}</h2>
                                        {/* <button style={{ marginLeft: '5px' }} className='gray' onClick={() => this.setState({ disable2fa: true })}>{window.i18n('ProfileDisable1')}</button> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div>
        );
    }
}

export default DisableTwofa;

