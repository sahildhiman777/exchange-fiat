import React, { Component } from 'react';
import Update from './update';
import Enable from './enable2fa';
import { connect } from 'react-redux';
import { compose } from 'redux';
import googlePlayimg from '../../../../../src/google-play.png';
import appstorelogo from '../../../../../src/Available-on-app-store.png';
import { createQR } from './actions';
import Config from '../../../../utils/config'
import ReactLoading from "react-loading";
import { withRouter } from "react-router-dom";

import {
    makeSelectAlterMyProfile,
    makeSelectAlterMyProfileError,
    makeSelectAlterMyProfileLoading,
    makeSelectAlterMyProfileSuccess,

    makeSelectG2fa,
    makeSelectG2faError,
    makeSelectG2faSuccess,
    makeSelectEnableG2faSuccess,
    makeSelectDisableG2faSuccess,

    makeSelectDisableG2faEmail,
    makeSelectDisableG2faEmailError,
    makeSelectDisableG2faEmailSuccess,
    makeSelectDisableG2faEmailLoading,

    makeSelectUser,
    makeSelectLoading,
    makeSelectError,
    makeSelectSuccess,
    makeSelectRedirect,

    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,

    makeSelectMyEmail,
    makeSelectMyEmailError,
    makeSelectMyEmailLoading,
    makeSelectMyEmailSuccess,

    makeSelectMyEmailVerify,
    makeSelectMyEmailVerifyError,
    makeSelectMyEmailVerifyLoading,
    makeSelectMyEmailVerifySuccess,

    makeSelectEmail2FaEnable,
    makeSelectEmail2FaEnableSuccess,
    makeSelectEmail2FaEnableError,
    makeSelectEmail2FaEnableLoading
} from './selectors';

import { alterProfile, loadUser, wallets, disable2faemail, enable2FaEmail, enable2FaEmailReset, emailverify } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import Swal from 'sweetalert2';
import Disable from './disable';
import jwt from 'jwt-decode'
import DisableModal from './disableModal';
const accessToken = localStorage.getItem("exchangefiatUserPanel");
class index extends Component {
    constructor(props) {

        super(props)
        this.state = {
            profile: false,
            checked: '',
            enable2fa: false,
            visible: false,
            id: '',
            firstName: '',
            lastName: '',
            email: '',
            input: 'this is the input for now',
            disable2fa: false,
            otp_Verfiy: '',
            mobileVerifyModel: false,
            emailchecked: '',
            blockloader: true,
            emailstatus: false


        }
        this.handleInput = this.handleInput.bind(this);
    }
    handleInput(x) {
    }
    // generate QR code
    componentDidMount() {
        const accessToken = localStorage.getItem("exchangefiatUserPanel");
        let decoded = jwt(accessToken);
        const data = {
            id: decoded.id
        }
        if (data.id) {
            this.props.dispatch(createQR(data));
        }
        if (this.props.user.emailstatus) {
            this.setState({ emailstatus: true })
        }
    }
    componentWillMount() {
        this.props.dispatch(wallets());
        this.props.dispatch(loadUser());
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user) {
            if (nextProps.user.google2fa === true) {
                this.setState({ checked: true, disableVisible: true })
            } else {
                this.setState({ checked: false, disableVisible: false })
            }

            if (nextProps.user.email_2fa) {
                this.setState({ emailchecked: true })
            } else {
                this.setState({ emailchecked: false })
            }
            if (nextProps.user.emailstatus) {
                this.setState({ emailstatus: true })
            }

            let userObj = nextProps.user;
            if (nextProps.profile) {
                userObj = nextProps.profile;
                this.setState({ blockloader: false })
            }
            if (nextProps.email2fadisablesuccess) {
                this.setState({ blockloader: false })
            }
            if (nextProps.enable2FaEmailSuccess) {
                this.setState({ blockloader: false })
            }

            this.setState({
                id: userObj._id,
                firstName: userObj.firstname || '',
                lastName: userObj.lastname || '',
                email: userObj.email || '',
            })
        }

        if (nextProps.wallets && nextProps.wallets.length > 0) {
            this.setState({ blockloader: false })
        }
    }

    handleCheck2FA(e) {
        if (e.target.checked == true) {
            this.setState({
                enable2fa: true,
                checked: true,
                disableVisible: false,
                disable2fa: false
            })
        } else {
            this.setState({
                enable2fa: false,
                checked: false,
                disableVisible: true,
                disable2fa: true
            })
        }
    }
    handleInput(event) {
        if (this.state.checked) {
            this.setState({ checked: true });
        } else {
            this.setState({ checked: false });
        }
    }

    handleChange = (e) => {
        if (e.target.name == 'firstName') {
            this.setState({ firstName: e.target.value })
        }
        if (e.target.name == 'lastName') {
            this.setState({ lastName: e.target.value })
        }
        if (e.target.name == 'email') {
            this.setState({ email: e.target.value })
        }
    }

    submit = (e) => {
        e.preventDefault();
        const data = {
            id: this.state.id,
            firstname: this.state.firstName,
            lastname: this.state.lastName,
            email: this.state.email,
            type: 'put'
        }
        this.setState({ swal: true, blockloader: true });
        this.props.dispatch(alterProfile(data));
        // return false;
    }
    on2faHide = (status) => {
        if (status === true) {
            this.setState({
                checked: true,
                enable2fa: false,
                visible: true,
                disableVisible: true
            })
        } else {
            this.setState({
                checked: false,
                enable2fa: false
            })
        }
    }
    clickState(e, data) {
        if (e.target.name == 'buy') {
            this.props.history.push(`/buycoin/${data.symbol}`)
        }
        if (e.target.name == 'sell') {
            this.props.history.push(`/sellcoin/${data.symbol}`)
        }

    }
    handleEmailVerify(e) {
        e.preventDefault();
        const data = {
            id: this.state.id,
            email: this.state.email,
        }
        this.props.dispatch(emailverify(data));

    }
    handleemail(e) {
        e.preventDefault();
        this.props.dispatch(enable2FaEmailReset());
        if (e.target.checked == true) {
            const data = {
                id: this.state.id,
                name: this.state.firstName,
                email: this.state.email,
            }
            this.props.dispatch(enable2FaEmail(data));
        } else {
            let data = {
                id: this.state.id,
                email: this.state.email,
            }
            this.setState({ emailchecked: false, blockloader: false })
            this.props.dispatch(disable2faemail(data));
        }
    }
    render() {
        let emailstatue = this.state.emailstatus;
        let asset = this.props.wallets;
        const { QR2fa, user } = this.props;

        return (
            <div className="container-fluid">
                {(this.state.blockloader) &&
                    <div className="blockloader">
                        <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                            <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                        </div>
                    </div>
                }
                {/* NEW DESIGN START */}
                <div className="profile-section">
                    <div className="top-profile"></div>
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-9">
                            <div className="tabs-profile">
                                <div className="row">
                                    <div className="col-sm-12 col-md-12 ">

                                        <nav className="tab-nav">
                                            <div className="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                <a className="nav-item nav-link tab-button active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"> {window.i18n('ProfileProfile')}</a>
                                                <a className="nav-item nav-link " id="nav-security-tab" data-toggle="tab" href="#nav-security" role="tab" aria-controls="nav-security" aria-selected="true"> {window.i18n('ProfileSecurity')}</a>
                                            </div>
                                        </nav>

                                        <div className="tab-content" id="nav-tabContent">
                                            <div className="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                                <div className="row">
                                                    <div className="col-md-3">
                                                        <div className="user-image">
                                                            <img src="/images/user.png" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-9">
                                                        <div className="profile-form">
                                                            <div className="row">
                                                                {
                                                                    this.state.profile &&
                                                                    <Update
                                                                        user={this.props.user}
                                                                        show={this.state.profile}
                                                                        onHide={() => this.setState({ profile: false })}
                                                                    />
                                                                }
                                                                <form className="user col-sm-12" onSubmit={this.submit} action="">
                                                                    <div className="form-group row">
                                                                        <div className="col-sm-6 mb-3 mb-sm-0">
                                                                            <input onChange={this.handleChange} type="text" className="form-control form-control-user" id="exampleFirstName" name="firstName" value={this.state.firstName} placeholder={`${window.i18n('ProfileFirstName')}`} required />
                                                                        </div>
                                                                        <div className="col-sm-6">
                                                                            <input onChange={this.handleChange} type="text" className="form-control form-control-user" id="exampleLastName" name="lastName" value={this.state.lastName} placeholder={`${window.i18n('RegisterLastName')}`} required />
                                                                        </div>
                                                                    </div>
                                                                    <div className="form-group row">
                                                                        <div className="col-sm-6 mb-3 mb-sm-0">
                                                                            <input disabled onChange={this.handleChange} type="email" className="form-control form-control-user" id="exampleInputEmail" name="email" value={this.state.email} placeholder={`${window.i18n('RegisterEmailAddress')}`} required />
                                                                        </div>
                                                                        <div className="col-sm-6 mb-3 mb-sm-0">
                                                                            {emailstatue ?
                                                                                <div className="verifiedmail"><i className="fa fa-check-circle" aria-hidden="true"></i> <span>{window.i18n('Verified')}</span></div>
                                                                                :
                                                                                <div className="verifybutton"><button className="mt-1 btn btn-primary btn-sm verify-email-button updatebtn" onClick={(e) => this.handleEmailVerify(e)}>{window.i18n('VerifyEmail')}</button></div>
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                    <div className="form-button">
                                                                        <button className="btn btn-primary btn-user btn-block updatebtn" type='submit' >{window.i18n('ProfileUpdate')}</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div className="tab-pane fade " id="nav-security" role="tabpanel" aria-labelledby="nav-security-tab">
                                                <div className="securty-section">
                                                    <div className="row">
                                                        <div className="col-md-12">
                                                            <table className="action-table">
                                                                <tbody>
                                                                    <tr>
                                                                        <td className="content">
                                                                            <p className="change_lable">{window.i18n('ProfileChangePassword')}</p>
                                                                        </td>
                                                                        <td className="table-button">
                                                                            <button type="button" className="change-button" onClick={() => this.setState({ editType: 'edit', profile: true })}>{window.i18n('ProfileChangePassword')}</button>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="content">{window.i18n('ProfileAuth')}</td>
                                                                        <td className="table-button">
                                                                            <label className="switch">
                                                                                <input type="checkbox" className="checkbox-input" onChange={(e) => this.handleCheck2FA(e)} checked={this.state.checked} />

                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td className="content">{window.i18n('ProfileEmail2FA')}</td>
                                                                        <td className="table-button">
                                                                            <label className="switch">
                                                                                <input type="checkbox" className="checkbox-input" name="emailverification" onChange={(e) => this.handleemail(e)} checked={this.state.emailchecked} />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                        </td>
                                                                    </tr>
                                                                    {/* <tr>
                                                                        <td className="content">{window.i18n('ProfilePhoneVerification')}</td>
                                                                        <td className="table-button">
                                                                            <label className="switch">
                                                                                <input type="checkbox" className="checkbox-input" />
                                                                                <span className="slider round"></span>
                                                                            </label>
                                                                        </td>
                                                                    </tr> */}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="clearfix"></div>
                                                {/* <div>
                                                    {this.state.disableVisible ?

                                                        <Disable
                                                            user={user}
                                                        />
                                                        :
                                                        <>
                                                            <div className="kt-subheader kt-grid__item" id="kt_subheader">
                                                                <div className="kt-container ">
                                                                    <div className="kt-subheader__main">
                                                                        <div className=''>
                                                                            <div className=' column-2'>
                                                                                <div className='top-div-in-column-2'>
                                                                                    <h2 className='auth-h2'>{window.i18n('ProfileEnablefactor')}</h2>
                                                                                </div>
                                                                                <div className='middle-div-in-column-2'>
                                                                                    <div style={{ marginBottom: '15px' }}>
                                                                                        <div>
                                                                                            <img src={QR2fa.image_data} />
                                                                                        </div>
                                                                                        <div className='playstorelogo'>
                                                                                            <a href='https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_IN' target="_blank"><img style={{ paddingRight: '8px' }} src={googlePlayimg} /></a>
                                                                                            <img src={appstorelogo} />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div style={{ marginLeft: '15px' }}>
                                                                                        <p>{window.i18n('ProfileFirst')} <br />{window.i18n('ProfileDownloadGoogle1')}</p>
                                                                                        <p>{window.i18n('ProfileScanBarcode')}<br />{window.i18n('ProfileScanBarcode1')}<br />{window.i18n('ProfileScanBarcode2')}</p>
                                                                                        <h5>{window.i18n('ProfileSecurityKey')}: {QR2fa.data}</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div className='bottom-div-in-column-2'>
                                                                                    <div>
                                                                                        <h1 style={{ fontSize: '20px' }}>{window.i18n('ProfileGoogleGuide')}</h1>
                                                                                    </div>
                                                                                    <div >
                                                                                        {window.i18n('ProfileGoogleAndroidOrApple')}<br />
                                                                                        {window.i18n('ProfileGoTo')} <span className='span-color-red'>{window.i18n('ProfileMenu')}</span> -> <span className='span-color-red'>{window.i18n('ProfileSetupAccount')}</span><br />
                                                                                        3.{window.i18n('ProfileChoose')}  <span className='span-color-red'>{window.i18n('ProfileScan')}</span> {window.i18n('Profileoption')}<br />
                                                                                        <span style={{ fontStyle: 'italic' }}> 4. <span style={{ color: ' #2A2728' }}> {window.i18n('settingUnableToScan')} </span>{window.i18n('ProfileChoose')} <span className='span-color-red'>{window.i18n('ProfileEnter')}</span>{window.i18n('ProfileType')}  <span>{window.i18n('ProfileSecurityKey')}</span><br /></span>
                                                                                        {window.i18n('settingSixDigit')}<br />
                                                                                        {window.i18n('settingEveryTime')}<br />
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </>
                                                    }
                                                </div> */}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                            <div className="currency-section">
                                <h5>{window.i18n('ProfileAvailableCoins')}</h5>
                                <div className="currency-table">
                                    <table>
                                        <tbody>
                                            {(() => {
                                                const options = [];
                                                if (asset) {
                                                    for (let i = 0; i < asset.length; i++) {
                                                        options.push(
                                                            <tr key={`currency${i}`}>
                                                                <td>
                                                                    <img src={require('../../../../images/' + asset[i].icon + '.png')} /> &nbsp;
                                                                <span style={{ cursor: "pointer" }} className="coin-type" >{asset[i].symbol}</span>
                                                                </td>

                                                                <td>
                                                                    {
                                                                        asset[i].sell_status ?
                                                                            <button
                                                                                type="submit"
                                                                                className="sell_button buy-sell-button-profile"
                                                                                name="sell"
                                                                                onClick={(e) => this.clickState(e, asset[i])}
                                                                            >
                                                                                {window.i18n('SellNow')}
                                                                            </button>
                                                                            :
                                                                            ""
                                                                    }
                                                                </td>
                                                                <td>
                                                                    {
                                                                        asset[i].buy_status ?
                                                                            <button
                                                                                type="submit"
                                                                                className="buy_button buy-sell-button-profile"
                                                                                name="buy"
                                                                                onClick={(e) => this.clickState(e, asset[i])}
                                                                            >
                                                                                {window.i18n('BuyNow')}
                                                                            </button>
                                                                            :
                                                                            ""
                                                                    }
                                                                </td>
                                                            </tr>
                                                        );
                                                    }
                                                    return options;
                                                }
                                            })()}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* NEW DESIGN END */}

                {
                    this.state.disable2fa &&
                    <DisableModal
                        user={this.props.user}
                        show={this.state.disable2fa}
                        onHide={(status) => {
                            if (status === true) {
                                this.setState({
                                    disable2fa: false,
                                    visible: false,
                                    checked: false,
                                    disableVisible: false
                                })
                            } else {
                                this.setState({
                                    disable2fa: false,
                                    checked: true
                                })
                            }
                        }}
                    />
                }

                {this.state.enable2fa && <Enable
                    user={user}
                    Skey={QR2fa.data}
                    show={this.state.enable2fa}
                    getInput={this.handleInput}
                    onHide={this.on2faHide}
                />}


            </div>

            //   <a className="scroll-to-top rounded" href="#page-top">
            //     <i className="fas fa-angle-up"></i>
            //   </a>

        )
    }
}

// export default index;


const mapStateToProps = createStructuredSelector({
    profile: makeSelectAlterMyProfile(),
    loading: makeSelectAlterMyProfileLoading(),
    error: makeSelectAlterMyProfileError(),
    success: makeSelectAlterMyProfileSuccess(),


    user: makeSelectUser(),
    QR2fa: makeSelectG2fa(),
    error: makeSelectG2faError(),
    success: makeSelectG2faSuccess(),
    usererror: makeSelectError(),
    userloading: makeSelectLoading(),
    usersuccess: makeSelectSuccess(),
    enablesuccess: makeSelectEnableG2faSuccess(),
    disablesuccess: makeSelectDisableG2faSuccess(),

    wallets: makeSelectMyWallets(),
    loading: makeSelectMyWalletsLoading(),
    error: makeSelectMyWalletsError(),
    success: makeSelectMyWalletsSuccess(),

    email: makeSelectMyEmail(),
    emailloading: makeSelectMyEmailLoading(),
    emailerror: makeSelectMyEmailError(),
    emailsuccess: makeSelectMyEmailSuccess(),

    email2fadisable: makeSelectDisableG2faEmail(),
    email2fadisableerror: makeSelectDisableG2faEmailError(),
    email2fadisablesuccess: makeSelectDisableG2faEmailSuccess(),
    email2fadisableloading: makeSelectDisableG2faEmailLoading(),

    enable2FaEmail: makeSelectEmail2FaEnable(),
    enable2FaEmailError: makeSelectEmail2FaEnableError(),
    enable2FaEmailSuccess: makeSelectEmail2FaEnableSuccess(),
    enable2FaEmailLoading: makeSelectEmail2FaEnableLoading(),

    emailverify: makeSelectMyEmailVerify(),
    emailverifyerror: makeSelectMyEmailVerifyError(),
    emailverifyloading: makeSelectMyEmailVerifyLoading(),
    emailverifysuccess: makeSelectMyEmailVerifySuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'myprofile', reducer });
const withSaga = injectSaga({ key: 'myprofile', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(index);