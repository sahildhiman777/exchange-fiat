import React, { Component } from 'react';
import ReactLoading from "react-loading";
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { compose } from 'redux';

import {
  makeSelectMyVerifyEmail,
  makeSelectMyVerifyEmailError,
  makeSelectMyVerifyEmailLoading,
  makeSelectMyVerifyEmailSuccess,
} from '../selectors';

import { verifyemail } from '../actions';
import reducer from '../reducer';
import saga from '../saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../../utils/injectReducer';
import injectSaga from '../../../../../utils/injectSaga';
import Swal from 'sweetalert2';
class index extends Component {
  // class verify extends Component {
  constructor(props) {

    super(props)
    this.state = {
        blockloader:true,
    }
}
componentWillMount(){
  let data = {
    id: this.props.match.params.id,
    user_code:this.props.match.params.user_code,
  }
  this.props.dispatch(verifyemail(data));

}
componentDidUpdate() {
  if(this.props.verifyemail){
    this.props.history.push('/profile')
  }
}
render() {
  return (
     <div className="verifyemail">
       {/* <h1>Verify Email</h1> */}
     </div>
  )
}
}


const mapStateToProps = createStructuredSelector({

  verifyemail:  makeSelectMyVerifyEmail(),
  verifyemailloading:  makeSelectMyVerifyEmailLoading(),
  verifyemailerror: makeSelectMyVerifyEmailError(),
  verifyemailsuccess: makeSelectMyVerifyEmailSuccess(),


});
export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'myprofile', reducer });
const withSaga = injectSaga({ key: 'myprofile', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);
// export default (index);