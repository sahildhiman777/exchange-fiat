// change user password
import { call, put, takeLatest } from 'redux-saga/effects';
import { ALTER_PASSWORD, ALTER_PROFILE, CREATE_QR, ENABLE2FA, DISABLE2FA, DISABLE2FAEMAIL,USER, WALLETS, EMAIL, EMAILVERIFY, ENABLE2FAEMAIL, SENDPHONECODE, VERIFYOTP, IMAGEUPLOAD } from './constants';
import { alterPasswordSuccess, alterPasswordFailed, alterProfileSuccess, alterProfileFailed, createQRSuccess, createQRFailed, enable2faFailed, enable2faSuccess, disable2faSuccess, disable2faFailed, userFailed, userSuccess, redirect, walletsSuccess, walletsFailed, emailSuccess, emailFailed, disable2faemailSuccess, disable2faemailFailed, enable2FaEmailSuccess, enable2FaEmailFailed, sendphonecodeFailed, sendphonecodeSuccess, verifyotpFailed, verifyotpSuccess, imageuploadFailed, imageuploadSuccess } from './actions';
import { requestSecure } from '../../../../utils/request';
import request from '../../../../utils/request';
import requestFile from '../../../../utils/request';
import Define from '../../../../utils/config';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../../toast/toast';

export function* alterPassword(action) {
  const type = action.userAccount.type;
  let requestURL = '';
  let options = {};
  if (type === 'put') {
    requestURL = `${Define.API_URL}changepassword`;
    options = {
      method: 'PUT',
      body: JSON.stringify(action.userAccount)
    }
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(alterPasswordSuccess(res));
    } else {
      yield put(alterPasswordFailed(res.data));
    }
  } catch (err) {
    yield put(alterPasswordFailed(err));
  }
}

export function* alterprofile(action) {
  const type = action.userProfile.type;
  let requestURL = '';
  let options = {};
  if (type === 'put') {
    requestURL = `${Define.API_URL}profile`;
    options = {
      method: 'PUT',
      body: JSON.stringify(action.userProfile)
    }
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(alterProfileSuccess(res.data));
      ToastTopEndSuccessFire(res.message)
    } else {
      yield put(alterProfileFailed(res.message));
    }
  } catch (err) {
    yield put(alterProfileFailed(err));
  }
}

export function* createQR(action) {
  const requestURL = `${Define.API_URL}2fa/${action.data.id}`;
  const options = {
    method: 'POST',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(createQRSuccess(res));
    } else {

      yield put(createQRFailed(res));


    }
  } catch (err) {
    yield put(createQRFailed(err));
  }
}

// Enable 
export function* enable(action) {
  const requestURL = `${Define.API_URL}2fa/enable/${action.data.id}`;
  const options = {
    method: 'PUT',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(enable2faSuccess(res.data));
      ToastTopEndSuccessFire("Google 2FA has enabled successfully.")
    } else {
      yield put(enable2faFailed(res.data));
      ToastTopEndErrorFire("Invalid code");
    }

  } catch (err) {
    yield put(enable2faFailed(err));
    ToastTopEndErrorFire("Something went wrong.");
  }
}

// Disable 
export function* disable(action) {
  const requestURL = `${Define.API_URL}2fa/disable/${action.data.id}`;
  const options = {
    method: 'PUT',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(disable2faSuccess(res.data));
      ToastTopEndSuccessFire("Google 2FA has disabled successfully.")
    } else {
      yield put(disable2faFailed(res.data));
      ToastTopEndErrorFire("Error");
    }

  } catch (err) {
    yield put(disable2faFailed(err));
    ToastTopEndErrorFire("Error");
  }

}

// Enable email 2fa
export function* enableEmail2FA(action) {
  const requestURL = `${Define.API_URL}enable2faemail`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(enable2FaEmailSuccess(res.data));
      ToastTopEndSuccessFire("Email 2FA has enabled successfully.")
    } else {
      yield put(enable2FaEmailFailed(res));
      ToastTopEndErrorFire("Error");
    }

  } catch (err) {
    yield put(enable2FaEmailFailed(err));
    ToastTopEndErrorFire("Error");
  }

}

// Disable email 2fa
export function* disableEmail(action) {
  const requestURL = `${Define.API_URL}disable2faemail`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(disable2faemailSuccess(res.data));
      ToastTopEndSuccessFire("Email 2FA has disabled successfully.")
    } else {
      yield put(disable2faemailFailed(res));
      ToastTopEndErrorFire("Error");
    }

  } catch (err) {
    yield put(disable2faemailFailed(err));
    ToastTopEndErrorFire("Error");
  }

}

export function* getUser() {
  const requestURL = `${Define.API_URL}current`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options)
    if (res.status) {
      yield put(userSuccess(res.data.user));
    } else {
      if (res.redirect) {
        yield put(redirect(res.redirect));
      } else {
        yield put(userFailed(res));
      }
    }
  } catch (err) {
    yield put(userFailed(err));
  }
}

export function* myWallets(action) {
  let currency = localStorage.getItem('currency') || "VND";
  const requestURL = `${Define.API_URL}currency/get_wallets?currency=${currency.toLowerCase()}`;
  const options = {
    method: 'GET',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(walletsSuccess(res.message));


  } catch (err) {
    yield put(walletsFailed(err));
  }
}

export function* myEmail(action) {
  const requestURL = `${Define.API_URL}send2fa_email`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(emailSuccess(res.message));
    ToastTopEndSuccessFire(res.message)
  } catch (err) {
    yield put(emailFailed(err));
    ToastTopEndErrorFire("Could not send verification email");
  }
}

export function* myEmailVerify(action) {
  const requestURL = `${Define.API_URL}emailverifysendcode`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(emailSuccess(res.message));
    ToastTopEndSuccessFire(res.message)
  } catch (err) {
    yield put(emailFailed(err));
    ToastTopEndErrorFire("Could not send verification email");
  }
}

export function* sendPhoneCode(action) {
  console.log(action,'action');
  
  const requestURL = `${Define.API_URL}sendotp`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(sendphonecodeSuccess(res.message));
    ToastTopEndSuccessFire(res.message)
  } catch (err) {
    yield put(sendphonecodeFailed(err));
    ToastTopEndErrorFire("Could not send verification code");
  }
}

export function* verifyPhoneotp(action) {
  const requestURL = `${Define.API_URL}verifiyotp`;
  const options = {
    method: 'POST',
    body: JSON.stringify(action.data),
  }
  try {
    const res = yield call(request, requestURL, options);
    yield put(verifyotpSuccess(res.message));
    ToastTopEndSuccessFire(res.message)
  } catch (err) {
    yield put(verifyotpFailed(err));
    ToastTopEndErrorFire("Could not send verification code");
  }
}

export function* imageupload(action) {
  const requestURL = `${Define.API_URL}kycimage`;
 
 
  const options = {
  method: 'POST',
  body: action.data,
 headers : {
 'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
 }
  }
  try {
  const res = yield call(requestFile, requestURL, options);
  yield put(imageuploadSuccess(res.message));
  ToastTopEndSuccessFire(res.message)
  } catch (err) {
  yield put(imageuploadFailed(err));
  ToastTopEndErrorFire("Could not save image");
  }
 }


/**
* Root saga manages watcher lifecycle
*/
export default function* appData() {
  yield takeLatest(ALTER_PASSWORD, alterPassword);
  yield takeLatest(ALTER_PROFILE, alterprofile);
  yield takeLatest(USER, getUser);
  yield takeLatest(CREATE_QR, createQR);
  yield takeLatest(ENABLE2FA, enable);
  yield takeLatest(DISABLE2FA, disable);
  yield takeLatest(DISABLE2FAEMAIL, disableEmail);
  yield takeLatest(WALLETS, myWallets);
  yield takeLatest(EMAILVERIFY, myEmailVerify);
  yield takeLatest(ENABLE2FAEMAIL, enableEmail2FA);
  yield takeLatest(SENDPHONECODE, sendPhoneCode);
  yield takeLatest(VERIFYOTP, verifyPhoneotp);
  yield takeLatest(IMAGEUPLOAD, imageupload);
}

