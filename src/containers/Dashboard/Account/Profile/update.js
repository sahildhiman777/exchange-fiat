import React, { Component } from 'react';
import Modal from 'react-bootstrap/Modal';
import { connect } from 'react-redux';
import { compose } from 'redux';

import { makeSelectAlterMyAccount, makeSelectAlterMyAccountLoading, makeSelectAlterMyAccountError, makeSelectAlterMyAccountSuccess } from './selectors';
import ReactLoading from "react-loading";

import { alterPassword } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../../toast/toast';

class update extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            username: '',
            password: '',
            confirmPassword: '',
            passwordError: false,
            invalidlength:false,
            passwordErrorempty: false,
            error: false,
            swal_show: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
        };
    }
    componentWillMount() {
        this.setState({ id: this.props.user._id })
    }
    handleChange(e) {
        if (e.target.name == 'password') {
            this.setState({ password: e.target.value })
        }
        if (e.target.name == 'confirmPassword') {
            this.setState({ confirmPassword: e.target.value })
        }
    }


    componentDidUpdate() {
        if (this.props.update) {
            ToastTopEndSuccessFire(this.props.update.message);
            // Swal.fire({
            //     title: this.props.update.message,
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 2000
            // }).then(result => {
                this.setState({ blockloader: false })
                window.location.reload()
            // });
        }
    }


    submit = (e) => {
        e.preventDefault();
        const data = {
            id: this.state.id,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword,
            type: 'put'
        }
        if(data.password<6){
            this.setState({  invalidlength: true, passwordError: false, passwordErrorempty: false })
            return
        }
        if (data.password == '') {
            this.setState({ passwordErrorempty: true, })
        }
        else if (data.password == data.confirmPassword) {
            this.setState({ passwordError: false, passwordErrorempty: false, invalidlength: false, blockloader: true })
            this.props.dispatch(alterPassword(data));
        } else {
            this.setState({ passwordError: true, passwordErrorempty: false, invalidlength: false })
        }
    }

    render() {
        return (
            <div>
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <React.Fragment>
                    {/* {user && this.props.type === 'edit' && */}
                    <Modal
                        show={this.props.show}
                        onHide={this.props.onHide}
                        size="md"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">{window.i18n('ProfileChangePassword')}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <form className="" onSubmit={this.submit}>
                                <div className="form-group row">
                                    <div className="col-lg-12 col-xl-12">
                                        <input onChange={(e) => this.handleChange(e)} type="password" className="form-control" name="password" value={this.state.password} placeholder="New password" aria-describedby="basic-addon1" />
                                    </div>
                                </div>
                                <div className="form-group row">
                                    <div className="col-lg-12 col-xl-12">
                                        <input onChange={(e) => this.handleChange(e)} type="password" className="form-control" name="confirmPassword" value={this.state.confirmPassword} placeholder="Confirm password" aria-describedby="basic-addon1" />
                                    </div>
                                </div>
                                {this.state.passwordError && <div className="invalid-feedback">{window.i18n('ProfileCnfrmPassword')}</div>}
                                {this.state.invalidlength && <div className="invalid-feedback">Please enter at least 6 digit password.</div>}
                                {this.state.passwordErrorempty && <div className="invalid-feedback">Please enter valid password.</div>}
                                <div className="modal-updt-cncl-btn">
                                    <button type='submit' className=" bank-button btn btn-success btn_color btn-md btn-tall btn-wide " >Save</button>
                                    <span onClick={this.props.onHide} className="btn btn-secondary btn-md btn-tall btn-wide span-btn" >Cancel</span>
                                </div>
                                <div className="">
                                    {/* {this.props.loading && <LoadingIndicator />} */}
                                </div>
                            </form>
                        </Modal.Body>
                    </Modal>
                </React.Fragment>
            </div>
        )
    }
}

// export default update

const mapStateToProps = createStructuredSelector({
    update: makeSelectAlterMyAccount(),
    loading: makeSelectAlterMyAccountLoading(),
    error: makeSelectAlterMyAccountError(),
    success: makeSelectAlterMyAccountSuccess(),


});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'myaccount', reducer });
const withSaga = injectSaga({ key: 'myaccount', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(update);
