import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMyAccount = state => state.myaccount || initialState;
const makeSelectAlterMyAccount = () =>
  createSelector(selectMyAccount, myaccountState => myaccountState.alterMyAccount);
const makeSelectAlterMyAccountError = () =>
  createSelector(selectMyAccount, myaccountState => myaccountState.alterError);
const makeSelectAlterMyAccountLoading = () =>
  createSelector(selectMyAccount, myaccountState => myaccountState.alterLoading);
const makeSelectAlterMyAccountSuccess = () =>
  createSelector(selectMyAccount, myaccountState => myaccountState.alterSuccess);



const selectMyProfile = state => state.myprofile || initialState;
const makeSelectAlterMyProfile = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.alterMyProfile);
const makeSelectAlterMyProfileError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.profile_alterError);
const makeSelectAlterMyProfileLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.profile_alterLoading);
const makeSelectAlterMyProfileSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.profile_alterSuccess);

const makeSelectG2fa = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.g2fa);
const makeSelectG2faError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.error);
const makeSelectG2faSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.success);
//enable G2fa
const makeSelectEnableG2fa = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enabletwo);
const makeSelectEnableG2faError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableError);
const makeSelectEnableG2faSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableSuccess);
const makeSelectEnableG2faLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableLoading);

const makeSelectDisableG2fa = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disabletwo);
const makeSelectDisableG2faError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableError);
const makeSelectDisableG2faSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableSuccess);
const makeSelectDisableG2faLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableLoading);

const makeSelectDisableG2faEmail = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableemail);
const makeSelectDisableG2faEmailError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableemailError);
const makeSelectDisableG2faEmailSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableemailSuccess);
const makeSelectDisableG2faEmailLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.disableemailLoading);

const makeSelectUser = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.user);
const makeSelectError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.usererror);
const makeSelectLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.userloading);
const makeSelectSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.usersuccess);
const makeSelectRedirect = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.userredirect);

const makeSelectMyWallets = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.wallets);
const makeSelectMyWalletsError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.Error);
const makeSelectMyWalletsLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.Loading);
const makeSelectMyWalletsSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.Success);

const makeSelectMyEmail = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.email);
const makeSelectMyEmailError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailError);
const makeSelectMyEmailLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailLoading);
const makeSelectMyEmailSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailSuccess);


const makeSelectMyEmailVerify = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailverify);
const makeSelectMyEmailVerifyError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailverifyError);
const makeSelectMyEmailVerifyLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailverifyLoading);
const makeSelectMyEmailVerifySuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.emailverifySuccess);


const makeSelectEmail2FaEnable = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableEmail2fa);
const makeSelectEmail2FaEnableError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableEmail2faError);
const makeSelectEmail2FaEnableLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableEmail2faLoading);
const makeSelectEmail2FaEnableSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.enableEmail2faSuccess);

  const makeSelectsendphonecode = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.sendphonecode);
const makeSelectsendphonecodeError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.sendphonecodeError);
const makeSelectsendphonecodeLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.sendphonecodeLoading);
const makeSelectsendphonecodeSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.sendphonecodeSuccess);

  const makeSelectverifyotp = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.verifyotp);
const makeSelectverifyotpError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.verifyotpError);
const makeSelectverifyotpLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.verifyotpLoading);
const makeSelectverifyotpSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.verifyotpSuccess);

  const makeSelectimageupload = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.imageupload);
const makeSelectimageuploadError = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.imageuploadError);
const makeSelectimageuploadLoading = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.imageuploadLoading);
const makeSelectimageuploadSuccess = () =>
  createSelector(selectMyProfile, myprofileState => myprofileState.imageuploadSuccess);

export {
  selectMyAccount,
  makeSelectAlterMyAccount,
  makeSelectAlterMyAccountError,
  makeSelectAlterMyAccountLoading,
  makeSelectAlterMyAccountSuccess,

  selectMyProfile,
  makeSelectAlterMyProfile,
  makeSelectAlterMyProfileError,
  makeSelectAlterMyProfileLoading,
  makeSelectAlterMyProfileSuccess,

  makeSelectG2fa,
  makeSelectG2faError,
  makeSelectG2faSuccess,
  makeSelectEnableG2fa,
  makeSelectEnableG2faError,
  makeSelectEnableG2faSuccess,
  makeSelectEnableG2faLoading,

  makeSelectDisableG2fa,
  makeSelectDisableG2faError,
  makeSelectDisableG2faSuccess,
  makeSelectDisableG2faLoading,

  makeSelectDisableG2faEmail,
  makeSelectDisableG2faEmailError,
  makeSelectDisableG2faEmailSuccess,
  makeSelectDisableG2faEmailLoading,

  makeSelectRedirect, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser,

  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,

  makeSelectMyEmail,
  makeSelectMyEmailError,
  makeSelectMyEmailLoading,
  makeSelectMyEmailSuccess,
  
  makeSelectMyEmailVerify,
  makeSelectMyEmailVerifyError,
  makeSelectMyEmailVerifyLoading,
  makeSelectMyEmailVerifySuccess,

  makeSelectEmail2FaEnable,
  makeSelectEmail2FaEnableSuccess,
  makeSelectEmail2FaEnableError,
  makeSelectEmail2FaEnableLoading,

  makeSelectsendphonecode,
  makeSelectsendphonecodeError,
  makeSelectsendphonecodeLoading,
  makeSelectsendphonecodeSuccess,

  makeSelectverifyotp,
  makeSelectverifyotpError,
  makeSelectverifyotpLoading,
  makeSelectverifyotpSuccess,
  
  makeSelectimageupload,
  makeSelectimageuploadError,
  makeSelectimageuploadLoading,
  makeSelectimageuploadSuccess,
};
