import React, { Component } from 'react';
import DisableModal from './disableModal';
import './style.css';

class disable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            disable2fa: false,
            visible: false,
        };
    }

    render() {
        return (
            <div>
                    <div className="container-fluid">
            
                      {/* <!-- Page Heading --> */}
                      
            
            
            
            
                      <div>
                {
                    this.state.disable2fa &&
                    <DisableModal
                        user={this.props.user}
                        show={this.state.disable2fa}
                        onHide={() => this.setState({ disable2fa: false, visible: false })}
                    />
                }
                <div className="">
                    <div className=" ">
                        <div className="">
                            <div className=''>
                                <div className='column-2'>
                                    <div className='top-div-in-column-2'>
                                        <h2 className='auth-h2'>{window.i18n('ProfileDisable')}</h2>
                                        <button style={{ marginLeft: '5px' }} className='gray' onClick={() => this.setState({ disable2fa: true })}>{window.i18n('ProfileDisable1')}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div></div></div></div>
                  
        )
    }
}

export default disable
