import {
    CREATE_QR,
    CREATE_QR_SUCCESS,
    CREATE_QR_ERROR,
    ENABLE2FA,
    ENABLE2FA_SUCCESS,
    ENABLE2FA_ERROR,
    ENABLE2FA_RESET,
    DISABLE2FA,
    DISABLE2FA_SUCCESS,
    DISABLE2FA_ERROR,
    DISABLE2FA_RESET
  } from './constants';
  
  export function createQR(data) {
    return { type: CREATE_QR, data };
  }
  export function createQRSuccess(data) {
    return { type: CREATE_QR_SUCCESS, data };
  }
  export function createQRFailed(error) {
    return { type: CREATE_QR_ERROR, error };
  }
  // enable
  export function enable2fa(data) {
    return { type: ENABLE2FA, data };
  }
  export function enable2faSuccess(data) {
    return { type: ENABLE2FA_SUCCESS, data };
  }
  export function enable2faFailed(error) {
    return { type: ENABLE2FA_ERROR, error };
  }
  export function enable2faReset() {
    return { type: ENABLE2FA_RESET, error };
  }
  // Disable
  export function disable2fa(data) {
    return { type: DISABLE2FA, data };
  }
  export function disable2faSuccess(data) {
    return { type: DISABLE2FA_SUCCESS, data };
  }
  export function disable2faFailed(error) {
    return { type: DISABLE2FA_ERROR, error };
  }
  