import React, { Component } from 'react';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../../../toast/toast';


 class disableModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: '',
            error: false,
            buttonClicked: false,
            swal_show: false,
            swal_msg_s: '',
            swal_msg_e: '',
            swal_loading: ' Loading ..',
        };
    }
    handleChange = (e) => {
        this.setState({ swal_show: false });
        this.setState({ [e.target.name]: e.target.value, swal: false });
    };

    submit = (e) => {
        e.preventDefault();
        this.setState({ buttonClicked: true })
        const data = {
            code: this.state.code,
            google2fa: false,
            type: 'disable',
            id: this.props.user._id
        }
        // this.props.dispatch(disable2fa(data));
    }
    componentDidUpdate() {
        if (this.props.success && this.state.buttonClicked) {
            ToastTopEndSuccessFire(this.state.swal_msg_s)
            // Swal.fire({
            //     title: "Success",
            //     type: "success",
            //     text: this.state.swal_msg_s,
            //     timer: 2000
            // }).then((result) => { 
                this.props.onHide(true)
            //  });
            this.setState({ buttonClicked: false })
        }
        if (this.props.error && this.state.buttonClicked) {
            ToastTopEndErrorFire(this.props.error);
            // Swal.fire({
            //     title: "Error",
            //     type: "error",
            //     text: this.props.error,
            //     timer: 2000
            // }).then(d => {
                // this.props.dispatch(alterReset())
            // });
            this.setState({
                buttonClicked: false
            })
        }
    }

    handleSubmit = (e) => {
        if (this.state.code.length >= 6) this.submit(e)
    };
    render() {
        return (
            <div>
                <React.Fragment>
                <Modal
                    show={this.props.show}
                    onHide={this.props.onHide}
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">{window.i18n('settingDisableFactor')}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div className="kt-wizard-v4" id="kt_apps_customer_add_customer" data-ktwizard-state="first">
                                <div className="kt-portlet">
                                    <div className="kt-portlet__body kt-portlet__body--fit">
                                        <div className="kt-grid">
                                            <div className="kt-grid__item kt-grid__item--fluid kt-wizard-v4__wrapper">
                                                <form className="kt-form" id="kt_apps_customer_add_customer_form" onSubmit={this.submit}>
                                                    <div className="kt-wizard-v4__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                        <div className="kt-section kt-section--first">
                                                            <div className="kt-wizard-v4__form">
                                                                <div className="row">
                                                                    <div className="col-xl-12">
                                                                        <div className="kt-section__body">
                                                                            <div className="form-group row">
                                                                                <label className="col-xl-3 col-lg-3 col-form-label">{window.i18n('ProfileEnterCode')}</label>
                                                                                <div className="col-lg-9 col-xl-9">
                                                                                    <input onChange={this.handleChange} className="form-control" type="text" name="code" value={this.state.code} placeholder="Secret Code" onKeyUp={this.handleSubmit} required />
                                                                                </div>
                                                                            </div>
                                                                            {/* {error && <div className="invalid-feedback">{error}</div>} */}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="modal-updt-cncl-btn">
                                                        <button onSubmit={this.submit} className="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u" >{window.i18n('settingDisable')}</button>
                                                        <span onClick={this.props.onHide} className="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u span-btn" >{window.i18n('ProfileCancle')}</span>
                                                    </div>
                                                    {/* <div className="kt-form__actions"> */}
                                                    {/* {this.props.loading && <LoadingIndicator />} */}
                                                    {/* </div> */}
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
            </div>
        )
    }
}

export default disableModal
