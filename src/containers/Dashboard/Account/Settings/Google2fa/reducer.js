import produce from 'immer';
import {
  CREATE_QR,
  CREATE_QR_SUCCESS,
  CREATE_QR_ERROR,
  ENABLE2FA,
  ENABLE2FA_SUCCESS,
  ENABLE2FA_ERROR,
  ENABLE2FA_RESET,
  DISABLE2FA,
  DISABLE2FA_SUCCESS,
  DISABLE2FA_ERROR
}
  from './constants';

export const initialState = {
  g2fa: false,
  success: false,
  error: false,
  enabletwo: false,
  enableSuccess: false,
  enableError: false,
  enableLoading: false,
  enableReset: false,
  disabletwo: false,
  disableError: false,
  disableSuccess: false,
  disableLoading: false
};
const g2faReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case CREATE_QR:
        draft.loading = true;
        draft.error = false;
        draft.success = false;
        draft.g2fa = false;
        break;

      case CREATE_QR_SUCCESS:
        draft.g2fa = action.data;
        draft.error = false;
        draft.success = true;
        draft.loading = false;
        break;

      case CREATE_QR_ERROR:
        draft.error = action.error.toString();
        draft.success = false;
        draft.loading = false;
        break;

      case ENABLE2FA:
        draft.enableError = false;
        draft.enableSuccess = false;
        draft.enabletwo = false;
        draft.enableLoading = true;
        break;

      case ENABLE2FA_SUCCESS:
        draft.enabletwo = action.data.google2fa;
        draft.enableError = false;
        draft.enableSuccess = true;
        draft.enableLoading = false;
        break;

      case ENABLE2FA_ERROR:
        draft.enableError = action.error.toString();
        draft.enableSuccess = false;
        draft.enabletwo = false;
        draft.enableLoading = true;
        break;

      case ENABLE2FA_RESET:
        draft.g2fa = false;
        draft.enableError = false;
        draft.enableSuccess = false;
        draft.enableLoading = false;
        break;

      case DISABLE2FA:
        draft.disableError = false;
        draft.disableSuccess = false;
        draft.disabletwo = false;
        draft.disableLoading = true;
        break;

      case DISABLE2FA_SUCCESS:
        draft.disabletwo = action.data.google2fa;
        draft.disableError = false;
        draft.disableSuccess = true;
        draft.disableLoading = false;
        break;

      case DISABLE2FA_ERROR:
        draft.disableError = action.error.toString();
        draft.disabletwo = false;
        draft.disableSuccess = false;
        draft.disableLoading = false;
        break;
    }
  });
export default g2faReducer;