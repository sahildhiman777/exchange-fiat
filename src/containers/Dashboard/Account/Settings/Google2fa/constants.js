export const CREATE_QR = 'exchangefiatUserPanel/g2fa/CREATE_QR';
export const CREATE_QR_SUCCESS = 'exchangefiatUserPanel/g2fa/CREATE_QR_SUCCESS';
export const CREATE_QR_ERROR = 'exchangefiatUserPanel/g2fa/CREATE_QR_ERROR';

export const ENABLE2FA = 'exchangefiatUserPanel/g2fa/ENABLE2FA';
export const ENABLE2FA_SUCCESS = 'exchangefiatUserPanel/g2fa/ENABLE2FA_SUCCESS';
export const ENABLE2FA_ERROR = 'exchangefiatUserPanel/g2fa/ENABLE2FA_ERROR';
export const ENABLE2FA_RESET = 'exchangefiatUserPanel/g2fa/ENABLE2FA_RESET';

export const DISABLE2FA = 'exchangefiatUserPanel/g2fa/DISABLE2FA';
export const DISABLE2FA_SUCCESS = 'exchangefiatUserPanel/g2fa/DISABLE2FA_SUCCESS';
export const DISABLE2FA_ERROR = 'exchangefiatUserPanel/g2fa/DISABLE2FA_ERROR';
export const DISABLE2FA_RESET = 'exchangefiatUserPanel/g2fa/DISABLE2FA_RESET';


