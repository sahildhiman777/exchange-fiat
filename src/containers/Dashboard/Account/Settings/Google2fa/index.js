import React, { Component } from 'react';
import './style.css';
import Enable from './enable2fa';



 class index extends Component {
     constructor(props) {
         super(props)
         this.state = {
             profile: false,
             enable2fa: false
         }
     }
    render() {
      console.log(this.props,'indexxxxxxxx');
      
        return (
            <div>
              
        <div className="container-fluid">

          {/* <!-- Page Heading --> */}
          

          <Enable
                // user={user}
                // Skey={QR2fa.data}
                show={this.state.enable2fa}
                onHide={() => this.setState({ enable2fa: false, visible: false })}
              />


          <div className="kt-subheader kt-grid__item" id="kt_subheader">
                <div className="kt-container ">
                  <div className="kt-subheader__main">
                    <div className=''>
                      <div className=' column-2'>
                        <div className='top-div-in-column-2'>
                          <h2 className='auth-h2'> {window.i18n('settingEnableFactor')} </h2>
                          <button className='green-enable-btn' onClick={() => this.setState({ enable2fa: true })}>{window.i18n('settingEnableButton')}</button>
                        </div>
                        <div className='middle-div-in-column-2'>
                          <div style={{ marginRight: 40}}>
                            <div>
                            <img style={{ width: '182px'}} src={process.env.PUBLIC_URL + "/images/barcode.png"} alt="mypic"/>
                              {/* <img src={QR2fa.image_data} /> */}
                            </div>
                            <div className='playstorelogo'>
                            <img src={process.env.PUBLIC_URL + "/images/google-play.png"} alt="mypic"/>
                              <img src={process.env.PUBLIC_URL + "/images/Available-on-app-store.png"} alt="mypic"/>
                            </div>
                          </div>
                          <div>
                            <p>{window.i18n('settingSetupFactor')} <br />{window.i18n('settingGoogleDownload')}</p>
                            <p>{window.i18n('settingScanBarcode')} <br />{window.i18n('settingEnterKey')}<br /> {window.i18n('settingManually')}.</p>
                            <h5>{window.i18n('settingSecurityKey ')}{}</h5>
                          </div>
                        </div>
                        <div className='bottom-div-in-column-2'>
                          <div>
                            <h1 style={{ fontSize: '20px' }}></h1>
                          </div>
                          <div>
                          {window.i18n('ProfileGoogleAndroidOrApple')}<br />
                          {window.i18n('settingGoto')} <span className='span-color-red'>{window.i18n('settingMenu')}</span> -> <span className='span-color-red'>{window.i18n('settingSetupAccount')}</span><br />
                          {window.i18n('settingChose')} <span className='span-color-red'>{window.i18n('settingSacanBarcode')} </span> {window.i18n('settingOption')} <br />
                            <span style={{ fontStyle: 'italic' }}> 4. <span style={{ color: ' #2A2728' }}>{window.i18n('settingUnableToScan')}</span> {window.i18n('settingChose')} <span className='span-color-red'>{window.i18n('settingProvidedKey')} </span> {window.i18n('settingType')}<span>{window.i18n('settingSecurityKey ')}</span><br /></span>
                            {window.i18n('settingSixDigit')}  <br />
                            {window.i18n('settingEveryTime')}  <br />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div></div></div>
      
        )
    }
}

export default index
