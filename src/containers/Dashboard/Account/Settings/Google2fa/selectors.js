import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectTwofa = state => state.g2fa || initialState;

const makeSelectG2fa = () =>
  createSelector(selectTwofa, twofaState => twofaState.g2fa);
const makeSelectG2faError = () =>
  createSelector(selectTwofa, twofaState => twofaState.error);
const makeSelectG2faSuccess = () =>
  createSelector(selectTwofa, twofaState => twofaState.success);
//enable G2fa
const makeSelectEnableG2fa = () =>
  createSelector(selectTwofa, twofaState => twofaState.enabletwo);
const makeSelectEnableG2faError = () =>
  createSelector(selectTwofa, twofaState => twofaState.enableError);
const makeSelectEnableG2faSuccess = () =>
  createSelector(selectTwofa, twofaState => twofaState.enableSuccess);
const makeSelectEnableG2faLoading = () =>
  createSelector(selectTwofa, twofaState => twofaState.enableLoading);
const makeSelectDisableG2fa = () =>
  createSelector(selectTwofa, twofaState => twofaState.disabletwo);
const makeSelectDisableG2faError = () =>
  createSelector(selectTwofa, twofaState => twofaState.disableError);
const makeSelectDisableG2faSuccess = () =>
  createSelector(selectTwofa, twofaState => twofaState.disableSuccess);
const makeSelectDisableG2faLoading = () =>
  createSelector(selectTwofa, twofaState => twofaState.disableLoading);
export {
  selectTwofa,
  makeSelectG2fa,
  makeSelectG2faError,
  makeSelectG2faSuccess,
  makeSelectEnableG2fa,
  makeSelectEnableG2faError,
  makeSelectEnableG2faSuccess,
  makeSelectEnableG2faLoading,
  makeSelectDisableG2fa,
  makeSelectDisableG2faError,
  makeSelectDisableG2faSuccess,
  makeSelectDisableG2faLoading
};