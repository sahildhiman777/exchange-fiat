import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMywallets = state => state.coinwallets || initialState;
const makeSelectWallets = () =>
  createSelector(selectMywallets, mywallets => mywallets.wallets);
const makeSelectWalletsError = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletserror);
const makeSelectWalletsLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletsloading);
const makeSelectWalletsSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletssuccess);

export {
  makeSelectWallets,
  makeSelectWalletsError,
  makeSelectWalletsLoading,
  makeSelectWalletsSuccess,
}  