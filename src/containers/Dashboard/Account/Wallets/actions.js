import {
  GET_WALLETS,
  GET_WALLETS_ERROR,
  GET_WALLETS_SUCCESS,
} from './constants';

export function getwallets() {
  return {
    type: GET_WALLETS,
  };
}
export function getwalletsSuccess(data) {
  return {
    type: GET_WALLETS_SUCCESS,
    data,
  };
}
export function getwalletsFailed(error) {
  return {
    type: GET_WALLETS_ERROR,
    error,
  };
}