import produce from 'immer';
import {
    GET_WALLETS,
    GET_WALLETS_SUCCESS,
    GET_WALLETS_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
    wallets: [],
    walletssuccess: false,
    walletserror: false,
    walletsloading: false,
};


/* eslint-disable default-case, no-param-reassign */
const walletsReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case GET_WALLETS:
                draft.wallets = [];
                draft.walletserror = false;
                draft.walletssuccess = false;
                draft.walletsloading = true;
                break;

            case GET_WALLETS_SUCCESS:
                draft.wallets = action.data;
                draft.walletserror = false;
                draft.walletssuccess = true;
                draft.walletsloading = false;
                break;

            case GET_WALLETS_ERROR:
                draft.walletserror = action.error.toString();
                draft.walletssuccess = false;
                draft.walletsloading = false;
                draft.wallets = [];
                break;
        }
    });

export default walletsReducer;
