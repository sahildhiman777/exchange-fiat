import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectWallets,
  makeSelectWalletsError,
  makeSelectWalletsLoading,
  makeSelectWalletsSuccess,
} from './selectors';

import { getwallets } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import { withRouter } from "react-router-dom";
import ReactLoading from "react-loading";
import DataTable from 'react-data-table-component';


class index extends Component {
  constructor(props) {
    super(props)
    this.state = {

    }
  }
  componentWillMount() {
    this.props.dispatch(getwallets());
  }

  componentWillReceiveProps(nextProps) {

  }

  walletBuyclick(data) {
    if (data.coin_code == 'USD') {
      this.props.history.push('/buycoin/$USD')
    } else if (data.coin_code == 'TRON-USDT' || data.coin_code == 'ERC20-USDT') {
      this.props.history.push('/buycoin/USDT')
    } else {
      this.props.history.push(`/buycoin/${data.coin_code}`)
    }
  }

  walletSellclick(data) {
    if (data.coin_code == 'USD') {
      this.props.history.push('/sellcoin/$USD')
    } else if (data.coin_code == 'TRON-USDT' || data.coin_code == 'ERC20-USDT') {
      this.props.history.push('/sellcoin/USDT')
    } else {
      this.props.history.push(`/sellcoin/${data.coin_code}`)
    }
  }


  render() {
    let wallets = this.props.walletsdata;
    
    let columns = [
      {
        name: `${window.i18n('Coin')}`,
        sortable: false,
        selector: 'symbol',
        cell: row => <div className="table_cell"><span className="wallet_icon"><img src={require(`../../../../images/${row.icon}.png`)} /></span><span className="symbol_name">{row.coin_code == 'TRON-USDT' ? 'USDT' : row.coin_code}</span></div>
      },
      {
        name: `${window.i18n('Name')}`,
        sortable: false,
        selector: 'coin_name',
        cell: row => <div className="table_cell">{row.coin_name.toUpperCase()}</div>
      },
      {
        name: `${window.i18n('Price')}`,
        sortable: false,
        selector: 'currency_rate',
        cell: row => <div className="table_cell">{row.currency_rate}</div>
      },
      {
        name: `${window.i18n('Currentbalance')}`,
        sortable: false,
        selector: 'coin_balance',
        cell: row => <div className="table_cell">{row.coin_balance}</div>
      },
      {
        name: `${window.i18n('Action')}`,
        sortable: false,
        cell: row => (
          <div className="row" style={{ width: "100%" }}>
            <div className="col-lg-6 col-md-12">
              {row.buy_status &&
              <button
                className="btn buy-sell-button-profile buy_button"
                name="buy"
        onClick={() => this.walletBuyclick(row)}>{window.i18n('Buy')}</button>
              }
            </div>
            <div className="col-lg-6 col-md-12">
            {row.sell_status &&
              <button
                className="btn buy-sell-button-profile sell_button"
                name="sell"
                onClick={() => this.walletSellclick(row)}>{window.i18n('Sell')}{row.sell_status}</button>
            }
            </div>
          </div>
        )
      },
    ]

    return (
      <div className="wallet_page">
        {(this.props.loading) &&
          <div className="blockloader">
            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
            </div>
          </div>
        }
        <div className="wallet_table">
          <DataTable
            highlightOnHover={true}
            columns={columns}
            noDataComponent={window.i18n('AllThereAre')}
            data={wallets}
          />
        </div>
      </div>
    )
  }
}



const mapStateToProps = createStructuredSelector({
  walletsdata: makeSelectWallets(),
  loading: makeSelectWalletsLoading(),
  error: makeSelectWalletsError(),
  success: makeSelectWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'coinwallets', reducer });
const withSaga = injectSaga({ key: 'coinwallets', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);