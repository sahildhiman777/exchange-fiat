export const GET_WALLETS = 'exchangefiatUserPanel/coinwallets/GET_WALLETS';
export const GET_WALLETS_SUCCESS = 'exchangefiatUserPanel/coinwallets/GET_WALLETS_SUCCESS';
export const GET_WALLETS_ERROR = 'exchangefiatUserPanel/coinwallets/GET_WALLETS_ERROR';