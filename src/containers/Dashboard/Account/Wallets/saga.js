import { call, put, takeLatest } from 'redux-saga/effects';
import { GET_WALLETS } from './constants';
import { getwallets, getwalletsFailed, getwalletsSuccess } from './actions';
import request from '../../../../utils/request';
import { requestSecure } from '../../../../utils/request';
import Define from '../../../../utils/config';


export function* getWallets() {
  let currency = localStorage.getItem('currency') || "VND";
  const requestURL = `${Define.API_URL}userwallets/wallets_list?currency=${currency.toLowerCase()}`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    yield put(getwalletsSuccess(res.data));
  } catch (err) {
    yield put(getwalletsFailed(err));
  }
}


/**
* Root saga manages watcher lifecycle
*/
export default function* appData() {
  yield takeLatest(GET_WALLETS, getWallets);
}