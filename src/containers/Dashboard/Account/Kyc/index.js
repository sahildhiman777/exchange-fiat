import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from "react-router-dom";
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import { createQR } from '../Profile/actions';
import jwt from 'jwt-decode';
import Disable from '../Profile/disable';
import {

  makeSelectG2fa,
  makeSelectG2faError,
  makeSelectG2faSuccess,
  makeSelectDisableG2faSuccess,



  makeSelectUser,
  makeSelectLoading,
  makeSelectError,
  makeSelectSuccess,
  makeSelectRedirect,



  makeSelectEnableAlterG2fa,
  makeSelectEnableG2fa,
  makeSelectEnableG2faError,
  makeSelectEnableG2faSuccess,
  makeSelectEnableG2faLoading,
  makeSelectsendphonecode,
  makeSelectsendphonecodeError,
  makeSelectsendphonecodeLoading,
  makeSelectsendphonecodeSuccess,

  makeSelectverifyotp,
  makeSelectverifyotpError,
  makeSelectverifyotpLoading,
  makeSelectverifyotpSuccess,
} from '../Profile/selectors';
import { enable2fa, sendphonecode, verifyotp, loadUser } from '../Profile/actions';
import reducer from '../Profile/reducer';
import saga from '../Profile/saga';
import Define from '../../../../utils/config';
import axios from 'axios';

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
      error: false,
      phonenumber: '',
      phoneVerify: false,
      verifyotp: '',
      kyc_id_two: '',
      kyc_id_three: '',
      kyc_level1: '',
      kyc_level2: '',
      kyc_level3: '',
    }
  }

  componentWillMount() {
    const accessToken = localStorage.getItem("exchangefiatUserPanel");
    let decoded = jwt(accessToken);
    const data = {
      id: decoded.id
    }
    if (data.id) {
      this.props.dispatch(createQR(data));
    }
    this.props.dispatch(loadUser());
  }

  handleChange = (e) => {
    if (e.target.name == 'phonenumber') {
      this.setState({ phonenumber: e.target.value })
    }
    if (e.target.name == 'verifyotp') {
      this.setState({ verifyotp: e.target.value })
    }
    this.setState({ [e.target.name]: e.target.value });
  };

  submit = (e) => {
    e.preventDefault();
    this.setState({
      buttonClicked: true
    })

    const data = {
      code: this.state.code,
      google2fa: true,
      Secfakey: this.props.Skey,
      type: 'enable',
      id: this.props.user._id
    }
    this.props.dispatch(enable2fa(data));

  }
  sendCode(e) {
    e.preventDefault();
    const data = {
      id: this.props.user._id,
      phone: this.state.phonenumber
    }
    this.setState({ phoneVerify: true })
    this.props.dispatch(sendphonecode(data));
  }

  verifyCode(e) {
    e.preventDefault();
    const data = {
      id: this.props.user._id,
      otp: this.state.verifyotp
    }
    this.props.dispatch(verifyotp(data));
  }
  uploadkyctwo = (e) => {
    e.preventDefault();
    // this.setState({ kyc_id_two: e.target.files[0] })
    let file = e.target.files[0];
    let id = this.props.user._id
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('id', id);
    formData.append('kyc_level', 'kyc_level2');
    axios.post(`${Define.API_URL}kycimage`, formData).then(res => {
      this.setState({kyc_level2: 'pending'})
      this.props.dispatch(loadUser());
    })

    
  }
  // submitkyctwo(e) {
  //   e.preventDefault();
  //   let file = this.state.kyc_id_two;
  //   let id = this.props.user._id
  //   const formData = new FormData();
  //   formData.append('file', file, file.name);
  //   formData.append('id', id);
  //   formData.append('kyc_level', 'kyc_level2');
  //   axios.post(`${Define.API_URL}kycimage`, formData).then(res => {
  //     this.setState({kyc_level2: 'pending'})
  //     this.props.dispatch(loadUser());
  //   })
  // }
  uploadkycthree = (e) => {
    e.preventDefault();
    // this.setState({ kyc_id_three: e.target.files[0] })
    let file = e.target.files[0];
    let id = this.props.user._id
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('id', id);
    formData.append('kyc_level', 'kyc_level3');
    axios.post(`${Define.API_URL}kycimage`, formData).then(res => {
      this.props.dispatch(loadUser());
    });
  }
  // submitkycthree(e) {
  //   e.preventDefault();
  //   let file = this.state.kyc_id_three;
  //   let id = this.props.user._id
  //   const formData = new FormData();
  //   formData.append('file', file, file.name);
  //   formData.append('id', id);
  //   formData.append('kyc_level', 'kyc_level3');
  //   axios.post(`${Define.API_URL}kycimage`, formData).then(res => {
  //     this.props.dispatch(loadUser());
  //   });
  // }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps.user.kyc_level2,'nextProps.user.kyc_level2');
    
    this.state.kyc_level1 = nextProps.user.kyc_level1
    this.state.kyc_level2 = nextProps.user.kyc_level2
    this.state.kyc_level3 = nextProps.user.kyc_level3
    if(nextProps.user.kyc_level1 == 'rejected'){
      this.state.kyc_level1 = 'rejected'
    }
    if (nextProps.user.kyc_level1 == 'approved') {
      this.state.kyc_level2 = 'notuploaded';
    }
    if (nextProps.verifyotp) {
      this.state.kyc_level1 = 'approved';
    }
    if (nextProps.user.kyc_level2 == 'approved') {
      this.state.kyc_level2 = 'approved';
    }
    if (nextProps.user.kyc_level2 == 'rejected') {
      this.state.kyc_level2 = 'rejected';
    }
    if (nextProps.user.kyc_level2 == 'pending') {
      this.state.kyc_level2 = 'pending';
    }
    if (nextProps.user.kyc_level3 == 'approved') {
      this.state.kyc_level3 = 'approved';
    }
    if (nextProps.user.kyc_level3 == 'rejected') {
      this.state.kyc_level3 = 'rejected';
    }
    if (nextProps.user.kyc_level3 == 'pending') {
      this.state.kyc_level3 = 'pending';
    }
  }
  render() {
    // console.log(this.props,'props');

    const { QR2fa, user } = this.props;
    return (
      <div className="kyc_section">
        <div className="kyc_div">
          <div className="kyc_header">
            <h3>{window.i18n('CompleteKYC')}</h3>
          </div>
          {/* Card section Start  */}
          <div className="kyc_card">
            <div className="row">
              <div className="col-md-4">
                <div class="card text-center">
                  <div class="card-body kyc_card_body">
                    <p class="card-text">(1)</p>
                    <div className="kyc_card_img"><img src={require('../../../../images/sms-token.png')} /></div>
                    <h6>{window.i18n('Verifyyour2FA')}</h6>
                    <p className="card_content">{window.i18n('Connectwithgoogle')}</p>
                    {this.state.kyc_level1 == 'approved' &&
                    <div className="approved_check">
                      <i class="fa fa-check"></i>
                      </div>
                      }
                       {this.state.kyc_level1 == 'rejected' &&
                    <div className="reject_check">
                      <i class="fa fa-ban"></i>
                      </div>
                      }
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div class="card text-center">
                  <div class="card-body kyc_card_body">
                    <p class="card-text">(2)</p>
                    <div className="kyc_card_img"><img src={require('../../../../images/identification-documents-error--v1.png')} /></div>
                    <h6>{window.i18n('GetVerified')}</h6>
                    <p className="card_content">{window.i18n('SelfieholdingGOV')}</p>
                    {this.state.kyc_level2 == 'approved' &&
                    <div className="approved_check">
                      <i class="fa fa-check"></i>
                      </div>
                      }
                      {this.state.kyc_level2 == 'rejected' &&
                    <div className="reject_check">
                      <i class="fa fa-ban"></i>
                      </div>
                      }
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div class="card text-center">
                  <div class="card-body kyc_card_body">
                    <p class="card-text">(3)</p>
                    <div className="kyc_card_img"><img src={require('../../../../images/coin-wallet--v1.png')} /></div>
                    <h6>{window.i18n('AddSettlement')}</h6>
                    <p className="card_content">{window.i18n('Addbank')}</p>
                    {this.state.kyc_level3 == 'approved' &&
                    <div className="approved_check">
                      <i class="fa fa-check"></i>
                      </div>
                      }
                       {this.state.kyc_level3 == 'rejected' &&
                    <div className="reject_check">
                      <i class="fa fa-ban"></i>
                      </div>
                      }
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Card section end  */}

          <div className="twofa_section">
            {/* 2fa section start  */}
            {this.state.kyc_level1 == 'notuploaded' || this.state.kyc_level1 == 'rejected' ?
              <div className="twofa_card_main">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12 twofa_card">
                      <nav>
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Google Auth</a>
                        </div>
                      </nav>
                      <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                          <p className="google_heading">{window.i18n('ScanQR')}</p>
                          <img src={QR2fa.image_data} />
                          <h5>{window.i18n('ProfileSecurityKey')}: {QR2fa.data}</h5>

                          <form onSubmit={this.submit}>
                            <div className="form-group">
                              <input onChange={this.handleChange} className="form-control" type="text" name="code" value={this.state.code} placeholder="Enter 2FA Code" onKeyUp={this.handleSubmit} required />
                            </div>
                            <div className="twofa_phn_button">
                              <button onSubmit={this.submit} className="phone_2fa_button" >{window.i18n('ProfileEnable')}</button>
                            </div>
                          </form>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              :
              <div className="kyc_level">
                {this.state.kyc_level2 == 'notuploaded' || this.state.kyc_level2 == 'rejected' ?
                  <div className="kyc_level2">
                    <div className="twofa_card_main">
                      <div class="container">
                        <div class="row">
                          <div class="col-md-12 twofa_card">
                            <div className="kyc_level_card">
                              <p className="card_content">{window.i18n('SelfieholdingGOV')}</p>
                              <div className="upload_icon"><img src={require('../../../../images/upload_icon.png')} /></div>
                              <form onSubmit={(e) => this.submitkyctwo(e)} enctype="multipart/form-data" >
                                <div className="file btn btn-lg btn-primary file_kyc">
                                  {window.i18n('Upload')}
							                    <input type="file" name="file" className="kyc_input" accept="image/png, image/jpeg" onChange={this.uploadkyctwo} />
                                </div>
                                {/* <button className="kyc_finish_button">{window.i18n('Finish')}</button> */}
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  :
                  <div className="kyc_level3">
                    {this.state.kyc_level2 == 'approved' ?
                      <div className="twofa_card_main">
                        <div class="container">
                          {this.state.kyc_level3 == 'notuploaded' || this.state.kyc_level3 == 'rejected' ?
                            <div class="row">
                              <div class="col-md-12 twofa_card">
                                <div className="kyc_level_card">
                                  <p className="card_content">{window.i18n('Addbank')}</p>
                                  <div className="upload_icon"><img src={require('../../../../images/upload_icon.png')} /></div>
                                  <form onSubmit={(e) => this.submitkycthree(e)} enctype="multipart/form-data" >
                                    <div className="file btn btn-lg btn-primary file_kyc">
                                    {window.i18n('Upload')}
                               <input type="file" name="file" className="kyc_input" accept="image/png, image/jpeg" onChange={this.uploadkycthree} />
                                    </div>
                                    {/* <button className="kyc_finish_button">{window.i18n('Finish')}</button> */}
                                  </form>
                                </div>
                              </div>
                            </div>
                            :
                            <div class="row">
                              {this.state.kyc_level3 == 'pending' ?
                                <div class="col-md-12 twofa_card">
                                  <div className="kyc_level_card">
                                    <p className="card_content">{window.i18n('KYCLEVEL3Pending')}</p>
                                  </div>
                                </div>
                                :
                                ""
                                // <div class="col-md-12 twofa_card">
                                //   <div className="kyc_level_card">
                                //     <p className="card_content">{window.i18n('CongratulationsKYC')}</p>
                                //   </div>
                                // </div>
                              }
                            </div>
                          }
                        </div>
                      </div>
                      :
                      <div className="twofa_card_main">
                        <div class="container">
                          <div class="row">
                            <div class="col-md-12 twofa_card">
                              <div className="kyc_level_card">
                                <p className="card_content">{window.i18n('KYCLEVEL2Pending')}</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    }
                  </div>
                }
              </div>
            }
            {/* 2fa section end  */}
          </div>

          {/* KYC Records start  */}
          <div className="kyc_record">
            <p className="kyc_record_heading">{window.i18n('KYCRecords')}
          {this.state.kyc_level3 == 'approved' &&
          <>
           <span>KYC Verified</span>
          </>}</p>

            <div class="table-responsive back_color">
              <table class="table table-bordered">
                <thead >
                  <th >{window.i18n('LEVEL')}</th>
                  <th >{window.i18n('Status')}</th>
                </thead>
                <tbody >
                  <tr >
                    <td >{window.i18n('KYCLEVEL1')}</td>
                    <td >
                      <div className="table_cell_kyc">
                        {/* {this.state.kyc_level1 == 'pending' || this.state.kyc_level1 == 'notuploaded' ? <div className="trading_table">{window.i18n('Pending')}</div> : <div className="trading_table_green">{window.i18n('Approved')}</div>} */}

                      {(this.state.kyc_level1 == 'rejected') ? <div className="trading_table_red">Rejected</div> : (this.state.kyc_level1 == 'pending' || this.state.kyc_level1 == 'notuploaded' ? <div className="trading_table">Pending</div> : <div className="trading_table_green">Approved</div>)}
                      </div>
                    </td>
                  </tr>
                  <tr >
                    <td >{window.i18n('KYCLEVEL2')}</td>
                    <td >
                      <div className="table_cell_kyc">
                        {/* {this.state.kyc_level2 == 'pending' || this.state.kyc_level2 == 'notuploaded' ? <div className="trading_table">{window.i18n('Pending')}</div> : <div className="trading_table_green">{window.i18n('Approved')}</div>} */}
                        {(this.state.kyc_level2 == 'rejected') ? <div className="trading_table_red">Rejected</div> : (this.state.kyc_level2 == 'pending' || this.state.kyc_level2 == 'notuploaded' ? <div className="trading_table">Pending</div> : <div className="trading_table_green">Approved</div>)}
                        </div>
                    </td>
                  </tr>
                  <tr >
                    <td >{window.i18n('KYCLEVEL3')}</td>
                    <td >
                      <div className="table_cell_kyc">
                        {/* {(this.state.kyc_level3 == 'pending' || this.state.kyc_level3 == 'notuploaded') ? <div className="trading_table">{window.i18n('Pending')}</div> : <div className="trading_table_green">{window.i18n('Approved')}</div>} */}

                        {(this.state.kyc_level3 == 'rejected') ? <div className="trading_table_red">Rejected</div> : (this.state.kyc_level3 == 'pending' || this.state.kyc_level3 == 'notuploaded' ? <div className="trading_table">Pending</div> : <div className="trading_table_green">Approved</div>)}
                        </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          {/* KYC Records End  */}
        </div>
      </div>

    )

  }
}


const mapStateToProps = createStructuredSelector({
  enable: makeSelectEnableG2fa(),
  error: makeSelectEnableG2faError(),
  success: makeSelectEnableG2faSuccess(),
  loading: makeSelectEnableG2faLoading(),

  user: makeSelectUser(),
  QR2fa: makeSelectG2fa(),
  error: makeSelectG2faError(),
  success: makeSelectG2faSuccess(),
  usererror: makeSelectError(),
  userloading: makeSelectLoading(),
  usersuccess: makeSelectSuccess(),
  enablesuccess: makeSelectEnableG2faSuccess(),
  disablesuccess: makeSelectDisableG2faSuccess(),

  codesend: makeSelectsendphonecode(),
  codesendSuccess: makeSelectsendphonecodeSuccess(),
  codesendEror: makeSelectsendphonecodeError(),
  codesendLoading: makeSelectsendphonecodeLoading(),

  verifyotp: makeSelectverifyotp(),
  verifyotpEror: makeSelectverifyotpError(),
  verifyotpLoading: makeSelectverifyotpLoading(),
  verifyotpSuccess: makeSelectverifyotpSuccess(),

});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'myprofile', reducer });
const withSaga = injectSaga({ key: 'myprofile', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);

// export default (index);

