import produce from 'immer';
import {
    WALLETS,
    WALLETS_ERROR,
    WALLETS_SUCCESS,
    GET_CURRENCY_SUCCESS,
    BUY_COIN,
    BUY_COIN_ERROR,
    BUY_COIN_SUCCESS,
    GET_BANKS,
    GET_BANKS_ERROR,
    GET_BANKS_SUCCESS,
    GET_SELL_BANKS,
    GET_SELL_BANKS_SUCCESS,
    GET_SELL_BANKS_ERROR,
    SELL_COIN,
    SELL_COIN_ERROR,
    SELL_COIN_SUCCESS,
    EMAIL, EMAIL_ERROR, EMAIL_SUCCESS, EMAIL_RESET,
    VERIFYEMAIL, VERIFYEMAIL_ERROR, VERIFYEMAIL_SUCCESS, VERIFIY_RESET,
    GET_WALLETS,
    GET_WALLETS_SUCCESS,
    GET_WALLETS_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
    banks: [],
    currency: [],
    wallets: false,
    success: false,
    error: false,
    loading: false,
    currencies: [],
    buycoin: false,
    buycoinsuccess: false,
    buycoinerror: false,
    buycoinloading: false,
    sellbank: [],
    sellbanksuccess: false,
    sellbankerror: false,
    sellbankloading: false,

    sellcoin: false,
    sellcoinsuccess: false,
    sellcoinerror: false,
    sellcoinloading: false,

    email: false,
    emailerror: false,
    emailsuccess: false,
    emailloading: false,

    verifyemail: false,
    verifyemailerror: false,
    verifyemailsuccess: false,
    verifyemailloading: false,

    walletsdata: [],
    walletssuccess: false,
    walletserror: false,
    walletsloading: false,
};


/* eslint-disable default-case, no-param-reassign */
const walletsReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {

            case WALLETS:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.wallets = false;
                break;

            case WALLETS_SUCCESS:
                draft.wallets = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case WALLETS_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;

            case GET_CURRENCY_SUCCESS:
                draft.currencies = action.data;
                break;

            case BUY_COIN:
                draft.buycoinloading = true;
                draft.buycoinerror = false;
                draft.buycoinsuccess = false;
                draft.buycoin = false;
                break;

            case BUY_COIN_SUCCESS:
                draft.buycoin = action.data;
                draft.buycoinerror = false;
                draft.buycoinsuccess = true;
                draft.buycoinloading = false;
                break;

            case BUY_COIN_ERROR:
                draft.buycoinerror = action.error.toString();
                draft.buycoinsuccess = false;
                draft.buycoinloading = false;
                draft.buycoin = false;
                break;

            case GET_BANKS:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.banks = [];
                break;

            case GET_BANKS_SUCCESS:
                draft.banks = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case GET_BANKS_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                draft.banks = [];
                break;

            case GET_SELL_BANKS:
                draft.sellbank = [];
                draft.sellbankerror = false;
                draft.sellbanksuccess = false;
                draft.sellbankloading = true;
                break;

            case GET_SELL_BANKS_SUCCESS:
                draft.sellbank = action.data;
                draft.sellbankerror = false;
                draft.sellbanksuccess = true;
                draft.sellbankloading = false;
                break;

            case GET_SELL_BANKS_ERROR:
                draft.sellbankerror = action.error.toString();
                draft.sellbanksuccess = false;
                draft.sellbankloading = false;
                draft.sellbank = [];
                break;

            case SELL_COIN:
                draft.sellcoinloading = true;
                draft.loading = true;
                draft.sellcoinerror = false;
                draft.sellcoinsuccess = false;
                draft.sellcoin = false;
                break;

            case SELL_COIN_SUCCESS:
                draft.sellcoin = action.data;
                draft.loading = false;
                draft.sellcoinerror = false;
                draft.sellcoinsuccess = true;
                draft.sellcoinloading = false;
                break;

            case SELL_COIN_ERROR:
                draft.sellcoinerror = action.error && action.error.toString();
                draft.loading = false;
                draft.sellcoinsuccess = false;
                draft.sellcoinloading = false;
                break;

            case EMAIL:
                draft.emailloading = true;
                draft.loading = true;
                draft.emailerror = false;
                draft.emailsuccess = false;
                draft.email = false;
                break;

            case EMAIL_SUCCESS:
                draft.email = action.data;
                draft.loading = false;
                draft.emailerror = false;
                draft.emailsuccess = true;
                draft.emailloading = false;
                break;

            case EMAIL_ERROR:
                draft.emailerror = action.error.toString();
                draft.emailsuccess = false;
                draft.emailloading = false;
                draft.loading = false;
                break;

            case EMAIL_RESET:
                draft.email = false;
                draft.emailerror = false;
                draft.emailsuccess = false;
                draft.loading = false;
                break;
            case VERIFYEMAIL:
                draft.verifyemailloading = true;
                draft.verifyemailerror = false;
                draft.verifyemailsuccess = false;
                draft.verifyemail = false;
                break;

            case VERIFYEMAIL_SUCCESS:
                draft.verifyemail = action.data;
                draft.verifyemailerror = false;
                draft.verifyemailsuccess = true;
                draft.verifyemailloading = false;
                break;

            case VERIFYEMAIL_ERROR:
                draft.verifyemailerror = action.error.toString();
                draft.verifyemailsuccess = false;
                draft.verifyemailloading = false;
                break;
            case VERIFIY_RESET:
                draft.verifyemail = false;
                draft.verifyemailerror = false;
                draft.verifyemailsuccess = false;
                break;
                case GET_WALLETS:
                    draft.walletsdata = [];
                    draft.walletserror = false;
                    draft.walletssuccess = false;
                    draft.walletsloading = true;
                    break;
    
                case GET_WALLETS_SUCCESS:
                    draft.walletsdata = action.data;
                    draft.walletserror = false;
                    draft.walletssuccess = true;
                    draft.walletsloading = false;
                    break;
    
                case GET_WALLETS_ERROR:
                    draft.walletserror = action.error.toString();
                    draft.walletssuccess = false;
                    draft.walletsloading = false;
                    draft.walletsdata = [];
                    break;
        }
    });

export default walletsReducer;
