import React, {Component} from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { withRouter, Link } from 'react-router-dom';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import {
  makeSelectBanks,
  makeSelectError,
  makeSelectLoading,
  makeSelectSuccess,
  makeSelectCurrency
} from './Account/Banks/selectors';
import { getBanks, alterReset } from './Account/Banks/actions';
import reducer from './Account/Banks/reducer';
import saga from './Account/Banks/saga';

class Bank extends Component {
  componentWillMount(){
    this.props.dispatch(getBanks());
  }
  render(){
    console.log(this.props,'this.props.bank');
    let banks = this.props.bank;
    return(
      <div>BANK</div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  banks: makeSelectBanks(),
  error: makeSelectError(),
  loading: makeSelectLoading(),
  success: makeSelectSuccess(),
  currency: makeSelectCurrency(),
});

export function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withReducer = injectReducer({ key: 'banks', reducer });
const withSaga = injectSaga({ key: 'banks', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter,
)(Bank);