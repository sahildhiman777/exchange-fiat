import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMywallets = state => state.mywallets || initialState;
const makeSelectMyWallets = () =>
  createSelector(selectMywallets, mywallets => mywallets.wallets);
const makeSelectMyWalletsError = () =>
  createSelector(selectMywallets, mywallets => mywallets.Error);
const makeSelectMyWalletsLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.loading);
const makeSelectMyWalletsSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.Success);

  const makeSelectCurrencies = () =>
  createSelector( selectMywallets, mywallets => mywallets.currencies );

  const makeSelectBuyCoin = () =>
  createSelector(selectMywallets, mywallets => mywallets.buycoin);
const makeSelectBuyCoinError = () =>
  createSelector(selectMywallets, mywallets => mywallets.buycoinerror);
const makeSelectBuyCoinLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.buycoinLoading);
const makeSelectBuyCoinSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.buycoinSuccess);

  const makeSelectBanks = () =>
  createSelector(selectMywallets, selectMywallets => selectMywallets.banks);


  const makeSelectSellBank = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellbank);
const makeSelectSellBankError = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellbankerror);
const makeSelectSellBankLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellbankloading);
const makeSelectSellBankSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellbanksuccess);


  const makeSelectSellCoin = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellcoin);
const makeSelectSellCoinError = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellcoinerror);
const makeSelectSellCoinLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellcoinloading);
const makeSelectSellCoinSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.sellcoinsuccess);

  const makeSelectMyEmail = () =>
  createSelector(selectMywallets, mywallets => mywallets.email);
const makeSelectMyEmailError = () =>
  createSelector(selectMywallets, mywallets => mywallets.emailerror);
const makeSelectMyEmailLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.emailloading);
const makeSelectMyEmailSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.emailsuccess);

const makeSelectMyVerifyEmail = () =>
  createSelector(selectMywallets, mywallets => mywallets.verifyemail);
const makeSelectMyVerifyEmailError = () =>
  createSelector(selectMywallets, mywallets => mywallets.verifyemailerror);
const makeSelectMyVerifyEmailLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.verifyemailloading);
const makeSelectMyVerifyEmailSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.verifyemailsuccess);

  const makeSelectWallets = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletsdata);
const makeSelectWalletsError = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletserror);
const makeSelectWalletsLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletsloading);
const makeSelectWalletsSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.walletssuccess);

  export {
    selectMywallets,
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
    makeSelectCurrencies,
    makeSelectBuyCoin,
    makeSelectBuyCoinError,
    makeSelectBuyCoinLoading,
    makeSelectBuyCoinSuccess,
    makeSelectBanks,
    makeSelectSellBank,
    makeSelectSellBankError,
    makeSelectSellBankLoading,
    makeSelectSellBankSuccess,
    makeSelectSellCoin,
    makeSelectSellCoinError,
    makeSelectSellCoinLoading,
    makeSelectSellCoinSuccess,

    makeSelectMyEmail,
    makeSelectMyEmailError,
    makeSelectMyEmailLoading,
    makeSelectMyEmailSuccess,

    makeSelectMyVerifyEmail,
    makeSelectMyVerifyEmailError,
    makeSelectMyVerifyEmailLoading,
    makeSelectMyVerifyEmailSuccess,
    makeSelectWallets,
    makeSelectWalletsError,
    makeSelectWalletsLoading,
    makeSelectWalletsSuccess,
  }  