import React, { Component } from 'react';
// import Bank from './bank'

class CoinList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buy: false
    }
}
  clickState(data) {
    // localStorage.setItem('coin_name', data.symbol)
    // localStorage.setItem('coin_price', data.bidPrice)
    this.setState({buy : true})
    

  }

  render() {
    let asset = this.props.wallets
    return (
      <div className="coinlist">
        {
            this.state.buy ?
            <div className="bank-section">
              
            </div>
          :
        <div className="row">
          {(() => {
            const options = [];
            if (asset) {
              for (let i = 0; i < asset.length; i++) {
                options.push(
                  <div className="col-md-4 mb-4 buy-now-header">
                    <div class="coin-header coin-header-btc">
                      <span classname="coin-header-span">
                        <h2 classname="coin-header-h2">
                          <img src={require('../../images/' + asset[i].symbol + '.png')} />
                          {asset[i].symbol == 'DASHBTC' || asset[i].symbol == 'DOGEBTC' ?
                            asset[i].symbol.substring(0, 4)
                            :
                            asset[i].symbol.substring(0, 3)
                          }
                        </h2>
                      </span>
                    </div>
                    <div className="card border-left-buy shadow py-2 mb-2">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-7">
                            <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{window.i18n('DashboardBestPrice')}</div>
                            <div className="h5 mb-0 font-weight-bold text-gray-800 price-text">{asset[i].bidPrice} VND/BTC</div>
                          </div>
                          <div className="col-md-5">
                            <button type="submit" class="buy_button buy-sell-button" name="buy" onClick={() => this.clickState(asset[i])}>{window.i18n('BuyNow')}</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Sell section */}
                    <div className="card border-left-sell  shadow py-2">
                      <div className="card-body">
                        <div className="row">
                          <div className="col-md-7">
                            <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{window.i18n('DashboardBestSellPrice')}</div>
                            <div className="h5 mb-0 font-weight-bold text-gray-800 price-text">{asset[i].bidPrice} VND/BTC</div>
                          </div>
                          <div className="col-md-5">
                            <button type="submit" class="sell_button buy-sell-button" name="sell" onClick={() => this.clickState(asset[i])}>{window.i18n('SellNow')}</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              }
              return options;
            }

          })()}
        </div>
  }
      </div>
    );
  }
}
export default (CoinList)