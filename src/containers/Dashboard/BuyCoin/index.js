import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,
  makeSelectBuyCoin,
  makeSelectBuyCoinError,
  makeSelectBuyCoinLoading,
  makeSelectBuyCoinSuccess,
  makeSelectBanks,

  makeSelectMyEmail,
  makeSelectMyEmailError,
  makeSelectMyEmailLoading,
  makeSelectMyEmailSuccess,

  makeSelectMyVerifyEmail,
  makeSelectMyVerifyEmailError,
  makeSelectMyVerifyEmailLoading,
  makeSelectMyVerifyEmailSuccess,
  makeSelectWallets,
  makeSelectWalletsError,
  makeSelectWalletsLoading,
  makeSelectWalletsSuccess,
} from '../selectors';

import { wallets, buy_coin_Failed, buy_coin, getBanks, email, verifyemail, emailReset, verifiyReset, getwallets } from '../actions';
import { loadUser } from "../../App/actions";
import reducer from '../reducer';
import saga from '../saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../utils/injectReducer';
import injectSaga from '../../../utils/injectSaga';
import { FormGroup, FormControl, FormLabel, Form } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../../toast/toast';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import Config from '../../../utils/config';
import ReactLoading from "react-loading";

var QRCode = require('qrcode.react');

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      email: '',
      firstName: '',
      coinName: '',
      coinprice: '',
      step2: true,
      coin_amount: '',
      bank_name: '',
      account_number: '',
      bank_code: '',
      country: '',
      holder_name: '',
      bank_address: '',
      step3: false,
      currencyType: '',
      total: '',
      price: '',
      coin: [],
      items: [],
      coinaddress: '',
      code: '',
      agree: false,
      currency: '',
      bank_id: '',
      created_at: '',
      updated_at: '',
      activeCoins: {},
      activebtc: false,
      activeeth: false,
      activeusdt: false,
      icon: '',
      activename: '',
      validatearress: false,
      buy_section: true,
      buy_form_section: false,
      blockloader: true,
      type_2fa: '',
      google_2fa: false,
      email_2fa: false,
      verifiy2fa: false,
      otpcode: '',
      otperror: false,
      confirm: false,
      currentWallet: false
    }
    this.handleChange = this.handleChange.bind(this);
    this.clickState = this.clickState.bind(this);
    // this.handleBuySubmit = this.handleBuySubmit.bind(this);
  }

  componentWillMount() {
    this.props.dispatch(getBanks());
    this.props.dispatch(wallets());
    this.props.dispatch(loadUser());
    this.props.dispatch(getwallets());
  }
  componentWillReceiveProps(props) {
  // if(props.verifiyemail){
  //   this.state.google_2fa = false
  //   this.state.email_2fa = false
  //   ToastTopEndSuccessFire(props.verifiyemail)
  //   this.props.dispatch(verifiyReset());
  // }
    
    if (props.wallets) {
      let coin_code = props.match.params.coin_code;
      let coinData = props.wallets;
      if (coinData) {
        let coin = coinData.find(coin => coin.symbol == coin_code);
        if (coin) {
          this.state.step2 = true;
          this.state.buy_section = true;
          this.state.coinName = coin.symbol;
          this.state.coinprice = this.buyPrice(coin.price) || 0;
          this.state.icon = coin.icon;
          this.state.activename = coin.name;
          this.state.coin_amount = this.state.coin_amount || coin.amount || 0;
          this.state.total = (this.buyPrice(coin.price) * Number(coin.amount)) || 0;
          this.state.currencyType = localStorage.getItem('currency') || "VND";

          for (let key in this.state.activeCoins) {
            this.state.activeCoins[key] = false;
          }
          this.state.activeCoins[coin.symbol] = true;
          this.state.total = this.state.coin_amount * this.state.coinprice;
          this.setState(this.state);
        } else {
          const accessToken = localStorage.getItem("exchangefiatUserPanel");
          if (accessToken) {
            const rt = this.props.history.location.pathname;
            if (rt === '/buycoin/transactions')
              this.props.history.push('/transactions')
          } else {
            this.props.history.push('/')
          }
        }
      }
      if (props.user) {
        this.state.id = props.user._id
        this.state.email = props.user.email
        this.state.firstName = props.user.firstname
        this.setState({ blockloader: false })
        if (props.user.google2fa == true) {
          this.setState({ type_2fa: "google_2fa" });
        } else if (props.user.email_2fa == true) {
          this.setState({ type_2fa: "email_2fa" });
        } else {
          this.setState({ type_2fa: "disabled" });
        }
        if (props.verifiyemailsuccess == true) {
          if (props.verifiyemail) {
            this.setState({ email_2fa: false, google_2fa: false, otpcode: '', blockloader:false })
            ToastTopEndSuccessFire(props.verifiyemail)
            this.state.confirm = true;
            this.props.dispatch(verifiyReset());
          }
        }
        if (props.emailsuccess == true) {
          this.setState({ email_2fa: true, blockloader: false, otpcode: '' });
          this.props.dispatch(emailReset());
        }
      }
      this.setState({ blockloader: props.loading });
    }

    if (props.userwallets) {
      let coin_code = props.match.params.coin_code;
      let selectedWallet = props.userwallets.find(coin => coin.coin_code == coin_code);
      if (selectedWallet) {
        this.setState({
          currentWallet: selectedWallet,
          coinaddress: selectedWallet.coin_address
        })
      }
    }

  }

  componentDidMount() {
    let currency = localStorage.getItem('currency');
    if (currency) {
      this.setState({ currencyType: currency })
    }

    if (this.props.wallets) {
      let coin_code = this.props.match.params.coin_code;
      
      let coinData = this.props.wallets;
      
      if (coinData) {
        let coin = coinData.find(coin => coin.symbol == coin_code);

        if (coin) {
          this.state.step2 = true;
          this.state.buy_section = true;
          this.state.coinName = coin.symbol;
          this.state.coinprice = this.buyPrice(coin.price);
          this.state.icon = coin.icon;
          this.state.activename = coin.name;
          this.state.coin_amount = coin.amount || 0;
          this.state.total = (this.buyPrice(coin.price) * Number(coin.amount)) || 0;

          for (let key in this.state.activeCoins) {
            this.state.activeCoins[key] = false;
          }
          this.state.activeCoins[coin.symbol] = true;
          this.setState(this.state);
        } else {
          if (this.props.user) {
            this.props.history.push('/dashboard')

          } else {
            this.props.history.push('/')
          }
        }
      }
    }
  }

  clickState(e, data) {
    if (e.target.name == 'buy') {
      this.state.buy_section = true;
      this.state.step2 = true;
      this.state.coinName = data.symbol;
      this.state.coinprice = this.buyPrice(data.price);
      this.state.icon = data.icon;
      this.state.activename = data.name;
      for (let key in this.state.activeCoins) {
        this.state.activeCoins[key] = false;
      }

      this.state.activeCoins[data.symbol] = true;
      this.setState(this.state);
    }

  }

  clickBuy(data) {
    let code = Math.floor(100000 + Math.random() * 900000);
    this.setState({
      step3: true,
      step2: false,
      buy_form_section: true,
      bank_name: data.bank_name,
      account_number: data.account_number,
      code: code,
      bank_code: data.bank_code,
      country: data.country,
      holder_name: data.holder_name,
      bank_address: data.bank_address,
      currency: data.currency,
      bank_id: data._id,
      updated_at: data.updated_at,
      created_at: data.created_at
    })
  }

  handleChange(e) {
    e.preventDefault();
    const name = e.target.name;
    if (name == 'coin_name') {
      const value = e.target.value;
      this.setState({ coinName: value });
    }
    // if (name == 'coin_address') {
    //   const value = e.target.value;
    //   var specialChars = /[^a-zA-Z0-9 ]/g;
    //   if (value.match(specialChars)) {
    //     this.setState({ validatearress: true })
    //     return;
    //   }
    //   this.setState({ coinaddress: value, validatearress: false });
    // }
    if (name == 'coin_amount') {
      const value = e.target.value;
      let total = value * this.state.coinprice;
      this.setState({ coin_amount: value, total: total });
    }
  }

  handleCheck(e) {
    this.setState({ agree: e.target.checked });
  }
  validateForm = () => {
    if (this.state.coin_amount.length > 0) {
      return true
    }
  }

  handleeBuySubmit = (e) => {
    e.preventDefault()
    if(this.props.user.emailstatus){
      if (this.state.type_2fa == 'google_2fa' || this.state.type_2fa == 'email_2fa') {
        const data = {
          id: this.state.id,
          email: this.state.email,
          name:this.state.firstName,
        }
  
        if (this.state.type_2fa == 'google_2fa') {
          this.setState({ google_2fa: true, blockloader: false });
        } 
        
        if (this.state.type_2fa == 'email_2fa') {
          this.setState({ blockloader: true })
          this.props.dispatch(email(data));
        }
      } else {
        ToastTopEndErrorFire(window.i18n('enable2fa'));
      }
      
      

    }else{
      ToastTopEndErrorFire('Please check your email and verify your email');
    }
    
  }
  otpcode = (e) => {
    e.preventDefault()
    this.setState({ otpcode: e.target.value, otperror: false },
      () => {
        if (this.state.otpcode.length >= 6) {
          this.submitOtp();
        } 
      })
  }
  submitOtp = (e) => {
    this.setState({ blockloader: true })

    let verifiydata = {
      id: this.state.id,
      code: this.state.otpcode,
      code_type: this.state.type_2fa
    }
    if (this.state.otpcode.length < 6) {
      this.setState({ otperror: true, blockloader: false });
    } else {
      this.props.dispatch(verifyemail(verifiydata));
    }

  }

  handleSellCancel(e) {
    ToastTopEndSuccessFire("Your sell transaction has been generated. You will receive coins after fiat admin approval ")
      this.props.history.push('/transactions')
  }
  backclick() {
    this.props.history.push('/dashboard')
    this.setState({ step3: false, step2: false, buy_section: false, total: '', coin_amount: '', coinaddress: '' })
  }
  backclickbuy() {
    this.setState({ step3: false, step2: true, total: '', coin_amount: '', total: '', coin_amount: '', coinaddress: '' })
  }


  clickBankButton(e) {
    let name = e.target.name
    let list = [];
    let coinlist = this.props.wallets;
    let selectedCoin = coinlist.find(coin => coin.name == name);

    if (selectedCoin) {
      this.props.history.push(`/buycoin/${selectedCoin.symbol}`)
      for (let coin_code in this.state.activeCoins) {
        this.state.activeCoins[coin_code] = false;
      }

      this.state.activeCoins[selectedCoin.symbol] = true;
      this.state.coinName = selectedCoin.symbol
      this.state.coinprice = this.buyPrice(selectedCoin.price)
      this.state.icon = selectedCoin.icon
    }

    this.setState(this.state);
  }

  componentDidUpdate() {
    if (this.props.buycoin) {
      ToastTopEndSuccessFire("Your buy transaction has been generated. You will receive coins after fiat admin approval ")
        this.props.dispatch(buy_coin_Failed(""));
        this.setState({ blockloader: false });
        this.props.history.push('/transactions')
    }

    if (this.props.buycoinerror && this.props.buycoinerror != "") {
      ToastTopEndErrorFire(this.props.buycoinerror);
        this.props.dispatch(buy_coin_Failed(""));
    }
    if (this.props.verifiyemailsuccess == true) {
      if (this.props.verifiyemail) {
        ToastTopEndSuccessFire(this.props.verifiyemail)
        this.state.email_2fa = false;
        this.state.blockloader = false;
        this.props.dispatch(verifiyReset());

      }
    }

  }

  renderBuyCoinButtons = () => {
    const buyCoins = this.props.wallets && this.props.wallets.filter(coin => coin.buy_status);
    const self = this;
    if (buyCoins) {
      return buyCoins.map(coin => {
        return (
          <button key={`buy-button-${coin.symbol}`} className={"bank_button" + (self.state.activeCoins[coin.symbol] ? ' activebtc' : '')} name={coin.name} onClick={(e) => self.clickBankButton(e)}>
            <img src={require(`../../../images/${coin.icon}.png`)} /> {coin.symbol}
          </button>
        );
      })
    } else {
      return "";
    }
  }
  confirmSubmit(e){
    e.preventDefault()
     let data = {
      coin_name: this.state.coinName,
      coin_address: this.state.coinaddress,
      currency_code: this.state.currencyType,
      coin_amount: this.state.coin_amount,
      total: this.state.total,
      user_id: this.props.user._id,
      email: this.props.user.email,
      transaction_id: new Date().getTime(),
      code: this.state.code,
      platform: 'exchangefiat',
      notify_url: 'http://www.testurl.com/nofiy',
      phone: '',
      return_url: '/transactions',
      order: '',
      currency: this.state.currency,
      deposit: true,
      status: 'pending',
      bank: this.state.bank_id,
      edited: false,
      payus_status: 'pending', 
    }
    this.props.dispatch(buy_coin(data));
  }
  confirmClose(e){
    e.preventDefault()
    this.setState({confirm:false})
  }

  buyPrice = (price) => {
    return Number(price) + (Number(price) * (Number(Config.buyFee) / 100))
  }

  render() {
    let banks = this.props.banks;
    let currency = localStorage.getItem('currency') || "VND";

    return (
      <div className="dashboard_section">

        {/* Confrm modal..... */}
        {(this.state.confirm == true) && 
        <div className="modal_2fa">
          <div className="card w-55">
            <div className="card-body">
              <div className="popup_header"><h2>{window.i18n('PleaseCopy')}</h2></div>
              <div className="col-lg-12">
                <div className="last_div border_last">
                        <div className="card blinking">
                          <FormGroup controlId="code" className="field_width copy_code codetext">
                            <FormControl
                              ref={(code) => this.code = code}
                              name="code"
                              type="text"
                              value={this.state.code}
                              disabled
                            />

                            <CopyToClipboard text={this.state.code} onCopy={() => {
                              ToastTopEndSuccessFire("Fund transfer code has been copied successfully")
                            }}>
                              <i className="copy_code_popup fa fa-copy"></i>
                            </CopyToClipboard>
                          </FormGroup>
                        </div>
                      </div>
              </div>
                <div className="confirm_popup_button">
                  <button className="confirm_button" onClick={(e)=>{this.confirmSubmit(e)}}>{window.i18n('Confirm')}</button>
                  <button className="close_button" onClick={(e)=>{this.confirmClose(e)}}>{window.i18n('Close')}</button>
              </div>
          </div>
        </div>
        </div>
        }
        {/* //end Confrm modal..... */}


        {/* 2fa modal..... */}
        {(this.state.google_2fa == true || this.state.email_2fa == true) && <div className="modal_2fa">
          <div className="card w-55">
            <div className="card-body">
              <i style={{ float: 'right', cursor: 'pointer' }} className="fa fa-times" aria-hidden="true" onClick={() => this.setState({ google_2fa: false, email_2fa: false })}></i>
              <div className="col-lg-8" style={{ margin: 'auto' }}>
                <div className="p-5" style={{ padding: '7rem !important' }}>
                  <div className="text-center">
                    {this.state.email_2fa == true && <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInPlaceholder')}</h1>}
                    {this.state.google_2fa == true && <h1 className="h4 text-gray-900 mb-4">Enter Google OTP</h1>}
                  </div>
                  <form className="user">
                    <div className="form-group">
                      {this.state.email_2fa == true && <input type="number" onChange={this.otpcode} name="otpcode" className="form-control form-control-user" id="exampleInputPassword" placeholder={window.i18n('LogInPlaceholder')} value={this.state.otpcode} required />}
                      {this.state.google_2fa == true && <input type="number" onChange={this.otpcode} name="otpcode" className="form-control form-control-user" id="exampleInputPassword" placeholder='Enter Google OTP' value={this.state.otpcode} required />}
                    </div>
                    {this.state.otperror && <p style={{ fontSize: '12px', color: 'red' }}>Please enter at least 6 digit</p>}
                    <button type="button" onClick={(e) => this.submitOtp(e)} className="btn btn-primary btn-user btn-block">
                    Submit OTP
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>}
        {/* //end 2fa modal..... */}

        {
          (this.state.blockloader) &&
          <div className="blockloader">
            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
            </div>
          </div>
        }
        {this.state.step3 ?
          <div className="buy_form_section">
            {
              this.state.buy_form_section &&
              <div className="container">

                <div className="row">
                  <div className="col-md-8">
                    <div className="left_side">
                      <div className="back_button"><a href="#" className="cancel_button" onClick={(e) => this.backclickbuy(e)}><i className="fa fa-angle-left"></i> {window.i18n('Back')}</a></div>
                      <h3>{window.i18n('BuyNowBuying')} {this.state.coinName}</h3>
                      <form onSubmit={(e) => { this.handleeBuySubmit(e) }}>
                        <div className="row">
                          <div className="col-md-12">
                            <FormGroup controlId="coin_amount" className="field_width ">
                              <FormLabel>{window.i18n('BuyAmount')}</FormLabel>
                              <FormControl
                                name="coin_amount"
                                type="number"
                                value={this.state.coin_amount}
                                onChange={(e) => this.handleChange(e)}
                              />
                            </FormGroup>

                            {/* <FormGroup controlId="coin_address" className="field_width buy_form_fields">
                              <FormLabel>{window.i18n('TransactionCoinAddress')}</FormLabel>
                              <FormControl
                                name="coin_address"
                                type="text"
                                value={this.state.coinaddress}
                                onChange={(e) => this.handleChange(e)}
                              />
                              {this.state.validatearress &&
                                <p style={{ color: 'red', fontSize: '10px' }} >Special symbols are not allowed</p>}
                            </FormGroup> */}

                            <p className="equivalentt">{window.i18n('Equivalent')} ≈ <span className="estimate-value estimate-value-buy">{new Intl.NumberFormat('en-US').format(this.state.total)}</span> <span className="estimate-value-buy">{this.state.currencyType}</span>

                            {/* {this.state.currencyType == 'USD' ? 
                            (this.state.total <= 50 ?
                              <><span className="estimate-value estimate-value-buy">{new Intl.NumberFormat('en-US').format(this.state.total)}</span> <span className="estimate-value-buy">{this.state.currencyType}</span> </>
                              :
                              <span className="estimate-value estimate-value-buy">The amount should not be greater than 50 USD.</span>
                              )
                            : 
                            (this.state.total <= 1159402.00 ? 
                              <><span className="estimate-value estimate-value-buy">{new Intl.NumberFormat('en-US').format(this.state.total)}</span> <span className="estimate-value-buy">{this.state.currencyType}</span> </>
                            : 
                            <span className="estimate-value estimate-value-buy">The amount should not be greater than 1159402.00 VND.</span>
                            )
                            } */}
                            </p>
                            <div className="submit_button">
                              <button className="buy_button buy-sell-button width-buy" disabled={!this.validateForm()} type="submit"> {window.i18n('Buy')}  {this.state.coinName} </button>
                            </div>
                            <p className="important_msg">{window.i18n('SellerPayment')}</p>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                  <div className="col-md-4">
                    <div className="right_side">
                      <div className="row">
                        <div className="col-md-6">
                          <div className="buyer-seller">
                            <p className="small-text">{window.i18n('Seller')}</p>
                            <p className="normal-bold-text owner-ads-buy">{window.i18n('ExchangeFiatDashboard')}</p>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="coin-info">
                            <p className="small-text">{window.i18n('asset')}</p>
                            <p className="normal-bold-text">
                              <img src={require('../../../images/' + this.state.icon + '.png')} />
                              {this.state.coinName}
                            </p>
                          </div>
                        </div>
                      </div>
                      <hr></hr>
                      <div className="last_div border_last">
                        <div className="card blinking">
                          <FormGroup controlId="code" className="field_width code_copy codetext">
                            <FormLabel className="codetext">*{window.i18n('PLEASE')}</FormLabel>
                            <FormControl
                              ref={(code) => this.code = code}
                              name="code"
                              type="text"
                              value={this.state.code}
                              disabled
                            />

                            <CopyToClipboard text={this.state.code} onCopy={() => {
                              ToastTopEndSuccessFire("Fund transfer code has been copied successfully")
                            }}>
                              <i className="code_copy_i fa fa-copy"></i>
                            </CopyToClipboard>
                          </FormGroup>
                        </div>
                      </div>
                      <div className="item-info" >
                        <span>{window.i18n('Price')}</span>
                        <span className="normal-bold-text-buy" >{new Intl.NumberFormat('en-US').format(this.state.coinprice)} {this.state.currencyType}<span className="normal-bold-text">/{this.state.coinName}</span></span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('Limit')}</span>
                        <span className="normal-bold-text right-float" >0.01 ~ <span className="count-maximum" >0.05623</span> <span className="left-count">0.05623</span> {this.state.coinName}</span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('BankName')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.bank_name}</span>
                        </span>
                      </div>

                      <div className="last_div">
                        <span>{window.i18n('AccountNumber')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.account_number}</span>
                        </span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('HolderName')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.holder_name}</span>
                        </span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('BankAddress')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.bank_address}</span>
                        </span>
                      </div>
                      <div className="last_div">
                        <span>{window.i18n('BankCode')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.bank_code}</span>
                        </span>
                      </div>
                      <div className="last_div ">
                        <span>{window.i18n('Country')} </span>
                        <span className="ads-bank-list">
                          <span className="detail">{this.state.country}</span>
                        </span>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            }
          </div>
          :

          <div>
            {
              this.state.step2 ?
                <div className="bank-section">
                  {this.state.buy_section &&
                    <div className="container">
                      <h3>{window.i18n('BuyNow')}</h3>
                      <div className="bank_main_div">
                        <div className="back_button"><a href="#" className="cancel_button" onClick={(e) => this.backclick(e)}><i className="fa fa-angle-left"></i> {window.i18n('Back')} </a></div>
                        <div className="bank_coin_button">
                          {this.renderBuyCoinButtons()}
                        </div>
                        {(() => {
                          const options = [];
                          if (banks) {

                            for (let i = 0; i < banks.length; i++) {

                              if (banks[i].type == "payin")
                                options.push(
                                  <div key={`buy-btc-asset-${i}`} className="buy-btc">
                                    <div className="card">
                                      <div className="row">
                                        <div className="col-md-2">
                                          <div>
                                            {
                                              banks[i].logo ?
                                                <img className="img-thumbnail" src={`${Config.bank_image_url}/${banks[i].logo}`} />
                                                :
                                                <img className="img-thumbnail" src={`${Config.bank_image_url}/images/default.png`} />
                                            }
                                          </div>
                                        </div>
                                        <div className="col-md-6">
                                          <p><span className="price_card">{new Intl.NumberFormat('en-US').format(this.state.coinprice)}</span> {currency}/{this.state.coinName}</p>
                                          <p>{window.i18n('AccountNumber')} {banks[i].account_number}</p>
                                          <p>{window.i18n('BankName')} {banks[i].bank_name}</p>
                                        </div>
                                        <div className="col-md-4">
                                          <div className="dash-buy-button">
                                            <button type="submit" className=" perpalbtn buy_button buy-sell-button" onClick={() => this.clickBuy(banks[i])}>Select</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                );
                            }
                            return options;
                          }
                        })()}
                      </div>
                    </div>
                  }
                </div>
                :
                ''
            }
          </div>
        }
      </div>

    )

  }
}


const mapStateToProps = createStructuredSelector({
  wallets: makeSelectMyWallets(),
  loading: makeSelectMyWalletsLoading(),
  error: makeSelectMyWalletsError(),
  success: makeSelectMyWalletsSuccess(),

  buycoin: makeSelectBuyCoin(),
  buycoinloading: makeSelectBuyCoinLoading(),
  buycoinerror: makeSelectBuyCoinError(),
  buycoinsuccess: makeSelectBuyCoinSuccess(),
  banks: makeSelectBanks(),

  email: makeSelectMyEmail(),
  emailloading: makeSelectMyEmailLoading(),
  emailerror: makeSelectMyEmailError(),
  emailsuccess: makeSelectMyEmailSuccess(),

  verifiyemail: makeSelectMyVerifyEmail(),
  verifiyemailerror: makeSelectMyVerifyEmailError(),
  verifiyemailloading: makeSelectMyVerifyEmailLoading(),
  verifiyemailsuccess: makeSelectMyVerifyEmailSuccess(),

  userwallets: makeSelectWallets(),
  userwalletsloading: makeSelectWalletsLoading(),
  userwalletserror: makeSelectWalletsError(),
  userwalletssuccess: makeSelectWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'mywallets', reducer });
const withSaga = injectSaga({ key: 'mywallets', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withRouter
)(index);

