export const WALLETS = 'exchangefiatUserPanel/wallets/WALLETS';
export const WALLETS_SUCCESS = 'exchangefiatUserPanel/wallets/WALLETS_SUCCESS';
export const WALLETS_ERROR = 'exchangefiatUserPanel/wallets/WALLETS_ERROR';

export const GET_CURRENCY = 'exchangefiatUserPanel/Transactions/GET_CURRENCY';
export const GET_CURRENCY_SUCCESS = 'exchangefiatUserPanel/Transactions/GET_CURRENCY_SUCCESS';
export const GET_CURRENCY_ERROR = 'exchangefiatUserPanel/Transactions/GET_CURRENCY_ERROR';

export const BUY_COIN = 'exchangefiatUserPanel/buycoin/BUY_COIN';
export const BUY_COIN_SUCCESS = 'exchangefiatUserPanel/buycoin/BUY_COIN_SUCCESS';
export const BUY_COIN_ERROR = 'exchangefiatUserPanel/buycoin/BUY_COIN_ERROR';

export const GET_BANKS = 'exchangefiatUserPanel/Banks/GET_BANKS';
export const GET_BANKS_SUCCESS = 'exchangefiatUserPanel/Banks/GET_BANKS_SUCCESS';
export const GET_BANKS_ERROR = 'exchangefiatUserPanel/Banks/GET_BANKS_ERROR';

export const GET_SELL_BANKS = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS';
export const GET_SELL_BANKS_SUCCESS = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS_SUCCESS';
export const GET_SELL_BANKS_ERROR = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS_ERROR';

export const SELL_COIN = 'exchangefiatUserPanel/sellycoin/SELL_COIN';
export const SELL_COIN_SUCCESS = 'exchangefiatUserPanel/sellycoin/SELL_COIN_SUCCESS';
export const SELL_COIN_ERROR = 'exchangefiatUserPanel/sellycoin/SELL_COIN_ERROR';

export const EMAIL = 'exchangefiatUserPanel/email/EMAIL';
export const EMAIL_SUCCESS = 'exchangefiatUserPanel/email/EMAIL_SUCCESS';
export const EMAIL_ERROR = 'exchangefiatUserPanel/email/EMAIL_ERROR';
export const EMAIL_RESET = 'giaodichcoin/g2fa/EMAIL_RESET';

export const VERIFYEMAIL = 'exchangefiatUserPanel/email/VERIFYEMAIL';
export const VERIFYEMAIL_SUCCESS = 'exchangefiatUserPanel/email/VERIFYEMAIL_SUCCESS';
export const VERIFYEMAIL_ERROR = 'exchangefiatUserPanel/email/VERIFYEMAIL_ERROR';
export const VERIFIY_RESET = 'giaodichcoin/g2fa/VERIFIY_RESET';

export const GET_WALLETS = 'exchangefiatUserPanel/wallets/GET_WALLETS';
export const GET_WALLETS_SUCCESS = 'exchangefiatUserPanel/wallets/GET_WALLETS_SUCCESS';
export const GET_WALLETS_ERROR = 'exchangefiatUserPanel/wallets/GET_WALLETS_ERROR';
