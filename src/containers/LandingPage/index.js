import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
    selectMywallets,
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
} from './components/AllCrypto/selectors';
import { wallets } from './components/AllCrypto/actions';
import reducer from './components/AllCrypto/reducer';
import saga from './components/AllCrypto/saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';

import { Link } from "react-router-dom";
import './style.css';
import LandingFooter from './components/Footer';
import Allcrypto from './components/AllCrypto';
import Buysellorder from './components/BuysellOrder';
import Cryptoconvertor from './components/Calculator';
import Currency from './components/CurrencySection';
import Marqe from './components/Marquee';
import $ from 'jquery';
import { withRouter } from "react-router-dom";

import Swal from 'sweetalert2';
class index extends Component {
    componentWillMount() {
        $(document).ready(function () {
            $('body').removeClass('back-clr')
        })
    }

    handlebuy() {
        if (!this.props.user) {
            this.props.history.push('/login')
        } else {
            this.props.history.push('/dashboard')
        }
    }
    handlesell() {
        if (!this.props.user) {
            this.props.history.push('/login')
        } else {
            this.props.history.push('/dashboard')
        }
    }


    render() {
        return (
            <div>
                <Marqe />
                <div className="hero-bg">
                    <div className="hero-light-bg  ">
                        <img src="/images/bg-2.png" width="100%" className="animate-light" />
                    </div>
                    <div className="content">
                        { this.props.user === false ?
                            <div className="menu-icon">
                                <Link to="/login"><button className='login-btn'>{window.i18n('login')}</button></Link>
                                <Link to="/register"> <button className='register-btn'>{window.i18n('register')}</button> </Link>
                                <div className="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src={`/images/${window.i18nCode}.png`} />
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a className="dropdown-item text-right" href="#" data-language="vn" onClick={this.props.changeLanguage}>
                                            Việt Nam &nbsp; <img data-language="vn" src="/images/vn.png" />
                                        </a>
                                        <a className="dropdown-item text-right" href="#" data-language="en" onClick={this.props.changeLanguage}>
                                            English &nbsp; <img data-language="en" src="/images/en.png" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            :
                            <div className="menu-icon">
                                <Link to="/dashboard"> <button className='register-btn'>{window.i18n('dashboard')}</button> </Link>
                                <Link to="#"><button className='login-btn' onClick={this.props.logout}>{window.i18n('logout')}</button></Link>
                                <div className="btn-group" role="group">
                                    <button id="btnGroupDrop1" type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src={`/images/${window.i18nCode}.png`} />
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                        <a className="dropdown-item text-right" href="#" data-language="vn" onClick={this.props.changeLanguage}>
                                            Việt Nam &nbsp; <img data-language="vn" src="/images/vn.png" />
                                        </a>
                                        <a className="dropdown-item text-right" href="#" data-language="en" onClick={this.props.changeLanguage}>
                                            English &nbsp; <img data-language="en" src="/images/en.png" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        }
                        {/* <span class="menu-btn" style={{fontSize :'30px' , cursor:'pointer'}} onclick="openNav()">&#9776; </span>  */}
                        <div className="hero-text ">
                            <img src="/images/logo-e.png" className="logo-icon" />
                            <h1 className="hero-text-h1">EXCHANGE FIAT</h1>
                            <p className="hero-text-p">{window.i18n('buyAndSellDig.Cur')}</p>
                            <button className="hero-buy-btn" onClick={() => this.handlebuy()}>{window.i18n('buyCrypto')}</button>
                            <button className="hero-sell-btn" onClick={() => this.handlesell()}>{window.i18n('sellCrypto')}</button>
                        </div>
                    </div>

                    <div className="hero-img"> <img src="/images/hero-img.png" /> </div>

                </div>
                <div className="hero-white-div"><img src="/images/save-tons-bg.svg" /></div>
                <Allcrypto />
                <Buysellorder />
                <Cryptoconvertor />
                <div className="container">
                    <div className="row buy-sell-margin">
                        <div className="col-lg-6 notAnimated animateBlock leftAlign left">
                            <div className="buy-sell-heading">
                                <h1>{window.i18n('butAndSellCryptoEasy')}</h1>
                                <p className="notAnimated animateBlock leftAlign left"> {window.i18n('Peer-to-Peer')}<br />
                                {window.i18n('BitcoinAltcoins')} <br />
                                {window.i18n('Platformswithfiat')}</p>
                            </div>
                        </div>
                        <div className="col-lg-6 notAnimated animateBlock rightAlign right">
                            <img src="/images/buy-sell.png" width="100%" />
                        </div>
                    </div>
                </div>


                {/* <!--start benefits section--> */}
                <div className="benefits-bg">
                    <div className="container">
                        <div className="benefits-heading">
                            <h1>{window.i18n('ExchangeFiatBen')} </h1>
                            <p > {window.i18n('Buy/Sell')}</p>
                        </div>
                        <div className="row">
                            <div className="col-lg-6 notAnimated animateBlock leftAlign left animation-duration: 1s;">
                                <img src="/images/benefits-img.png" className="benefits-img" />
                            </div>
                            <div className="col-lg-6">
                                <ul className="benefits-ul">
                                    <li>{window.i18n('LowPrice')}</li>
                                    <li>{window.i18n('Trade')}</li>
                                    <li>{window.i18n('Withdraw')}</li>
                                    <li>{window.i18n('SecureStorage')}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!--end benefits section--> */}
                <Currency />
                {/* <!--start wallet section--> */}
                <div className="container">
                    <div className="row wallet-margin">
                        <div className="col-lg-6 animation notAnimated animateBlock leftAlign left">
                            <img src="/images/wallet.png" width="100%" />


                        </div>
                        <div className="col-lg-6 animation notAnimated animateBlock rightAlign right">
                            <div className="wallet-heading">
                                <h1>{window.i18n('FiatWallet')}</h1>
                                <p className="">{window.i18n('FiatWallet')}</p>
                                <button className="wallet-btn">{window.i18n('EXPLORE')}</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* <!--end wallet section--> */}

                {/* <!--start why people choose section--> */}
                <div className="people-choose-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-6 animated notAnimated animateBlock leftAlign left">
                                <div className="people-choose-heading">
                                    <h4>{window.i18n('strngSize')}</h4>
                                    <h1>{window.i18n('whyPeople')}</h1>
                                    <p>{window.i18n('Fast-Secured')}</p>
                                    <button className="people-choose-btn">{window.i18n('EXPLORE')}</button>
                                </div>
                            </div>
                            <div className="col-lg-6 animated notAnimated animateBlock rightAlign right">
                                <img src="/images/people-img.png" width="100%" className="people-img" />
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!--end why people choose section--> */}

                {/* <!--start features section--> */}
                <div className="container">
                    <div className="row features-row">
                        <div className="col-lg-4 features-blue">
                            <div className="features-card">
                                <img src={require('../../images/icon1.png')} />
                                <h4>{window.i18n('Scalability')}</h4>
                                <p>{window.i18n('ScalabilityIsOne')} </p>
                                <i className="far fa-arrow-alt-circle-right"></i>
                            </div>
                        </div>
                        <div className="col-lg-4 features-purple">
                            <div className="features-card">
                                <img src={require('../../images/icon2.png')} />
                                <h4>{window.i18n('Privacy')}</h4>
                                <p>{window.i18n('PrivacyImportance')} </p>
                                <i className="far fa-arrow-alt-circle-right"></i>
                            </div>
                        </div>
                        <div className="col-lg-4 features-green">
                            <div className="features-card">
                                <img src={require('../../images/icon3.png')} />
                                <h4>{window.i18n('Security')}</h4>
                                <p>{window.i18n('Blockchain-Can')}</p>
                                <i className="far fa-arrow-alt-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </div>

                {/* <!--end services section--> */}
                {/* <!--start logo section--> */}
                <div className="container">
                    <marquee className="marquee-footer">
                        <ul className="logo-ul">
                            <li>SONICXGAMINGS</li>
                            <li>PAYUS INC</li>
                            <li>SONICX BLOCKCHAIN </li>
                            <li>EZUGI</li>
                            <li>EVOLUTION </li>
                            <li>SOXBET.ORG</li>
                            <li>SONICEX.ORG</li>
                            <li>SONICXGAMINGS</li>
                        </ul>
                    </marquee>
                </div>
                {/* end logo Section */}
                <LandingFooter />
            </div>
        )
    }
}

// export default withRouter(index);

const mapStateToProps = createStructuredSelector({
    wallets: makeSelectMyWallets(),
    loading: makeSelectMyWalletsLoading(),
    error: makeSelectMyWalletsError(),
    success: makeSelectMyWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(index);
