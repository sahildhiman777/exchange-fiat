import {
  WALLETS,
  WALLETS_ERROR,
  WALLETS_SUCCESS,
} from './constants';

export function wallets(data) {
  return { type: WALLETS, data };
}
export function walletsSuccess(data) {
  return { type: WALLETS_SUCCESS, data };
}
export function walletsFailed(error) {
  return { type: WALLETS_ERROR, error };
}