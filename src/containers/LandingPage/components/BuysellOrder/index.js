import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
} from '../AllCrypto/selectors';

import { wallets } from '../AllCrypto/actions';
import reducer from '../AllCrypto/reducer';
import saga from '../AllCrypto/saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import { withRouter } from 'react-router-dom';
import NumberFormat from 'react-number-format';
import Config from '../../../../utils/config';
// var NumberFormat = require('react-number-format');
class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            coinname: '',
            price: '',
            total: '',
            amount: '',
            checked: false,
            icon: '',
            name: '',
            symbol:'',
            currencyType:'',
        }
        this.handlesubmitbuy = this.handlesubmitbuy.bind(this);
    }
    componentDidUpdate(){
        let currency = localStorage.getItem('currency') || "VND";
        this.state.currencyType = currency;
    }
    componentWillMount() {
        let currency = localStorage.getItem('currency') || "VND";
        this.setState({currencyType:currency})
        // this.props.dispatch(wallets());
    }
    componentDidMount() {
        let value = this.state.coinname;
        let asset = this.props.wallets && this.props.wallets.find(coin => coin.symbol == value);
        if (asset) {
            if (!this.state.checked)
                asset.price = this.buyPrice(asset.price);
            else
                asset.price = this.sellPrice(asset.price);
            this.setState({ ...asset, coinname: value })
        } else {
            this.setState({ coinname: value })
        }
    }
    buyPrice = (price) => {
        return Number(price) + (Number(price) * (Number(Config.buyFee) / 100))
      }

      sellPrice = (price) => {
        return Number(price) - (Number(price) * (Number(Config.sellFee) / 100))
      }

    handlecoin(e) {
        let value = e.target.value;
        this.setState({symbol:value})
        let asset = this.props.wallets.find(coin => coin.symbol == value);
        console.log(asset);
        if (asset) {
            if (!this.state.checked)
                asset.price = this.buyPrice(asset.price);
            else
                asset.price = this.sellPrice(asset.price);

            this.setState({
                ...asset, coinname: e.target.value,
                amount: "",
                total: ''
            })
        } else {
            this.setState({ coinname: e.target.value })
        }
    }
    handlebuy(e) {
        if (e.target.name == 'amount') {
            let total = e.target.value * this.buyPrice(this.state.price) ;
            this.setState({
                amount: e.target.value,
                total: new Intl.NumberFormat('en-US').format(total)
            })
        } if (e.target.name == 'price') {
            this.setState({
                price: new Intl.NumberFormat('en-US').format(this.buyPrice(e.target.value))
            })
        } if (e.target.name == 'total') {
            this.setState({
                total: new Intl.NumberFormat('en-US').format(this.buyPrice(e.target.value))
            })
        }

    }

    handlesell(e) {
        if (e.target.name == 'amount') {
            let total = e.target.value * this.sellPrice(this.state.price);
            this.setState({
                amount: e.target.value,
                total: new Intl.NumberFormat('en-US').format(total)
            })
        } if (e.target.name == 'price') {
            this.setState({
                price: new Intl.NumberFormat('en-US').format(this.sellPrice(e.target.value))
            })
        } if (e.target.name == 'total') {
            this.setState({
                total: new Intl.NumberFormat('en-US').format(this.sellPrice(e.target.value))
            })
        }

    }
    handleCheck(e) {
        this.setState({
            checked: e.target.checked,
            name: '',
            coinname: '',
            icon: '',
            amount: '',
            total: '',
            price: '',
            coinSelectError: false, error: false, priceError: false
        });
    }
    handlesubmitbuy() {
        //Validations------>>
        if (!this.state.name) {
            this.setState({ coinSelectError: true, error: false, priceError: false })
            return
        } else if (!this.state.amount) {
            this.setState({ error: true, coinSelectError: false, priceError: false })
            return
        } else if (!this.state.price) {
            this.setState({ error: false, coinSelectError: false, priceError: true })
            return
        } else if (!this.state.total) {
            this.setState({ error: false, coinSelectError: false, priceError: true })
            return
        }
        this.setState({ error: false, coinSelectError: false, priceError: false })
        //endd Validations------>>

        if (this.state.amount != '') {
            this.setState({ error: false })
            let data = {
                name: this.state.name,
                symbol: this.state.coinname,
                icon: this.state.icon,
                buy_status: true,
                sell_status: false,
                price: this.state.price,
                amount: this.state.amount
            }
            data['buy_section'] = true;
            data['sell_section'] = false;
            const accessToken = localStorage.getItem("exchangefiatUserPanel");
        if (!accessToken) {
            this.props.history.push('/login')
        } else {
            this.props.history.push(`/buycoin/${this.state.symbol}`)
        }
        } else {
            this.setState({ error: true })
        }
    }
    handlesubmitsell() {
        //Validations------>>
        if (!this.state.name) {
            this.setState({ coinSelectError: true, error: false, priceError: false })
            return
        } else if (!this.state.amount) {
            this.setState({ error: true, coinSelectError: false, priceError: false })
            return
        } else if (!this.state.price) {
            this.setState({ error: false, coinSelectError: false, priceError: true })
            return
        } else if (!this.state.total) {
            this.setState({ error: false, coinSelectError: false, priceError: true })
            return
        }
        this.setState({ error: false, coinSelectError: false, priceError: false })
        //endd Validations------>>

        if (this.state.amount != '') {
            this.setState({ error: false })
            let data = {
                name: this.state.name,
                symbol: this.state.coinname,
                icon: this.state.icon,
                buy_status: true,
                sell_status: false,
                price: this.state.price,
                amount: this.state.amount
            }

            const accessToken = localStorage.getItem("exchangefiatUserPanel");
            if (!accessToken) {
                this.props.history.push('/login')
            } else {
                this.props.history.push(`/sellcoin/${this.state.symbol}`)
            }
        } else {
            this.setState({ error: true })
        }
    }
    render() {
        let asset
        if (!this.state.checked) {
            asset = this.props.wallets && this.props.wallets.filter(coin => coin.buy_status)
            
        } else {
            asset = this.props.wallets && this.props.wallets.filter(coin => coin.sell_status)
        }

        return (
            <div>
                <div className="container">
                    <div className="create-buy-heading">
                        <h1>{window.i18n('BuysellOrderCreate')}</h1>
                    </div>
                    <div className="row create-buy-margin">
                        <div className="col-lg-6 ">
                            <img src="/images/create-buy-sell.png" className="create-img" />
                        </div>

                        <div className="col-lg-6 notAnimated animateBlock rightAlign right">
                            <div className="create-form-div ">
                                <div className="create-group">
                                    <select type="text" className="create-input" id="sel1" value={this.state.coinname} onChange={(e) => this.handlecoin(e)} required>
                                        <option disabled value="">- {window.i18n('BuysellOrderSelectCoin')} -</option>
                                        {(() => {
                                            const option = [];
                                            if (asset) {
                                                for (let i = 0; i < asset.length; i++) {
                                                    option.push(
                                                        <option key={`animate${i}`} value={asset[i].symbol}>{asset[i].name.toUpperCase()} {'(' + asset[i].symbol + ')'}</option>
                                                    );
                                                }
                                                return option;
                                            }
                                        })()}
                                    </select>
                                    {/* <label class="create-label"><img src="/images/bit-icon.png"/> BITCOIN(BTC)</label> */}
                                </div>


                                <div className="toggle-btn-div">
                                    <div className="button r" id="button-1">
                                        <p className="buyCryptoName" > {window.i18n('buyCrypto')}  </p>
                                        <p className="sellCryptoName" >{window.i18n('sellCrypto')}</p>
                                        <input type="checkbox" className="checkbox" onChange={(e) => this.handleCheck(e)}
                                            defaultChecked={this.state.checked} />ON
                                        <div className="knobs"></div>
                                        <div className="layer"></div>
                                    </div>

                                </div>

                                {/* <form onSubmit={this.handlesubmit}> */}
                                
                                {this.state.checked ?
                                <div className="create-group">
                                <input type="number" className="create-input" name="amount" value={this.state.amount} onChange={(e) => this.handlesell(e)} required />
                                <span className="bar">{this.state.coinname}</span>
                                <label className="create-label">{window.i18n('BuysellOrderAmount')}</label>
                            </div>
                                :
                                <div className="create-group">
                                    <input type="number" className="create-input" name="amount" value={this.state.amount} onChange={(e) => this.handlebuy(e)} required />
                                    <span className="bar">{this.state.coinname}</span>
                                    <label className="create-label">{window.i18n('BuysellOrderAmount')}</label>
                                </div>
                                }

                                <div className="create-group">
                                <NumberFormat thousandSeparator={true} thousandsGroupStyle="thousand" className="create-input" name="price" value={this.state.price} onChange={(e) => this.handlebuysell(e)} required/>
                                    {/* <input type="number" className="create-input" name="price" value={this.state.price} onChange={(e) => this.handlebuysell(e)} required /> */}
                                    <span className="bar">{this.state.currencyType}</span>
                                    <label className="create-label">{window.i18n('BuysellOrderPrice')}</label>
                                </div>
                                <div className="create-group">
                                <NumberFormat thousandSeparator={true} thousandsGroupStyle="thousand" className="create-input" name="total" value={this.state.total} onChange={(e) => this.handlebuysell(e)} required/>
                                    {/* <input type="number" className="create-input" name="total" value={this.state.total} onChange={(e) => this.handlebuysell(e)} required /> */}
                                    <span className="bar">{this.state.currencyType}</span>
                                    <label className="create-label">{window.i18n('BuysellOrderTotal')}</label>
                                </div>
                                {this.state.error && <><div className="error_msg"><p>{window.i18n('BuysellOrderEnterAmount')}</p></div> </>}
                                {this.state.coinSelectError && <><div className="error_msg"><p>{window.i18n('BuysellOrderSelectCoin')}</p></div> </>}
                                {this.state.priceError && <><div className="error_msg"><p>{window.i18n('BuysellOrderEnterPrice')}</p></div> </>}
                                <div className="buy-btc-div">
                                    {this.state.checked ?
                                        <button className="sell-btc-btn" name="sell" onClick={() => this.handlesubmitsell()}>
                                            {window.i18n('BuysellOrderSELL')} {this.state.coinname}
                                        </button>
                                        :
                                        <button className="buy-btc-btn" name="buy" onClick={() => this.handlesubmitbuy()}>
                                          {window.i18n('BuysellOrderBUY')} {this.state.coinname}
                                        </button>
                                    }
                                </div>
                                {/* </form> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default withRouter(index)

const mapStateToProps = createStructuredSelector({
    wallets: makeSelectMyWallets(),
    loading: makeSelectMyWalletsLoading(),
    error: makeSelectMyWalletsError(),
    success: makeSelectMyWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(index);

