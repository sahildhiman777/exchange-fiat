import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMywallets = state => state.landingpage || initialState;
const makeSelectMyWallets = () =>
  createSelector(selectMywallets, landingpage => landingpage.wallets);
const makeSelectMyWalletsError = () =>
  createSelector(selectMywallets, landingpage => landingpage.Error);
const makeSelectMyWalletsLoading = () =>
  createSelector(selectMywallets, landingpage => landingpage.Loading);
const makeSelectMyWalletsSuccess = () =>
  createSelector(selectMywallets, landingpage => landingpage.Success);

  const makeSelectBanks = () =>
  createSelector(selectMywallets, landingpage => landingpage.banks);
  const makeSelectCurrency = () =>
  createSelector(selectMywallets,landingpage => landingpage.currency);
const makeSelectError = () =>
  createSelector(selectMywallets, landingpage => landingpage.bankerror);
const makeSelectLoading = () =>
  createSelector(selectMywallets,landingpage => landingpage.bankloading);
const makeSelectSuccess = () =>
  createSelector(selectMywallets,landingpage => landingpage.banksuccess);

  const makeSelectSellBank = () =>
  createSelector(selectMywallets, landingpage => landingpage.sellbank);
const makeSelectSellBankError = () =>
  createSelector(selectMywallets, landingpage => landingpage.sellbankerror);
const makeSelectSellBankLoading = () =>
  createSelector(selectMywallets, landingpage => landingpage.sellbankloading);
const makeSelectSellBankSuccess = () =>
  createSelector(selectMywallets, landingpage => landingpage.sellbanksuccess);


  export {
    selectMywallets,
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
    makeSelectBanks,
    makeSelectError,
    makeSelectLoading,
    makeSelectSuccess,
    makeSelectCurrency,
    makeSelectSellBank,
    makeSelectSellBankError,
    makeSelectSellBankLoading,
    makeSelectSellBankSuccess,
  }  