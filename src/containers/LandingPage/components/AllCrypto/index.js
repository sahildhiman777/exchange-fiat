import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
    selectMywallets,
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
} from './selectors';

import { wallets } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import Config from '../../../../utils/config';
import OwlCarousel from 'react-owl-carousel2';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';
// import 'react-owl-carousel2/style.css';

class Allcrypto extends Component {

    buyPrice = (price) => {
        return Number(price) + (Number(price) * (Number(Config.buyFee) / 100))
    }

    sellPrice = (price) => {
        return Number(price) - (Number(price) * (Number(Config.sellFee) / 100))
    }


    componentWillMount() {
        this.props.dispatch(wallets());
    }
    // function Allcrypto () {
    handlebuy(data) {
        const accessToken = localStorage.getItem("exchangefiatUserPanel");
        // data['buy_section'] = true;
        // data['sell_section'] = false;
        // localStorage.setItem("homeBuyData", JSON.stringify(data));
        if (!accessToken) {
            this.props.history.push('/login')
        } else {
            // this.props.history.push('/buycoin')
            this.props.history.push(`/buycoin/${data.symbol}`)
        }

    }
    handlesell(data) {
        const accessToken = localStorage.getItem("exchangefiatUserPanel");
        if (!accessToken) {
            this.props.history.push('/login')
        } else {
            this.props.history.push(`/sellcoin/${data.symbol}`)
        }
    }
    render() {
        let asset = this.props.wallets;
        const options = {
            items: 3,
            nav: false,
            rewind: true,
            autoplay: true,
            // responsiveClass: true,
            responsive: {
                320: {
                    items: 1
                },
                480: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        };
        return (

            <div>
                <div className="container">
                    <div className="all-crypto-heading">
                        <h1>{window.i18n('AllCrypto')}</h1>
                        <p>{window.i18n('AllCryptoClickOnOne')}</p>
                    </div>
                    <section>
                        <OwlCarousel options={options} >
                            {(() => {
                                const option = [];
                                if (asset) {
                                    for (let i = 0; i < asset.length; i++) {
                                        option.push(
                                            <div key={`carouselCoin${i}`} className="item">
                                                <div className="main-card-div">
                                                    <div className="row">
                                                        <div className="col-md-7 col-sm-5 col-xs-12">
                                                            <div className="card-icon">
                                                                <img src={require('../../../../images/' + asset[i].icon + '.png')} />
                                                                <p>{asset[i].name.toUpperCase()} {(asset[i].symbol)}</p>
                                                            </div>
                                                        </div>
                                                        <div className="col-md-5 col-sm-5 col-xs-12 ">
                                                            <div className="card-pink">
                                                                {
                                                                    asset[i].sell_status ?
                                                                        <a className="home_button" name="sell" onClick={() => this.handlesell(asset[i])}> {window.i18n('AllCryptoSellCrypto')} <br /> {new Intl.NumberFormat('en-US').format(this.sellPrice(asset[i].price))}</a>
                                                                        :
                                                                        <a className="home_button disabled" name="sell"> {window.i18n('AllCryptoSellCrypto')} <br /> {new Intl.NumberFormat('en-US').format(this.sellPrice(asset[i].price))}</a>
                                                                }
                                                            </div>
                                                            <div className="card-green">
                                                                {
                                                                    asset[i].buy_status ?
                                                                        <a className="home_button" name="buy" disabled={true} onClick={() => this.handlebuy(asset[i])}> {window.i18n('AllCryptoBuyCrypto')}<br /> {new Intl.NumberFormat('en-US').format(this.buyPrice(asset[i].price))}</a>
                                                                        :
                                                                        <a className="home_button disabled" name="buy" disabled={true}>
                                                                            {window.i18n('AllCryptoBuyCrypto')}<br /> {new Intl.NumberFormat('en-US').format(this.buyPrice(asset[i].price))}
                                                                        </a>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        );
                                    }
                                    return option;
                                } else {
                                    return (<div><p>{window.i18n('AllCryptoLoading')}</p></div>)
                                }
                            })()}
                        </OwlCarousel>
                        {/* <div className="owl-carousel owl-theme">
        {(() => {
            const options = [];
                    if (asset) {
                        for (let i = 0; i < asset.length; i++) {
                            options.push(
                                <div className="item">
                                <div className="main-card-div">
                                <div className="row">
                                    <div className="col-lg-7  col-7 card-pr">
                                        <div className="card-icon">
                                        <img src={require('../../../../images/' + asset[i].icon)} />
                                <p>{asset[i].name.toUpperCase()} {(asset[i].symbol)}</p>
                                        </div>
                                    </div>
                                    <div className="col-lg-5 col-sm-5 col-5 card-pl card-pr">
                                        <div className="card-pink">
                                            <a href="/dashboard" className="home_button">{window.i18n('sellCrypto')}<br/> ${asset[i].price}</a>
                                        </div>
                                        <div className="card-green">
                                            <a href="/dashboard"  className="home_button">{window.i18n('buyCrypto')}<br/> ${asset[i].price}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             </div>
                         );
                    }
                    return options;
                  }
                })()}
      </div> */}
                    </section>

                </div>
            </div>
        )

    }
}


// export default withRouter(Allcrypto)

const mapStateToProps = createStructuredSelector({
    wallets: makeSelectMyWallets(),
    loading: makeSelectMyWalletsLoading(),
    error: makeSelectMyWalletsError(),
    success: makeSelectMyWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(Allcrypto);

