import { call, put, takeLatest } from 'redux-saga/effects';
import {  WALLETS, GET_BANKS, GET_SELL_BANKS} from './constants';
import { wallets, walletsSuccess, walletsFailed, getBanksFailed, getBanksSuccess, getSellBanksSuccess, getSellBanksFailed} from './actions';
import request from '../../../../utils/request';
import { requestSecure} from '../../../../utils/request';
import Define from '../../../../utils/config';


export function* myWallets(action) {
  let currency = localStorage.getItem('currency') || "VND";
  const requestURL = `${Define.API_URL}currency/get_wallets?currency=${currency.toLowerCase()}`;
    const options = {
        method: 'GET',
        body: JSON.stringify(action.data),
    }
    try{
      const res = yield call(request, requestURL, options);
      yield put(walletsSuccess(res.message));
      
      
    }catch (err) {
      yield put(walletsFailed(err));
  }
}

export function* getBanks() {
  const requestURL = `${Define.API_URL}banks/list`;
  const options = {
    method: 'GET',
  }

  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getBanksSuccess(res.data));
    } else {
      yield put(getBanksFailed(res.data));
    }
  } catch (err) {
    yield put(getBanksFailed(err));
  }
}

export function* getSellbank() {
  const requestURL = `${Define.API_URL}userbank/bank_list`;
  const options = {
    method: 'GET',
  }
  try {
    const res = yield call(requestSecure, requestURL, options);
    if (res.status) {
      yield put(getSellBanksSuccess(res.data));
    } else {
      yield put(getSellBanksFailed(res.data));
    }
  } catch (err) {
    yield put(getSellBanksFailed(err));
  }
}
  
/**
* Root saga manages watcher lifecycle
*/
export default function* appData() {
  yield takeLatest(WALLETS, myWallets);
  yield takeLatest(GET_BANKS, getBanks);
  yield takeLatest(GET_SELL_BANKS, getSellbank);
}