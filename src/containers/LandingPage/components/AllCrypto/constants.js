export const WALLETS = 'exchangefiatUserPanel/wallets/WALLETS';
export const WALLETS_SUCCESS = 'exchangefiatUserPanel/wallets/WALLETS_SUCCESS';
export const WALLETS_ERROR = 'exchangefiatUserPanel/wallets/WALLETS_ERROR';

export const GET_BANKS = 'exchangefiatUserPanel/Banks/GET_ALL_BANKS';
export const GET_BANKS_SUCCESS = 'exchangefiatUserPanel/Banks/GET_BANKS_SUCCESS';
export const GET_BANKS_ERROR = 'exchangefiatUserPanel/Banks/GET_BANKS_ERROR';

export const GET_SELL_BANKS = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS';
export const GET_SELL_BANKS_SUCCESS = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS_SUCCESS';
export const GET_SELL_BANKS_ERROR = 'exchangefiatUserPanel/Banks/GET_SELL_BANKS_ERROR';