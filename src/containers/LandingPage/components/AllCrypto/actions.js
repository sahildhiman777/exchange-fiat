import {
  WALLETS,
  WALLETS_ERROR,
  WALLETS_SUCCESS,
  GET_BANKS,
  GET_BANKS_ERROR,
  GET_BANKS_SUCCESS,
  GET_SELL_BANKS,
  GET_SELL_BANKS_ERROR,
  GET_SELL_BANKS_SUCCESS,
} from './constants';

export function wallets(data) {
  return { type: WALLETS, data };
}
export function walletsSuccess(data) {
  return { type: WALLETS_SUCCESS, data };
}
export function walletsFailed(error) {
  return { type: WALLETS_ERROR, error };
}

export function getBanks() {
  console.log('hele......');
  
  return {
    type: GET_BANKS,
  };
}
export function getBanksSuccess(data) {
  return {
    type: GET_BANKS_SUCCESS,
    data,
  };
}
export function getBanksFailed(error) {
  return {
    type: GET_BANKS_ERROR,
    error,
  };
}

export function getSellBanks() {
  return {
    type: GET_SELL_BANKS,
  };
}
export function getSellBanksSuccess(data) {
  return {
    type: GET_SELL_BANKS_SUCCESS,
    data,
  };
}
export function getSellBanksFailed(error) {
  return {
    type: GET_SELL_BANKS_ERROR,
    error,
  };
}