import produce from 'immer';
import { WALLETS, WALLETS_ERROR, WALLETS_SUCCESS,GET_BANKS,
    GET_BANKS_ERROR,
    GET_BANKS_SUCCESS,
    GET_SELL_BANKS,
    GET_SELL_BANKS_SUCCESS,
    GET_SELL_BANKS_ERROR,
} from './constants';

// The initial state of the App
export const initialState = {
   
  wallets: false,
  success: false,
  error: false,
  loading: false,
  banks:[],
  currency: [],
  banksuccess: false,
  bankerror: false,
  bankloading: false,
  sellbank: [],
  sellbanksuccess: false,
  sellbankerror: false,
  sellbankloading: false,
};


/* eslint-disable default-case, no-param-reassign */
const walletsReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {

            case WALLETS:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.wallets = false;
                break;

            case WALLETS_SUCCESS:
                draft.wallets = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case WALLETS_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;

            case GET_BANKS:
                draft.bankloading = true;
                break;

            case GET_BANKS_SUCCESS:
                draft.banks = action.data ? action.data : [];
                draft.currency = action.data.currency ? action.data.currency : [];
                draft.bankerror = false;
                draft.banksuccess = true;
                draft.bankloading = false;
                break;

            case GET_BANKS_ERROR:
                draft.bankerror = action.error.toString();
                draft.banksuccess = false;
                draft.bankloading = false;
                draft.banks = [];
                break;

            case GET_SELL_BANKS:
                draft.sellbank = [];
                draft.sellbankerror = false;
                draft.sellbanksuccess = false;
                draft.sellbankloading = true;
                break;

            case GET_SELL_BANKS_SUCCESS:
                draft.sellbank = action.data;
                draft.sellbankerror = false;
                draft.sellbanksuccess = true;
                draft.sellbankloading = false;
                break;

            case GET_SELL_BANKS_ERROR:
                draft.sellbankerror = action.error.toString();
                draft.sellbanksuccess = false;
                draft.sellbankloading = false;
                draft.sellbank = [];
                break;

                }
    });

export default walletsReducer;
