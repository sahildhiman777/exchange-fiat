import { call, put, takeLatest } from 'redux-saga/effects';
import {  WALLETS } from './constants';
import {  wallets, walletsSuccess, walletsFailed } from './actions';
import request from '../../../../utils/request';
import Define from '../../../../utils/config';


export function* myWallets(action) {
  console.log(action,'action');
  
  const requestURL = `${Define.API_URL}currency/get_wallets`;
    const options = {
        method: 'GET',
    }
    try{
      const res = yield call(request, requestURL, options);
      yield put(walletsSuccess(res.message));
    }catch (err) {
      yield put(walletsFailed(err));
  }
}

  
/**
* Root saga manages watcher lifecycle
*/
export default function* appData() {
  yield takeLatest(WALLETS, myWallets);
}