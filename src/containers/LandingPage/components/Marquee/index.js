import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import {
  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,
} from '../AllCrypto/selectors';

import reducer from '../AllCrypto/reducer';
import saga from '../AllCrypto/saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import Marquee from "react-smooth-marquee";


// class index extends Component
class index extends Component {
  componentDidMount() {
  }
  componentWillReceiveProps(nextprops) {
  }

  render() {
    let asset = this.props.wallets;
    return (
      <div>
        <div className="grid grid--container marquee">
          <div className='marque_main'>
            <Marquee>
              {(() => {
                const options = [];
                if (asset) {

                  for (let i = 0; i < asset.length; i++) {
                    options.push(
                      <div key={`marqueoptions${i}`} className="mrq_link">
                        <img width="20px;" className="img-responsive coin_img" src={require('../../../../images/' + asset[i].icon + '.png')} />
                        <span className="cName">{asset[i].symbol}</span>
                        <span className="growth green"> {new Intl.NumberFormat('en-US').format(asset[i].highPrice)}<i className="fa fa-arrow-up" aria-hidden="true"></i></span>
                        <span className="growth red"> {new Intl.NumberFormat('en-US').format(asset[i].lowPrice)}<i className="fa fa-arrow-down" aria-hidden="true"></i></span>
                      </div>
                    );
                  }
                  return options;
                }
              })()}
            </Marquee>
          </div>
        </div>
      </div>
    )
  }

}


const mapStateToProps = createStructuredSelector({
  wallets: makeSelectMyWallets(),
  loading: makeSelectMyWalletsLoading(),
  error: makeSelectMyWalletsError(),
  success: makeSelectMyWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
  return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(index);