import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectMywallets = state => state.mywallets || initialState;
const makeSelectMyWallets = () =>
  createSelector(selectMywallets, mywallets => mywallets.wallets);
const makeSelectMyWalletsError = () =>
  createSelector(selectMywallets, mywallets => mywallets.Error);
const makeSelectMyWalletsLoading = () =>
  createSelector(selectMywallets, mywallets => mywallets.Loading);
const makeSelectMyWalletsSuccess = () =>
  createSelector(selectMywallets, mywallets => mywallets.Success);

  export {
    selectMywallets,
    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
  }  