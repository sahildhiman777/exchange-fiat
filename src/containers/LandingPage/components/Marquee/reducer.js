import produce from 'immer';
import { WALLETS, WALLETS_ERROR, WALLETS_SUCCESS } from './constants';

// The initial state of the App
export const initialState = {
  wallets: false,
  success: false,
  error: false,
  loading: false,
};


/* eslint-disable default-case, no-param-reassign */
const walletsReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {

            case WALLETS:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.wallets = false;
                break;

            case WALLETS_SUCCESS:
                draft.wallets = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case WALLETS_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;
        }
    });

export default walletsReducer;
