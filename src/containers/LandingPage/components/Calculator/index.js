import React, { Component } from 'react';
import Config from '../../../../utils/config'

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: 'BTC',
            target: 'BTC',
            exchanging: '',
            recieving: '',
            error: '',
        }
    }

    handlecoin(e) {
        e.preventDefault();
        let source = '';
        let target = '';
        if (e.target.name === 'source') {
            source = e.target.value;
            this.setState({ source: e.target.value });
        }
        if (e.target.name === 'target') {
            target = e.target.value;
            this.setState({ target: e.target.value });
        }
    }
    handlevalue(e) {
        e.preventDefault();
        if (e.target.name === 'exchanging') {
            this.setState({ exchanging: e.target.value });
        }
        if (e.target.name === 'recieving') {
            this.setState({ recieving: e.target.value });
        }
    }
    handleconvert(e) {
        e.preventDefault();
        let source = this.state.source;
        let target = this.state.target;

        fetch(`https://api.cryptonator.com/api/ticker/${source}-${target}`)
            .then(res => res.json())
            .then(
                (result) => {
                    if (result.error != '') {
                        this.setState({
                            error: result.error, recieving: ''
                        });
                    }
                    if (result.success) {
                        let price = result.ticker.price;
                        let value = this.state.exchanging;
                        let total = price * value;
                        this.setState({
                            recieving: total, error: ''
                        });

                    }

                },
            )

    }
    render() {
        let asset = Config.coinlist;
        return (
            <div>
                <div className="crypto-converter-bg">
                    <div className="container">
                        <div className="crypto-converter-heading">
                            <p>{window.i18n('Calculator')}</p>
                            <h1>{window.i18n('CalculatorCryptoConverter')}</h1>
                        </div>
                        <div className="row">
                            <div className="col-lg-6 notAnimated animateBlock leftAlign left">
                                <div className="crypto-form-div">
                                    <div className="create-group">
                                        <input type="text" className="create-input" name="exchanging" value={this.state.exchanging} onChange={(e) => this.handlevalue(e)} required />
                                        <label className="create-label">{window.i18n('CalculatorExchanging')}</label>
                                        <select className="source" name="source" onChange={(e) => this.handlecoin(e)}>
                                            {(() => {
                                                const option = [];
                                                if (asset) {
                                                    for (let i = 0; i < asset.length; i++) {
                                                        option.push(
                                                            <option key={`options${i}`} value={asset[i].symbol.toLowerCase()}>{asset[i].symbol}</option>
                                                        );
                                                    }
                                                    return option;
                                                }
                                            })()}
                                        </select>
                                    </div>
                                    <div className="create-group">
                                        <input type="text" className="create-input" name="recieving" value={this.state.recieving} onChange={(e) => this.handlevalue(e)} required/>
                                        <label className="create-label">{window.i18n('CalculatorReceiving')}</label>
                                        <select className="target" name="target" onChange={(e) => this.handlecoin(e)}>
                                            {(() => {
                                                const option = [];
                                                if (asset) {
                                                    for (let i = 0; i < asset.length; i++) {
                                                        option.push(
                                                            <option key={`options2${i}`} value={asset[i].symbol.toLowerCase()}>{asset[i].symbol}</option>
                                                        );
                                                    }
                                                    return option;
                                                }
                                            })()}
                                        </select>
                                    </div>
                                    {this.state.error &&
                                        <>
                                            <p className="convert_error">{this.state.error}</p>
                                        </>
                                    }
                                    <div className="crypto-converter-div"><button className="crypto-converter-btn" onClick={(e) => this.handleconvert(e)}>{window.i18n('CalculatorCONVERTNOW')}</button> </div>
                                </div>
                            </div>
                            <div className="col-lg-6 notAnimated animateBlock rightAlign right">
                                <img src="/images/crypto-converter.png" className="crypto-converter-img" width="100%" />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default index
