import React, { Component } from 'react'
import { connect } from 'react-redux';
import { compose } from 'redux';

import { wallets, getBanks } from '../AllCrypto/actions';
import reducer from '../AllCrypto/reducer';
import saga from '../AllCrypto/saga';
import injectReducer from '../../../../utils/injectReducer';
import injectSaga from '../../../../utils/injectSaga';
import MailchimpSubscribe from "react-mailchimp-subscribe";

const CustomForm = ({ status, message, onValidated }) => {
    let email, name;
    const submit = () =>
      email &&
      email.value.indexOf("@") > -1 &&
      onValidated({
        EMAIL: email.value,
      });
  
    return (
        <>
      <div className="subscrib_form">
        {/* {status === "sending" && <div className="subscrib_status" style={{ color: "blue" }}>sending...</div>}
        {status === "error" && (
          <div className="subscrib_status"
            style={{ color: "red" }}
            dangerouslySetInnerHTML={{ __html: message }}
          />
        )}
        {status === "success" && (
          <div className="subscrib_status"
            style={{ color: "green" }}
            dangerouslySetInnerHTML={{ __html: message }}
          />
        )} */}
        <div className=""><input
          ref={node => (email = node)}
          type="email"
          className="form-control input-email" 
          placeholder="E-mail"
        /></div>
        <div className=" sub-pl">
        <button onClick={submit} className="subscribe-btn">{window.i18n('FooterSubscribe')}</button>
        </div>
      </div>
      <br/>
      <div className="">
      {status === "sending" && <div className="subscrib_status">sending...</div>}
        {status === "error" && (
          <div className="subscrib_status" dangerouslySetInnerHTML={{ __html: message }}
          />
        )}
        {status === "success" && (
          <div className="subscrib_status" dangerouslySetInnerHTML={{ __html: message }}
          />
        )}
      </div>
      </>
    );
  };


class LandingFooter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currency: '',

        }
    }
    componentDidMount() {
        const currency = localStorage.getItem('currency') || "VND"
        this.setState({ currency: currency });
        localStorage.setItem('currency', currency)
    }
    handlecurrency = (e) => {
        this.setState({
            currency: e.target.value
        }, () => {
            localStorage.setItem('currency', this.state.currency)
            this.props.dispatch(getBanks());
            this.props.dispatch(wallets());
        })
    }
    render() {
        const url = "https://jster.us7.list-manage.com/subscribe/post?u=ed40c0084a0c5ba31b3365d65&id=ec6f32bf5e";
        return (
            <div>
                <div className="footer-bg">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4">
                                <div>
                                    <a href="#"><img src="/images/site_logo.png" className="footer-logo" /> </a>
                                    <span className="footer-siteseal-img"><img src="/images/siteseal.png" /></span>
                                    <p className="footer-p">{window.i18n('FooterFastSeured')}</p>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="footer-menu-heading">
                                    <h4>{window.i18n('FooterMainMenu')}</h4></div>
                                <div className="row">

                                    <div className="col-lg-6 col-6">
                                        <div className="footer-main-menu">

                                            <ul>
                                                <li>{window.i18n('FooterHome')}</li>
                                                <li>{window.i18n('FooterNews')}</li>
                                                <li>{window.i18n('FooterAboutUs')}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-lg-6 col-6">
                                        <div className="footer-menu1">
                                            <ul>
                                                <li>{window.i18n('FooterFAQ')}</li>
                                                <li>{window.i18n('FooterContact')}</li>
                                                <li>{window.i18n('FooterContact')}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-12">
<div className="footer-subscribe">
    <h4>{window.i18n('FooterSubscribe')}</h4>
    <div className="subscribe-div">
    <MailchimpSubscribe
url={url}
render={({ subscribe, status, message }) => (
<CustomForm
status={status}
message={message}
onValidated={formData => subscribe(formData)}
/>
)}
/>
        {/* <div className="">
             <input type="email" className="form-control input-email" placeholder="E-mail" />
        </div>
        <div className=" sub-pl">
            <button className="subscribe-btn">{window.i18n('FooterSubscribe')}</button>
            </div> */}
    </div>
    <p>{window.i18n('FooterNewLetter')}</p>
</div>
<div className="currency_div">
    <select className="currency_button" value={this.state.currency} onChange={(e) => this.handlecurrency(e)}>
        <option value="VND">VND</option>
        <option value="USD">USD</option>
    </select>
</div>
</div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(LandingFooter);
