import React from 'react'

export default function index() {
    return (
        <div>
            <div className="fiat-bg">
    <div className="container">
        <div className="row">
            <div className="col-lg-6 notAnimated animateBlock leftAlign left">
                <div className="fiat-heading">
                    <h1> {window.i18n('CurrencySec.Title')} </h1>
                    <p className="">{window.i18n('CurrencySec.ParaGraph')}</p>
                </div>
            </div>
            <div className="col-lg-6 fiat-img-margin notAnimated animateBlock rightAlign right">
                <img src="/images/fiat-img.png" className="fiat-img" />
            </div>
        </div>
    </div>
</div>
        </div>
    )
}
