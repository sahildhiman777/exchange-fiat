export const LOGIN = 'exchangefiatUserPanel/Login/LOGIN';
export const LOGIN_SUCCESS = 'exchangefiatUserPanel/Login/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'exchangefiatUserPanel/Login/LOGIN_ERROR';
export const LOGIN_2FA = 'exchangefiatUserPanel/Login/LOGIN_2FA';
export const LOGIN_2FA_SUCCESS = 'exchangefiatUserPanel/Login/LOGIN_2FA_SUCCESS';
export const LOGIN_2FA_ERROR = 'exchangefiatUserPanel/Login/LOGIN_2FA_ERROR';