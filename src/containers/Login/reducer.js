import produce from 'immer';
import { LOGIN, LOGIN_ERROR, LOGIN_SUCCESS, LOGIN_2FA, LOGIN_2FA_SUCCESS, LOGIN_2FA_ERROR } from './constants';

// The initial state of the App
export const initialState = {
    user: false,
    success: false,
    error: false,
    loading: false,
    twofaSuccess: false,
    twofaError: false,
};
/* eslint-disable default-case, no-param-reassign */
const loginReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case LOGIN:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.user = false;
                break;

            case LOGIN_SUCCESS:
                draft.user = action.data.user;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case LOGIN_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;
            case LOGIN_2FA:
                draft.error = false;
                draft.twofaSuccess = false;
                draft.loading = true;
                break;
            case LOGIN_2FA_SUCCESS:
                draft.twofaError = false;
                draft.twofaSuccess = true;
                draft.loading = false;
                break;
            case LOGIN_2FA_ERROR:
                draft.twofaError = action.data.toString();
                draft.twofaSuccess = false;
                draft.loading = false;
                break;
        }
    });

export default loginReducer;
