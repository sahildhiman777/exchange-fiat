import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser, makeSelect2faSuccess, makeSelect2faError } from './selectors';
import { login, loginOtp } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import { Link } from "react-router-dom";
import ReactLoading from "react-loading";

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            otp: '',
            email_otp: '',
            error: false,
            langChanged: 0
        }

        this.handleChange = this.handleChange.bind(this);
        this.submit = this.submit.bind(this);
        this.submitOtp = this.submitOtp.bind(this);
    }

    componentWillMount() {
        let body = document.getElementsByTagName("body");
        body = body[0];
        body.classList.add("back-clr");
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ blockloader: nextProps.loading });

        if (nextProps.user) {
            if (nextProps.user.google2fa) {
                if (nextProps.twoFaSuccess) {
                    window.location.href = "/dashboard"
                }
            } else if (nextProps.user.email_2fa) {
                if (nextProps.twoFaSuccess) {
                    console.log(nextProps.user, 'user next');

                    window.location.href = "/dashboard"
                }
            } else {
                window.location.href = "/dashboard"
            }
        }
    }

    validateForm = () => {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = (e) => {
        let otpname = e.target.name;
        this.setState({
            [e.target.name]: e.target.value

        }, () => {
            if (this.state.otp.length >= 6) {
                this.submitOtp(otpname);
            } if (this.state.email_otp.length >= 6) {
                this.submitOtp(otpname);
            }
        });
    }

    submit = (e) => {
        e.preventDefault();
        const { email, password } = this.state;
        const data = {
            email: email,
            password: password
        }
        this.props.dispatch(login(data))
        this.setState({
            email: '', password: '', blockloader: true
        })
    }

    submitOtp(otptype) {
        if (otptype == 'email_otp') {
            // otptype && otptype.preventDefault();
            const otp = this.state.email_otp;
            this.props.dispatch(loginOtp({ userToken: otp, otptype: otptype }));
            this.setState({ otp: '', blockloader: true })
        } else {
            // otptype && otptype.preventDefault();
            const otp = this.state.otp;
            this.props.dispatch(loginOtp({ userToken: otp, otptype: otptype }));
            this.setState({ otp: '', blockloader: true })
        }
    }

    changeLanguage = (e) => {
        this.props.changeLanguage(e);
        this.setState({
            langChanged: Date.now()
        })
    }


    render() {
        return (
            <div  >
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <div className="container">
                    {/* <!-- Outer Row --> */}
                    <div className="row justify-content-center">
                        <div className="col-xl-10 col-lg-12 col-md-9">
                            <div className="card o-hidden border-0 shadow-lg my-5">
                                <div className="text-right">
                                    <div className="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src={`/images/${window.i18nCode}.png`} />
                                        </button>
                                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                            <a className="dropdown-item text-right" href="#" data-language="vn" onClick={this.changeLanguage}>
                                                Việt Nam &nbsp; <img data-language="vn" src="/images/vn.png" />
                                            </a>
                                            <a className="dropdown-item text-right" href="#" data-language="en" onClick={this.changeLanguage}>
                                                English &nbsp; <img data-language="en" src="/images/en.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0">
                                    {/* <!-- Nested Row within Card Body -->bg-login-image */}
                                    <div className="row">
                                        <div className="col-lg-6 d-none d-lg-block " >
                                            {this.props.user.google2fa == true ?
                                                <div className="login-data-otp">
                                                    <h2><a href='/'><img src="/images/site-logo-color.png" /></a></h2>
                                                    <h5>{window.i18n('LogInBuySell')}</h5>
                                                </div>
                                                : (this.props.user.email_2fa == true ?
                                                    <div className="login-data-otp">
                                                        <h2><a href='/'><img src="/images/site-logo-color.png" /></a></h2>
                                                        <h5>{window.i18n('LogInBuySell')}</h5>
                                                    </div>
                                                    :
                                                    < div className="login-data">
                                                        <h2><a href='/'><img src="/images/site-logo-color.png" /></a></h2>
                                                        <h5>{window.i18n('LogInBuySell')}</h5>
                                                    </div>)
                                            }

                                        </div>
                                        {/* Login */}
                                        {this.props.user.google2fa == true ?
                                            <div className="col-lg-6">
                                                <div className="p-5" style={{ padding: '7rem !important' }}>
                                                    <div className="text-center">
                                                        <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInGoogleOTP')}</h1>
                                                    </div>
                                                    <form className="user" onSubmit={this.submitOtp}>
                                                        <div className="form-group">
                                                            <input type="number" onChange={this.handleChange} name="otp" className="form-control form-control-user" id="exampleInputPassword" placeholder={`${window.i18n('LogInOTP')}`} value={this.state.otp} required />
                                                        </div>
                                                        <button type="submit" className="btn btn-primary btn-user btn-block">
                                                            {window.i18n('LogIn')}
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                            : (this.props.user.email_2fa == true ?
                                                <div className="col-lg-6">
                                                    <div className="p-5" style={{ padding: '7rem !important' }}>
                                                        <div className="text-center">
                                                            <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInPlaceholder')}</h1>
                                                        </div>
                                                        <form className="user" onSubmit={this.submitOtp}>
                                                            <div className="form-group">
                                                                <input type="number" onChange={this.handleChange} name="email_otp" className="form-control form-control-user" id="exampleInputPassword" placeholder={window.i18n('LogInPlaceholder')} value={this.state.email_otp} required />
                                                            </div>
                                                            <button type="submit" className="btn btn-primary btn-user btn-block">
                                                                {window.i18n('LogIn')}
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                : <div className="col-lg-6">
                                                    <div className="p-5">
                                                        <div className="text-center">
                                                            <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInWelcome')}!</h1>
                                                        </div>
                                                        <form className="user" onSubmit={this.submit} action="">
                                                            <div className="form-group">
                                                                <input onChange={this.handleChange} type="email" className="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" name="email" value={this.state.email} placeholder={`${window.i18n('LogInEmail')}`} required />
                                                            </div>
                                                            <div className="form-group">
                                                                <input onChange={this.handleChange} type="password" className="form-control form-control-user" id="exampleInputPassword" placeholder={`${window.i18n('LogInPaasword')}`} name="password" value={this.state.password} required />
                                                            </div>
                                                            <div className="form-group">
                                                                <div className="custom-control custom-checkbox small">
                                                                    <input type="checkbox" className="custom-control-input" id="customCheck" />
                                                                    <label className="custom-control-label" htmlFor="customCheck">{window.i18n('LogInRememberMe')}</label>
                                                                </div>
                                                            </div>
                                                            <button className="btn btn-primary btn-user btn-block" type='submit' disabled={!this.validateForm()}>{window.i18n('LogIn')}</button>
                                                        </form>
                                                        <hr />

                                                        <div className="text-center">
                                                            <Link to="/forgotpassword" className="small">{window.i18n('LogInForgot')}</Link>
                                                        </div>

                                                        <div className="text-center">
                                                            <Link to="/register" className="small">{window.i18n('LogInCreate')}</Link>
                                                        </div>

                                                    </div>
                                                </div>
                                            )

                                        }

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}


const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    success: makeSelectSuccess(),
    twoFaSuccess: makeSelect2faSuccess(),
    twoFaError: makeSelect2faError(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'login', reducer });
const withSaga = injectSaga({ key: 'login', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(index);

