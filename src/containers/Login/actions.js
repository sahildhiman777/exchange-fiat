
import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGIN_2FA, LOGIN_2FA_SUCCESS, LOGIN_2FA_ERROR, LOGIN_ENABLE_SUCCESS } from './constants';

export function login(data) {
    return { type: LOGIN, data };
}
export function loginSuccess(data) {
    return { type: LOGIN_SUCCESS, data };
}
export function loginFailed(error) {
    return { type: LOGIN_ERROR, error };
}
export function loginOtp(data) {
    return { type: LOGIN_2FA, data };
}
export function loginOtpSuccess(data) {
    return { type: LOGIN_2FA_SUCCESS, data };
}
export function loginOtpFailed(data) {
    return { type: LOGIN_2FA_ERROR, data };
}
