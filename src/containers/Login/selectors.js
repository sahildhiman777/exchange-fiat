/**
 * Homepage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectLogin = state => state.login || initialState;

const makeSelectUser = () =>
    createSelector(selectLogin, loginState => loginState.user);
const makeSelectError = () =>
    createSelector(selectLogin, loginState => loginState.error);
const makeSelectLoading = () =>
    createSelector(selectLogin, loginState => loginState.loading);
const makeSelectSuccess = () =>
    createSelector(selectLogin, loginState => loginState.success);
const makeSelect2faSuccess = () =>
    createSelector(selectLogin, loginState => loginState.twofaSuccess);
const makeSelect2faError = () =>
    createSelector(selectLogin, loginState => loginState.twofaError);

export { selectLogin, makeSelect2faError, makeSelect2faSuccess, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser };
