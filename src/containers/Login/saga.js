/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { LOGIN, LOGIN_2FA } from './constants';
import { loginFailed, loginSuccess, loginOtpSuccess, loginOtpFailed } from './actions';
import { redirect } from '../App/actions';
import request, { requestSecureOtp } from '../../utils/request';
import Define from '../../utils/config';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';

export function* doLogin(action) {
    const requestURL = `${Define.API_URL}login`;
    const options = {
        method: 'POST',
        body: JSON.stringify(action.data),
    }
    try {
        const res = yield call(request, requestURL, options);

        if (res.status) {
            if (res.data.user.google2fa) {
                localStorage.setItem('exchangefiatUserPanelOtpToken', res.data.user.token);
                localStorage.setItem('2faUsername', res.data.user.name);
                yield put(redirect(false));
                yield put(loginSuccess(res.data));
            } else if (res.data.user.email_2fa) {
                localStorage.setItem('exchangefiatUserPanelOtpToken', res.data.user.token);
                localStorage.setItem('2faUsername', res.data.user.name);

                const email2fa_url = `${Define.API_URL}send2fa_email`;
                const post_data = {
                    method: 'POST',
                    body: JSON.stringify({ email: res.data.user.email }),
                }
                try {
                    yield call(request, email2fa_url, post_data);
                } catch (err) {
                    console.log("Error sending email 2fa", err);
                }

                yield put(redirect(false));
                yield put(loginSuccess(res.data));
            } else {
                localStorage.setItem('exchangefiatUserPanel', res.data.user.token);
                yield put(loginSuccess(res.data));
                ToastTopEndSuccessFire("Welcome to ExchangeFiat!, \"" + res.data.user.name + "\" ")
                // Swal.fire({
                //     title: "Welcome to ExchangeFiat!, \"" + res.data.user.name + "\" ",
                //     type: "success",
                //     showConfirmButton: false,
                //     timer: 1000
                // })
            }
        } else {
            yield put(loginFailed(res.message));
            ToastTopEndErrorFire(res.message)
            // Swal.fire({
            //     title: res.message,
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 1000
            // })
        }

    } catch (err) {
        console.log(err);
        ToastTopEndErrorFire("Something went wrong, please try again.")
        // Swal.fire({
        //     title: "Something went wrong, please try again.",
        //     type: "success",
        //     showConfirmButton: false,
        //     timer: 1000
        // })
        yield put(loginFailed(err));
    }
}
export function* doLoginOtp(action) {
    let otpType = action.data.otptype
    let requestURL;
    if (otpType == 'otp') {
        requestURL = `${Define.API_URL}2fa`;
    } else {
        requestURL = `${Define.API_URL}email_2fa`;
    }
    const options = {
        method: 'POST',
        body: JSON.stringify(action.data),
    }
    try {
        const res = yield call(requestSecureOtp, requestURL, options);

        if (res.status) {
            const tokenOtp = localStorage.getItem("exchangefiatUserPanelOtpToken");
            const userName = localStorage.getItem("2faUsername");
            localStorage.setItem('exchangefiatUserPanel', tokenOtp);
            localStorage.removeItem("exchangefiatUserPanelOtpToken");
            localStorage.removeItem("2faUsername");
            yield put(redirect(false));
            yield put(loginOtpSuccess(res.data));
            ToastTopEndSuccessFire("Welcome to ExchangeFiat!, \"" + userName + "\" ")
            // Swal.fire({
            //     title: "Welcome to ExchangeFiat!, \"" + userName + "\" ",
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 1000
            // })
        } else {
            yield put(loginOtpFailed(res.data));
            ToastTopEndErrorFire(res.data)
            // Swal.fire({
            //     title: res.data,
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 1000
            // });
        }

    } catch (err) {
        console.log(err);
        yield put(loginOtpFailed(err));
        ToastTopEndErrorFire("Something went wrong, please try again.")
        // Swal.fire({
        //     title: "Something went wrong, please try again.",
        //     type: "success",
        //     showConfirmButton: false,
        //     timer: 1000
        // })
    }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* userData() {
    yield takeLatest(LOGIN, doLogin);
    yield takeLatest(LOGIN_2FA, doLoginOtp);
}
