import produce from 'immer';
import { RESETPASSWORD, RESETPASSWORD_SUCCESS, RESETPASSWORD_ERROR, RESETPASSWORD_RESET, VERIFYTOKKEN, VERIFYTOKKEN_SUCCESS, VERIFYTOKKEN_ERROR } from './constants';

// The initial state of the App
export const initialState = {
    user: false,
    success: false,
    error: false,
    loading: false,

    verifyuser: false,
    verifysuccess: false,
    verifyerror: false,
    verifyloading: false,
};
/* eslint-disable default-case, no-param-reassign */
const resetPasswordReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case RESETPASSWORD:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.user = false;
                break;

            case RESETPASSWORD_RESET:
                draft.loading = false;
                draft.error = false;
                draft.success = false;
                draft.user = false;
                break;

            case RESETPASSWORD_SUCCESS:
                draft.user = action.data;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case RESETPASSWORD_ERROR:
                draft.error = action.error.toString();
                draft.success = false;
                draft.loading = false;
                break;

            case VERIFYTOKKEN:
                draft.verifyloading = true;
                draft.verifyerror = false;
                draft.verifysuccess = false;
                draft.verifyuser = false;
                break;

            case VERIFYTOKKEN_SUCCESS:
                draft.verifyuser = action.data;
                draft.verifyerror = false;
                draft.verifysuccess = true;
                draft.verifyloading = false;
                break;

            case VERIFYTOKKEN_ERROR:
                draft.verifyerror = action.error.toString();
                draft.verifysuccess = false;
                draft.verifyloading = false;
                break;
        }
    });

export default resetPasswordReducer;
