/**
 * Homepage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectResetPassword = state => state.resetpassword || initialState;

const makeSelectUser = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.user);
const makeSelectError = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.error);
const makeSelectLoading = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.loading);
const makeSelectSuccess = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.success);

// const selectVerifyTokken = state => state.verify || initialState;

const makeSelectVerifyTokken= () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.verifyuser);
const makeSelectVerifyTokkenError = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.verifyerror);
const makeSelectVerifyTokkenLoading = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.verifyloading);
const makeSelectVerifyTokkenSuccess = () =>
    createSelector(selectResetPassword, resetpasswordState => resetpasswordState.verifysuccess);

export { selectResetPassword, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser, makeSelectVerifyTokken, makeSelectVerifyTokkenError, makeSelectVerifyTokkenLoading, makeSelectVerifyTokkenSuccess };
