import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser, makeSelectVerifyTokken, makeSelectVerifyTokkenError, makeSelectVerifyTokkenLoading, makeSelectVerifyTokkenSuccess } from './selectors';
import { resetPassword, verifyTokken, resetPasswordReset } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';
import { Redirect } from 'react-router-dom'
import ReactLoading from "react-loading";


class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            email: '',
            password: '',
            confirmpassword: '',
            error: false,
            isRedirect: true,

        }
    }
    componentWillMount() {
        const query = new URLSearchParams(this.props.location.search);
        const token = query.get('token');
        let data = { 'token': token };
        if (token) {
            this.setState({ isRedirect: false })
        }
        this.props.dispatch(verifyTokken(data))
    }

    componentWillReceiveProps(nextProps) {
        let email = nextProps.verifyuser.email;
        let id = nextProps.verifyuser._id;
        this.setState({ email: email, id: id, blockloader: nextProps.loading })
    }

    componentDidUpdate() {
        if (this.props.user) {
            ToastTopEndSuccessFire(this.props.user.message)
            // Swal.fire({
            //     title: this.props.user.message,
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 2000
            // }).then(result => {
                this.props.dispatch(resetPasswordReset());
                this.props.history.push('/login')
            // });
        }
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    submit = (e) => {
        e.preventDefault();
        if (this.state.password == this.state.confirmpassword) {
            this.setState({ error: false, blockloader: true })
            const data = {
                id: this.state.id,
                email: this.state.email,
                password: this.state.password,
            }

            this.props.dispatch(resetPassword(data))
        } else {
            this.setState({ error: true })
        }
    }
    render() {
        return (
            <div className="forget-section">
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <div className="container">
                    {/* <!-- Outer Row --> */}
                    <div className="row justify-content-center">

                        <div className="col-xl-10 col-lg-12 col-md-9">

                            <div className="card o-hidden border-0 shadow-lg my-5">
                                <div className="card-body p-0">
                                    {/* <!-- Nested Row within Card Body -->bg-login-image */}
                                    <div className="row">
                                        <div className="col-lg-6 d-none d-lg-block " >
                                            <div className="login-data">
                                                <h2><img src="/images/site-logo-color.png" /></h2>
                                                <h5> {window.i18n('LogInBuySell')}</h5>
                                            </div>
                                        </div>
                                        {/* Login */}
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4"> {window.i18n('ResetPassword')} </h1>
                                                </div>
                                                <form className="user" onSubmit={this.submit} action="">
                                                    <div className="form-group">
                                                        <input onChange={this.handleChange} type="password" className="form-control form-control-user" name="password" value={this.state.password} placeholder={`${window.i18n('ResetEnterPassword')}`} required />
                                                    </div>
                                                    <div className="form-group">
                                                        <input onChange={this.handleChange} type="password" className="form-control form-control-user" name="confirmpassword" value={this.state.confirmpassword} placeholder={`${window.i18n('ResetConfirmPassword')}`} required />
                                                    </div>
                                                    {this.state.error &&
                                                        <>
                                                            <p>{window.i18n('ResetPasswordNotMatch')} </p>
                                                        </>
                                                    }
                                                    <button className="btn btn-primary btn-user btn-block" type='submit' >{window.i18n('AllSubmit')}</button>
                                                </form>
                                                <hr />
                                                <div className="text-center">
                                                    <Link to="/login" className="small">{window.i18n('LogIn')}</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    success: makeSelectSuccess(),

    verifyuser: makeSelectVerifyTokken(),
    verifyloading: makeSelectVerifyTokkenLoading(),
    verifyerror: makeSelectVerifyTokkenError(),
    verifysuccess: makeSelectVerifyTokkenSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'resetpassword', reducer });
const withSaga = injectSaga({ key: 'resetpassword', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(index);

// export default (index);