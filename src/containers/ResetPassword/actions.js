import { RESETPASSWORD, RESETPASSWORD_SUCCESS, RESETPASSWORD_ERROR, RESETPASSWORD_RESET, VERIFYTOKKEN, VERIFYTOKKEN_SUCCESS, VERIFYTOKKEN_ERROR} from './constants';

export function resetPassword(data) {
    return { type: RESETPASSWORD, data };
}
export function resetPasswordSuccess(data) {
    return { type: RESETPASSWORD_SUCCESS, data };
}
export function resetPasswordFailed(error) {
    return { type: RESETPASSWORD_ERROR, error };
}
export function resetPasswordReset() {
  return { type: RESETPASSWORD_RESET };
}


export function verifyTokken(data) {
  return { type: VERIFYTOKKEN, data };
}
export function verifyTokkenSuccess(data) {
  return { type: VERIFYTOKKEN_SUCCESS, data };
}
export function verifyTokkenFailed(error) {
  return { type: VERIFYTOKKEN_ERROR, error };
}
