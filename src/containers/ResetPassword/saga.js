/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { RESETPASSWORD,VERIFYTOKKEN } from './constants';
import { resetPassword, resetPasswordFailed, resetPasswordSuccess, verifyTokken, verifyTokkenSuccess, verifyTokkenFailed } from './actions';
import { redirect } from '../App/actions';
import request from '../../utils/request';
import Define from '../../utils/config';
import { loginFailed } from '../Login/actions';

export function* doverify(action) {
    const requestURL = `${Define.API_URL}verifytokken`;
    const options = {
        method: 'POST',
        body: JSON.stringify(action.data),
    }
    try {
        const res = yield call(request, requestURL, options);
        if (res.status == "success") {
          yield put(verifyTokkenSuccess(res.userInfo));
        } else {      
            yield put(verifyTokkenFailed(res.message));
        }

    } catch (err) {
        yield put(verifyTokkenFailed(err));
    }
}

export function* doreset(action) {
  const requestURL = `${Define.API_URL}resetpassword`;
  const options = {
      method: 'POST',
      body: JSON.stringify(action.data),
  }
  try {
      
      const res = yield call(request, requestURL, options);
      if (res.status) {
        yield put(resetPasswordSuccess(res));
      } else {      
          yield put(resetPasswordFailed(res.message));
      }

  } catch (err) {
      yield put(resetPasswordFailed(err));
  }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* userData() {
    yield takeLatest(VERIFYTOKKEN, doverify);
    yield takeLatest(RESETPASSWORD, doreset);
}
