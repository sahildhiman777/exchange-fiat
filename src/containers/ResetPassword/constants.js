export const RESETPASSWORD = 'exchangefiatUserPanel/resetpassword/RESETPASSWORD';
export const RESETPASSWORD_SUCCESS = 'exchangefiatUserPanel/resetpassword/RESETPASSWORD_SUCCESS';
export const RESETPASSWORD_ERROR = 'exchangefiatUserPanel/resetpassword/RESETPASSWORD_ERROR';
export const RESETPASSWORD_RESET = 'exchangefiatUserPanel/resetpassword/RESETPASSWORD_RESET';

export const VERIFYTOKKEN = 'exchangefiatUserPanel/verifytokken/VERIFYTOKKEN';
export const VERIFYTOKKEN_SUCCESS = 'exchangefiatUserPanel/verifytokken/VERIFYTOKKEN_SUCCESS';
export const VERIFYTOKKEN_ERROR = 'exchangefiatUserPanel/verifytokken/VERIFYTOKKEN_ERROR';