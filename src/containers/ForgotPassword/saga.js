/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { FORGOT } from './constants';
import { forgotSuccess, forgotFailed } from './actions';
import request from '../../utils/request';
import Define from '../../utils/config';

export function* doforgot(action) {
    const requestURL = `${Define.API_URL}forgotpassword`;
    const options = {
        method: 'POST',
        body: JSON.stringify(action.data),
    }
    try {
        const res = yield call(request, requestURL, options); 
        if (res.status == "success") {
          yield put(forgotSuccess(res.message));
        } else {      
            yield put(forgotFailed(res.message));
        }

    } catch (err) {
        yield put(forgotFailed(err));
    }
}
/**
 * Root saga manages watcher lifecycle
 */
export default function* userData() {
    yield takeLatest(FORGOT, doforgot);
}
