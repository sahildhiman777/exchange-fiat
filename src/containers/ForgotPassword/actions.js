import { FORGOT, FORGOT_SUCCESS, FORGOT_ERROR, FORGOT_RESET} from './constants';

export function forgot(data) {
    return { type: FORGOT, data };
}
export function forgotSuccess(data) {
    return { type: FORGOT_SUCCESS, data };
}
export function forgotFailed(error) {
    return { type: FORGOT_ERROR, error };
}
export function forgotReset() {
    return { type: FORGOT_RESET }
}
