export const FORGOT = 'exchangefiatUserPanel/forgot/FORGOT';
export const FORGOT_SUCCESS = 'exchangefiatUserPanel/forgot/FORGOT_SUCCESS';
export const FORGOT_ERROR = 'exchangefiatUserPanel/forgot/FORGOT_ERROR';
export const FORGOT_RESET = 'exchangefiatUserPanel/forgot/FORGOT_RESET';