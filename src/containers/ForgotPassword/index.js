import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser } from './selectors';
import { forgot, forgotReset } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';
import ReactLoading from "react-loading";
import { withRouter } from "react-router-dom";


class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            error: false,
            invalidmail: false,
            langChanged: 0
        }
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value, invalidmail: false });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ blockloader: nextProps.forgotloading });
    }

    componentDidUpdate() {
        if (this.props.forgotuser) {
            ToastTopEndSuccessFire(this.props.forgotuser)
            // Swal.fire({
            //     title: this.props.forgotuser,
            //     type: "success",
            //     showConfirmButton: false,
            //     timer: 2000
            // }).then(result => {
                this.props.dispatch(forgotReset())
                this.props.history.push('/login')
            // });
        }

        if (this.props.forgoterror) {
            ToastTopEndErrorFire(this.props.forgoterror)
            // Swal.fire({
            //     title: this.props.forgoterror,
            //     type: "Error",
            //     showConfirmButton: false,
            //     timer: 2000
            // })
            this.props.dispatch(forgotReset())
            this.props.history.push('/login')
        }
    }

    submit = (e) => {
        e.preventDefault();
        const { email } = this.state;
        const data = {
            email: email
        }
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(data.email)) {
            this.setState({ blockloader: true, invalidmail: false });
            this.props.dispatch(forgot(data))
        } else {
            this.setState({ invalidmail: true })
            return
        }
    }

    changeLanguage = (e) => {
        this.props.changeLanguage(e);
        this.setState({
            langChanged: Date.now()
        })
    }

    render() {
        return (
            <div className="forget-section">
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}
                <div className="container">
                    {/* <!-- Outer Row --> */}
                    <div className="row justify-content-center">
                        <div className="col-xl-10 col-lg-12 col-md-9">
                            <div className="card o-hidden border-0 shadow-lg my-5">
                                <div className="text-right">
                                    <div className="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src={`/images/${window.i18nCode}.png`} />
                                        </button>
                                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                            <a className="dropdown-item text-right" href="#" data-language="vn" onClick={this.changeLanguage}>
                                                Việt Nam &nbsp; <img data-language="vn" src="/images/vn.png" />
                                            </a>
                                            <a className="dropdown-item text-right" href="#" data-language="en" onClick={this.changeLanguage}>
                                                English &nbsp; <img data-language="en" src="/images/en.png" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body p-0">
                                    {/* <!-- Nested Row within Card Body -->bg-login-image */}
                                    <div className="row">
                                        <div className="col-lg-6 d-none d-lg-block " >
                                            <div className="login-data">
                                                <h2><img src="/images/site-logo-color.png" /></h2>
                                                <h5>{window.i18n('FrgtPassBuyndSell')}</h5>
                                            </div>
                                        </div>
                                        {/* Login */}
                                        <div className="col-lg-6">
                                            <div className="p-5">
                                                <div className="text-center">
                                                    <h1 className="h4 text-gray-900 mb-4">{window.i18n('FrgtPassForgot')}</h1>
                                                </div>
                                                <form className="user" onSubmit={this.submit} action="">
                                                    <div className="form-group" style={{ marginBottom: '7px' }}>
                                                        <input onChange={this.handleChange} type="email" className="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" name="email" value={this.state.email} placeholder={window.i18n('FrgtEmail')} required />
                                                    </div>
                                                    {this.state.invalidmail && <p style={{ marginLeft: '10px', marginBottom: '2px', fontSize: '10px', color: 'red' }}>Invalid mail address</p>}
                                                    <button className="btn btn-primary btn-user btn-block" type='submit' >{window.i18n('FrgtPassSubmit')}</button>
                                                </form>
                                                <hr />
                                                <div className="text-center">
                                                    <Link to="/login" className="small">{window.i18n('FrgtPassLogin')}</Link><br></br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = createStructuredSelector({
    forgotuser: makeSelectUser(),
    forgotloading: makeSelectLoading(),
    forgoterror: makeSelectError(),
    forgotsuccess: makeSelectSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'forgot', reducer });
const withSaga = injectSaga({ key: 'forgot', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(index);