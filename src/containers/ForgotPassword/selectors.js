/**
 * Homepage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectForgot = state => state.forgot || initialState;

const makeSelectUser = () =>
    createSelector(selectForgot, forgotState => forgotState.forgotuser);
const makeSelectError = () =>
    createSelector(selectForgot, forgotState => forgotState.forgoterror);
const makeSelectLoading = () =>
    createSelector(selectForgot, forgotState => forgotState.forgotloading);
const makeSelectSuccess = () =>
    createSelector(selectForgot, forgotState => forgotState.forgotsuccess);

export { selectForgot, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser };
