import produce from 'immer';
import { FORGOT, FORGOT_SUCCESS, FORGOT_ERROR, FORGOT_RESET } from './constants';

// The initial state of the App
export const initialState = {
    user: false,
    success: false,
    error: false,
    loading: false,
    forgotuser: false,
    forgotsuccess: false,
    forgoterror: false,
    forgotloading: false
};
/* eslint-disable default-case, no-param-reassign */
const forgotReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case FORGOT:
                draft.forgotloading = true;
                draft.forgoterror = false;
                draft.forgotsuccess = false;
                draft.forgotuser = false;
                break;

            case FORGOT_SUCCESS:
                draft.forgotuser = action.data;
                draft.forgoterror = false;
                draft.forgotsuccess = true;
                draft.forgotloading = false;
                break;

            case FORGOT_ERROR:
                draft.forgoterror = action.error.toString();
                draft.forgotsuccess = false;
                draft.forgotloading = false;
                break;

            case FORGOT_RESET:
                draft.forgotloading = false;
                draft.forgoterror = false;
                draft.forgotsuccess = false;
                draft.forgotuser = false;
                break;
        }
    });

export default forgotReducer;
