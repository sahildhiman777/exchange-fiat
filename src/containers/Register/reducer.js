import produce from 'immer';
import { REGISTER, REGISTER_ERROR, REGISTER_SUCCESS, REGISTER_RESET, EMAIL, EMAIL_ERROR, EMAIL_SUCCESS, VERIFYEMAIL, VERIFYEMAIL_ERROR, VERIFYEMAIL_SUCCESS,
    WALLETS,
    WALLETS_ERROR,
    WALLETS_SUCCESS,} from './constants';

// The initial state of the App
export const initialState = {
    user: false,
    success: false,
    error: false,
    loading: false,
    email: false,
    emailsuccess: false,
    emailerror: false,
    emailloading: false,
    verifyemail: false,
    verifyemailsuccess: false,
    verifyemailerror: false,
    verifyemailloading: false,
    wallets: false,
    walletssuccess: false,
    walletserror: false,
    walletsloading: false,
};
/* eslint-disable default-case, no-param-reassign */
const registerReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case REGISTER:
                draft.loading = true;
                draft.error = false;
                draft.success = false;
                draft.user = false;
                break;

            case REGISTER_SUCCESS:
                draft.user = action.data.user;
                draft.error = false;
                draft.success = true;
                draft.loading = false;
                break;

            case REGISTER_ERROR:
                draft.error = action.error;
                draft.success = false;
                draft.loading = false;
                break;

            case REGISTER_RESET:
                draft.error = false;
                draft.success = false;
                draft.loading = false;
                break;

            case EMAIL:
                draft.emailloading = true;
                draft.emailerror = false;
                draft.emailsuccess = false;
                draft.email = false;
                break;

            case EMAIL_SUCCESS:
                draft.email = action.data;
                draft.emailerror = false;
                draft.emailsuccess = true;
                draft.emailloading = false;
                break;

            case EMAIL_ERROR:
                draft.emailerror = action.error.toString();
                draft.emailsuccess = false;
                draft.emailloading = false;
                break;

            case VERIFYEMAIL:
                draft.verifyemailloading = true;
                draft.verifyemailerror = false;
                draft.verifyemailsuccess = false;
                draft.verifyemail = false;
                break;

            case VERIFYEMAIL_SUCCESS:
                draft.verifyemail = action.data.emailstatus;
                draft.verifyemailerror = false;
                draft.verifyemailsuccess = true;
                draft.verifyemailloading = false;
                console.log(draft.verifyemail);
                break;

            case VERIFYEMAIL_ERROR:
                draft.verifyemailerror = action.error.toString();
                draft.verifyemailsuccess = false;
                draft.verifyemailloading = false;
                break;

                case WALLETS:
                    draft.walletsloading = true;
                    draft.walletserror = false;
                    draft.walletssuccess = false;
                    draft.wallets = false;
                    break;
    
                case WALLETS_SUCCESS:
                    draft.wallets = action.data;
                    draft.walletserror = false;
                    draft.walletssuccess = true;
                    draft.walletsloading = false;
                    break;
    
                case WALLETS_ERROR:
                    draft.walletserror = action.error.toString();
                    draft.walletssuccess = false;
                    draft.walletsloading = false;
                    break;

        }
    });

export default registerReducer;
