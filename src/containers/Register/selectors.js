/**
 * Homepage selectors
 */
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectRegister = state => state.register || initialState;

const makeSelectUser = () =>
    createSelector(selectRegister, registerState => registerState.user);
const makeSelectError = () =>
    createSelector(selectRegister, registerState => registerState.error);
const makeSelectLoading = () =>
    createSelector(selectRegister, registerState => registerState.loading);
const makeSelectSuccess = () =>
    createSelector(selectRegister, registerState => registerState.success);

    const makeSelectMyEmail = () =>
  createSelector(selectRegister, registerState => registerState.email);
const makeSelectMyEmailError = () =>
  createSelector(selectRegister, registerState => registerState.emailError);
const makeSelectMyEmailLoading = () =>
  createSelector(selectRegister, registerState => registerState.emailLoading);
const makeSelectMyEmailSuccess = () =>
  createSelector(selectRegister, registerState => registerState.emailSuccess);

  const makeSelectMyVerifyEmail = () =>
  createSelector(selectRegister, registerState => registerState.verifyemail);
const makeSelectMyVerifyEmailError = () =>
  createSelector(selectRegister, registerState => registerState.verifyemailError);
const makeSelectMyVerifyEmailLoading = () =>
  createSelector(selectRegister, registerState => registerState.verifyemailLoading);
const makeSelectMyVerifyEmailSuccess = () =>
  createSelector(selectRegister, registerState => registerState.verifyemailSuccess);

  const makeSelectMyWallets = () =>
  createSelector(selectRegister, registerState => registerState.wallets);
  const makeSelectMyWalletsError = () =>
    createSelector(selectRegister, registerState => registerState.walletsError);
  const makeSelectMyWalletsLoading = () =>
    createSelector(selectRegister, registerState => registerState.walletsloading);
  const makeSelectMyWalletsSuccess = () =>
    createSelector(selectRegister, registerState => registerState.walletsSuccess);


export { selectRegister, makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser,
    makeSelectMyEmail,
    makeSelectMyEmailError,
    makeSelectMyEmailLoading,
    makeSelectMyEmailSuccess, 
    makeSelectMyVerifyEmail,
    makeSelectMyVerifyEmailError,
    makeSelectMyVerifyEmailLoading,
    makeSelectMyVerifyEmailSuccess,

    makeSelectMyWallets,
    makeSelectMyWalletsError,
    makeSelectMyWalletsLoading,
    makeSelectMyWalletsSuccess,
  };
