export const REGISTER = 'exchangefiatUserPanel/Register/REGISTER';
export const REGISTER_SUCCESS = 'exchangefiatUserPanel/Register/REGISTER_SUCCESS';
export const REGISTER_ERROR = 'exchangefiatUserPanel/Register/REGISTER_ERROR';
export const REGISTER_RESET = 'exchangefiatUserPanel/Register/REGISTER_RESET';

export const EMAIL = 'exchangefiatUserPanel/email/EMAIL';
export const EMAIL_SUCCESS = 'exchangefiatUserPanel/email/EMAIL_SUCCESS';
export const EMAIL_ERROR = 'exchangefiatUserPanel/email/EMAIL_ERROR';

export const VERIFYEMAIL = 'exchangefiatUserPanel/email/VERIFYEMAIL';
export const VERIFYEMAIL_SUCCESS = 'exchangefiatUserPanel/email/VERIFYEMAIL_SUCCESS';
export const VERIFYEMAIL_ERROR = 'exchangefiatUserPanel/email/VERIFYEMAIL_ERROR';


export const WALLETS = 'exchangefiatUserPanel/wallets/WALLETS';
export const WALLETS_SUCCESS = 'exchangefiatUserPanel/wallets/WALLETS_SUCCESS';
export const WALLETS_ERROR = 'exchangefiatUserPanel/wallets/WALLETS_ERROR';