import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { makeSelectError, makeSelectLoading, makeSelectSuccess, makeSelectUser,makeSelectMyEmail,
    makeSelectMyEmailError,
    makeSelectMyEmailLoading,
    makeSelectMyEmailSuccess, 
    selectMywallets,
  makeSelectMyWallets,
  makeSelectMyWalletsError,
  makeSelectMyWalletsLoading,
  makeSelectMyWalletsSuccess,
} from './selectors';
import { register, registerReset, email, wallets } from './actions';
import reducer from './reducer';
import saga from './saga';
import { createStructuredSelector } from 'reselect';
import injectReducer from '../../utils/injectReducer';
import injectSaga from '../../utils/injectSaga';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';
import { Link } from "react-router-dom";
import $ from 'jquery';
import ReactLoading from "react-loading";
import { withRouter } from "react-router-dom";
import Config from "../../utils/config";

class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            repeatPass: '',
            passwordError: false,
            invalidlength: false,
            firstName: '',
            lastName: '',
            swal: false,
            langChanged: 0
        };
    }

    handleChange = (e) => {
        this.setState({ swal: false });
        this.setState({ [e.target.name]: e.target.value, swal: false });
    };

    submit = (e) => {

        e.preventDefault();
        let currency = localStorage.getItem('currency');
        // let coinList = Config.coinlist;
        let coinList = this.props.wallets;
        for(let i=0; i<coinList.length; i++){
            if(coinList[i].symbol == '$USD'){
                coinList[i].symbol = 'USD';
            }
            if(coinList[i].symbol == 'USDT'){
                coinList[i].symbol = 'ERC20-USDT';
            }
        }
        const data = {
            firstname: this.state.firstName,
            lastname: this.state.lastName,
            email: this.state.email,
            password: this.state.password,
            type: 'create',
            currency : currency,
            coinList : coinList,

        }
        if (data.password.length < 6) {
            this.setState({ invalidlength: true, passwordError: false, })
            return
        }
        if (data.password == this.state.repeatPass) {
            this.setState({ swal: true, blockloader: true, passwordError: false, invalidlength: false });
            this.props.dispatch(register(data));
            return false;
        } else {
            this.setState({ passwordError: true, invalidlength: false })
        }

    }

    componentWillMount() {
        this.props.dispatch(wallets());
        $(document).ready(function () {
            $('body').addClass('back-clr');
        });
    }

    componentDidUpdate() {
        if (this.props.success) {
            ToastTopEndSuccessFire("User is registered successfully.")
                const data = {
                    email: this.state.email,
                }
                this.props.dispatch(email(data));
                this.props.dispatch(registerReset());
                this.setState({
                    firstName: '', lastName: '', email: '', password: '', repeatPass: '', blockloader: true
                }, () => {
                    this.props.history.push('/login')
                })
        }
        if(this.props.email !=false){
            this.setState({ blockloader: false
            }, () => {
            })
            this.props.history.push('/login');
        }
        if (this.props.error) {
            ToastTopEndErrorFire(this.props.error)
                this.props.dispatch(registerReset());
                this.setState({
                    firstName: '', lastName: '', email: '', password: '', repeatPass: '', blockloader: false
                })
        }
    }

    changeLanguage = (e) => {
        this.props.changeLanguage(e);
        this.setState({
            langChanged: Date.now()
        })
    }

    render() {
        return (
            <div >
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>
                }
                <div className="container">
                    <div className="card o-hidden border-0 shadow-lg my-5">
                        <div className="text-right">
                            <div className="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" className="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src={`/images/${window.i18nCode}.png`} />
                                </button>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                                    <a className="dropdown-item text-right" href="#" data-language="vn" onClick={this.changeLanguage}>
                                        Việt Nam &nbsp; <img data-language="vn" src="/images/vn.png" />
                                    </a>
                                    <a className="dropdown-item text-right" href="#" data-language="en" onClick={this.changeLanguage}>
                                        English &nbsp; <img data-language="en" src="/images/en.png" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="card-body p-0">
                            {/* <!-- Nested Row within Card Body --> */}
                            <div className="row">
                                <div className="col-lg-5 d-none d-lg-block ">
                                    <div className="login-data">
                                        <h2><a href="/"><img src="/images/site-logo-color.png" /></a></h2>
                                        <h5>{window.i18n('LogInBuySell')}</h5>
                                    </div>
                                </div>
                                <div className="col-lg-7">
                                    <div className="p-5">
                                        <div className="text-center">
                                            <h1 className="h4 text-gray-900 mb-4">{window.i18n('LogInCreate')}</h1>
                                        </div>
                                        <form className="user" onSubmit={this.submit} action="">
                                            <div className="form-group row">
                                                <div className="col-sm-6 mb-3 mb-sm-0">
                                                    <input onChange={this.handleChange} type="text" className="form-control form-control-user" id="exampleFirstName" name="firstName" value={this.state.firstName} placeholder={`${window.i18n('RegisterFirstName')}`} required />
                                                </div>
                                                <div className="col-sm-6">
                                                    <input onChange={this.handleChange} type="text" className="form-control form-control-user" id="exampleLastName" name="lastName" value={this.state.lastName} placeholder={`${window.i18n('RegisterLastName')}`} required />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <input onChange={this.handleChange} type="email" className="form-control form-control-user" id="exampleInputEmail" name="email" value={this.state.email} placeholder={`${window.i18n('RegisterEmailAddress')}`} required />
                                            </div>
                                            <div className="form-group row">
                                                <div className="col-sm-6 mb-3 mb-sm-0">
                                                    <input onChange={this.handleChange} type="password" className="form-control form-control-user" id="exampleInputPassword" name="password" value={this.state.password} placeholder={`${window.i18n('RegisterPassword')}`} required />
                                                </div>
                                                <div className="col-sm-6">
                                                    <input onChange={this.handleChange} type="password" className="form-control form-control-user" id="exampleRepeatPassword" name="repeatPass" value={this.state.repeatPass} placeholder={`${window.i18n('RegisterRepeatPassword')}`} required />
                                                </div>
                                            </div>
                                            {this.state.passwordError && <div className="invalid-feedback">{window.i18n('ResetPasswordNotMatch')}</div>}
                                            {this.state.invalidlength && <div className="invalid-feedback">{window.i18n('RegisterPasswordError1')}</div>}
                                            <button className="btn btn-primary btn-user btn-block" type='submit' >{window.i18n('RegisterAccount')}</button>
                                        </form>
                                        <hr />
                                        <div className="text-center">
                                            <Link to="/forgotpassword" className="small">{window.i18n('LogInForgot')}</Link>
                                        </div>
                                        <div className="text-center">
                                            <Link to="/login" className="small">{window.i18n('RegisterAlready')}</Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


const mapStateToProps = createStructuredSelector({
    user: makeSelectUser(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    success: makeSelectSuccess(),

    email: makeSelectMyEmail(),
    emailloading: makeSelectMyEmailLoading(),
    emailerror: makeSelectMyEmailError(),
    emailsuccess: makeSelectMyEmailSuccess(),

    wallets: makeSelectMyWallets(),
    walletsloading: makeSelectMyWalletsLoading(),
    walletserror: makeSelectMyWalletsError(),
    walletssuccess: makeSelectMyWalletsSuccess(),
});

export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'register', reducer });
const withSaga = injectSaga({ key: 'register', saga });

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
    withRouter
)(index);

