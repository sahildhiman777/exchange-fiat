/**
 * Gets the repositories of the user from Github
 */
import { call, put, takeLatest } from 'redux-saga/effects';
import { REGISTER, EMAIL, VERIFYEMAIL, WALLETS} from './constants';
import { registerFailed, registerSuccess,emailSuccess, emailFailed, verifyemailFailed, verifyemailSuccess, wallets, walletsSuccess, walletsFailed,} from './actions';
// import { redirect } from '../Main/actions';
import request from '../../utils/request';
import Define from '../../utils/config';
import Swal from 'sweetalert2';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../../toast/toast';
const axios = require('axios');
export function* doRegister(action) {
    const requestURL = `${Define.API_URL}register`;
    const options = {
        method: 'POST',
        body: JSON.stringify(action.data),
    }
    try {
        const res = yield call(request, requestURL, options);

        if (res.status) {
                yield put(registerSuccess(res.data));
            }
         else {      
            yield put(registerFailed(res.message));
        }

    } catch (err) {
        yield put(registerFailed(err.toString()));
    }
}

export function* myEmail(action) {
    const requestURL = `${Define.API_URL}emailverifysendcode`;
    const options = {
      method: 'POST',
      body: JSON.stringify(action.data),
    }
    try {
      const res = yield call(request, requestURL, options);
      yield put(emailSuccess(res.message));
      ToastTopEndSuccessFire('Please check your email and verify your email')
    } catch (err) {
      yield put(emailFailed(err));
      ToastTopEndErrorFire("Could not send verification email")
    }
  }

  export function* myVerifyEmail(action) {
    const requestURL = `${Define.API_URL}verifymail`;
    const options = {
      method: 'POST',
      body: JSON.stringify(action.data),
    }
    try {
      const res = yield call(request, requestURL, options);
      yield put(verifyemailSuccess(res.data));
      ToastTopEndSuccessFire(res.message)
    } catch (err) {
      yield put(verifyemailFailed(err));
    }
  }

  export function* myWallets(action) {
    let currency = localStorage.getItem('currency') || "VND";
    const requestURL = `${Define.API_URL}currency/get_wallets?currency=${currency.toLowerCase()}`;
    const options = {
      method: 'GET',
      body: JSON.stringify(action.data),
    }
    try {
      const res = yield call(request, requestURL, options);
      yield put(walletsSuccess(res.message));
  
  
    } catch (err) {
      yield put(walletsFailed(err));
    }
  }

/**
 * Root saga manages watcher lifecycle
 */
export default function* userData() {
    yield takeLatest(REGISTER, doRegister);
    yield takeLatest(EMAIL, myEmail);
    yield takeLatest(VERIFYEMAIL, myVerifyEmail);
    yield takeLatest(WALLETS, myWallets);
}
