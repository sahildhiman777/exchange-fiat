import { REGISTER, REGISTER_SUCCESS, REGISTER_ERROR, REGISTER_RESET,EMAIL,
    EMAIL_ERROR,
    EMAIL_SUCCESS,
    VERIFYEMAIL,
    VERIFYEMAIL_ERROR,
    VERIFYEMAIL_SUCCESS,
    WALLETS,
  WALLETS_ERROR,
  WALLETS_SUCCESS,
} from './constants';

export function registerReset() {
    return { type: REGISTER_RESET };
  }
export function register(data) {
    return { type: REGISTER, data };
}
export function registerSuccess(data) {
    return { type: REGISTER_SUCCESS, data };
}
export function registerFailed(error) {
    return { type: REGISTER_ERROR, error };
}
export function email(data) {
    return { type: EMAIL, data };
  }
  export function emailSuccess(data) {
    return { type: EMAIL_SUCCESS, data };
  }
  export function emailFailed(error) {
    return { type: EMAIL_ERROR, error };
  }
  export function verifyemail(data) {
    return { type: VERIFYEMAIL, data };
  }
  export function verifyemailSuccess(data) {
    return { type: VERIFYEMAIL_SUCCESS, data };
  }
  export function verifyemailFailed(error) {
    return { type: VERIFYEMAIL_ERROR, error };
  }

  export function wallets() {
    return { type: WALLETS };
  }
  export function walletsSuccess(data) {
    return { type: WALLETS_SUCCESS, data };
  }
  export function walletsFailed(error) {
    return { type: WALLETS_ERROR, error };
  }