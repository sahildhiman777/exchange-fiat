import React, { Component } from 'react';
import { Link } from "react-router-dom";

class index extends Component {
    render() {
        return (
            // Begin Page Content
            <div className="container-fluid">

                {/* 404 Error Text */}
                <div className="text-center">
                    <div className="error mx-auto" data-text="404">404</div>
                    <p className="lead text-gray-800 mb-5">{window.i18n('PageNotFound')}</p>
                    <p className="text-gray-500 mb-0">{window.i18n('Itlookslike')}</p>
                    <Link to="/">&larr; {window.i18n('BacktoDashboard')}</Link>
                </div>

            </div>
            // /.container-fluid
        )
    }
}
export default index;
