import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { selectTransactions } from '../../../containers/Dashboard/Account/Transactions/selectors';
class index extends Component {
    selectTransactions(event) {
        // console.log(event, 'event');
        // console.log('KKJKJJK');
        
    }
    render() {
        return (
            <React.Fragment>
                <ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

                    {/* <!-- Sidebar - Brand --> */}
                    <Link className="sidebar-brand d-flex align-items-center justify-content-center" to="/">
                        <div className="sidebar-brand-icon rotate-n-15">
                            <img src="/images/logo-e.png" style={{ width: "45px", transform: "rotate(13deg)" }} />
                        </div>
                        {/* <div className="sidebar-brand-text mx-3"><img src="/images/site_logo.png" alt="Exchange Fiat" style={{width: "120px"}}/></div> */}
                        <div className="sidebar-brand-text mx-3">ExchangeFiat</div>
                    </Link>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider my-0" />

                    {/* <!-- Nav Item - Dashboard --> */}
                    <li className="nav-item">
                        <Link className="nav-link" to="/dashboard">
                            <i className="fas fa-fw fa-tachometer-alt"></i>
                            <span>{window.i18n('dashboard')}</span>
                        </Link>
                    </li>

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider" />

                    {/* <!-- Heading --> */}
                    <div className="sidebar-heading">
                        {window.i18n('account')}
                    </div>

                    {/* <!-- Nav Item - Pages Collapse Menu --> */}
                    <li className="nav-item">
                        <Link className="nav-link collapsed" to="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            <i className="fas fa-fw fa-cog"></i>
                            <span>{window.i18n('userInfoLabel')}</span>
                        </Link>
                        <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                            <div className="bg-white py-2 collapse-inner rounded">
                                <h6 className="collapse-header">{window.i18n('SidebarUserInformation')}:</h6>
                                <Link className="collapse-item" to="/profile">{window.i18n('SidebarProfile')}</Link>
                                <Link className="collapse-item" to="/wallets">{window.i18n('wallets')}</Link>
                                <Link className="collapse-item" to="/kyc">{window.i18n('kyc')}</Link>
                                <Link className="collapse-item" to="/bank">{window.i18n('SidebarBanks')}</Link>
                            </div>
                        </div>
                    </li>

                    {/* <!-- Nav Item - Utilities Collapse Menu --> */}
                    <li className="nav-item">
                        <Link className="nav-link collapsed" to="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
                            <i className="fas fa-fw fa-wrench"></i>
                            <span>{window.i18n('SidebarTransactions')}</span>
                        </Link>
                        <div id="collapseUtilities" className="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
                            <div className="bg-white py-2 collapse-inner rounded">
                                <h6 className="collapse-header">{window.i18n('SidebarTransactions')}:</h6>
                                <Link className="collapse-item" to="transactions" >{window.i18n('SidebarTransactions')}</Link>
                                {/* <Link className="collapse-item" to="transactions" onClick={this.selectTransactions('buy')}>Buy</Link>
                                <Link className="collapse-item" to="transactions" onClick={this.selectTransactions('sell')}>Sell</Link> */}
                            </div>
                        </div>
                    </li>

                    {/* <!-- Divider --> */}
                    {/* <hr className="sidebar-divider" /> */}

                    {/* <!-- Heading --> */}
                    {/* <div className="sidebar-heading">
                        Addons
                    </div> */}

                    {/* <!-- Nav Item - Pages Collapse Menu --> */}
                    {/* <li className="nav-item active">
                        <a className="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
                            <i className="fas fa-fw fa-folder"></i>
                            <span>Pages</span>
                        </a>
                        <div id="collapsePages" className="collapse show" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                            <div className="bg-white py-2 collapse-inner rounded">
                                <h6 className="collapse-header">Login Screens:</h6>
                                <a className="collapse-item" href="login.html">Login</a>
                                <a className="collapse-item" href="register.html">Register</a>
                                <a className="collapse-item" href="forgot-password.html">Forgot Password</a>
                                <div className="collapse-divider"></div>
                                <h6 className="collapse-header">Other Pages:</h6>
                                <a className="collapse-item" href="404.html">404 Page</a>
                                <a className="collapse-item active" href="blank.html">Blank Page</a>
                            </div>
                        </div>
                    </li> */}

                    {/* <!-- Nav Item - Charts --> */}
                    {/* <li className="nav-item">
                        <a className="nav-link" href="charts.html">
                            <i className="fas fa-fw fa-chart-area"></i>
                            <span>Charts</span></a>
                    </li> */}

                    {/* <!-- Nav Item - Tables --> */}
                    {/* <li className="nav-item">
                        <a className="nav-link" href="tables.html">
                            <i className="fas fa-fw fa-table"></i>
                            <span>Tables</span></a>
                    </li> */}

                    {/* <!-- Divider --> */}
                    <hr className="sidebar-divider d-none d-md-block" />

                    {/* <!-- Sidebar Toggler (Sidebar) --> */}
                    <div className="text-center d-none d-md-inline">
                        <button className="rounded-circle border-0 sliderclose" id="sidebarToggle"></button>
                    </div>

                </ul>
            </React.Fragment>
        )
    }
}

export default index
