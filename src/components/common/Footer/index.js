import React, { Component } from 'react';
import { wallets, getBanks, getSellBanks } from '../../../containers/LandingPage/components/AllCrypto/actions';

import { connect } from 'react-redux';
import { compose } from 'redux';
import reducer from '../../../containers/LandingPage/components/AllCrypto/reducer';
import saga from '../../../containers/LandingPage/components/AllCrypto/saga';
import injectReducer from '../../../utils/injectReducer';
import injectSaga from '../../../utils/injectSaga';

class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currency: '',

        }
    }
    componentDidMount() {
        const currency = localStorage.getItem('currency') || "VND"
        this.setState({ currency: currency });
        localStorage.setItem('currency', currency)
    }
    handlecurrency = (e) => {
        this.setState({
            currency: e.target.value
        }, () => {
            localStorage.setItem('currency', this.state.currency)
            this.props.dispatch(wallets());
            this.props.dispatch(getBanks());
            this.props.dispatch(getSellBanks());
        })
    }

//  function Footer() {
       render(){
        return (
            <div>
                <footer className="sticky-footer bg-white">
                 <div className="container my-auto">
                    <div className="copyright text-center my-auto">
                        <div className="row">
                            <div className="col-md-6">
                            <span>{window.i18n('FOOTERCOPYRIGHT')}</span>
                            </div>
                            <div className="col-md-6">
                            <div className="currency_div_dashboard">
                                    <select className="currency_button" value={this.state.currency} onChange={(e) => this.handlecurrency(e)}>
                                        <option value="VND">VND</option>
                                        <option value="USD">USD</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                       
                    </div>
                 </div>
                </footer>
            </div>
        )
       }
    }


// export default Footer
export function mapDispatchToProps(dispatch) {
    return { dispatch };
}

const withReducer = injectReducer({ key: 'landingpage', reducer });
const withSaga = injectSaga({ key: 'landingpage', saga });

const withConnect = connect(
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect,
)(Footer);