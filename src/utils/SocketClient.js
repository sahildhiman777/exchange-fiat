import openSocket from 'socket.io-client';
import config from './config';
const requestURL = `${config.socket_url}`;
const socket = openSocket(requestURL);

function subscribeToTimer(cb) {
    socket.on('update', timestamp => cb(null, timestamp));
    socket.emit('subscribeToTimer', 500);
}

export { subscribeToTimer };