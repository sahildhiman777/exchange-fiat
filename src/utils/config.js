import { enable } from "../containers/Dashboard/Account/Profile/saga";

const config = {
    // API_URL: 'http://localhost:7000/user/v1/',
    // baseUrl: 'http://localhost:3000/',
    // socket_url: 'http://localhost:7001',
    API_URL: 'http://178.128.102.201:7000/user/v1/',
    baseUrl: 'http://178.128.102.201:3000/',
    socket_url: 'http://178.128.102.201:7001',
    bank_image_url: 'http://178.128.102.201/fiat-console/static',
    platforms: {
        'exchange': {
            'label': 'SonicX',
            'background-color': '#074665',
            'text-color': '#ffffff'
        },
        'igamebit': {
            'label': 'IGameBit',
            'background-color': '#FE5F01',
            'text-color': '#ffffff'
        },
        'fortunext': {
            'label': 'FortuNEXT',
            'background-color': '#1C6994',
            'text-color': '#ffffff'
        },
        'caltechcasino': {
            'label': 'CalTechCasino',
            'background-color': '#ffff00',
            'text-color': '#000000'
        },
        'payus': {
            'label': 'Payus',
            'background-color': '#3574FA',
            'text-color': '#ffffff'
        }
    },
    coinlist: [
        {
            'name': 'bitcoin',
            'symbol': 'BTC',
            'icon': 'BTCUSDT.png',
            'buy_status': true,
            'sell_status': true
        },
        {
            'name': 'ethereum',
            'symbol': 'ETH',
            'icon': 'ETHBTC.png',
            'buy_status': true,
            'sell_status': true
        },
        {
            'name': 'sonicex',
            'symbol': 'SOX',
            'icon': 'SOX.png',
            'buy_status': true,
            'sell_status': true
        },
        {
            'name': 'digitalusd',
            'symbol': '$USD',
            'icon': 'USD.png',
            'buy_status': true,
            'sell_status': true
        },
        {
            'name': 'usdt',
            'symbol': 'USDT ',
            'icon': 'usdt.png',
            'buy_status': false,
            'sell_status': true
        }
    ],
    buyFee: 2.5,
    sellFee: 2.5

}

export default config;
