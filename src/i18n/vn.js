export default {
    'login': 'Đăng nhập',
    'register': 'Đăng ký',
    'dashboard': 'bảng điều khiển',
    'Alerts': 'Cảnh báo',
    'logout': 'Đăng xuất',
    'account': 'Tài khoản',
    'SearchFor': 'Tìm kiếm...',
    'Messages': 'Tin nhắn',
    'NoMessage': 'Không có tin nhắn mới',
    'MessagesNewAalerts': ' Không có thông báo mới',
    'Profile': 'Hồ sơ',
    'Settings': 'Cài đặt',
    'userInfoLabel': 'Thông tin người dùng',
    'exchangeFiatHeader': 'TRAO ĐỔI',
    'buyAndSell.Dig.Cur': 'MUA VÀ BÁN TIỀN ĐIỆN TỬ KỸ THUẬT SỐ',
    'buyCrypto': 'NHƯNG CRYPTO',
    'BuyAmount': ' tiền',
    'Buy': 'Số',
    'Seller': 'Người bán',
    'Buyer': 'Người mua',
    'BuyNowBuying': 'Bạn đang mua',
    'SellNowSelling': 'Bạn đang bán',
    'SellDeposit': 'Vui lòng gửi tiền của bạn đến địa chỉ dưới đây. Quản trị viên sẽ phê duyệt yêu cầu của bạn sau khi nhận được tiền.',
    'SellCopyCoin': 'Sao chép địa chỉ tiền xu',

    'sellCrypto': 'BÁN CRYPTO',
    'butAndSellCryptoEasy': 'Mua và bán tiền điện tử dễ dàng, nhanh chóng và an toàn',
    'Peer-to-Peer': 'Trao đổi ngang hàng (P2P)',
    'BitcoinAltcoins': 'MUA / BÁN Bitcoin & Altcoin nhanh chóng được bảo đảm-Dễ dàng.',
    'ExchangeFiatBen': 'Trao đổi lợi ích Fiat',
    'Platformswithfiat': 'Các nền tảng với trao đổi tiền điện tử được hỗ trợ fiat cung cấp một nền tảng cho việc mua, bán hoặc trao đổi tiền điện tử. BTC-BCH-DASH-XRP-ETH-LTC-FNEXT-WDX & 30+ allcoin cho fiat địa phương. Nền tảng này đang hỗ trợ 5 quốc gia để trao đổi với fiatnow địa phương và nhiều quốc gia khác sẽ được bổ sung trong tương lai gần.',
    'Buy/Sell': 'Mua / bán tiền điện tử một cách dễ dàng. Gửi tiền kỹ thuật số của bạn và nhận tiền mặt trong phút, chuyển tiền địa phương của bạn vào ví fiat của bạn và nhận tiền điện tử của bạn lựa chọn ngay sau khi chuyển khoản ngân hàng của bạn được xác nhận trong vài phút.',
    'LowPrice': 'Trao đổi giá thấp',
    'Trade': 'Dễ dàng giao dịch',
    'Withdraw': 'Rút tiền Fiat',
    'SecureStorage': 'Lưu trữ an toàn',
    'FiatWallet': 'Trao đổi ví Fiat',
    'strngSize': 'Trao đổi kích thước Fiat MẠNH MONG',
    'whyPeople': 'Tại sao mọi người chọn Exchange Fiat',
    'Fast-Secured': 'Bảo đảm nhanh & hỗ trợ đầy đủ 24/7',
    'EXPLORE': 'KHÁM PHÁ',
    'SidebarExchangeFiat': 'Trao đổi',
    'SidebarUserInformation': 'Thông tin người dùng',
    'SidebarProfile': 'Hồ sơ',
    'SidebarBanks': 'Ngân hàng',
    'SidebarTransactions': 'Giao dịch',

    'Scalability': 'Khả năng mở rộng',
    'ScalabilityIsOne': 'Khả năng mở rộng là một trong những vấn đề quan trọng nhất trong blockchain và là trọng tâm của cả những người hành nghề trong ngành và các nhà nghiên cứu học thuật kể từ khi Bitcoin ra đời. Bài viết này là khởi đầu của một loạt các bài viết tập trung vào khả năng mở rộng blockchain, cung cấp một cách có hệ thống để phân loại các giải pháp khác nhau và phân tích ưu và nhược điểm của chúng.',
    'Privacy': 'Riêng tư',
    'PrivacyImportance': 'Quyền riêng tư là vô cùng quan trọng tại nhóm các công ty Blockchain (nhóm Blockchain trực tuyến). Chúng tôi nhận ra tầm quan trọng của việc bảo vệ thông tin được lưu trữ trên máy chủ hoặc mạng của chúng tôi hoặc dự định sẽ được lưu trữ trên máy chủ hoặc mạng của chúng tôi và liên quan đến một cá nhân.',
    'Security': 'Bảo vệ',
    'Blockchain-Can': 'Blockchain có thể mạnh mẽ, an toàn và đáng tin cậy - miễn là công nghệ được thực thi đúng cách. Công nghệ nối thêm các mục vào sổ cái được xác nhận bởi cộng đồng người dùng rộng hơn là bởi một cơ quan trung ương. Mỗi khối đại diện cho một bản ghi giao dịch và chuỗi liên kết chúng.',
    'SONICXGAMINGS': 'SONICXGAMING',
    'PAYUS INC': 'THANH TOÁN INC',
    'SONICX BLOCKCHAIN': 'SONIC X BLOCKCHAIN',
    'FOOTERCOPYRIGHT':'Bản quyền © ️ Trao đổi Fiat 2020',
    'EZUGI': 'EZUGI',
    'SOXBET.ORG': 'SOXBET.ORG',
    'SONICEX.ORG': 'SONICEX.ORG',
    'SONICXGAMINGS': 'SONICXGAMING Trao đổi &copy; Bản quyền Fiat 2020 ',
    'Page Not Found': 'Không tìm thấy trang',
    'Itlookslike': 'Có vẻ như bạn đã tìm thấy một trục trặc trong ma trận ...',
    'BacktoDashboard': 'Quay lại Bảng điều khiển',
    'YouAreBuying': 'Bạn đang mua',
    'Amount': 'Số tiền',
    'CoinAddress': 'Địa chỉ tiền xu',
    'PLEASE': 'XIN VUI LÒNG MÃ SỐ NÀY TRÊN LƯU Ý KHI BẠN CHUYỂN TIỀN',
    'Disclaimer': 'Tôi đã đọc và đồng ý từ chối trách nhiệm',
    'SellerPayment': 'Quan trọng: Người bán có quyền từ chối giao dịch của bạn nếu bạn không lưu ý mã 6 chữ số.',
    'Seller': 'Người bán',
    'ExchangeFiatDashboard': 'Trao đổi Fiat',
    'asset': 'Loại tài sản',
    'Price': 'Giá bán',
    'Limit': 'Giới hạn',
    'BankName': 'Tên ngân hàng:',
    'VND': 'Đồng',
    'AccountNumber': 'Số tài khoản',
    'AccountHolder:': 'Chủ tài khoản:',
    'BankAddress:': 'Địa chỉ ngân hàng:',
    'BankCode:': 'Mã ngân hàng:',
    'Country:': 'Quốc gia:',
    'YourSelling': 'Bán hàng của bạn',
    'Amount': 'Số tiền',
    'CoinAddress': 'Đồng tiền',
    'TransferFund': 'XIN VUI LÒNG MÃ SỐ NÀY TRÊN LƯU Ý KHI BẠN CHUYỂN TIỀN',
    'Equivalent': 'Tương đương',
    'IhaveRead': 'Tôi đã đọc và đồng ý từ chối trách nhiệm',
    'ImportantSeller': 'Quan trọng: Người bán có quyền từ chối thanh toán của bạn nếu tên thật của bạn khác với tên tài khoản ngân hàng của bạn.',
    'ImportantBuyer': 'Quan trọng: Người mua có quyền từ chối giao dịch của bạn nếu bạn sẽ không chuyển tiền vào địa chỉ nhất định.',
    'BuyNow': 'Mua ngay',
    'SellNow': 'Bán ngay',
    'AddBank': 'Thêm ngân hàng',
    'BankName': 'tên ngân hàng',
    'HolderName': 'Tên chủ sở hữu',
    'Action': 'Hoạt động',
    'ProfileChangePassword': 'Đổi mật khẩu',
    'ProfileAuth': 'Ủy quyền hai yếu tố',
    'ProfileEmailVerification': 'Email xác thực',
    'ProfileEmail2FA': 'Email 2FA',
    'ProfilePhoneVerification': 'Xác minh điện thoại',
    'ProfileEnablefactor': 'Kích hoạt xác thực Google 2 yếu tố',
    'ProfileEnable': 'Kích hoạt',
    'ProfileFirst': 'Để thiết lập xác thực hai yếu tố trước tiên bạn cần phải',
    'ProfileDownloadGoogle1': 'tải xuống Google Trình xác thực:',
    'ProfileScanBarcode': 'Sau đó quét mã vạch trên hoặc, nếu bạn không thể',
    'ProfileScanBarcode1': 'quét mã vạch, bạn có thể nhập "Khóa bảo mật"',
    'ProfileScanBarcode2': 'thủ công',
    'ProfileSecurityKey': 'Chìa khóa bảo mật',
    'ProfileDisable': 'Vô hiệu hóa xác thực 2 yếu tố của Google',
    'ProfileDisable1': 'Vô hiệu hóa',
    'ProfileEnterCode': 'Nhập mã',
    'ProfileAvailableCoins': 'Tiền có sẵn',
    'ProfileMenu': 'Thực đơn',
    'ProfileSetupAccount': 'Thiết lập tài khoản',
    'ProfileChoose': 'Chọn',
    'Code': 'Nhập mã Google 2fa',
    'secretCode': 'mã bí mật',
    'ProfileScan': 'Quét mã vạch',
    'ProfileOption': 'tùy chọn và quét mã vạch hiển thị trên trang này',
    'ProfileEnter': 'Nhập khóa được cung cấp',
    'ProfileType': 'và gõ vào',
    'Disable2': 'Vô hiệu hóa xác thực hai yếu tố',
    'ProfileGoogleGuide': 'Hướng dẫn Trình xác thực Google',
    'ProfileGoogleAndroidOrApple': '1. Cài đặt Google Trình xác thực cho Android hoặc Apple và mở Google Trình xác thực',
    'ProfileGoTo': '2: Đi đến',
    'ProfileFactor': 'Kích hoạt xác thực hai yếu tố',
    'ProfileEnterCode': 'Nhập mã',
    'ProfileEnable': 'Kích hoạt',
    'ProfileCancel': 'Hủy bỏ',
    'ProfileSave': 'Tiết kiệm',
    'ProfileProfile': 'Hồ sơ',
    'ProfileSecurity': 'Bảo vệ',
    'ProfileFirstName': 'Tên đầu tiên',
    'ProfileUpdate': 'Cập nhật',
    'ProfileCnfrmPassword': 'Mật khẩu và mật khẩu xác nhận không khớp.',
    'settingEnableFactor': 'Kích hoạt xác thực Google 2 yếu tố',
    'settingEnableButton': 'Kích hoạt',
    'settingSetupFactor': 'Để thiết lập xác thực hai yếu tố trước tiên bạn cần phải',
    'settingGoogleDownload': 'tải xuống Trình xác thực Google:',
    'settingScanBarcode': 'Sau đó quét mã vạch trên hoặc, nếu bạn không thể',
    'settingManually': 'thủ công',
    'settingSecurityKey ': 'Chìa khóa bảo mật:',
    'settingGoogleGuide': 'Hướng dẫn ',
    'settingMenu': 'Thực đơn',
    'settingSetupAccount': 'Thiết lập tài khoản',
    'settingChose': '3. Chọn',
    'settingSacanBarcode': 'Quét mã vạch',
    'settingOption': 'tùy chọn và quét mã vạch hiển thị trên trang này',
    'settingUnableToScan': ' Nếu bạn không thể quét mã vạch:',
    'settingProvidedKey': 'Nhập khóa được cung cấp',
    'settingType': ' và gõ vào',
    'settingSixDigit': '5. Một số sáu chữ số sẽ xuất hiện trong màn hình chính của ứng dụng Google Trình xác thực của bạn',
    'settingEveryTime': '6. Mỗi lần bạn đăng nhập vào Trao đổi, bạn phải nhập mã 2FA mới.',
    'settingDisableFactor': 'Vô hiệu hóa xác thực hai yếu tố ',
    'settingDisable': 'Vô hiệu hóa',
    'AllAreYou': 'Bạn có chắc chắn muốn xóa ngân hàng?',
    'AllThereAre': 'Không có hồ sơ để hiển thị',
    'AllBanks': 'Tất cả các ngân hàng',
    'AllBanks': 'Tất cả các ngân hàng',
    'AllTotal': 'Toàn bộ',
    'AllAddBanks': 'Thêm ngân hàng',
    'AllPay-inBanks': 'Ngân hàng thanh toán',
    'AllPay-outBanks': 'Ngân hàng chi trả',
    'AllAccountNumber': 'Số tài khoản',
    'AllBankName': 'Tên ngân hàng',
    'AllHolderName': 'Tên chủ sở hữu',
    'AllBankAddress': 'địa chỉ ngân hàng',
    'AllBankCode': 'Mã ngân hàng',
    'AllStatus': 'Trạng thái',
    'AllCreatedAt': 'Tạo tại',
    'AllUpdatedAt': 'Cập nhật tại',
    'AllAction': 'Hoạt động',
    'AllCountry': 'Quốc gia',
    'AllBank': 'ngân hàng',
    'AllCreateBank': 'Tạo ngân hàng',
    'Select': 'Lựa chọn',
    'AllSelectCountry': 'Chọn quốc gia',
    'AllSelectBank': 'Chọn ngân hàng',
    'AllSubmit': 'Gửi đi',
    'AllCancel': 'Hủy bỏ',
    'TooltipEdit': 'Biên tập',
    'TooltipDelete': 'Xóa bỏ',
    'UpdateBank': 'hàng',
    'Update': 'Cập nhật',
    'UpdateCancel': 'Hủy bỏ',
    'Back': 'Trở lại',
    'Transaction1': 'Giao dịch',
    'TransactionsPending': 'Đang chờ xử lý',
    'TransactionsApproved': 'Tán thành',
    'TransactionsRejected': 'Từ chối',
    'TransactionPayus': 'ID giao dịch Yus',
    'TransactionWantTo': 'Bạn có chắc chắn, bạn muốn',
    'TransactionWantTo1': 'giao dịch này?',
    'TransactionsSelectCurrency': 'Chọn tiền tệ',
    'TransactionsTransactionID': 'giao dịch ID',
    'TransactionsUser': 'Người dùng',
    'TransactionHash': 'Băm giao dịch',
    'TransactionsAccountDetails': 'Chi tiết tài khoản',
    'TransactionsAmount': 'Số tiền',
    'TransactionsCreatedat': 'Tạo tại',
    'TransactionFundTransferCode': 'Mã chuyển tiền',
    'TransactionReturnURL': 'Trả về URL',
    'TransactionsWithdrawStatus': 'Tình trạng rút tiền ',
    'TransactionsAction': 'Hoạt động',
    'TransactionsNoRecordsDisplay': 'Không có hồ sơ để hiển thị',
    'TransactionsBuyTransactions': 'Giao dịch mua',
    'TransactionsSellTransactions': 'Bán giao dịch',
    'TransactionsPleaseSelect': 'Vui lòng chọn Phê duyệt hoặc từ chốiVui lòng chọn Phê duyệt hoặc từ chối',
    'TransactionsPleaseSelectBank': 'Vui lòng chọn một ngân hàng',
    'TransactionApproved': 'Tán thành',
    'TransactionRejected': 'Từ chối',
    'TransactionSelectCurrency': 'Chọn tiền tệ',
    'TransactionSearch': 'Tìm kiếm',
    'TransactionClickToTrack': 'Nhấn vào đây để theo dõi',
    'TransactionDetails': 'Chi tiết giao dịch',
    'TransactionNote': 'Ghi chú',
    'TransactionPlatform': 'Nền tảng',
    'TransactionUserID': 'Tên người dùng',
    'TransactionEmail': 'E-mail',
    'TransactionPhone': 'Điện thoại',
    'TransactionCoinName': 'Tên tiền',
    'TransactionCoinAmount': 'Số tiền',
    'TransactionActualDepositAmount': 'Số tiền ký gửi thực tế',
    'TransactionCoinAddress': 'Địa chỉ tiền xu',
    'TransactionCurrencyName': 'Tên tiền tệ',
    'TransactionTotalAmount': 'Tổng cộng',
    'TransactionReturnURL': 'Trả về URL',
    'TransactionNotifyURL': 'Thông báo URL',
    'TransactionFundTransfercode': 'Mã chuyển tiền',
    'TransactionBankDetails': 'Thông tin chi tiết ngân hàng',
    'TransactionAccountHolderName': 'Tên Chủ Tài khoản',
    'TransactionBankName': 'Tên ngân hàng',
    'TransactionAdminName': 'Tên quản trị viên',
    'TransactionTransactionHash': 'Hash giao dịch',
    'TransactionInvoiceNumber': 'Số hóa đơn',
    'TransactionPayusTransactionID': 'ID giao dịch Payus',
    'TransactionStatus': 'Trạng thái',
    'Country': 'Quốc gia',
    'WalletsName': 'Tên',
    'WalletsSymbol': 'Biểu tượng',
    'WalletsBuy': 'Mua',
    'WalletsYourBuying': 'Mua hàng của bạn',
    'WalletsAmmount': 'Số tiền',
    'WalletsCoinAddress': 'Địa chỉ tiền xu',
    'WalletsEquivalent': 'Tương đương',
    'WalletsBuyBTC': 'Mua BTC',
    'WalletsImportant': 'Quan trọng: Người bán có quyền từ chối thanh toán của bạn nếu tên thật của bạn khác với tên tài khoản ngân hàng của bạn.',
    'WalletsCryptoVN': 'Tiền điện tử',
    'WalletsAsset': 'Loại tài sản',
    'WalletsVND': 'Đồng /',
    'WalletsBTC': 'BTC',
    'WalletsPayment': 'Thanh toán:',
    'WalletsVPBank': 'Ngân hàng VP',
    'WalletsSELL': 'BÁN',
    'WalletsTextModal': 'Một số văn bản trong phương thức.',
    'WalletsClose': 'Đóng',
    'DashboardBack': 'Trở lại',
    'DashboardBuy': 'Mua',
    'DashboardSell': 'Bán',
    'DashboardSelect': 'Lựa chọn',
    'DashboardBestPrice': 'Giá mua tốt nhất',
    'DashboardBestSellPrice': 'Giá bán tốt nhất',
    'DashboardVND/BTC': 'VND/BTC',
    'FrgtPassBuyndSell': 'Mua và bán tiền kỹ thuật số',
    'FrgtPassForgot': 'Quên mật khẩu',
    'FrgtPassSubmit': 'Gửi đi',
    'FrgtPassLogin': 'Đăng nhập',
    'FrgtEmail': 'Nhập địa chỉ email...',
    'AllCrypto': 'Tất cả tiền điện tử ở đây',
    'AllCryptoCLickOnOne': 'Nhấp vào một trong những đồng tiền được liệt kê dưới đây để tạo các lệnh mua hoặc bán của bạn.',
    'AllCryptoSellCrypto': 'BÁN CRYPTO',
    'AllCryptoBuyCrypto': 'MUA CRYPTO',
    'AllCryptoLoading': 'Đang tải...',
    'BuysellOrderEnterAmount': 'Nhập số tiền',
    'BuysellOrderCreate': 'Tạo đơn hàng Mua / Bán',
    'BuysellOrderSelectCoin': 'Chọn tiền',
    'BuysellOrderEnterPrice': 'Vui lòng nhập giá hoặc tổng số',
    'BuysellOrderAmount': 'Số tiền',
    'BuysellOrderPrice': 'Giá bán',
    'BuysellOrderTotal': 'Toàn bộ',
    'BuysellOrderSELL': 'BÁN',
    'BuysellOrderBUY': 'MUA',
    'Calculator': 'MÁY TÍNH',
    'CalculatorCryptoConverter': 'Chuyển đổi tiền điện tử',
    'CalculatorExchanging': 'Trao đổi',
    'CalculatorReceiving': 'Nhận',
    'CalculatorCONVERTNOW': 'CHUYỂN ĐỔI',
    'CurrencySec.Title': 'Tiền tệ Fiat',
    'CurrencySec.ParaGraph': 'Giao dịch tiền tệ quen thuộc trên blockchain đưa ra những thách thức độc đáo khi'
        + 'so với những người liên quan đến tiền điện tử hiện có. Một trong những chính '
        + 'thách thức là làm thế nào để đảm bảo tất cả các phiên bản của giao dịch tiền tệ ngang bằng với một'
        + 'khác. Điều này đòi hỏi một thực thể hoặc thực thể để cung cấp sự hỗ trợ của một số loại. '
        + 'Với ExchangeFiat, không có bức tường nào ở giữa Fiat vs Crypto, bạn có thể chuyển đổi'
        + 'tất cả các loại tiền điện tử được hỗ trợ trên nền tảng của chúng tôi vào fiat cục bộ của bạn ngay lập tức. '
        + 'Chúng tôi được cấp phép cho liên bang & Tiểu bang California cho MTL. Hỗ trợ nhiều '
        + 'triển khai tiềm năng khác nhau sẽ yêu cầu công nghệ đủ linh hoạt để'
        + 'chứa điều này. Mục tiêu của chúng tôi là tạo ra các công cụ công nghệ cần thiết để thực hiện. '
        + 'một loại tiền tệ kỹ thuật số Chúng tôi đang sử dụng bộ công cụ blockchain được phát triển tại'
        + 'WorldexLab để xây dựng các nguyên mẫu WorldexChain. Chúng tôi đã tham gia với một số '
        + 'của các ngân hàng trung ương trong nỗ lực tìm hiểu những cân nhắc của họ và giải quyết chúng'
        + 'trong nghiên cứu của chúng tôi khi có thể. Các ngân hàng trung ương thường xuyên thực hiện các tài liệu nghiên cứu '
        + 'với các học giả bên ngoài trong một lĩnh vực cụ thể, công việc này nằm trong truyền thống đó và'
        + 'như với tất cả các nghiên cứu học thuật, không ngụ ý chứng thực bất kỳ vị trí chính sách nào. '
        + 'Chúng tôi hiện đang hỗ trợ 10 quốc gia và nhiều hơn nữa trong tương lai. Tiền điện tử để '
        + 'Fiat ở Thái Lan - Indonesia - Nigeria - Malaysia - Việt Nam - Phillipines - Lào -'
        + 'Campuchia và hơn thế nữa.',
    'FooterFastSeured': 'NHANH CHÓNG-DỄ DÀNG NHƯ 1-2-3 Cần tiền mặt thật nhanh? gửi hầu hết tiền điện tử của bạn vào nền tảng của chúng tôi và nhận chuyển khoản vào ngân hàng của bạn trong vài phút.',
    'FooterMainMenu': 'Thực đơn chính',
    'FooterHome': 'Trang Chủ',
    'FooterNews': 'Tin tức',
    'FooterAboutUs': 'Về chúng tôi',
    'FooterFAQ': 'Câu hỏi thường gặp',
    'FooterContact': 'Tiếp xúc',
    'FooterContact': 'Ủng hộ',
    'FooterSubscribe': 'Theo dõi',
    'E-mail': 'E-mail',
    'FooterNewLetter': 'Theo dõi bản tin của chúng tôi',
    'LogInBuySell': 'Mua và bán tiền kỹ thuật số ',
    'LogInGoogleOTP': 'Nhập Google OTP',
    'LogIn': 'Đăng nhập',
    'LogInWelcome': 'Chào mừng bạn',
    'LogInEmail': 'Nhập email',
    'LogInPaasword': 'Mật khẩu',
    'LogInOTP': 'Nhập OTP',
    'LogInPlaceholder': 'Nhập email OTP',
    'LogInRememberMe': 'Nhớ tôi',
    'LogInForgot': 'Quên mật khẩu?',
    'LogInCreate': 'Tạo một tài khoản!',
    'RegisterAccount': 'đăng ký tài khoản',
    'RegisterAlready': 'Bạn co săn san để tạo một tai khoản? Đăng nhập!',
    'ResetPassword': 'Đặt lại mật khẩu',
    'ResetPasswordNotMatch': 'Mật khẩu và mật khẩu xác nhận không khớp.',
    'RegisterPasswordError1': 'Vui lòng nhập ít nhất 6 chữ số mật khẩu',
    'RegisterFirstName': 'Tên đầu tiên',
    'RegisterLastName': 'Họ',
    'RegisterEmailAddress': 'Địa chỉ email',
    'RegisterPassword': 'Mật khẩu',
    'RegisterRepeatPassword': 'Lặp lại mật khẩu',
    'ResetEnterPassword': 'Nhập mật khẩu',
    'ResetConfirmPassword': 'Xác nhận mật khẩu',
    'CreatedAt': 'Tạo tại',
    'Search': 'Tìm kiếm',
    'UpdateBank': 'Ngân hàng cập nhật',
    'Buy': 'mua',
    'Sell': 'Bán',
    'PleaseAddyourbank': 'Vui lòng thêm ngân hàng của bạn',
    'Verified': 'Đã xác minh',
    'VerifyEmail': 'Xác nhận Email',
    'PleaseCopy': 'Vui lòng sao chép mã chuyển tiền.',
    'Confirm': 'Xác nhận',
    'Close': 'Đóng',
    "wallets": "Ví tiền",
    "kyc": "KYC",
    "enable2fa": "Vui lòng kích hoạt xác thực 2FA",
    "CompleteKYC": "Hoàn thành KYC",
    "Verifyyour2FA": "Xác nhận 2FA của bạn",
    "Connectwithgoogle": "Kết nối với trình xác thực google",
    "GetVerified": "Được xác thực!",
    "SelfieholdingGOV": "Selfie giữ ID GOV với ghi chú địa chỉ trang web của chúng tôi vào ngày hôm nay (ví dụ: exchange fiat 01/30/2019) cũng tải lên hóa đơn tiện ích hoặc sao kê ngân hàng khớp với tên chủ tài khoản",
    "AddSettlement" : "Thêm thông tin giải quyết",
    "Addbank" : "Thêm địa chỉ ngân hàng hoặc tiền điện tử",
    "ScanQR" : "Quét bên dưới mã QR trong ứng dụng Google auth của bạn.",
    "Upload" : "Tải lên",
    "Finish" : "Hoàn thành",
    "KYCLEVEL3Pending" : "KYC LEVEL 3 đang chờ xử lý",
    "KYCLEVEL2Pending" : "KYC LEVEL 2 đang chờ xử lý",
    "CongratulationsKYC": "Xin chúc mừng KYC đã hoàn thành",
    "KYCRecords": "Hồ sơ KYC",
    "LEVEL" : "CẤP ĐỘ",
    "Status" : "Trạng thái",
    "KYCLEVEL1": "KYC LEVEL 1 (Xác minh điện thoại Google 2FA HOẶC điện thoại)",
    "KYCLEVEL2" : "KYC LEVEL 2 (Tải lên bằng chứng ID chính phủ)",
    "KYCLEVEL3": "KYC LEVEL 3 (Tải lên bằng chứng ID chính phủ bằng văn bản và Selfie)",
    "Pending": "Đang chờ xử lý",
    "Approved": "Tán thành",
    "Name" : "Tên",
    "Coin" : "Đồng tiền",
    "Price": "Giá bán",
    "Currentbalance": "Số dư hiện tại",
    "Action" : "Hoạt động",
    "Buy" : "Mua",
    "Sell" : "Bán",
    "Currency" : "Tiền tệ",
    "Selectcurrency": "Chọn tiền tệ",
};