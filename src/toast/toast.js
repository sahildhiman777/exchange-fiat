import Swal from 'sweetalert2';

const ToastTopEnd = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000
});

const ToastTopEndSuccessFire = (title) => {
    ToastTopEnd.fire({
        type: 'success',
        title: title,
        // icon: 'success',
        background: '#4ba745',
    });
}

const ToastTopEndWarningFire = (title) => {
    ToastTopEnd.fire({
        type: 'warning',
        title: title,
        // icon: 'warning',
        background: '#858796',
    });
}

const ToastTopEndErrorFire = (title) => {
    ToastTopEnd.fire({
        type: 'error',
        title: title,
        // icon: 'error',
        background: '#dc3545',
    });
}

export {
    ToastTopEndSuccessFire,
    ToastTopEndWarningFire,
    ToastTopEndErrorFire
}
