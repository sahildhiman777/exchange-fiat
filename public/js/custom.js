$(document).ready(function() {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      dots: false,
      loop: true,
      margin: 10,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        800: {
          items: 2
        },
        1200: {
          items: 3
        }
      },
      autoplay: true,
      autoplayTimeout: 2000,
      autoplayHoverPause: true
      
    });
    $('.play').on('click', function() {
      owl.trigger('play.owl.autoplay', [2000])
    })
    $('.stop').on('click', function() {
      owl.trigger('stop.owl.autoplay')
    })
  })
//nav//

  function openNav() {
    document.getElementById("mySidenav").style.width = "280px";
  }
  
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

//animation//
  $(function() {
    var $elements = $('.animateBlock.notAnimated'); //contains elements of nonAnimated class
    var $window = $(window);
    $window.trigger('scroll');
    $window.on('scroll', function(e) {
      $elements.each(function(i, elem) { //loop through each element
        if ($(this).hasClass('animated')) // check if already animated
          return;
        animateMe($(this));
      });
    });
  });
   
  function animateMe(elem) {
    var winTop = $(window).scrollTop(); // calculate distance from top of window
    var winBottom = winTop + $(window).height();
    var elemTop = $(elem).offset().top - 400; // element distance from top of page
    var elemBottom = elemTop + $(elem).height();
    if ((elemBottom <= winBottom) && (elemTop <= winTop)) {
      // exchange classes if element visible
      $(elem).removeClass('notAnimated').addClass('animated');
    }
  }
